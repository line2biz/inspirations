
#import "SNDay.h"

@interface SNDay ()

@end

@implementation SNDay

#pragma mark - Initialization
- (instancetype)initWithMaxPoints:(NSInteger)maxPoints points:(NSInteger)points dayType:(SNDayType)dayType dayState:(SNDayState)dayState
{
    return [self initWithMaxPoints:maxPoints points:points dayType:dayType dayState:dayState swoops:0 steps:0 calories:0];
}

- (instancetype)initWithMaxPoints:(NSInteger)maxPoints points:(NSInteger)points dayType:(SNDayType)dayType dayState:(SNDayState)dayState swoops:(NSInteger)swoops steps:(NSInteger)steps calories:(NSInteger)calories
{
    self = [super init];
    if (self) {
        _maxPoints = maxPoints;
        _points = points;
        _dayType = dayType;
        _progress = (CGFloat)points/(CGFloat)_maxPoints;
        _dayState = dayState;
        
        _smallAchivement = (_progress > 0.75f) ? YES : NO;
        _greatAchievement = (_progress > 0.95) ? YES : NO;
        
        _swoops = swoops;
        _steps = steps;
        _calories = calories;
    }
    return self;
}

#pragma mark - Public Methods

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\n maxPoints : %zd\n points : %zd\n dayType : %zd\n progress : %.4f\n dayState : %zd\n Moves : %zd\n steps : %zd\n calories : %zd\n}",self.maxPoints, self.points, self.dayType, self.progress, self.dayState, self.swoops, self.steps, self.calories];
}

@end
