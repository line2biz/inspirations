

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SNControlType) {
    SNControlTypeHeight,
    SNControlTypeWeight
};

typedef NS_ENUM(NSInteger, SNMeasureUnit) {
    SNMeasureUnitMeters,
    SNMeasureUnitFeets,
    SNMeasureUnitKilos,
    SNMeasureUnitPounds
};

@interface SNHeightWeightControl : UIControl

@property (assign, nonatomic) SNControlType controlType;
@property (assign, nonatomic) SNMeasureUnit measureUnit;
@property ( nonatomic, getter=theCurrentValue) CGFloat currentValue;

- (void)changeToAlternativeMeasureUnit;

@end
