

#import <UIKit/UIKit.h>

typedef void(^AnimationLabelCompletitionBlock)(void);

@interface AnimatedLabel : UILabel

@property (strong, readonly, nonatomic) NSArray *arrayOfPointsValues;
@property (assign, readonly, nonatomic) NSTimeInterval animationDuration;

- (void)animateTextFromMinValue:(NSInteger)minValue
                     toMaxValue:(NSInteger)maxValue
                     withPrefix:(NSString *)prefix
                         suffix:(NSString *)suffix
                      maxPrefix:(NSString *)maxPrefix
                      maxSuffix:(NSString *)maxSuffix
                       duration:(NSTimeInterval)duration
                       andSteps:(NSInteger)stepsAmount;

- (void)animateTextFromMinValue:(NSInteger)minValue
                     toMaxValue:(NSInteger)maxValue
                     withPrefix:(NSString *)prefix
                         suffix:(NSString *)suffix
                      maxPrefix:(NSString *)maxPrefix
                      maxSuffix:(NSString *)maxSuffix
                       duration:(NSTimeInterval)duration
                       andSteps:(NSInteger)stepsAmount
                andCompletition:(AnimationLabelCompletitionBlock)completitionBlock;

- (void)animateTextFromArray:(NSArray *)array withDuration:(NSTimeInterval)duration;

+ (NSString *)textWithThousendsFormat:(NSInteger)amount;

- (void)stopTimer;

@end