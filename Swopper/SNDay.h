

#import <Foundation/Foundation.h>
#import "SNSummaryChart.h"
#import "SNWeekGraph.h"

typedef NS_ENUM(NSInteger, SNDayType) {
    SNDayTypeMonday,
    SNDayTypeTuesday,
    SNDayTypeWednesday,
    SNDayTypeThursday,
    SNDayTypeFriday,
    SNDayTypeSaturday,
    SNDayTypeSunday
};

typedef NS_ENUM(NSInteger, SNDayState) {
    SNDayStateCompleted,
    SNDayStateInProgress,
    SNDayStateFuture
};

@interface SNDay : NSObject <SNSummaryChartProtocol, SNWeekGraphProtocol>

@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) NSInteger maxPoints;
@property (assign, nonatomic) NSInteger points;
@property (assign, readonly, nonatomic) SNDayType dayType;
@property (assign, readonly, nonatomic) CGFloat progress;
@property (assign, nonatomic) SNDayState dayState;
@property (assign, readonly, nonatomic) BOOL smallAchivement;
@property (assign, readonly, nonatomic) BOOL greatAchievement;

@property (assign, nonatomic) NSInteger swoops;
@property (assign, nonatomic) NSInteger steps;
@property (assign, nonatomic) NSInteger calories;
@property (assign, nonatomic) CGFloat swoppsPercentage;

- (instancetype)initWithMaxPoints:(NSInteger)maxPoints points:(NSInteger)points dayType:(SNDayType)dayType dayState:(SNDayState)dayState;
- (instancetype)initWithMaxPoints:(NSInteger)maxPoints points:(NSInteger)points dayType:(SNDayType)dayType dayState:(SNDayState)dayState swoops:(NSInteger)swoops steps:(NSInteger)steps calories:(NSInteger)calories;


@end
