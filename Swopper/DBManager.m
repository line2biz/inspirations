

#import "DMManager.h"

NSString *const DMManagerModelNameValue       = @"Swopper";

@implementation DMManager
{
    BOOL _isLoading;
}

@synthesize defaultContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




#pragma mark - Initialization
//
//- (id)init
//{
//    self = [super init];
//    if (self) {
//        [DMCategory serializeCategories:nil];
//    }
//    return self;
//}


+ (DMManager *)sharedManager
{
    static dispatch_once_t once;
    static DMManager *__inctance;
    dispatch_once(&once, ^ { __inctance = [[DMManager alloc] init]; });
    return __inctance;
}

+ (NSManagedObjectContext *)managedObjectContext
{
    return [[DMManager sharedManager] defaultContext];
}

- (NSURL *)storeURL
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return [url URLByAppendingPathComponent:[DMManagerModelNameValue stringByAppendingString:@".sqlite"]];
}


#pragma mark - Core Data stack
- (NSManagedObjectContext *)defaultContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:DMManagerModelNameValue withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self storeURL];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]); 
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext
{
    NSError *error = nil;
    
    NSManagedObjectContext *managedObjectContext = self.defaultContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [[NSFileManager defaultManager] removeItemAtURL:[self storeURL] error:nil];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Serialization
+ (BOOL)serializeDataDictionary:(NSDictionary *)dictionary error:(NSError **)error
{
    
    return YES;

}

@end
