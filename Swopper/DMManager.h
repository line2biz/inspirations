
#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const DMManagerModelNameValue;

@interface DMManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext * defaultContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DMManager *)sharedManager;
+ (NSManagedObjectContext *)managedObjectContext;
- (void)saveContext;

+ (BOOL)serializeDataDictionary:(NSDictionary *)dictionary error:(NSError **)error;

@end

//@protocol DMManagerDisplayValueProtocol <NSObject>
//
//@required
//- (NSString *)displayNameForPropertyName:(NSString *)propertyName;
//- (NSString *)displayValueForPropertyName:(NSString *)propertyName;
//
//@end
