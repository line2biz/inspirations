

#import "SNAdditionalMath.h"

@implementation SNAdditionalMath


#pragma mark - Custom Functions
float SQRF(float value) {
    return value*value;
}

double SQRD(double value) {
    return value*value;
}

int SQR(int value) {
    return value*value;
}

float RadiansFromDegrees(float degrees) {
    return ( (M_PI * (degrees)) / 180 );
}

CGFloat FloatPartOfNumber(CGFloat number){
    return number - (NSInteger)number;
}

CGFloat RandomFloatBetweenSmallAndBig(CGFloat small, CGFloat big) {
    CGFloat diff = big - small;
    return (((CGFloat) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + small;
}

#pragma mark - Custom Blocks

void(^AddPointToArray)(CGPoint, NSMutableArray*) = ^(CGPoint point, NSMutableArray *mArray) {
    [mArray addObject:[NSValue valueWithCGPoint:point]];
};

#pragma mark Point From Array
CGPoint(^GetPointFromArrayAtIndex)(NSArray *, NSInteger) = ^(NSArray *array, NSInteger index) {
    NSValue *point = [array objectAtIndex:index];
    CGPoint pointToReturn = [point CGPointValue];
    return pointToReturn;
};

#pragma mark Get Line Length
CGFloat (^GetLineLength)(CustomLine) = ^(CustomLine line) {
    CGFloat length = sqrt(SQRF(line.endPoint.x-line.startPoint.x) + SQRF(line.endPoint.y - line.startPoint.y));
    return length;
};

#pragma mark Get End Point of Line With Start Point, Angle and Legth
CGPoint (^GetPointFromPointAngleLength)(CGPoint, CGFloat, CGFloat) = ^(CGPoint point, CGFloat angle, CGFloat length) {
    CGPoint centerPoint = point;
    CGPoint result;
    CGFloat eps = 0.00001;
    result.x = centerPoint.x + length * cosf(RadiansFromDegrees(angle));
    result.y = centerPoint.y + length * sinf(RadiansFromDegrees(angle));
    if (result.x < eps) {
        result.x = 0;
    }
    if (result.y < eps) {
        result.y = 0;
    }
    return result;
};

#pragma mark Get Angle Of Line
CGFloat(^GetAngleOfLine)(CustomLine) = ^(CustomLine line) {
    CGFloat xDiff = line.endPoint.x - line.startPoint.x;
    CGFloat yDiff = line.endPoint.y - line.startPoint.y;
    CGFloat result = atan2(yDiff, xDiff) * (180 / M_PI);
    return result;
};

#pragma mark Get Middle Points from Line
NSArray * (^GetMidAmountPointsForLineAndItsAngle)(NSInteger,CustomLine,CGFloat) = ^(NSInteger amount, CustomLine line, CGFloat angle) {
    NSMutableArray *mArray = [NSMutableArray new];
    
    for (NSInteger i=0; i < amount; i++) {
        if (i == 0) {
            CGPoint point = GetPointFromPointAngleLength(line.startPoint, angle, GetLineLength(line) / (amount+1));
            AddPointToArray(point, mArray);
        } else {
            CGPoint point = GetPointFromPointAngleLength(GetPointFromArrayAtIndex(mArray, i - 1),angle,GetLineLength(line) / (amount+1));
            AddPointToArray(point, mArray);
        }
    }
    
    return mArray;
};


#pragma mark Points on Circle
NSArray *(^GetPointsOnCircleWithCenterRadiusStartAngle)(NSInteger,CGPoint,CGFloat,CGFloat) = ^(NSInteger amount, CGPoint center, CGFloat radius, CGFloat startAngle) {
    NSMutableArray *mArray = [NSMutableArray new];
    
    CGFloat angle;
    CGFloat anglesPerElement = 360.0f / amount;
    for (NSInteger i=0; i < amount; i++) {
        angle = startAngle - anglesPerElement*i;
        CGFloat x = center.x + radius * cosf(RadiansFromDegrees(angle));
        CGFloat y = center.y + radius * sinf(RadiansFromDegrees(angle));
        CGPoint point = CGPointMake(x, y);
        AddPointToArray(point,mArray);
    }
    
    return mArray;
};


#pragma mark Add Objects from Array to Array
NSArray *(^AddObjectsFromArrayToArray)(NSArray *, NSMutableArray *) = ^(NSArray *source, NSMutableArray *target) {
    NSMutableArray *mArray = [NSMutableArray new];
    for (int i=0; i<source.count; i++) {
        [target addObject:[source objectAtIndex:i]];
    }
    return mArray;
};

#pragma mark Add Object at Index from Array to Array
void (^AddObjectAtIndexFromArrayToArray)(NSInteger, NSArray *, NSMutableArray *) = ^(NSInteger idx, NSArray *source, NSMutableArray *target) {
    if (idx < source.count) {
        [target addObject:[source objectAtIndex:idx]];
    }
};

#pragma mark Add Objects at Indexes from Array to Array
void (^AddObjectsAtIndexesFromArrayToArray)(NSArray *, NSArray *, NSMutableArray *) = ^(NSArray *indexes, NSArray *source, NSMutableArray *target) {
    if (indexes.count > 0) {
        for (NSNumber *number in indexes) {
            NSInteger idx = [number integerValue];
            if (idx < source.count) {
                [target addObject:[source objectAtIndex:idx]];
            }
        }
    }
};

#pragma mark Get size of text for drawing
NSArray *(^SizeAndAttrStrFromTextWithFontColor)(NSString *,UIFont *,UIColor*) = ^(NSString *text, UIFont *font, UIColor *textColor) {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName : textColor};
    NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
    CGSize sizeOfText = [text sizeWithAttributes:stringAttrs];
    
    NSMutableArray *mArrayOfResults = [NSMutableArray arrayWithCapacity:2];
    [mArrayOfResults addObject:[NSValue valueWithCGSize:sizeOfText]];
    [mArrayOfResults addObject:attrStr];
    
    return mArrayOfResults;
};

#pragma mark - Compare Dates by day factor
+ (NSComparisonResult)compareDate:(NSDate *)firstDate withDate:(NSDate *)secondDate {
    
    NSDate *date1 = firstDate;
    NSDate *date2 = secondDate;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date2 interval:NULL forDate:date2];
    return [date1 compare:date2];
}


#define kCGPointEpsilon FLT_EPSILON

CGFloat
ccpLength(const CGPoint v)
{
	return sqrtf(ccpLengthSQ(v));
}

CGFloat
ccpDistance(const CGPoint v1, const CGPoint v2)
{
	return ccpLength(ccpSub(v1, v2));
}

CGPoint
ccpNormalize(const CGPoint v)
{
	return ccpMult(v, 1.0f/ccpLength(v));
}

CGPoint
ccpForAngle(const CGFloat a)
{
	return ccp(cosf(a), sinf(a));
}

CGFloat
ccpToAngle(const CGPoint v)
{
	return atan2f(v.y, v.x);
}

CGPoint ccpLerp(CGPoint a, CGPoint b, float alpha)
{
	return ccpAdd(ccpMult(a, 1.f - alpha), ccpMult(b, alpha));
}

float clampf(float value, float min_inclusive, float max_inclusive)
{
	if (min_inclusive > max_inclusive) {
        float tmp=min_inclusive;
        min_inclusive = max_inclusive;
        max_inclusive = tmp;
	}
	return value < min_inclusive ? min_inclusive : value < max_inclusive? value : max_inclusive;
}

CGPoint ccpClamp(CGPoint p, CGPoint min_inclusive, CGPoint max_inclusive)
{
	return ccp(clampf(p.x,min_inclusive.x,max_inclusive.x), clampf(p.y, min_inclusive.y, max_inclusive.y));
}

CGPoint ccpFromSize(CGSize s)
{
	return ccp(s.width, s.height);
}

CGPoint ccpCompOp(CGPoint p, float (*opFunc)(float))
{
	return ccp(opFunc(p.x), opFunc(p.y));
}

BOOL ccpFuzzyEqual(CGPoint a, CGPoint b, float var)
{
	if(a.x - var <= b.x && b.x <= a.x + var)
		if(a.y - var <= b.y && b.y <= a.y + var)
			return true;
	return false;
}

CGPoint ccpCompMult(CGPoint a, CGPoint b)
{
	return ccp(a.x * b.x, a.y * b.y);
}

float ccpAngleSigned(CGPoint a, CGPoint b)
{
	CGPoint a2 = ccpNormalize(a);
	CGPoint b2 = ccpNormalize(b);
	float angle = atan2f(a2.x * b2.y - a2.y * b2.x, ccpDot(a2, b2));
	if( fabs(angle) < kCGPointEpsilon ) return 0.f;
	return angle;
}

CGPoint ccpRotateByAngle(CGPoint v, CGPoint pivot, float angle)
{
	CGPoint r = ccpSub(v, pivot);
	float cosa = cosf(angle), sina = sinf(angle);
	float t = r.x;
	r.x = t*cosa - r.y*sina + pivot.x;
	r.y = t*sina + r.y*cosa + pivot.y;
	return r;
}


BOOL ccpSegmentIntersect(CGPoint A, CGPoint B, CGPoint C, CGPoint D)
{
	float S, T;
    
	if( ccpLineIntersect(A, B, C, D, &S, &T )
	   && (S >= 0.0f && S <= 1.0f && T >= 0.0f && T <= 1.0f) )
		return YES;
    
	return NO;
}

CGPoint ccpIntersectPoint(CGPoint A, CGPoint B, CGPoint C, CGPoint D)
{
	float S, T;
    
	if( ccpLineIntersect(A, B, C, D, &S, &T) ) {
		// Point of intersection
		CGPoint P;
		P.x = A.x + S * (B.x - A.x);
		P.y = A.y + S * (B.y - A.y);
		return P;
	}
    
	return CGPointZero;
}

BOOL ccpLineIntersect(CGPoint A, CGPoint B,
					  CGPoint C, CGPoint D,
					  float *S, float *T)
{
	// FAIL: Line undefined
	if ( (A.x==B.x && A.y==B.y) || (C.x==D.x && C.y==D.y) ) return NO;
    
	const float BAx = B.x - A.x;
	const float BAy = B.y - A.y;
	const float DCx = D.x - C.x;
	const float DCy = D.y - C.y;
	const float ACx = A.x - C.x;
	const float ACy = A.y - C.y;
    
	const float denom = DCy*BAx - DCx*BAy;
    
	*S = DCx*ACy - DCy*ACx;
	*T = BAx*ACy - BAy*ACx;
    
	if (denom == 0) {
		if (*S == 0 || *T == 0) {
			// Lines incident
			return YES;
		}
		// Lines parallel and not incident
		return NO;
	}
    
	*S = *S / denom;
	*T = *T / denom;
    
	// Point of intersection
	// CGPoint P;
	// P.x = A.x + *S * (B.x - A.x);
	// P.y = A.y + *S * (B.y - A.y);
    
	return YES;
}

float ccpAngle(CGPoint a, CGPoint b)
{
	float angle = acosf(ccpDot(ccpNormalize(a), ccpNormalize(b)));
	if( fabs(angle) < kCGPointEpsilon ) return 0.f;
	return angle;
}

#pragma mark - Math line functions
double det (double a, double b, double c, double d) {
	return a * d - b * c;
}

line getLine(CGPoint a, CGPoint b) {
    line result;
    result.a = a.y - b.y;
    result.b = b.x - a.x;
    result.c = a.x * b.y - b.x * a.y;
    return result;
}

CGPoint intesectionPoint(line m, line n) {
    double zn = det (m.a, m.b, n.a, n.b);
    const double EPS = 1e-9;
	if (fabs (zn) < EPS)
		return CGPointZero;
    CGPoint res;
	res.x = - det (m.c, m.b, n.c, n.b) / zn;
	res.y = - det (m.a, m.c, n.a, n.c) / zn;
    
    return res;
}

@end

#pragma mark - Bezier Path Category

#define POINTSTRING(_CGPOINT_) (NSStringFromCGPoint(_CGPOINT_))
#define VALUE(_INDEX_) [NSValue valueWithCGPoint:points[_INDEX_]]
#define POINT(_INDEX_) [(NSValue *)[points objectAtIndex:_INDEX_] CGPointValue]

// Return distance between two points
static float distance (CGPoint p1, CGPoint p2)
{
	float dx = p2.x - p1.x;
	float dy = p2.y - p1.y;
	
	return sqrt(dx*dx + dy*dy);
}

@implementation UIBezierPath (Points)
void getPointsFromBezier(void *info, const CGPathElement *element)
{
    NSMutableArray *bezierPoints = (__bridge NSMutableArray *)info;
    CGPathElementType type = element->type;
    CGPoint *points = element->points;
    if (type != kCGPathElementCloseSubpath)
    {
        if ((type == kCGPathElementAddLineToPoint) ||
            (type == kCGPathElementMoveToPoint))
            [bezierPoints addObject:VALUE(0)];
        else if (type == kCGPathElementAddQuadCurveToPoint)
            [bezierPoints addObject:VALUE(1)];
        else if (type == kCGPathElementAddCurveToPoint)
            [bezierPoints addObject:VALUE(2)];
    }
}

- (NSArray *)points
{
    NSMutableArray *points = [NSMutableArray array];
    CGPathApply(self.CGPath, (__bridge void *)points, getPointsFromBezier);
    return points;
}

// Return a Bezier path buit with the supplied points
+ (UIBezierPath *) pathWithPoints: (NSArray *) points
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (points.count == 0) return path;
    [path moveToPoint:POINT(0)];
    for (int i = 1; i < points.count; i++)
        [path addLineToPoint:POINT(i)];
    return path;
}

- (CGFloat) length
{
    NSArray *points = self.points;
    float totalPointLength = 0.0f;
    for (int i = 1; i < points.count; i++)
        totalPointLength += distance(POINT(i), POINT(i-1));
    return totalPointLength;
}

- (NSArray *) pointPercentArray
{
    // Use total length to calculate the percent of path consumed at each control point
    NSArray *points = self.points;
    NSInteger pointCount = points.count;
    
    float totalPointLength = self.length;
    float distanceTravelled = 0.0f;
    
	NSMutableArray *pointPercentArray = [NSMutableArray array];
	[pointPercentArray addObject:@(0.0)];
    
	for (NSInteger i = 1; i < pointCount; i++)
	{
		distanceTravelled += distance(POINT(i), POINT(i-1));
		[pointPercentArray addObject:@(distanceTravelled / totalPointLength)];
	}
	
	// Add a final item just to stop with. Probably not needed.
	[pointPercentArray addObject:[NSNumber numberWithFloat:1.1f]]; // 110%
    
    return pointPercentArray;
}

- (CGPoint) pointAtPercent: (CGFloat) percent withSlope: (CGPoint *) slope
{
    NSArray *points = self.points;
    NSArray *percentArray = self.pointPercentArray;
    CFIndex lastPointIndex = points.count - 1;
    
    if (!points.count)
        return CGPointZero;
    
    // Check for 0% and 100%
    if (percent <= 0.0f) return POINT(0);
    if (percent >= 1.0f) return POINT(lastPointIndex);
    
    // Find a corresponding pair of points in the path
    CFIndex index = 1;
    while ((index < percentArray.count) &&
           (percent > ((NSNumber *)percentArray[index]).floatValue))
        index++;
    
    // This should not happen.
    if (index > lastPointIndex) return POINT(lastPointIndex);
    
    // Calculate the intermediate distance between the two points
    CGPoint point1 = POINT(index -1);
    CGPoint point2 = POINT(index);
    
    float percent1 = [[percentArray objectAtIndex:index - 1] floatValue];
    float percent2 = [[percentArray objectAtIndex:index] floatValue];
    float percentOffset = (percent - percent1) / (percent2 - percent1);
    
    float dx = point2.x - point1.x;
    float dy = point2.y - point1.y;
    
    // Store dy, dx for retrieving arctan
    if (slope) *slope = CGPointMake(dx, dy);
    
    // Calculate new point
    CGFloat newX = point1.x + (percentOffset * dx);
    CGFloat newY = point1.y + (percentOffset * dy);
    CGPoint targetPoint = CGPointMake(newX, newY);
    
    return targetPoint;
}

void getBezierElements(void *info, const CGPathElement *element)
{
    NSMutableArray *bezierElements = (__bridge NSMutableArray *)info;
    CGPathElementType type = element->type;
    CGPoint *points = element->points;
    
    switch (type)
    {
        case kCGPathElementCloseSubpath:
            [bezierElements addObject:@[@(type)]];
            break;
        case kCGPathElementMoveToPoint:
        case kCGPathElementAddLineToPoint:
            [bezierElements addObject:@[@(type), VALUE(0)]];
            break;
        case kCGPathElementAddQuadCurveToPoint:
            [bezierElements addObject:@[@(type), VALUE(0), VALUE(1)]];
            break;
        case kCGPathElementAddCurveToPoint:
            [bezierElements addObject:@[@(type), VALUE(0), VALUE(1), VALUE(2)]];
            break;
    }
}

- (NSArray *) bezierElements
{
    NSMutableArray *elements = [NSMutableArray array];
    CGPathApply(self.CGPath, (__bridge void *)elements, getBezierElements);
    return elements;
}

+ (UIBezierPath *) pathWithElements: (NSArray *) elements
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (elements.count == 0) return path;
    
    for (NSArray *points in elements)
    {
        if (!points.count) continue;
        CGPathElementType elementType = (CGPathElementType)[points[0] integerValue];
        switch (elementType)
        {
            case kCGPathElementCloseSubpath:
                [path closePath];
                break;
            case kCGPathElementMoveToPoint:
                if (points.count == 2)
                    [path moveToPoint:POINT(1)];
                break;
            case kCGPathElementAddLineToPoint:
                if (points.count == 2)
                    [path addLineToPoint:POINT(1)];
                break;
            case kCGPathElementAddQuadCurveToPoint:
                if (points.count == 3)
                    [path addQuadCurveToPoint:POINT(2) controlPoint:POINT(1)];
                break;
            case kCGPathElementAddCurveToPoint:
                if (points.count == 4)
                    [path addCurveToPoint:POINT(3) controlPoint1:POINT(1) controlPoint2:POINT(2)];
                break;
        }
    }
    
    return path;
}


+(UIBezierPath *)interpolateCGPointsWithCatmullRom:(NSArray *)pointsAsNSValues closed:(BOOL)closed alpha:(float)alpha {
    if ([pointsAsNSValues count] < 4)
        return nil;
    
    NSInteger endIndex = (closed ? [pointsAsNSValues count] : [pointsAsNSValues count]-2);
    NSAssert(alpha >= 0.0 && alpha <= 1.0, @"alpha value is between 0.0 and 1.0, inclusive");
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    NSInteger startIndex = (closed ? 0 : 1);
    for (NSInteger ii=startIndex; ii < endIndex; ++ii) {
        CGPoint p0, p1, p2, p3;
        NSInteger nextii      = (ii+1)%[pointsAsNSValues count];
        NSInteger nextnextii  = (nextii+1)%[pointsAsNSValues count];
        NSInteger previi      = (ii-1 < 0 ? [pointsAsNSValues count]-1 : ii-1);
        
        [pointsAsNSValues[ii] getValue:&p1];
        [pointsAsNSValues[previi] getValue:&p0];
        [pointsAsNSValues[nextii] getValue:&p2];
        [pointsAsNSValues[nextnextii] getValue:&p3];
        
        float d1 = ccpLength(ccpSub(p1, p0));
        float d2 = ccpLength(ccpSub(p2, p1));
        float d3 = ccpLength(ccpSub(p3, p2));
        
        CGPoint b1, b2;
        b1 = ccpMult(p2, powf(d1, 2*alpha));
        b1 = ccpSub(b1, ccpMult(p0, powf(d2, 2*alpha)));
        b1 = ccpAdd(b1, ccpMult(p1,(2*powf(d1, 2*alpha) + 3*powf(d1, alpha)*powf(d2, alpha) + powf(d2, 2*alpha))));
        b1 = ccpMult(b1, 1.0 / (3*powf(d1, alpha)*(powf(d1, alpha)+powf(d2, alpha))));
        
        b2 = ccpMult(p1, powf(d3, 2*alpha));
        b2 = ccpSub(b2, ccpMult(p3, powf(d2, 2*alpha)));
        b2 = ccpAdd(b2, ccpMult(p2,(2*powf(d3, 2*alpha) + 3*powf(d3, alpha)*powf(d2, alpha) + powf(d2, 2*alpha))));
        b2 = ccpMult(b2, 1.0 / (3*powf(d3, alpha)*(powf(d3, alpha)+powf(d2, alpha))));
        
        if (ii==startIndex)
            [path moveToPoint:p1];
        
        [path addCurveToPoint:p2 controlPoint1:b1 controlPoint2:b2];
    }
    
    if (closed)
        [path closePath];
    
    return path;
}

+(UIBezierPath *)interpolateCGPointsWithHermite:(NSArray *)pointsAsNSValues closed:(BOOL)closed {
    if ([pointsAsNSValues count] < 2)
        return nil;
    
    NSInteger nCurves = (closed ? [pointsAsNSValues count] : [pointsAsNSValues count]-1);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    for (NSInteger ii=0; ii < nCurves; ++ii) {
        NSValue *value  = pointsAsNSValues[ii];
        
        CGPoint curPt, prevPt, nextPt, endPt;
        [value getValue:&curPt];
        if (ii==0)
            [path moveToPoint:curPt];
        
        NSInteger nextii = (ii+1)%[pointsAsNSValues count];
        NSInteger previi = (ii-1 < 0 ? [pointsAsNSValues count]-1 : ii-1);
        
        [pointsAsNSValues[previi] getValue:&prevPt];
        [pointsAsNSValues[nextii] getValue:&nextPt];
        endPt = nextPt;
        
        float mx, my;
        if (closed || ii > 0) {
            mx = (nextPt.x - curPt.x)*0.5 + (curPt.x - prevPt.x)*0.5;
            my = (nextPt.y - curPt.y)*0.5 + (curPt.y - prevPt.y)*0.5;
        }
        else {
            mx = (nextPt.x - curPt.x)*0.5;
            my = (nextPt.y - curPt.y)*0.5;
        }
        
        CGPoint ctrlPt1;
        ctrlPt1.x = curPt.x + mx / 3.0;
        ctrlPt1.y = curPt.y + my / 3.0;
        
        [pointsAsNSValues[nextii] getValue:&curPt];
        
        nextii = (nextii+1)%[pointsAsNSValues count];
        previi = ii;
        
        [pointsAsNSValues[previi] getValue:&prevPt];
        [pointsAsNSValues[nextii] getValue:&nextPt];
        
        if (closed || ii < nCurves-1) {
            mx = (nextPt.x - curPt.x)*0.5 + (curPt.x - prevPt.x)*0.5;
            my = (nextPt.y - curPt.y)*0.5 + (curPt.y - prevPt.y)*0.5;
        }
        else {
            mx = (curPt.x - prevPt.x)*0.5;
            my = (curPt.y - prevPt.y)*0.5;
        }
        
        CGPoint ctrlPt2;
        ctrlPt2.x = curPt.x - mx / 3.0;
        ctrlPt2.y = curPt.y - my / 3.0;
        
        [path addCurveToPoint:endPt controlPoint1:ctrlPt1 controlPoint2:ctrlPt2];
    }
    
    if (closed)
        [path closePath];
    
    return path;
}











@end
