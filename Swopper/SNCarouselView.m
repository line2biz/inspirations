

#import "SNCarouselView.h"
#import "SNAdditionalMath.h"

@interface SNCarouselView () <UIGestureRecognizerDelegate>
{
    NSArray *_arrayOfPositions;
    CGPoint _previousPoint, _center;
    CGFloat _minSideLength;
    NSInteger _previousIndex;
    BOOL _actionsDisabled;
    CGFloat _startDegree, _radius;
    CGFloat _maxScale, _minScale;
    CGFloat _animationDuration;
    CGFloat _maxPostitionDistance;
}

@property (assign, nonatomic) NSInteger currentIndex;

@end

@implementation SNCarouselView

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor clearColor];
    
    _minSideLength = MIN(CGRectGetMaxX(self.bounds), CGRectGetMaxY(self.bounds));
    _center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    _startDegree = 90.0;
    _radius = _minSideLength/2 - 50;
    _maxScale = 1.0f;
    _minScale = 0.5f;
    _animationDuration = 0.5f;
    _maxPostitionDistance = 0.0f;
    
    [self setUserInteractionEnabled:YES];
    
    UIPanGestureRecognizer *panGestureRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    panGestureRecog.delegate = self;
    [self addGestureRecognizer:panGestureRecog];
    
}

#pragma mark - Private Methods
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!_arrayOfPositions.count) {
        return;
    }
    
    CGAffineTransform affineTransform = CATransform3DGetAffineTransform([self common3DTransform]);
    
    for (NSInteger i=0; i<_views.count; i++) {
        NSInteger positionIndex = i - _currentIndex;
        if (positionIndex < 0) {
            positionIndex = _views.count + positionIndex;
        }
        CGPoint position = GetPointFromArrayAtIndex(_arrayOfPositions,positionIndex);
        position = CGPointApplyAffineTransform(position, affineTransform);
        
        UIView *view = (UIView *)[_views objectAtIndex:i];
        view.center = position;
        
        // scaling
        CATransform3D scale = CATransform3DIdentity;
        if (positionIndex == 0) {
            scale = CATransform3DScale(scale, _maxScale, _maxScale, 1.0);
            scale = CATransform3DTranslate(scale, 0.0, 0.0, _maxScale);
        } else {
            CustomLine line;
            line.startPoint = GetPointFromArrayAtIndex(_arrayOfPositions,0);
            line.endPoint = GetPointFromArrayAtIndex(_arrayOfPositions,positionIndex);
            CGFloat length = GetLineLength(line);
            
            CGFloat scaleDif = _maxScale - _minScale;
            CGFloat scaleFactor = _maxScale - length/_maxPostitionDistance * scaleDif;
            
            scale = CATransform3DScale(scale, scaleFactor, scaleFactor, 1.0);
            scale = CATransform3DTranslate(scale, 0.0, 0.0, scaleFactor);
        }
        
        view.layer.transform = scale;
        
        if (![self.subviews containsObject:view]) {
            UITapGestureRecognizer *tapGestureRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
            tapGestureRecog.delegate = self;
            [view addGestureRecognizer:tapGestureRecog];
            
            [self addSubview:view];
        }
    }
}

- (CATransform3D)common3DTransform
{
    CATransform3D transform = CATransform3DIdentity;
    transform = CATransform3DRotate(transform, RadiansFromDegrees(40.0), 1.0, 0.0, 0.0);
    return transform;
}

- (void)changePositionAnimation
{
    if (!_arrayOfPositions.count) {
        return;
    }
    
    _actionsDisabled = YES;
    
    // change real positions of view in a moment before animation finishes
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((_animationDuration - _animationDuration*0.17f) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self layoutSubviews];
    });
    
//    // positions for direction
//    CGPoint startPosition = GetPointFromArrayAtIndex(_arrayOfPositions, 0);
//    CGPoint endPosition = ((UIView *)[_views objectAtIndex:_currentIndex]).center;
    for (NSInteger i=0; i<_views.count; i++) {
        // get position indexes
        NSInteger positionIndex = i - _currentIndex;
        if (positionIndex < 0) {
            positionIndex = _views.count + positionIndex;
        }
        
        NSInteger previousPositionIndex = i - _previousIndex;
        if (previousPositionIndex < 0) {
            previousPositionIndex = _views.count + previousPositionIndex;
        }
        
        // get view
        UIView *view = (UIView *)[_views objectAtIndex:i];
        
        // transform definition
        CGAffineTransform affineTransform = CATransform3DGetAffineTransform([self common3DTransform]);
        
//        // get angles
//        CGFloat startAngle = _startDegree - previousPositionIndex*(360.0f/_views.count);
//        CGFloat endAngle = _startDegree - positionIndex*(360.0f/_views.count);
//        
//        // find needed direction
//        BOOL clockwise;
//        if (startPosition.x <= endPosition.x) {
//            clockwise = YES;
//        } else {
//            clockwise = NO;
//        }
//        
//        // create circular path
//        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//        [bezierPath addArcWithCenter:_center
//                              radius:_radius
//                          startAngle:RadiansFromDegrees(startAngle)
//                            endAngle:RadiansFromDegrees(endAngle)
//                           clockwise:clockwise];
//        
//        // transform bezier path
//        NSArray *bezierElements = [bezierPath bezierElements];
//        NSMutableArray *mArrayOfElements = [NSMutableArray new];
//        for (NSArray *array in bezierElements) {
//            NSMutableArray *mArrayOfElementComponents = [NSMutableArray new];
//            for (id obj in array) {
//                CGPoint newPoint;
//                if ([obj isKindOfClass:[NSNumber class]]) {
//                    [mArrayOfElementComponents addObject:obj];
//                } else
//                    if ([obj isKindOfClass:[NSValue class]]) {
//                        NSValue *pointValue = (NSValue *)obj;
//                        newPoint = [pointValue CGPointValue];
//                        newPoint = CGPointApplyAffineTransform(newPoint, affineTransform);
//                        AddPointToArray(newPoint,mArrayOfElementComponents);
//                    } else {
//                        [mArrayOfElementComponents addObject:obj];
//                    }
//            }
//            [mArrayOfElements addObject:mArrayOfElementComponents];
//        }
//        bezierPath = [UIBezierPath pathWithElements:mArrayOfElements];
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        
        CGPoint startPoint = GetPointFromArrayAtIndex(_arrayOfPositions,previousPositionIndex);
        startPoint = CGPointApplyAffineTransform(startPoint, affineTransform);
        CGPoint endPoint = GetPointFromArrayAtIndex(_arrayOfPositions,positionIndex);
        endPoint = CGPointApplyAffineTransform(endPoint, affineTransform);
        
        [bezierPath moveToPoint:startPoint];
        [bezierPath addLineToPoint:endPoint];
        
        // animation settings part
        CGFloat animationDuration = _animationDuration;
        
        // change position
        CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        anim.path = bezierPath.CGPath;
        anim.duration = animationDuration;
        anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        
        // scale view
        CABasicAnimation *scaling = [CABasicAnimation animationWithKeyPath:@"transform"];
        CATransform3D scale = CATransform3DIdentity;
        if (positionIndex == 0) {
            scale = CATransform3DScale(scale, _maxScale, _maxScale, 1.0);
            scale = CATransform3DTranslate(scale, 0.0, 0.0, _maxScale);
        } else {
            CustomLine line;
            line.startPoint = GetPointFromArrayAtIndex(_arrayOfPositions,0);
            line.endPoint = GetPointFromArrayAtIndex(_arrayOfPositions,positionIndex);
            CGFloat length = GetLineLength(line);
            
            CGFloat scaleDif = _maxScale - _minScale;
            CGFloat scaleFactor = _maxScale - length/_maxPostitionDistance * scaleDif;
            
            scale = CATransform3DScale(scale, scaleFactor, scaleFactor, 1.0);
            scale = CATransform3DTranslate(scale, 0.0, 0.0, scaleFactor);
        }
        scaling.duration = animationDuration;
        scaling.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        scaling.removedOnCompletion = NO;
        scaling.fillMode = kCAFillModeForwards;
        scaling.toValue = [NSValue valueWithCATransform3D:scale];
        
        // add animation and completition
        [CATransaction setAnimationDuration:animationDuration];
        
        [CATransaction setCompletionBlock:^{
            [view.layer removeAllAnimations];
            if (i == _views.count - 1) {
                _actionsDisabled = NO;
                [self.delegate carouselView:self finishedAnimation:YES forIndex:_currentIndex];
            }
        }];
        
        [CATransaction begin];
        [view.layer addAnimation:anim forKey:@"move"];
        [view.layer addAnimation:scaling forKey:@"scale"];
        [CATransaction commit];
    }
}

#pragma mark - Public Methods
- (void)setCurrentIndexTo:(NSInteger)index animated:(BOOL)animated
{
    self.currentIndex = index;
    if (animated) {
        [self changePositionAnimation];
    } else {
        [self layoutSubviews];
    }
}

#pragma mark Gesture Handler
- (void)panGestureHandler:(UIPanGestureRecognizer *)sender
{
    if (!_actionsDisabled) {
        CGPoint point = [sender locationInView:self];
        switch (sender.state) {
            case UIGestureRecognizerStateBegan:
            {
                _previousPoint = point;
            }
                break;
            case UIGestureRecognizerStateEnded:
            {
                if (_previousPoint.x < point.x) {
                    self.currentIndex--;
                } else {
                    self.currentIndex++;
                }
                [self changePositionAnimation];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    if (!_actionsDisabled) {
        NSInteger objectIndex = [_views indexOfObject:sender.view];
        self.currentIndex = objectIndex;
        [self changePositionAnimation];
    }
}

#pragma mark - Property Accessors
- (void)setViews:(NSArray *)views
{
    if (views) {
        _views = views;
        
        CGFloat maxViewSide = 0;
        for (UIView *view in _views) {
            maxViewSide = MAX(maxViewSide,MAX(CGRectGetWidth(view.frame), CGRectGetHeight(view.frame)));
        }
        
        _radius = _minSideLength/2 - maxViewSide/2 * _maxScale;
        _arrayOfPositions = GetPointsOnCircleWithCenterRadiusStartAngle(views.count,_center,_radius, _startDegree);
        
        // max distance from current position
        for (NSInteger i=1; i<_arrayOfPositions.count; i++) {
            CustomLine line;
            line.startPoint = GetPointFromArrayAtIndex(_arrayOfPositions,0);
            line.endPoint = GetPointFromArrayAtIndex(_arrayOfPositions,i);
            CGFloat length = GetLineLength(line);
            _maxPostitionDistance = MAX(_maxPostitionDistance, length);
        }
        
        _currentIndex = 0;
        _previousIndex = 0;
        [self layoutSubviews];
    }
}

- (void)setCurrentIndex:(NSInteger)currentIndex
{
    _previousIndex = _currentIndex;
    if (currentIndex < 0) {
        currentIndex = _views.count - 1;
    }
    if (currentIndex > _views.count - 1) {
        currentIndex = 0;
    }
    _currentIndex = currentIndex;
    
    [self.delegate carouselView:self changedIndexTo:_currentIndex];
}











@end
