

#import "SNHeightWeightControl.h"
#import "UIFont+Customization.h"
#import "SNAdditionalMath.h"

//#define kCmToFeet 0.032808399f
//#define kKiloToPound 2.20462262f
//#define kFeetToInches 12

#define kCmToFeet 0.032808399f
#define kKiloToPound 2.20462262f
#define kFeetToInches 12

@interface SNHeightWeightControl() <UIGestureRecognizerDelegate>
{
    CGFloat _rulerHeight, _rulerLength;
    CGFloat _mainLine, _middleLine, _smallLine;
    CGFloat _minValue, _maxValue, _minShownV, _maxShownV;
    NSInteger _amountOfValues;
    CGFloat _spacing, _pointWidth, _availableDragSpace;
    
    CGPoint _startTouchPoint, _endTouchPoint;
    
    UIColor *_gridColor, *_chosenColor;
    
    UIFont *_textMainFont, *_textCurrentFont;
    
    CGFloat _moveDelta, _preMinShownV;
    
    CGFloat _currentValue;
    
    // currently not in use
    //    CGRect _rectMeasureUnit;
}

- (CGFloat)theCurrentValue;

@end

@implementation SNHeightWeightControl

@synthesize currentValue = _currentValue;

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    _controlType = SNControlTypeHeight;
    _measureUnit = SNMeasureUnitMeters;
    _amountOfValues = 26;
    _pointWidth = 3.5f;
    _gridColor = [UIColor colorWithRed:189/255.0 green:195/255.0 blue:202/255.0 alpha:1.0f];
    _chosenColor = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0f];
    
    _textMainFont = [UIFont franklinGothicStdBookCondesedWithSize:27];
    _textCurrentFont = [UIFont franklinGothicStdBookCondesedWithSize:75];
    
    [self mainCalculations];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandler:)];
    [panGesture setMinimumNumberOfTouches:1];
    [self addGestureRecognizer:panGesture];
    panGesture.delegate = self;
    
    //    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    //    tapGesture.delegate = self;
    //    [self addGestureRecognizer:tapGesture];
}

#pragma mark - Calculations
- (void)mainCalculations
{
    [self calculateLinesSizes];
    [self calculateMinMaxValues];
}

- (void)calculateLinesSizes
{
    if (_controlType == SNControlTypeHeight) {
        _rulerHeight = 0.33f * CGRectGetWidth(self.bounds);
        _rulerLength = CGRectGetHeight(self.bounds);
        _availableDragSpace = CGRectGetHeight(self.bounds);
    } else {
        _rulerHeight = 0.4f * CGRectGetHeight(self.bounds);
        _rulerLength = CGRectGetWidth(self.bounds);
        _availableDragSpace = CGRectGetWidth(self.bounds);
    }
    _mainLine = 0.6f * _rulerHeight;
    _middleLine = 0.6f * _mainLine;
    _smallLine = 0.6f * _middleLine;
    
    _spacing = _rulerLength / _amountOfValues;
    
}

- (void)calculateMinMaxValues
{
    CGFloat min, max, cur;
    if (_controlType == SNControlTypeHeight) {
        min = 40.0f;
        max = 300.0f;
        cur = 170.0f;
        if (_measureUnit == SNMeasureUnitMeters) {
            _minValue = min - _amountOfValues/2;
            _maxValue = max + _amountOfValues/2;
            _currentValue = cur;
        } else if (_measureUnit == SNMeasureUnitFeets) {
            _minValue = min * kCmToFeet * kFeetToInches - _amountOfValues/2;
            _maxValue = max * kCmToFeet * kFeetToInches + _amountOfValues/2;
            _currentValue = cur * kCmToFeet * kFeetToInches;
        }
    } else {
        min = 30.0f;
        max = 160.0f;
        cur = 50.0f;
        if (_measureUnit == SNMeasureUnitKilos) {
            _minValue = min - _amountOfValues/2;
            _maxValue = max + _amountOfValues/2;
            _currentValue = cur;
        } else if (_measureUnit == SNMeasureUnitPounds) {
            _minValue = min * kKiloToPound - _amountOfValues/2;
            _maxValue = max * kKiloToPound + _amountOfValues/2;
            _currentValue = cur * kKiloToPound;
        }
    }
    _minShownV = _currentValue - _amountOfValues/2;
    _maxShownV = _currentValue + _amountOfValues/2;
}

#pragma mark - Gesture Recognizer Handler
- (void)gestureHandler:(UIPanGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _moveDelta = 0.0f;
            _preMinShownV = _minShownV;
            _startTouchPoint = [sender locationInView:self];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            _endTouchPoint = [sender locationInView:self];
            
            CGFloat deltaValue;
            if (self.controlType == SNControlTypeHeight) {
                deltaValue = _endTouchPoint.y - _startTouchPoint.y ;
            } else {
                deltaValue = _startTouchPoint.x - _endTouchPoint.x;
            }
            
            _moveDelta = deltaValue / _spacing;
            _minShownV = _preMinShownV + _moveDelta;
            _maxShownV = _minShownV + _amountOfValues;
            
            if (_minShownV <= _minValue) {
                _minShownV = _minValue;
                _maxShownV = _minValue + _amountOfValues;
            } else if (_maxShownV >= _maxValue) {
                _maxShownV = _maxValue;
                _minShownV = _maxShownV - _amountOfValues;
            }
            _currentValue = _minShownV + _amountOfValues/2;
            [self setNeedsDisplay];
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            _currentValue = _currentValue - (_currentValue - roundf(_currentValue));
            _minShownV = _currentValue - _amountOfValues/2;
            _maxShownV = _minShownV + _amountOfValues;
            [self setNeedsDisplay];
        }
            break;
            
        default:
            break;
    }
}


// currently not in use
//- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
//{
//    CGPoint point = [sender locationInView:sender.view];
//    if (CGRectContainsPoint(_rectMeasureUnit, point)) {
//        [self changeToAlternativeMeasureUnit];
//    }
//}

#pragma mark - Drawing Methods
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // all lines
    for (NSInteger i=_minShownV; i<_minShownV + _amountOfValues; i++) {
        // prepare context
        CGContextSetStrokeColorWithColor(context, _gridColor.CGColor);
        CGContextSetLineWidth(context, _pointWidth);
        // prepare data
        CGPoint startPoint;
        CGFloat angle;
        if (self.controlType == SNControlTypeHeight) {
            startPoint = CGPointMake(0.0f, _rulerLength - (i - _minShownV) * _spacing);
            angle = 0.0f;
        } else {
            startPoint = CGPointMake((i - _minShownV) * _spacing, 0.0f);
            angle = 90.0f;
        }
        CustomLine line;
        line.startPoint = startPoint;
        CGFloat lineLength;
        
        NSInteger iMeasurementUnitParts;
        if (self.measureUnit == SNMeasureUnitFeets) {
            iMeasurementUnitParts = kFeetToInches;
        } else {
            iMeasurementUnitParts = 10;
        }
        
        if (i % iMeasurementUnitParts > 0) {
            NSInteger iNextDevision = iMeasurementUnitParts / 2;
            if (i % iNextDevision > 0) {
                lineLength = _smallLine;
            } else {
                lineLength = _middleLine;
            }
        } else {
            lineLength = _mainLine;
        }
        
        line.endPoint = GetPointFromPointAngleLength(line.startPoint,angle,lineLength);
        
        if (i%iMeasurementUnitParts == 0) {
            // main value text drawing
            UIFont* font = _textMainFont;
            UIColor* textColor = _gridColor;
            UIColor *newColor;
            const CGFloat *colorComponents = CGColorGetComponents(textColor.CGColor);
            if (i >= _currentValue - 1 && i <= _currentValue + 1) {
                newColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.33f];
            } else if (i >= _currentValue - 2 && i <= _currentValue +2) {
                newColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.66f];
            } else {
                newColor = textColor;
            }
            NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : newColor };
            NSString *textToDraw;
            if (_measureUnit != SNMeasureUnitFeets) {
                textToDraw = [NSString stringWithFormat:@"%zd",i];
            } else {
                CGFloat fFeetAmount = i / kFeetToInches;
                textToDraw = [NSString stringWithFormat:@"%.0f",fFeetAmount];
            }
            
            NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:textToDraw attributes:stringAttrs];
            CGSize size = [textToDraw sizeWithAttributes:stringAttrs];
            
            CGPoint textPoint = line.endPoint;
            if (self.controlType == SNControlTypeHeight) {
                textPoint.y -= size.height / 2;
                textPoint.x += 5;
            } else {
                textPoint.y += 5;
                textPoint.x -= size.width / 2;
            }
            
            if (i != _currentValue) {
                [attrStr drawAtPoint:textPoint];
            }
        }
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:line.startPoint];
        [path addLineToPoint:line.endPoint];
        
        CGContextAddPath(context, path.CGPath);
        CGContextStrokePath(context);
    }
    
    // current value line
    CGContextSetStrokeColorWithColor(context, _chosenColor.CGColor);
    CGPoint startPoint;
    CGFloat angle;
    if (self.controlType == SNControlTypeHeight) {
        startPoint = CGPointMake(0.0f, CGRectGetHeight(self.bounds)/2);
        angle = 0.0f;
    } else {
        startPoint = CGPointMake(CGRectGetWidth(self.bounds)/2, 0.0f);
        angle = 90.0f;
    }
    CustomLine line;
    line.startPoint = startPoint;
    line.endPoint = GetPointFromPointAngleLength(line.startPoint,angle,_rulerHeight);
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:line.startPoint];
    [path addLineToPoint:line.endPoint];
    
    CGContextAddPath(context, path.CGPath);
    CGContextStrokePath(context);
    
    // current value text drawing
    UIFont* font = _textCurrentFont;
    UIColor* textColor = _chosenColor;
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
    
    NSString *textToDraw;
    if (_measureUnit != SNMeasureUnitFeets) {
        textToDraw = [NSString stringWithFormat:@"%zd",(NSInteger)_currentValue];
    } else {
        CGFloat fFeetValue = (NSInteger)(_currentValue) / (CGFloat)kFeetToInches;
        textToDraw = [NSString stringWithFormat:@"%zd", (NSInteger)fFeetValue];
    }
    NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:textToDraw attributes:stringAttrs];
    CGSize size = [textToDraw sizeWithAttributes:stringAttrs];
    
    CGPoint textPoint = line.endPoint;
    if (self.controlType == SNControlTypeHeight) {
        textPoint.y -= size.height / 2 - 7;
    } else {
        textPoint.x -= size.width / 2;
    }
    
    [attrStr drawAtPoint:textPoint];
    
    NSString *measurement;
    if (self.controlType == SNControlTypeHeight) {
        if (_measureUnit == SNMeasureUnitMeters) {
            measurement = @"cm";
        } else if (_measureUnit == SNMeasureUnitFeets) {
            measurement = @"ft";
        }
    } else {
        if (_measureUnit == SNMeasureUnitKilos) {
            measurement = @"kg";
        } else if (_measureUnit == SNMeasureUnitPounds) {
            measurement = @"lbs";
        }
    }
    font = _textMainFont;
    stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
    textToDraw = measurement;
    attrStr = [[NSAttributedString alloc] initWithString:textToDraw attributes:stringAttrs];
    CGSize measSize = [textToDraw sizeWithAttributes:stringAttrs];
    [attrStr drawAtPoint:CGPointMake(textPoint.x + size.width, textPoint.y + size.height/2 - 5)];
    
    // draw inches part
    if (self.measureUnit == SNMeasureUnitFeets) {
        
        UIFont* font = _textCurrentFont;
        UIColor* textColor = _chosenColor;
        NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
        CGFloat fFeetValue = (NSInteger)(_currentValue) / (CGFloat)kFeetToInches;
        NSString *sInches = [NSString stringWithFormat:@"%.0f\"", FloatPartOfNumber(fFeetValue) *kFeetToInches];
        NSAttributedString *attrStrInches = [[NSAttributedString alloc] initWithString:sInches attributes:stringAttrs];
        CGPoint nextPoint = CGPointMake(textPoint.x + size.width + measSize.width, textPoint.y);
        [attrStrInches drawAtPoint:nextPoint];
    }
    
    // draw change measurement unit proposal
    //    UIColor *measureTextColor = _gridColor;
    //    UIFont *measureTextFont = _textMainFont;
    //    stringAttrs = @{
    //                    NSFontAttributeName : measureTextFont,
    //                    NSForegroundColorAttributeName : measureTextColor
    //                    };
    //    NSString *sMeasureText;
    //    if (_measureUnit == SNMeasureUnitMeters) {
    //        sMeasureText = @"feet >";
    //    } else if (_measureUnit == SNMeasureUnitFeets) {
    //        sMeasureText = @"< meters";
    //    } else if (_measureUnit == SNMeasureUnitKilos) {
    //        sMeasureText = @"pounds >";
    //    } else if (_measureUnit == SNMeasureUnitPounds) {
    //        sMeasureText = @"< kilos";
    //    }
    //    attrStr = [[NSAttributedString alloc] initWithString:sMeasureText attributes:stringAttrs];
    //    CGSize sizeMeasureText = [sMeasureText sizeWithAttributes:stringAttrs];
    //    _rectMeasureUnit = CGRectMake(CGRectGetWidth(self.bounds) - sizeMeasureText.width, textPoint.y + size.height, sizeMeasureText.width, sizeMeasureText.height);
    //    [attrStr drawInRect:_rectMeasureUnit];
    
    // gradient color filling
    CGPoint myStartPoint, myEndPoint;
    UIBezierPath *gradientPath;
    if (self.controlType == SNControlTypeHeight) {
        gradientPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 0.0f, _rulerHeight + 5, _rulerLength)];
        
        myStartPoint.x = _rulerHeight / 2;
        myStartPoint.y = 0.0f;
        myEndPoint.x = _rulerHeight / 2;
        myEndPoint.y = _rulerLength;
    } else {
        gradientPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 0.0f, _rulerLength, _rulerHeight + 5)];
        
        myStartPoint.x = 0.0;
        myStartPoint.y = _rulerHeight / 2;
        myEndPoint.x = _rulerLength;
        myEndPoint.y = _rulerHeight / 2;
    }
    CGContextAddPath(context, gradientPath.CGPath);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGGradientRef myGradient;
    CGColorSpaceRef myColorspace;
    size_t num_locations = 4;
    CGFloat locations[4] = { 0.0, 0.25, 0.75, 1.0 };
    CGFloat components[16] =  { 1.0, 1.0, 1.0, 1.0,   // Start color
        1.0, 1.0, 1.0, 0.0,   // Mid Color
        1.0, 1.0, 1.0, 0.0,   // Mid Color
        1.0, 1.0, 1.0, 1.0 }; // End color
    
    myColorspace = CGColorSpaceCreateDeviceRGB();
    myGradient = CGGradientCreateWithColorComponents (myColorspace, components,
                                                      locations, num_locations);
    
    CGContextDrawLinearGradient (context, myGradient, myStartPoint, myEndPoint, 0);
}

#pragma mark - Public Methods
- (void)changeToAlternativeMeasureUnit
{
    switch (_measureUnit) {
        case SNMeasureUnitKilos:
        {
            self.measureUnit = SNMeasureUnitPounds;
        }
            break;
        case SNMeasureUnitPounds:
        {
            self.measureUnit = SNMeasureUnitKilos;
        }
            break;
        case SNMeasureUnitMeters:
        {
            self.measureUnit = SNMeasureUnitFeets;
        }
            break;
        case SNMeasureUnitFeets:
        {
            self.measureUnit = SNMeasureUnitMeters;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Property Accessors
- (void)setControlType:(SNControlType)controlType
{
    _controlType = controlType;
    if (_controlType == SNControlTypeHeight) {
        _measureUnit = SNMeasureUnitMeters;
    } else {
        _measureUnit = SNMeasureUnitKilos;
    }
    [self mainCalculations];
    [self setNeedsDisplay];
}

- (void)setMeasureUnit:(SNMeasureUnit)measureUnit
{
    switch (measureUnit) {
        case SNMeasureUnitKilos:
        {
            if (self.controlType != SNControlTypeWeight) {
                return;
            }
            if (_measureUnit != measureUnit) {
                _measureUnit = measureUnit;
                CGFloat currentValue = _currentValue;
                [self calculateMinMaxValues];
                self.currentValue = roundf(currentValue / kKiloToPound);
            }
        }
            break;
        case SNMeasureUnitPounds:
        {
            if (self.controlType != SNControlTypeWeight) {
                return;
            }
            if (_measureUnit != measureUnit) {
                _measureUnit = measureUnit;
                CGFloat currentValue = _currentValue;
                [self calculateMinMaxValues];
                self.currentValue = roundf(currentValue * kKiloToPound);
            }
        }
            break;
        case SNMeasureUnitMeters:
        {
            if (self.controlType != SNControlTypeHeight) {
                return;
            }
            if (_measureUnit != measureUnit) {
                _measureUnit = measureUnit;
                CGFloat currentValue = _currentValue;
                [self calculateMinMaxValues];
                self.currentValue = roundf(currentValue / kCmToFeet / kFeetToInches);
            }
        }
            break;
        case SNMeasureUnitFeets:
        {
            if (self.controlType != SNControlTypeHeight) {
                return;
            }
            if (_measureUnit != measureUnit) {
                _measureUnit = measureUnit;
                CGFloat currentValue = _currentValue;
                [self calculateMinMaxValues];
                self.currentValue = roundf(currentValue * kCmToFeet * kFeetToInches);
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)setCurrentValue:(CGFloat)currentValue
{
    _currentValue = currentValue;
    _minShownV = currentValue - _amountOfValues/2;
    _maxShownV = currentValue + _amountOfValues/2;
    [self setNeedsDisplay];
}

- (CGFloat)theCurrentValue
{
    switch (_measureUnit) {
        case SNMeasureUnitMeters:
        {
            return _currentValue;
        }
            break;
        case SNMeasureUnitFeets:
        {
            return _currentValue / kCmToFeet / kFeetToInches;
        }
            break;
        case SNMeasureUnitKilos:
        {
            return _currentValue;
        }
            break;
        case SNMeasureUnitPounds:
        {
            return _currentValue / kKiloToPound;
        }
            break;
            
        default:
            break;
    }
}



@end
