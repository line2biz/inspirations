

#import <UIKit/UIKit.h>

@protocol SNWeekGraphProtocol;
@protocol SNWeekGraphDelegate;
@protocol SNWeekGraphDatasource;

@interface SNWeekGraph : UIControl

@property (strong, nonatomic) NSArray *week;

@property (strong, nonatomic) UIColor *colorBkgRing;
@property (strong, nonatomic) UIColor *colorCaloriesMin;
@property (strong, nonatomic) UIColor *colorCaloriesMax;
@property (strong, nonatomic) UIColor *colorDayCircle;
@property (strong, nonatomic) UIColor *colorText;
@property (strong, nonatomic) UIColor *colorCurrentDayText;
@property (strong, nonatomic) UIFont *textFont;

@property (strong, nonatomic) NSDate *currentDate;

@property (assign, nonatomic) NSInteger minimumAmount;
@property (assign, nonatomic) NSInteger maxAmount;

@property (strong, nonatomic) NSString *annotationStepsText;
@property (strong, nonatomic) UIFont *annotationFont;
@property (strong, nonatomic) UIColor *annotationTextColor;

@property (strong, nonatomic) id <SNWeekGraphDelegate> delegate;
@property (weak, nonatomic) IBOutlet id <SNWeekGraphDatasource> dataSource;

@end


@protocol SNWeekGraphProtocol <NSObject>

@required
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) NSInteger steps;
@property (assign, nonatomic) NSInteger swoops;
@property (assign, nonatomic) NSInteger calories;

@end

@protocol SNWeekGraphDelegate <NSObject>
@required
- (void)weekGraphView:(SNWeekGraph *)weekGraphView changedCurrentDate:(NSDate *)date;
- (void)weekGraphView:(SNWeekGraph *)weekGraphView didChangeWeek:(NSArray *)week;

@end

@protocol SNWeekGraphDatasource <NSObject>

@required
- (NSArray *)weekGraphView:(SNWeekGraph *)weekGraphView willChangeWeekForward:(BOOL)forward;

@end


