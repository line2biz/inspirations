

#import "SNWeekGraph.h"
#import "SNAppearance.h"
#import "SNAdditionalMath.h"

#import "SNDay.h"

@interface SNWeekGraph () <UIGestureRecognizerDelegate>
{
    NSArray *_arrayOfCenterPoints;
    CGFloat _dayRadius;
    CGPoint _startPoint;
}

@end

@implementation SNWeekGraph

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)awakeFromNib
{
    NSArray *sourceData = [self.dataSource weekGraphView:self willChangeWeekForward:YES];
    if (sourceData) {
        self.week = sourceData;
    } else {
        
    }
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor whiteColor];
    
    _colorBkgRing = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:1.0];
    _colorCaloriesMin = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    _colorCaloriesMax = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:0/255.0 alpha:1.0];
    _colorDayCircle = [UIColor whiteColor];
    _colorText = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    _colorCurrentDayText = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0];
    _currentDate = [NSDate date];
    
    _annotationStepsText = LMLocalizedString(@"Schritte", nil);
    _annotationFont = [UIFont fontWithName:@"Arial" size:10];
    _annotationTextColor = _colorText;
    
    _textFont = [UIFont fontWithName:@"Arial" size:15];
    
    UITapGestureRecognizer *tapGestureRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    tapGestureRecog.delegate = self;
    [self addGestureRecognizer:tapGestureRecog];
    
    UIPanGestureRecognizer *panGestureRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    panGestureRecog.delegate = self;
    [self addGestureRecognizer:panGestureRecog];
}

#pragma mark - Private Methods
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (!self.week.count) {
        NSLog(@"\n%s\nNo data to show\n",__FUNCTION__);
        return;
    }
    __block BOOL objectDoesntConfirmProtocol = NO;
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj conformsToProtocol:@protocol(SNWeekGraphProtocol)]) {
            objectDoesntConfirmProtocol = YES;
            *stop = YES;
        }
    }];
    
    if (objectDoesntConfirmProtocol) {
        NSLog(@"\n%s\nOne or more objects doesn`t confirm to protocol!\n",__FUNCTION__);
        return;
    }
    
    // **************** additional variables ******************
    CGFloat boundsWidth = CGRectGetWidth(self.bounds);
    CGFloat boundsHeight = CGRectGetHeight(self.bounds);
    
//    CGFloat maxPoint = 0.00001f;
//    
//    for (id obj in self.week) {
//        NSInteger calories = 0;
//        calories = [[obj valueForKey:@"calories"] integerValue];
//        
//        // max point value
//        if (maxPoint < calories) {
//            maxPoint = calories;
//        }
//    }
    
    // ====== annotation =======
    CGFloat annotationHeight = 0.2f * CGRectGetHeight(self.bounds);
    CGFloat annotationWidth = CGRectGetWidth(self.bounds);
    CGRect annotationRect = CGRectMake(0, CGRectGetHeight(self.bounds) - annotationHeight, annotationWidth, annotationHeight);
    
    // ====== graph ======
    CGFloat graphWidth = boundsWidth;
    CGFloat graphHeight = boundsHeight - annotationHeight;
    CGFloat dayDiametr = graphWidth / 7.0f - graphWidth / 7.0f * 0.2f;
    
    CGRect graphRect = self.bounds;
    graphRect.size.height -= annotationHeight;
    
    // **************** Calculate start points for days ***************
    CustomLine daysCenterLine;
    
    daysCenterLine.startPoint = CGPointMake(graphRect.origin.x, graphRect.origin.y + graphHeight - dayDiametr/2);
    daysCenterLine.endPoint = CGPointMake(graphRect.origin.x + graphWidth, graphRect.origin.y + graphHeight - dayDiametr/2);
    NSArray *arrayOfPoints = GetMidAmountPointsForLineAndItsAngle(7, daysCenterLine, GetAngleOfLine(daysCenterLine));
    _arrayOfCenterPoints = arrayOfPoints;
    
    // **************** Calculate lines minimum positions *****************
    CGFloat linesStartHeight = graphRect.origin.y + graphHeight - dayDiametr;
    CGFloat linesEndHeight = 0.22f * dayDiametr + (graphRect.origin.y + graphHeight - dayDiametr);
    CGFloat minLineHeight = linesEndHeight - linesStartHeight;
    
    // **************** Context *****************
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    // **************** Draw Step Target Line *****************
    
    UIColor *strikeColor = self.colorCaloriesMin;
    
    NSInteger maxSteps = 0;
    NSInteger weeklySumIfSteps = 0;
    for (id obj in self.week) {
        maxSteps = MAX(maxSteps, [[obj valueForKey:@"steps"] integerValue]);
        weeklySumIfSteps += [[obj valueForKey:@"steps"] integerValue];
    }
    CGFloat availableHeight = graphHeight - dayDiametr/2;
    
    CGFloat startY = self.bounds.origin.y + 1.0f;
    CGFloat difference = (CGFloat)maxSteps / (CGFloat)self.maxAmount;
    if ( difference > 1.0f && difference < 2.0f) {
        startY += FloatPartOfNumber(difference)*availableHeight;
    } else if (difference >= 2.0f) {
        startY += availableHeight - 1.0f;
    }
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(self.bounds.origin.x, startY)];
    [linePath addLineToPoint:CGPointMake(CGRectGetWidth(self.bounds), startY)];
    
    CGContextAddPath(context, linePath.CGPath);
    CGContextSetStrokeColorWithColor(context, strikeColor.CGColor);
    CGContextSetLineWidth(context, 1.0f);
    
    NSInteger amount = 150;
    size_t num_locations = amount;
    CGFloat dashLength = CGRectGetWidth(self.bounds) / amount;
    CGFloat lengths[amount];
    for (NSInteger i = 0; i < amount; i++) {
        lengths[i] = dashLength;
    }
    
    CGContextSetLineDash(context, 1.0f, lengths, num_locations);
    CGContextStrokePath(context);
    num_locations = 0;
    CGContextSetLineDash(context, 0.0, nil, num_locations);
    
    // **************** Draw Progress Lines, Days and Percentage Circles *****************
    for (NSInteger i=0; i < 7; i++) {
        NSInteger calories = 0;
        
        id obj;
        if (i < self.week.count) {
            obj = [self.week objectAtIndex:i];
            calories = [[obj valueForKey:@"calories"] integerValue];
        }
        
        CGPoint dayCenterPoint = GetPointFromArrayAtIndex(arrayOfPoints, i);
        CGFloat whiteLineWidth = 0.1f * dayDiametr;
        CGFloat linesWidth = (dayDiametr - whiteLineWidth)*0.8f;
        
        CGFloat linesStartX = dayCenterPoint.x - linesWidth/2;
        
        // === calculate line progress here ===
        
        CGFloat thirdLineProgress = 0.0f;
        if (maxSteps > self.maxAmount) {
            thirdLineProgress = (CGFloat)calories/(CGFloat)maxSteps;
        } else {
            thirdLineProgress = (self.maxAmount) ? (CGFloat)calories/(CGFloat)self.maxAmount : 0;
        }
        
        
        CGFloat progressHeight = (graphHeight - dayDiametr) * thirdLineProgress;
        CGRect rect = CGRectMake(linesStartX, linesStartHeight - progressHeight, linesWidth, progressHeight + minLineHeight);
        UIBezierPath *lineBezier = [UIBezierPath bezierPathWithRect:rect];
        
        lineBezier = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(linesWidth/2, linesWidth/2)];
        
        CGContextAddPath(context, lineBezier.CGPath);
        UIColor *fillColor;
        if (calories < self.minimumAmount) {
            fillColor = self.colorCaloriesMin;
        } else {
            fillColor = self.colorCaloriesMax;
        }
        
        NSDate *dayDate = [obj valueForKey:@"date"];
        if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
            const CGFloat *colorComponents = CGColorGetComponents(fillColor.CGColor);
            fillColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
        }
        
        CGContextSetFillColorWithColor(context, fillColor.CGColor);
        CGContextSetLineWidth(context, 0.0f);
        CGContextFillPath(context);
        
        // === icon drawing ===
//        CGPoint arcCenter = CGPointMake(rect.origin.x + linesWidth/2, rect.origin.y + linesWidth/2);
        CGFloat radius = linesWidth * 0.8f / 2;
        _dayRadius = radius;
        
//        if (calories >= self.minimumAmount) {
//            UIBezierPath *circlePath = [UIBezierPath bezierPathWithArcCenter:arcCenter
//                                                                      radius:radius
//                                                                  startAngle:RadiansFromDegrees(0)
//                                                                    endAngle:RadiansFromDegrees(360)
//                                                                   clockwise:YES];
//            CGContextAddPath(context, circlePath.CGPath);
//            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
//            CGContextFillPath(context);
//            UIImage *image;
//            if (calories > self.minimumAmount) {
//                image = [UIImage imageNamed:@"Icon-Step-Cup-Done"];
//            } else {
//                image = [UIImage imageNamed:@"Icon-Step-CheckMark-Done"];
//            }
//            CGSize imageSize = image.size;
//            [image drawInRect:CGRectMake(arcCenter.x - imageSize.width/2, arcCenter.y - imageSize.height/2, imageSize.width, imageSize.height)];
//        }
        
        // === day drawing ===
        UIColor *circleColor;
        if (obj) {
            NSDate *dayDate = [obj valueForKey:@"date"];
            if ([SNAdditionalMath compareDate:dayDate withDate:_currentDate] == NSOrderedSame) {
                circleColor = self.colorCurrentDayText;
            } else {
                circleColor = self.colorDayCircle;
            }
        } else {
            circleColor = self.colorDayCircle;
        }
        radius = dayDiametr/2 - whiteLineWidth/2;
        UIBezierPath *dayPath = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                               radius:radius
                                                           startAngle:RadiansFromDegrees(0)
                                                             endAngle:RadiansFromDegrees(360)
                                                            clockwise:YES];
        CGContextAddPath(context, dayPath.CGPath);
        CGContextSetFillColorWithColor(context, circleColor.CGColor);
        CGContextSetStrokeColorWithColor(context, self.backgroundColor.CGColor);
        CGContextSetLineWidth(context, whiteLineWidth);
        CGContextDrawPath(context, kCGPathFillStroke);
        
        // === percentage circle drawing ===
        CGFloat startAngle = - 90;
        UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                              radius:radius - whiteLineWidth
                                                          startAngle:RadiansFromDegrees(startAngle)
                                                            endAngle:RadiansFromDegrees(360)
                                                           clockwise:YES];
        UIColor *drawColor;
        
        drawColor = self.colorBkgRing;
        if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
            const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
            drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
        }
        CGContextAddPath(context, circle.CGPath);
        CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
        CGContextSetLineWidth(context, whiteLineWidth);
        CGContextDrawPath(context, kCGPathStroke);
        
        if (obj) {
            CGFloat endAngle = (CGFloat)calories / (CGFloat)self.maxAmount * 360 + startAngle;
            UIBezierPath *percentage = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                           radius:radius - whiteLineWidth
                                                                       startAngle:RadiansFromDegrees(startAngle)
                                                                         endAngle:RadiansFromDegrees(endAngle)
                                                                        clockwise:YES];
            
            drawColor = self.colorCaloriesMin;
            if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
            }
            
            CGContextAddPath(context, percentage.CGPath);
            CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
            CGContextSetLineWidth(context, whiteLineWidth);
            CGContextDrawPath(context, kCGPathStroke);
        }
        
        
        // === text drawing ===
        UIFont* font = _textFont;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EE"];
        [dateFormat setLocale:[SNAppDelegate currentLocale]];
        NSString *text = @"";
        
        UIColor *textColor;
        if (obj) {
            NSDate *dayDate = [obj valueForKey:@"date"];
            NSString *dayName = [dateFormat stringFromDate:dayDate];
            text = [dayName substringToIndex:2];
            if ([SNAdditionalMath compareDate:dayDate withDate:_currentDate] == NSOrderedSame) {
                textColor = [UIColor whiteColor];
            } else if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                textColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
            } else {
                textColor = self.colorText;
            }
        } else {
            const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
            textColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
        }
        NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
        NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
        CGSize size = [text sizeWithAttributes:stringAttrs];
        CGPoint pointOfDrawing = CGPointMake(dayCenterPoint.x - size.width/2, dayCenterPoint.y - size.height/2);
        [attrStr drawAtPoint:pointOfDrawing];
    }
    
    // ************** Draw annotation ***************
    
    // create text for time interval and annotation
    NSArray *arrayOfData = [self.week mutableCopy];
    arrayOfData = [arrayOfData sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [obj1 valueForKey:@"date"];
        NSDate *date2 = [obj2 valueForKey:@"date"];
        return [SNAdditionalMath compareDate:date1 withDate:date2];
    }];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormatter.dateFormat = format;
    [dateFormatter setLocale:[SNAppDelegate currentLocale]];
    
    NSDate *date1 = [[arrayOfData firstObject] valueForKey:@"date"];
    NSDate *date2 = [[arrayOfData lastObject] valueForKey:@"date"];
    NSString *firstPart = [dateFormatter stringFromDate:date1];
    NSString *secondPart = [dateFormatter stringFromDate:date2];
    
    NSString *weekInterval = [NSString stringWithFormat:@"%@ - %@",firstPart,secondPart];
    NSString *annotation = [NSString stringWithFormat:@"%@ %@ %@", [SNAppearance textWithThousendsFormat:weeklySumIfSteps], self.annotationStepsText, LMLocalizedString(@"in dieser Woche", nil)];
    
    // create sizes and attributed strings
    NSArray *arrayIntervalObjs = SizeAndAttrStrFromTextWithFontColor(weekInterval, self.annotationFont, self.annotationTextColor);
    NSArray *arrayAnnotationObjs = SizeAndAttrStrFromTextWithFontColor(annotation, self.annotationFont, self.annotationTextColor);
    
    CGSize intervalSize = [[arrayIntervalObjs firstObject] CGSizeValue];
    CGSize annotationSize = [[arrayAnnotationObjs firstObject] CGSizeValue];
    NSAttributedString *sInterval = [arrayIntervalObjs lastObject];
    NSAttributedString *sAnnotation = [arrayAnnotationObjs lastObject];
    
    // calculate positions
    CGFloat fMarginTop = 2.0f;
    CGFloat fAllHeight = intervalSize.height + annotationSize.height + fMarginTop;
    
    CGFloat fIntervalStartX = (annotationWidth - intervalSize.width)/2;
    CGFloat fAnnotationStartX = (annotationWidth - annotationSize.width)/2;
    
    CGFloat fStartY = annotationRect.origin.y + (annotationRect.size.height - fAllHeight)/2;
    
    // draw text
    [sInterval drawInRect:CGRectMake(fIntervalStartX, fStartY, intervalSize.width, intervalSize.height)];
    [sAnnotation drawInRect:CGRectMake(fAnnotationStartX + 2, fStartY + intervalSize.height + fMarginTop, annotationSize.width, annotationSize.height)];
    
    // draw annotation
//    UIBezierPath *examplePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fAnnotationStartX, fStartY + intervalSize.height + fMarginTop, annotationSize.height, annotationSize.height)];
//    
//    CGContextSetFillColorWithColor(context, self.colorCaloriesMin.CGColor);
//    CGContextAddPath(context, examplePath.CGPath);
//    CGContextSetLineWidth(context, 0.0f);
//    CGContextFillPath(context);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setNeedsDisplay];
}

- (void)animateChangeToNext:(BOOL)next
{
    UIView *viewToRender = self;
    
    UIGraphicsBeginImageContextWithOptions(viewToRender.bounds.size, YES, 0);
//    [viewToRender drawViewHierarchyInRect:viewToRender.bounds afterScreenUpdates:YES];
    [viewToRender.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageView.image = image;
    
    [self addSubview:imageView];
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 0.5;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName: (next) ? kCAMediaTimingFunctionEaseIn : kCAMediaTimingFunctionEaseOut];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    if (next) {
        normalForm = CATransform3DTranslate(normalForm, -CGRectGetWidth(self.bounds), 0.0, 0.0);
    } else {
        normalForm = CATransform3DTranslate(normalForm, CGRectGetWidth(self.bounds), 0.0, 0.0);
    }
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    [CATransaction setCompletionBlock:^{
        [imageView removeFromSuperview];
    }];
    [CATransaction begin];
    
    [imageView.layer addAnimation:transformAnimation forKey:@"animationTransition"];
    
    [CATransaction commit];
}

- (void)animateStopOfChangeToNext:(BOOL)next
{
    __block CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 0.25;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName: (next) ? kCAMediaTimingFunctionEaseIn : kCAMediaTimingFunctionEaseOut];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    __block CATransform3D normalForm = CATransform3DIdentity;
    if (next) {
        normalForm = CATransform3DTranslate(normalForm, -CGRectGetWidth(self.bounds)*0.25, 0.0, 0.0);
    } else {
        normalForm = CATransform3DTranslate(normalForm, CGRectGetWidth(self.bounds)*0.25, 0.0, 0.0);
    }
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    [CATransaction setCompletionBlock:^{
        normalForm = CATransform3DIdentity;
        normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
        transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
        [CATransaction setAnimationDuration:0.25];
        [CATransaction setCompletionBlock:^{
            [self.layer removeAllAnimations];
        }];
        [CATransaction begin];
        
        [self.layer addAnimation:transformAnimation forKey:@"animationTransitionBack"];
        
        [CATransaction commit];
        
    }];
    [CATransaction begin];
    
    [self.layer addAnimation:transformAnimation forKey:@"animationTransitionForward"];
    
    [CATransaction commit];
}

#pragma mark Gesture Recognizer Handler
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    __block NSInteger dayIndex = 0;
    __block BOOL found = NO;
    [_arrayOfCenterPoints enumerateObjectsUsingBlock:^(NSValue *vPoint, NSUInteger idx, BOOL *stop) {
        CGPoint center = [vPoint CGPointValue];
        CGRect rectOfDay = CGRectMake(center.x - _dayRadius, self.bounds.origin.y, _dayRadius*2, CGRectGetHeight(self.bounds));
        if (CGRectContainsPoint(rectOfDay, point)) {
            found = YES;
            dayIndex = idx;
            *stop = YES;
        }
    }];
    
    if (found) {
        self.currentDate = (NSDate *)[[self.week objectAtIndex:dayIndex] valueForKey:@"date"];
        [self.delegate weekGraphView:self changedCurrentDate:self.currentDate];
    }
}

- (void)panGestureHandler:(UIPanGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _startPoint = point;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_startPoint.x < point.x) {
                NSArray *source = [self.dataSource weekGraphView:self willChangeWeekForward:NO];
                if (source) {
                    [self animateChangeToNext:NO];
                    self.week = source;
                } else {
                    [self animateStopOfChangeToNext:NO];
                }
            } else {
                NSArray *source = [self.dataSource weekGraphView:self willChangeWeekForward:YES];
                if (source) {
                    [self animateChangeToNext:YES];
                    self.week = source;
                } else {
                    [self animateStopOfChangeToNext:YES];
                }
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Public Methods


#pragma mark - Property Accessors
- (void)setWeek:(NSArray *)week
{
    __block BOOL objectDoesntConfirmProtocol = NO;
    [week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj conformsToProtocol:@protocol(SNWeekGraphProtocol)]) {
            objectDoesntConfirmProtocol = YES;
            *stop = YES;
        }
    }];
    
    if (objectDoesntConfirmProtocol) {
        NSLog(@"\n%s\nOne or more objects doesn`t confirm to protocol!\n",__FUNCTION__);
        return;
    }
    _week = week;
    [self.delegate weekGraphView:self didChangeWeek:self.week];
    [self setNeedsDisplay];
}

- (void)setColorBkgRing:(UIColor *)colorBkgRing
{
    _colorBkgRing = colorBkgRing;
    [self setNeedsDisplay];
}

- (void)setColorCaloriesMax:(UIColor *)colorCaloriesMax
{
    _colorCaloriesMax = colorCaloriesMax;
    [self setNeedsDisplay];
}

- (void)setColorCaloriesMin:(UIColor *)colorCaloriesMin
{
    _colorCaloriesMin = colorCaloriesMin;
    [self setNeedsDisplay];
}

- (void)setColorCurrentDayText:(UIColor *)colorCurrentDayText
{
    _colorCurrentDayText = colorCurrentDayText;
    [self setNeedsDisplay];
}

- (void)setColorDayCircle:(UIColor *)colorDayCircle
{
    _colorDayCircle = colorDayCircle;
    [self setNeedsDisplay];
}

- (void)setColorText:(UIColor *)colorText
{
    _colorText = colorText;
    [self setNeedsDisplay];
}

- (void)setMinimumAmount:(NSInteger)minimumAmount
{
    _minimumAmount = minimumAmount;
    [self setNeedsDisplay];
}

- (void)setTextFont:(UIFont *)textFont
{
    _textFont = textFont;
    [self setNeedsDisplay];
}

- (void)setMaxAmount:(NSInteger)maxAmount
{
    if (maxAmount) {
        _maxAmount = maxAmount;
        [self setNeedsDisplay];
    }
}

- (void)setCurrentDate:(NSDate *)currentDate
{
    if (currentDate) {
        _currentDate = currentDate;
    }
    [self setNeedsDisplay];
}

@end
