

#import "AnimatedLabel.h"

#import "SNAppearance.h"

@interface AnimatedLabel()
{
    NSTimer *_timer;
    NSTimeInterval _timePassed;
}
@property (strong, nonatomic) AnimationLabelCompletitionBlock completitionBlock;

@end

@implementation AnimatedLabel

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    
}


#pragma mark - Public Methods
- (void)animateTextFromArray:(NSArray *)array withDuration:(NSTimeInterval)duration
{
    if (_timer) {
        [self startTimer:NO];
    }
    _arrayOfPointsValues = array;
    _animationDuration = duration;
    [self startTimer:YES];
}

- (void)animateTextFromMinValue:(NSInteger)minValue
                     toMaxValue:(NSInteger)maxValue
                     withPrefix:(NSString *)prefix
                         suffix:(NSString *)suffix
                      maxPrefix:(NSString *)maxPrefix
                      maxSuffix:(NSString *)maxSuffix
                       duration:(NSTimeInterval)duration
                       andSteps:(NSInteger)stepsAmount;
{
    [self animateTextFromMinValue:minValue
                       toMaxValue:maxValue
                       withPrefix:prefix
                           suffix:suffix
                        maxPrefix:maxPrefix
                        maxSuffix:maxSuffix
                         duration:duration
                         andSteps:stepsAmount
                  andCompletition:nil];
}

- (void)animateTextFromMinValue:(NSInteger)minValue
                     toMaxValue:(NSInteger)maxValue
                     withPrefix:(NSString *)prefix
                         suffix:(NSString *)suffix
                      maxPrefix:(NSString *)maxPrefix
                      maxSuffix:(NSString *)maxSuffix
                       duration:(NSTimeInterval)duration
                       andSteps:(NSInteger)stepsAmount
                andCompletition:(AnimationLabelCompletitionBlock)completitionBlock
{
    NSInteger startValue = minValue, endValue = maxValue;
    NSInteger midValuesAmount = stepsAmount;
    CGFloat valueStep = (CGFloat)(endValue - startValue) / (CGFloat)midValuesAmount;
    NSMutableArray *mArrayOfValues = [NSMutableArray new];
    
    if (!prefix) {
        prefix = @"";
    }
    if (!suffix) {
        suffix = @"";
    }
    
    if (!maxPrefix) {
        maxPrefix = prefix;
    }
    if (!maxSuffix) {
        maxSuffix = suffix;
    }
    
    for (int i=1; i<midValuesAmount+1; i++) {
        NSString *amountString;
        NSInteger value = startValue + valueStep*i;
        NSString *formattedValue = [SNAppearance textWithThousendsFormat:value]; //[AnimatedLabel textWithThousendsFormat:value];
        if (i == midValuesAmount) {
            amountString = [NSString stringWithFormat:@"%@%@%@", maxPrefix, formattedValue, maxSuffix];
        } else {
            amountString = [NSString stringWithFormat:@"%@%@%@", prefix, formattedValue, suffix];
        }
        [mArrayOfValues addObject:amountString];
    }
    if (completitionBlock) {
        _completitionBlock = completitionBlock;
    }
    
    [self animateTextFromArray:mArrayOfValues withDuration:duration];
}

+ (NSString *)textWithThousendsFormat:(NSInteger)amount
{
    NSInteger thousends = 0, rest = 0;
    thousends = (amount - amount%1000) / 1000;
    rest = amount%1000;
    NSString *sThousends = @"";
    if (thousends) {
        sThousends = [NSString stringWithFormat:@"%zd",thousends];
    }
    NSString *sRest = @"0";
    if (rest) {
        sRest = [NSString stringWithFormat:@"%zd",rest];
    } else {
        if (thousends) {
            sRest = @"000";
        }
    }
    if (thousends) {
        if (sRest.length == 1) {
            sRest = [NSString stringWithFormat:@"00%@",sRest];
        }
        if (sRest.length == 2) {
            sRest = [NSString stringWithFormat:@"0%@",sRest];
        }
    }
    
    NSString *stringResult = [NSString stringWithFormat:@"%@%@",(sThousends.length) ? [NSString stringWithFormat:@"%@.",sThousends]:sThousends,sRest];
    return stringResult;
}

- (void)stopTimer
{
    [self startTimer:NO];
}

#pragma mark - Timer Methods
- (void)startTimer:(BOOL)start
{
    NSTimeInterval duration = _animationDuration / _arrayOfPointsValues.count;
    if (start) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:duration
                                                  target:self
                                                selector:@selector(timerFired)
                                                userInfo:nil
                                                 repeats:YES];
    } else {
        _timePassed = 0.0f;
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)timerFired
{
    NSTimeInterval duration = _animationDuration / _arrayOfPointsValues.count;
    if (_timePassed >= duration * _arrayOfPointsValues.count) {
        [self startTimer:NO];
        if (self.completitionBlock) {
            _completitionBlock();
        }
        return;
    }
    _timePassed += duration;
    self.text = [_arrayOfPointsValues objectAtIndex:_timePassed / duration - 1];
}


@end
