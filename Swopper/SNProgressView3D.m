
#import "SNProgressView3D.h"

#define DEGREES_TO_RADIANS(angle) (((angle)* M_PI) / 180.0 )

@interface SNProgressView3D ()
{
    CAShapeLayer *_frameLayer, *_progressLayer, *_pointLayer;
    CALayer *_mainLayer;
    CGFloat _sideLength;
    CGFloat _previousPoint;
    CGFloat _progress;
    CGFloat _destinationPoint;
    
    CALayer *_bronzeLayer, *_silverLayer, *_goldenLayer;
    
    NSTimer *_timer;
    NSTimeInterval _timerDuration, _timerInterval, _timeElapsed;
}

@end

@implementation SNProgressView3D

#pragma mark - Custom Types
typedef struct {
	CGPoint startPoint, endPoint;
} CustomLine;

#pragma mark - Instance Methods
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

#pragma mark - View Methods

- (void)layoutSubviews
{
    [super layoutSubviews];
    _sideLength = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    if (_mainLayer) {
        [_mainLayer removeFromSuperlayer];
    }
    _mainLayer = [self mainLayer];
    if (!self.dontTransform) {
        [self transformLayer:_mainLayer];        
    }
    
    [self.layer addSublayer:_mainLayer];
}

- (void)initialize
{
    _controlType = SNProgressView3DTypeNormal;
    self.backgroundColor = [UIColor clearColor];
    _previousPoint = 0;
    _currentPoint = 0;
    _maxPoints = 10;
    _pathColor = [UIColor blueColor];
    _progressColor = [UIColor orangeColor];
    _lineWidth = 3.0;
    _sideLength = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    _showProgressPoint = YES;
}

- (CGPoint)pointFromAngle:(float)angle
{
    CGPoint centerPoint = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
    CGPoint result;
    CGFloat radius = _sideLength/2;
    result.x = centerPoint.x + (radius - _lineWidth) * cos([self toRad:(angle) withMax:360]);
    result.y = centerPoint.y + (radius - _lineWidth) * sin([self toRad:(angle) withMax:360]);
    return result;
}

- (float)toRad:(float)deg  withMax:(float)max{
    return ( (M_PI * (deg)) / (max/2) );
}

#pragma mark Timer Methods
- (void)timerHandler
{
    _timeElapsed += _timerInterval;
    if (_timeElapsed >= _timerDuration) {
        [self stopProgressingTimer];
        if (self.delegate) {
            [self.delegate progressView:self timerActive:NO];
        }
    } else {
        [self setCurrentPoint:_timeElapsed];
        if (self.delegate) {
            [self.delegate progressView:self timerActive:YES];
        }
    }
}


- (void)animationTimerHandler:(NSTimer *)sender
{
    _timeElapsed += _timerInterval;
    if (_timeElapsed >= _timerDuration) {
        [self stopProgressingTimer];
    }
    CGFloat additionTimes = _timerDuration / _timerInterval;
    CGFloat valueToAdd = _destinationPoint / additionTimes;
    self.currentPoint += valueToAdd;
}

#pragma mark - Layers Creation
- (CALayer *)mainLayer
{
    CALayer *mainLayer = [CALayer layer];
    mainLayer = [CALayer layer];
    mainLayer.bounds = self.bounds;
    mainLayer.frame = self.bounds;
    
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    CGRect workingRect = mainLayer.bounds;
    
    UIGraphicsBeginImageContext(workingRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGLayerRef layer = CGLayerCreateWithContext(context, workingRect.size, NULL);
    if (layer) {
        CGContextRef layerContext = CGLayerGetContext(layer);
        
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextSetLineWidth(layerContext, _lineWidth);
        
        CGFloat radius = _sideLength/2 - _lineWidth;
        
        // path
        CGFloat startAngle = - 90.0f;
        UIBezierPath *bkgPath = [UIBezierPath bezierPathWithArcCenter:center
                                                               radius:radius
                                                           startAngle:DEGREES_TO_RADIANS(startAngle)
                                                             endAngle:DEGREES_TO_RADIANS(360.0f)
                                                            clockwise:YES];
        
        CGContextAddPath(layerContext, bkgPath.CGPath);
        CGContextSetStrokeColorWithColor(layerContext, _pathColor.CGColor);
        CGContextStrokePath(layerContext);
        
        // progress
        CGFloat endAngle = 360.0f * _progress + startAngle;
        UIBezierPath *progressPath = [UIBezierPath bezierPathWithArcCenter:center
                                                                    radius:radius
                                                                startAngle:DEGREES_TO_RADIANS(startAngle)
                                                                  endAngle:DEGREES_TO_RADIANS(endAngle)
                                                                 clockwise:YES];
        
        CGContextSetStrokeColorWithColor(layerContext, _progressColor.CGColor);
        CGContextAddPath(layerContext, progressPath.CGPath);
        CGContextStrokePath(layerContext);
        
        // step line drawing block
        void(^DrawStepLine)(CGFloat, UIColor*, UIColor*, CGFloat, BOOL) = ^(CGFloat value, UIColor *strokeColor, UIColor *fillColor, CGFloat width, BOOL isCentered) {
            CGFloat stepProgress = value / self.maxPoints;
            CGFloat endAngle = 360.0f * stepProgress + startAngle;
            UIBezierPath *stepLine;
            if (isCentered) {
                stepLine = [UIBezierPath bezierPathWithArcCenter:center
                                                          radius:radius
                                                      startAngle:DEGREES_TO_RADIANS(endAngle - width/2)
                                                        endAngle:DEGREES_TO_RADIANS(endAngle + width/2)
                                                       clockwise:YES];
            } else {
                stepLine = [UIBezierPath bezierPathWithArcCenter:center
                                                          radius:radius
                                                      startAngle:DEGREES_TO_RADIANS(endAngle - width)
                                                        endAngle:DEGREES_TO_RADIANS(endAngle)
                                                       clockwise:YES];
            }
            CGContextSetStrokeColorWithColor(layerContext, strokeColor.CGColor);
            CGContextSetFillColorWithColor(layerContext, fillColor.CGColor);
            CGContextAddPath(layerContext, stepLine.CGPath);
            CGContextDrawPath(layerContext, kCGPathFillStroke);
        };
        
        // progress point
        if (self.controlType != SNProgressView3DTypeSteps) {
            if (self.showProgressPoint) {
                CGPoint pointOnView = [self pointFromAngle:endAngle];
                CGRect pointFrame = CGRectMake(pointOnView.x - _lineWidth/2, pointOnView.y - _lineWidth/2, _lineWidth, _lineWidth);
                UIBezierPath *pointPath = [UIBezierPath bezierPathWithOvalInRect:pointFrame];
                
                CGContextSetStrokeColorWithColor(layerContext, _progressColor.CGColor);
                CGContextSetFillColorWithColor(layerContext, _progressColor.CGColor);
                CGContextAddPath(layerContext, pointPath.CGPath);
                CGContextDrawPath(layerContext, kCGPathFillStroke);
            }
        } else {
            DrawStepLine(_currentPoint, [UIColor whiteColor], [UIColor whiteColor], 3.0f, NO);
        }
        
        // steps drawing
        if (self.controlType == SNProgressView3DTypeSteps) {
            if (self.bronzeStep) {
                DrawStepLine(self.bronzeStep, [UIColor whiteColor], [UIColor whiteColor], 1.0f, YES);
            }
            if (self.silverStep) {
                DrawStepLine(self.silverStep, [UIColor whiteColor], [UIColor whiteColor], 1.0f, YES);
            }
            if (self.goldenStep) {
                DrawStepLine(self.goldenStep, [UIColor whiteColor], [UIColor whiteColor], 1.0f, YES);
            }
        }
        
        // end drawing
        CGContextDrawLayerInRect(context, workingRect, layer);
        
        CFRelease(layer);
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:workingRect];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [mainLayer addSublayer:imageView.layer];
    if (self.controlType == SNProgressView3DTypeSteps) {
        if (self.bronzeStep) {
            _bronzeLayer = [self stepLayerWithValue:self.bronzeStep doneIconName:@"Icon-Step-CheckMark-Done" iconName:@"Icon-Step-CheckMark"];
            [mainLayer addSublayer:_bronzeLayer];
        }
        if (self.silverStep) {
            _silverLayer = [self stepLayerWithValue:self.silverStep doneIconName:@"Icon-Step-CheckMark-Done" iconName:@"Icon-Step-CheckMark"];
            [mainLayer addSublayer:_silverLayer];
        }
        if (self.goldenStep) {
            _goldenLayer = [self stepLayerWithValue:self.goldenStep doneIconName:@"Icon-Step-Cup-Done" iconName:@"Icon-Step-Cup"];
            [mainLayer addSublayer:_goldenLayer];
        }
    }
    
    return mainLayer;
}

- (CALayer *)stepLayerWithValue:(NSInteger)value doneIconName:(NSString *)doneIconName iconName:(NSString *)iconName
{
    CALayer *layer = [CALayer layer];
    
    layer.frame = CGRectMake(0, 0, 16, 16);
    layer.bounds = CGRectMake(0, 0, 16, 16);
    
    CGPoint (^GetPointFromPointAngleLength)(CGPoint, CGFloat, CGFloat) = ^(CGPoint point, CGFloat angle, CGFloat length) {
        CGPoint centerPoint = point;
        CGPoint result;
        CGFloat eps = 0.00001;
        result.x = centerPoint.x + length * cosf(DEGREES_TO_RADIANS(angle));
        result.y = centerPoint.y + length * sinf(DEGREES_TO_RADIANS(angle));
        if (result.x < eps) {
            result.x = 0;
        }
        if (result.y < eps) {
            result.y = 0;
        }
        return result;
    };
    UIImage *icon;
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    CGFloat radius = _sideLength/2 - _lineWidth/2;
    CGFloat startAngle = -90;
    CGFloat stepProgress = (CGFloat)value / (CGFloat)self.maxPoints;
    CGFloat endAngle = 360.0f * stepProgress + startAngle;
    if (self.currentPoint >= value) {
        icon = [UIImage imageNamed:doneIconName];
    } else {
        icon = [UIImage imageNamed:iconName];
    }
    CGPoint point = GetPointFromPointAngleLength(center,endAngle,radius + icon.size.height);
    UIImageView *imageView = [[UIImageView alloc] initWithImage:icon];
    [layer addSublayer:imageView.layer];
    
    [layer setPosition:point];
    
    return layer;
}


#pragma mark - Transformation Method
- (void)transformLayer:(CALayer *)layer
{
    layer.transform = [self transform3D];
}

- (CATransform3D)transform3D
{
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -200.0;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, - M_PI / 8, 0.0f, 1.0f, 0.0f);
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI * 0.3f , 1.0f, 0.0f, 0.0f);
    return rotationAndPerspectiveTransform;
}


#pragma mark - Public methods
- (void)configureProgressViewWithMaxPoints:(CGFloat)maxPoints
                              currentPoint:(CGFloat)currentPoint
                                 pathColor:(UIColor *)pathColor
                             progressColor:(UIColor *)progressColor
                                 lineWidth:(CGFloat)lineWidth
{
    _maxPoints = maxPoints;
    if (currentPoint >= maxPoints) {
        _currentPoint = maxPoints;
    }
    _currentPoint = currentPoint;
    _previousPoint = currentPoint;
    _progress = ((CGFloat)self.currentPoint / (CGFloat)self.maxPoints);
    _pathColor = pathColor;
    _progressColor = progressColor;
    _lineWidth = lineWidth;
    
    [self layoutSubviews];
}

- (void)startProgressingTimerWithDuration:(NSTimeInterval)duration
{
    _timerDuration = duration;
    _timeElapsed = 0.0f;
    _timerInterval = 0.1f;
    [self setMaxPoints:_timerDuration];
    [self setCurrentPoint:_timeElapsed];
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval
                                              target:self
                                            selector:@selector(timerHandler)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)stopProgressingTimer
{
    [_timer invalidate];
    _timer = nil;
}

- (void)pauseProgressingTimer
{
    [_timer invalidate];
    _timer = nil;
    if (self.delegate) {
        [self.delegate progressView:self paused:YES];
    }
}

- (void)continueProgressingTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval
                                              target:self
                                            selector:@selector(timerHandler)
                                            userInfo:nil
                                             repeats:YES];
    if (self.delegate) {
        [self.delegate progressView:self paused:NO];
    }
}

- (void)setProgressAtPoint:(NSTimeInterval)point
{
    [self stopProgressingTimer];
    [self startProgressingTimerWithDuration:_timerDuration];
    _timeElapsed = point;
}

- (BOOL)timerIsInProgress
{
    return (_timer) ? YES : NO;
}

- (void)animateToValue:(CGFloat)currentValue
               withMax:(CGFloat)maxValue
          withDuration:(CGFloat)duration
{
    if ([self timerIsInProgress]) {
        [self stopProgressingTimer];
    }
    self.currentPoint = 0;
    self.maxPoints = maxValue;
    _timerInterval = 0.1f;
    _destinationPoint = currentValue;
    _timeElapsed = 0.0f;
    _timerDuration = duration;
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval
                                              target:self
                                            selector:@selector(animationTimerHandler:)
                                            userInfo:nil
                                             repeats:YES];
}


#pragma mark - Property Accessors
- (void)setMaxPoints:(CGFloat)maxPoints
{
    _maxPoints = maxPoints;
    _progress = self.currentPoint / self.maxPoints;
    [self layoutSubviews];
}

- (void)setCurrentPoint:(CGFloat)currentPoint
{
    if (_maxPoints != 0 && currentPoint > _maxPoints) {
        currentPoint = _maxPoints;
    }
    if (currentPoint < 0) {
        currentPoint = 0;
    }
    _previousPoint = _currentPoint;
    _currentPoint = currentPoint;
    _progress = self.currentPoint / self.maxPoints;
    [self layoutSubviews];
}

- (void)setPathColor:(UIColor *)pathColor
{
    _pathColor = pathColor;
    _frameLayer = nil;
    [self layoutSubviews];
}

- (void)setProgressColor:(UIColor *)progressColor
{
    _progressColor = progressColor;
    _progressLayer = nil;
    [self layoutSubviews];
}

- (void)setLineWidth:(CGFloat)lineWidth
{
    _lineWidth = lineWidth;
    _frameLayer = nil;
    _progressLayer = nil;
    [self layoutSubviews];
}

- (void)setControlType:(SNProgressView3DType)controlType
{
    _controlType = controlType;
    [self layoutSubviews];
}

- (void)setGoldenStep:(NSInteger)goldenStep
{
    if (goldenStep > _maxPoints) {
        goldenStep = _maxPoints;
    }
    _goldenStep = goldenStep;
    [self layoutSubviews];
}

- (void)setSilverStep:(NSInteger)silverStep
{
    if (silverStep > _maxPoints) {
        silverStep = _maxPoints;
    }
    _silverStep = silverStep;
    [self layoutSubviews];
}

- (void)setBronzeStep:(NSInteger)bronzeStep
{
    if (bronzeStep > _maxPoints) {
        bronzeStep = _maxPoints;
    }
    _bronzeStep = bronzeStep;
    [self layoutSubviews];
}

- (void)setShowProgressPoint:(BOOL)showProgressPoint
{
    _showProgressPoint = showProgressPoint;
    [self layoutSubviews];
}

- (void)setDontTransform:(BOOL)dontTransform
{
    _dontTransform = dontTransform;
    [self layoutSubviews];
}

@end
