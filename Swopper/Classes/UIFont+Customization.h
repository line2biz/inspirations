

#import <UIKit/UIKit.h>

@interface UIFont (Customization)

+ (UIFont *)robotoRegularWithSize:(CGFloat)size;

+ (UIFont *)franklinGothicStdBookCondesedWithSize:(CGFloat)size;
+ (UIFont *)franklinGothicStdBookCondesedMedWithSize:(CGFloat)size;
+ (UIFont *)franklinGothicStdBookMedWithSize:(CGFloat)size;

@end
