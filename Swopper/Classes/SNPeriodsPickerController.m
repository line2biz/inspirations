
#import "SNPeriodsPickerController.h"
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "DMUser.h"

@interface SNPeriodsPickerController () <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSInteger _loadIdx;
    CGFloat _startCurveAlpha;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (assign, nonatomic) SNPeriodLength periodLength;
@property (strong, nonatomic) DMUser *user;
@property (assign, nonatomic) BOOL infoIsSaved;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation SNPeriodsPickerController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadIdx = 0;
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Speichern", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    
    self.infoIsSaved = NO;
    [SNAppearance customizeViewController:self];
    
    [self.nextButton addTarget:self
                        action:@selector(saveButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self controllerStart];
    [self controllerLoaded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.infoIsSaved) {
        [self.delegate periodPickerController:self withPeriodType:self.periodsType chosenPeriod:self.periodLength];
    }
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)controllerLoaded
{
    self.user = [DMUser defaultUser];
    switch (self.periodsType) {
        case SNPeriodsTypeHours:
        {
            [self.pickerView selectRow:self.user.hourPeriod inComponent:0 animated:NO];
        }
            break;
        case SNPeriodsTypeDays:
        {
            [self.pickerView selectRow:self.user.dayPeriod inComponent:0 animated:NO];
        }
            break;
        case SNPeriodsTypeWeek:
        {
            [self.pickerView selectRow:self.user.weekPeriod inComponent:0 animated:NO];
        }
            break;
            
        default:
            break;
    }
}

- (void)saveButtonTapped
{
    self.infoIsSaved = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Public Methods
+ (NSString *)titleForLength:(SNPeriodLength)length
{
    switch (length) {
        case SNPeriodLengthHoursNever:
            return LMLocalizedString(@"nie", nil);
            break;
        case SNPeriodLengthHoursOne:
            return LMLocalizedString(@"jede 1 Std", nil);
            break;
        case SNPeriodLengthHoursTwo:
            return LMLocalizedString(@"alle 2 Std", nil);
            break;
        case SNPeriodLengthHoursThree:
            return LMLocalizedString(@"alle 3 Std", nil);
            break;
        case SNPeriodLengthHoursFour:
            return LMLocalizedString(@"alle 4 Std", nil);
            break;
        case SNPeriodLengthHoursFive:
            return LMLocalizedString(@"alle 5 Std", nil);
            break;
        case SNPeriodLengthHoursSix:
            return LMLocalizedString(@"alle 6 Std", nil);
            break;
        case SNPeriodLengthHoursSeven:
            return LMLocalizedString(@"alle 7 Std", nil);
            break;
        case SNPeriodLengthHoursEight:
            return LMLocalizedString(@"alle 8 Std", nil);
            break;
        case SNPeriodLengthDaysNever:
            return LMLocalizedString(@"nie", nil);
            break;
        case SNPeriodLengthDaysEveryOne:
            return LMLocalizedString(@"jeden Tag", nil);
            break;
        case SNPeriodLengthDaysEveryTwo:
            return LMLocalizedString(@"alle 2 Tage", nil);
            break;
        case SNPeriodLengthDaysEveryThree:
            return LMLocalizedString(@"alle 3 Tage", nil);
            break;
        case SNPeriodLengthDaysEveryFour:
            return LMLocalizedString(@"alle 4 Tage", nil);
            break;
        case SNPeriodLengthWeekNever:
            return LMLocalizedString(@"nie", nil);
            break;
        case SNPeriodLengthWeekWorkdays:
            return LMLocalizedString(@"an Werktagen", nil);
            break;
        case SNPeriodLengthWeekHolidays:
            return LMLocalizedString(@"an Wochenenden", nil);
            break;
        case SNPeriodLengthWeekAll:
            return LMLocalizedString(@"die ganze Woche", nil);
            break;
            
        default:
            break;
    }
}

+ (NSInteger)periodForLength:(SNPeriodLength)length
{
    switch (length) {
        case SNPeriodLengthHoursNever:
            return 0;
            break;
        case SNPeriodLengthHoursOne:
            return 1;
            break;
        case SNPeriodLengthHoursTwo:
            return 2;
            break;
        case SNPeriodLengthHoursThree:
            return 3;
            break;
        case SNPeriodLengthHoursFour:
            return 4;
            break;
        case SNPeriodLengthHoursFive:
            return 5;
            break;
        case SNPeriodLengthHoursSix:
            return 6;
            break;
        case SNPeriodLengthHoursSeven:
            return 7;
            break;
        case SNPeriodLengthHoursEight:
            return 8;
            break;
        case SNPeriodLengthDaysNever:
            return 0;
            break;
        case SNPeriodLengthDaysEveryOne:
            return 1;
            break;
        case SNPeriodLengthDaysEveryTwo:
            return 2;
            break;
        case SNPeriodLengthDaysEveryThree:
            return 3;
            break;
        case SNPeriodLengthDaysEveryFour:
            return 4;
            break;
        case SNPeriodLengthWeekNever:
            return 0;
            break;
        case SNPeriodLengthWeekWorkdays:
            return 1;
            break;
        case SNPeriodLengthWeekHolidays:
            return 2;
            break;
        case SNPeriodLengthWeekAll:
            return 3;
            break;
            
        default:
            break;
    }
}

#pragma mark - Animation
- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}



#pragma mark - Picker View Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (self.periodsType) {
        case SNPeriodsTypeDays:
        {
            return 5;
        }
            break;
        case SNPeriodsTypeHours:
        {
            return 9;
        }
            break;
        case SNPeriodsTypeWeek:
        {
            return 4;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Picker View Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (self.periodsType) {
        case SNPeriodsTypeDays:
        {
            return [SNPeriodsPickerController titleForLength:row + 9];
        }
            break;
        case SNPeriodsTypeHours:
        {
            return [SNPeriodsPickerController titleForLength:row];
        }
            break;
        case SNPeriodsTypeWeek:
        {
            return [SNPeriodsPickerController titleForLength:row + 14];
        }
            break;
            
        default:
            break;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (self.periodsType) {
        case SNPeriodsTypeDays:
        {
            self.periodLength = row + 9;
        }
            break;
        case SNPeriodsTypeHours:
        {
            self.periodLength = row;
        }
            break;
        case SNPeriodsTypeWeek:
        {
            self.periodLength = row + 14;
        }
            break;
            
        default:
            break;
    }
}

@end
