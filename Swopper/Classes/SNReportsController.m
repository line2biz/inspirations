
#import "SNReportsController.h"

#import "SNAppearance.h"
#import "SNReportCell.h"
#import "SNSessionCell.h"
#import "DMReport+Category.h"
#import "DMSession+Category.h"
#import "DMCalorie.h"
#import "SNReportController.h"
#import "SNReportTemp.h"



@interface SNReportsController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *items;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (strong, nonatomic) NSArray *productImages;

@end

@implementation SNReportsController

- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    [SNAppearance customizeViewController:self];
    
    if (!self.managedObjectContext) {
        self.managedObjectContext  = [[DMManager sharedManager] defaultContext];
    }
    
    self.navigationItem.title = LMLocalizedString(@"Hall Of Fame", nil);
    
    
    
    // parse file
    NSString* fileRoot = [[SNLocalizationManager sharedManager] pathForResource:@"Callories" ofType:@"tsv"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    for (NSInteger i=0; i<allLinedStrings.count; i++) {
        NSString *line = [allLinedStrings objectAtIndex:i];
        NSArray *arrayOfSubstrings = [line componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\t"]];
        
        
        if ([arrayOfSubstrings count] >= 4) {
            NSDictionary *dict = @{@"title":arrayOfSubstrings[0], @"callories":arrayOfSubstrings[1], @"result":arrayOfSubstrings[2], @"desc":arrayOfSubstrings[3]};
            [mArrayOfDicts addObject:dict];
        }
    }
    
    
    
    self.productImages = mArrayOfDicts;
    [self loadData];
//
//    NSMutableArray *array = [NSMutableArray new];
//    for (NSDictionary *dict in mArrayOfDicts) {
//        [array addObject:[dict valueForKey:@"callories"]];
//    }
    
//    self.productImages = [mArrayOfDicts valueForKeyPath:@"callories"];
    
////    self.productImages 
//    
//    __block NSInteger indexOfProduct = 0;
//    CGFloat caloriesFromSession = [self.session caloriesPerSession];
//    [mArrayOfDicts enumerateObjectsUsingBlock:^(NSDictionary *product, NSUInteger idx, BOOL *stop) {
//        NSInteger productCallories = [product[@"callories"] integerValue];
//        if (caloriesFromSession >= productCallories) {
//            indexOfProduct = idx;
//        }
//    }];
//    
//    NSDictionary *productDict = [mArrayOfDicts objectAtIndex:indexOfProduct];
//    NSString *productDescription = productDict[@"desc"];
//    BOOL result = ([productDict[@"result"] isEqualToString:@"Positiv"]) ? YES : NO;
//    
//   
//    
//    UIImage *productImage = [UIImage imageNamed:[NSString stringWithFormat:@"%zd",[productDict[@"callories"] integerValue]]];
//    if (productImage) {
//        self.imageViewProduct.image = productImage;
//    }
}

#pragma mark - Private Methods
- (void)loadData {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMReport class])];
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"SELF != %@", self.currentReport];
    
    NSError *error = nil;
    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"%s Error fetching data: %@", __FUNCTION__, error);
        return;
    }
    
    
    NSMutableArray *array = [NSMutableArray new];
    for (DMReport *report in results) {
        SNReportTemp *temp = [SNReportTemp new];
        temp.report = report;
        
        double avgCountsPerMinute = 0;
        for (DMSession *session in report.sessions) {
            avgCountsPerMinute += [session countsPerMinute];
        }
        if ([report.sessions count]) {
            avgCountsPerMinute = avgCountsPerMinute / report.sessions.count;
        }
        
        temp.avgCMPValue = (NSInteger)round(avgCountsPerMinute);
        
        [array addObject:temp];
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"avgCMPValue" ascending:NO];
    NSArray *sortedArray = [array sortedArrayUsingDescriptors:@[sort]];
    
    if ([sortedArray count] > 10) {
        sortedArray = [sortedArray subarrayWithRange:NSMakeRange(0, 10)];
    }
    
    if (self.currentReport) {
        SNReportTemp *temp = [SNReportTemp new];
        temp.report = self.currentReport;
        
        double avgCountsPerMinute = 0;
        for (DMSession *session in self.currentReport.sessions) {
            avgCountsPerMinute += [session countsPerMinute];
        }
        if ([self.currentReport.sessions count]) {
            avgCountsPerMinute = avgCountsPerMinute / self.currentReport.sessions.count;
        }
        
        temp.avgCMPValue = (NSInteger)round(avgCountsPerMinute);
        
        array = [NSMutableArray arrayWithArray:sortedArray];
        [array addObject:temp];
    }
    
    sortedArray = [array sortedArrayUsingDescriptors:@[sort]];
    self.items = sortedArray;
    
    [self.tableView reloadData];
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    SNReportTemp *temp = self.items[indexPath.row];
    
    [segue.destinationViewController setManagedObjectContext:self.managedObjectContext];
    [segue.destinationViewController setReport:temp.report];
}


#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}


- (void)configureCell:(SNReportCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
//    NSIndexPath *newIndex = [NSIndexPath indexPathForRow:indexPath.section inSection:0];
//    DMReport *report = [self.fetchedResultsController objectAtIndexPath:newIndex];
//    
//    unsigned int seconds = (unsigned int)round([report.interval doubleValue]);
//    NSString *string = [NSString stringWithFormat:@"%02u:%02u", seconds / 3600, (seconds / 60) % 60];
//    
//    
//    NSInteger value = 0;
//    for (DMSession *session in report.sessions) {
//        value += [session caloriesPerSession] / session.sessionDuration.floatValue * report.interval.floatValue;
//    }
//    value = value / report.sessions.count;
//    
//    NSString *title = nil;
//    UIImage *image = nil;
//    
//    for (NSInteger idx = 0; idx < [self.productImages count]; idx++) {
//        NSDictionary *dict = self.productImages[idx];
//        if (value >= [dict[@"callories"] integerValue]) {
//            title = dict[@"desc"];
//            image = [UIImage imageNamed:[NSString stringWithFormat:@"%zd", [dict[@"callories"] integerValue]]];
//        }
//    }
//    
//    cell.titleLabel.text = title;
//    if (!image) {
//        image = [UIImage imageNamed:@"Negativ"];
//    }
//    cell.avatarView.image = image;
//    
//    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"dd MM yyyy" options:0 locale:[SNAppDelegate currentLocale]];
//    NSDateFormatter *formatter = [NSDateFormatter new];
//    [formatter setDateFormat:format];
//    cell.dateLabel.text = [formatter stringFromDate:report.date];
//    cell.detailLabel.text = [LMLocalizedString(@"Tageswert", nil) stringByAppendingFormat:@": %zd kcal / %@", value, string];
//    
//    
//    UIButton *button = (UIButton *)cell.accessoryView;
//    NSSet *allTargets = [button allTargets];
//    if (![allTargets containsObject:self]) {
//        [button addTarget:self action:@selector(didTapButtonInfo:) forControlEvents:UIControlEventTouchUpInside];
//    } else {
//        NSLog(@"WE ARE HERE");
//    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SNReportCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    SNReportTemp *temp = self.items[indexPath.row];
    
    NSString *title = nil;
    if (temp.avgCMPValue == 0) {
        title = @"";
    }
    else if (temp.avgCMPValue == 1 ) {
        title = LMLocalizedString(@"1 Move pro Minute", nil);
    }
    else {
        title = [NSString stringWithFormat:@"%zd %@", temp.avgCMPValue, LMLocalizedString(@"Moves pro Minute", nil)];
    }
    
    cell.titleLabel.text = title;
    
    
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"dd MM yyyy" options:0 locale:[SNAppDelegate currentLocale]];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    cell.dateLabel.text = [formatter stringFromDate:temp.report.date];

    NSString *subtitle = nil;
    if ([temp.report.sessions count] == 1) {
        subtitle = LMLocalizedString(@"1. Messung", nil);
    } else {
        subtitle = [NSString stringWithFormat:@"%zd. %@", [temp.report.sessions count], LMLocalizedString(@"Messung", nil)];
    }
    
    cell.detailLabel.text = subtitle;
    cell.sortOrderLabel.text = [NSString stringWithFormat:@"%zd", (indexPath.row + 1)];
    cell.active = (self.currentReport == temp.report);
    
    return cell;
}


@end
