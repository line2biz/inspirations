

#import <UIKit/UIKit.h>

@interface SNGradientView : UIView

// Storyboard user defined attributes
@property (strong, nonatomic) UIColor *startColor;
@property (strong, nonatomic) UIColor *endColor;

@end
