
#import <UIKit/UIKit.h>

@class DMReport, DMSession;

@interface SNTimeIntervalController : UIViewController

@property (strong, nonatomic) DMSession *session;
@property (strong, nonatomic) DMReport *report;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
