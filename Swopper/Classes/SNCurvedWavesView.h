

#import <UIKit/UIKit.h>

@interface SNCurvedWavesView : UIView

@property (assign, nonatomic) CGFloat multiplier;

- (void)turnTimerOff:(BOOL)turnOff;

@end
