

#import "SNWeek.h"
#import "SNDay.h"

@implementation SNWeek

- (NSString *)description
{
    NSString *desc;
    if (self.arrayOfDays) {
        desc = [NSString stringWithFormat:@"\nWeek:\n %@\n",self.arrayOfDays];
    }
    return desc;
}

#pragma mark - Public Methods
+ (NSArray*)createTestData
{
    NSMutableArray *mArray = [NSMutableArray new];
    for (NSInteger i=0; i<7; i++) {
        SNDayState dayState;
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [gregorian setLocale:[SNAppDelegate currentLocale]];
        NSDateComponents *todayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        NSInteger weekday = [todayComponents weekday];
        
        weekday = ((weekday + 5) % 7) + 1;
        weekday -= 1;
        if (i>weekday) {
            dayState = SNDayStateFuture;
        } else if (i == weekday) {
            dayState = SNDayStateInProgress;
        } else {
            dayState = SNDayStateCompleted;
        }
        CGFloat points = arc4random_uniform(101);
        
        NSInteger swoops = 0;
        NSInteger steps = 0;
        NSInteger calories = 0;
        if (dayState != SNDayStateFuture) {
            swoops = arc4random_uniform(51);
            steps = arc4random_uniform(76);
            calories = swoops+steps;
        }
        SNDay *day = [[SNDay alloc] initWithMaxPoints:100 points:points dayType:i dayState:dayState swoops:swoops steps:steps calories:calories];
        [mArray addObject:day];
    }
    
    return mArray;
}

+ (NSArray *)previousWeekTestData
{
    NSMutableArray *mArray = [NSMutableArray new];
    for (NSInteger i=0; i<7; i++) {
        CGFloat points = arc4random_uniform(101);
        NSInteger swoops = arc4random_uniform(51);
        NSInteger steps = arc4random_uniform(76);
        NSInteger calories = swoops+steps;
        SNDay *day = [[SNDay alloc] initWithMaxPoints:100 points:points dayType:i dayState:SNDayStateCompleted swoops:swoops steps:steps calories:calories];
        [mArray addObject:day];
    }
    return mArray;
}

+ (NSArray *)futureWeekTestData
{
    NSMutableArray *mArray = [NSMutableArray new];
    for (NSInteger i=0; i<7; i++) {
        SNDay *day = [[SNDay alloc] initWithMaxPoints:100 points:0 dayType:i dayState:SNDayStateFuture];
        [mArray addObject:day];
    }
    return mArray;
}

- (BOOL)isCurrentWeek
{
    __block BOOL answer = NO;
    [self.arrayOfDays enumerateObjectsUsingBlock:^(SNDay *dayObj, NSUInteger idx, BOOL *stop) {
        if (dayObj.dayState == SNDayStateInProgress) {
            answer = YES;
            *stop = YES;
        }
    }];
    return answer;
}

@end
