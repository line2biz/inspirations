
#import "SNLanguagesController.h"
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "SNOrangeButton.h"

#define kCELL_IDENTIFIER @"Cell"

@interface SNLanguagesController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@end

@implementation SNLanguagesController

- (instancetype)init
{
    self = [super init];
    if (self) {
//        self = [super initWithStyle:UITableViewStyleGrouped];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCELL_IDENTIFIER];
    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
//    self.clearsSelectionOnViewWillAppear = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:NULL];
    
    self.navigationItem.leftBarButtonItem = nil;
    
    _loadIdx = 0;
    [self controllerStart];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController.viewControllers firstObject] == self) {
        if (!_loadIdx) {
            _loadIdx++;
        } else {
            [self controllerStart];
        }
        [self startCurveAnimation];
        
    } else {
        
    }
    
    
}


#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Private Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCELL_IDENTIFIER forIndexPath:indexPath];
    
    NSString *str = nil;
    switch (indexPath.row) {
        case 0: {
            str = LMLocalizedString(@"Englisch", nil);
            if ([[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:@"en"]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
            break;
        case 1: {
            str = LMLocalizedString(@"Deutsch", nil);
            if ([[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:@"de"]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
            break;
    }
    
    cell.textLabel.text = str;
    cell.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:cell.textLabel.font.pointSize];
    cell.textLabel.textColor = [UIColor customDarkGrayGolor];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *sLang = nil;
    switch (indexPath.row) {
        case 0:
            sLang = @"en";
            break;
            
        case 1:
            sLang = @"de";
            break;
    }
    
    if (![[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:sLang]) {
        [[SNLocalizationManager sharedManager] setLanguage:sLang];
    }
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    [self.tableView reloadData];
}
@end
