

#import <UIKit/UIKit.h>

typedef void(^didTapVideoImageBlock)(void);

@interface SNVideoCell : UITableViewCell

@property (strong, nonatomic) UIImage *videoImage;
@property (strong, nonatomic) NSString *titleLabel;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) UIFont *titleFont;
@property (strong, nonatomic) UIFont *textFont;
@property (strong, nonatomic) didTapVideoImageBlock videoImageBlock;

- (CGFloat)cellHeight;

@end


@interface NSString (mine)

- (CGFloat)getHeightOfTextForWidth:(CGFloat)width andFont:(UIFont *)font;

@end