

#import <Foundation/Foundation.h>

@interface SNWeek : NSObject

@property (strong, nonatomic) NSArray *arrayOfDays;

+ (NSArray *)createTestData;
+ (NSArray *)previousWeekTestData;
+ (NSArray *)futureWeekTestData;

- (BOOL)isCurrentWeek;

@end
