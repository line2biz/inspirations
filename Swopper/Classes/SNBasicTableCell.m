

#import "SNBasicTableCell.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@interface SNBasicTableCell ()
{
    CAShapeLayer *_borderLayer;
}

@end

@implementation SNBasicTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    self.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.textLabel.font.pointSize];
    self.textLabel.textColor = [UIColor customDarkGrayGolor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat fLineWidth = 0.5f;
    
    if (!_borderLayer) {
        _borderLayer = [CAShapeLayer new];
        _borderLayer.lineWidth = fLineWidth;
        _borderLayer.fillColor = [[UIColor clearColor] CGColor];
        _borderLayer.strokeColor = [[UIColor customLightGrayColor] CGColor];
        [self.layer addSublayer:_borderLayer];
    }
    
    UIBezierPath *bezierPath = [[UIBezierPath alloc] init];
    [bezierPath moveToPoint:CGPointMake(CGRectGetMinX(self.textLabel.frame), 0)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMaxX(self.bounds) - fLineWidth/2, 0)];
    [bezierPath moveToPoint:CGPointMake(CGRectGetMaxX(self.bounds) - fLineWidth/2, CGRectGetHeight(self.bounds) - fLineWidth / 2)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMinX(self.textLabel.frame), CGRectGetHeight(self.bounds) - fLineWidth / 2)];
    _borderLayer.path = [bezierPath CGPath];
    _borderLayer.frame = self.bounds;
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
