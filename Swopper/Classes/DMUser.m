
#import "DMUser.h"

#import "DMSession+Category.h"

NSString * const DMUserSexKey                       = @"sex";
NSString * const DMUserWeightKey                    = @"weight";
NSString * const DMUserHeightKey                    = @"height";
NSString * const DMUserAgeKey                       = @"age";
NSString * const DMUserDefaultKey                   = @"DMUserDefaultKey";
NSString * const DMUserNotificationDate             = @"DMUserNotificationDate";
NSString * const DMUserMemoryFirst                  = @"DMUserMemoryFirst";
NSString * const DMUserMemorySecond                 = @"DMUserMemorySecond";

NSString * const DMUserHourPeriod                   = @"DMUserHourPeriod";
NSString * const DMUserDayPeriod                    = @"DMUserDayPeriod";
NSString * const DMUserWeekPeriod                   = @"DMUserWeekPeriod";
NSString * const DMUserBD                           = @"DMUserBD";
NSString * const DMUserFirstLaunch                  = @"DMUserFirstLaunch";
NSString * const DMUserFirstExercise                = @"DMUserFirstExercise";
NSString * const DMUserUSunits                      = @"DMUserUSunits";

@implementation DMUser

#pragma mark - Coding Protocol
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        _sex = [coder decodeIntegerForKey:DMUserSexKey];
        _weight = [coder decodeIntegerForKey:DMUserWeightKey];
        _height = [coder decodeIntegerForKey:DMUserHeightKey];
        _age = [coder decodeIntegerForKey:DMUserAgeKey];
        _notificationDate = [coder decodeObjectForKey:DMUserNotificationDate];
        _memoryFirstSwitchIsOn = [coder decodeBoolForKey:DMUserMemoryFirst];
        _memorySecondSwitchIsOn = [coder decodeBoolForKey:DMUserMemorySecond];
        _hourPeriod = [coder decodeIntegerForKey:DMUserHourPeriod];
        _dayPeriod = [coder decodeIntegerForKey:DMUserDayPeriod];
        _weekPeriod = [coder decodeIntegerForKey:DMUserWeekPeriod];
        _birthDate = [coder decodeObjectForKey:DMUserBD];
        _firstLaunch = [coder decodeBoolForKey:DMUserFirstLaunch];
        _firstExercise = [coder decodeBoolForKey:DMUserFirstExercise];
        _USunits = [coder decodeBoolForKey:DMUserUSunits];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInteger:_sex forKey:DMUserSexKey];
    [aCoder encodeInteger:_weight forKey:DMUserWeightKey];
    [aCoder encodeInteger:_height forKey:DMUserHeightKey];
    [aCoder encodeInteger:_age forKey:DMUserAgeKey];
    [aCoder encodeObject:_notificationDate forKey:DMUserNotificationDate];
    [aCoder encodeBool:_memoryFirstSwitchIsOn forKey:DMUserMemoryFirst];
    [aCoder encodeBool:_memorySecondSwitchIsOn forKey:DMUserMemorySecond];
    [aCoder encodeInteger:_hourPeriod forKey:DMUserHourPeriod];
    [aCoder encodeInteger:_dayPeriod forKey:DMUserDayPeriod];
    [aCoder encodeInteger:_weekPeriod forKey:DMUserWeekPeriod];
    [aCoder encodeObject:_birthDate forKey:DMUserBD];
    [aCoder encodeBool:_firstLaunch forKey:DMUserFirstLaunch];
    [aCoder encodeBool:_firstExercise forKey:DMUserFirstExercise];
    [aCoder encodeBool:_USunits forKey:DMUserUSunits];
}


#pragma mark - Public Methods
+ (DMUser *)defaultUser
{
    static DMUser *user;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSData *notesData = [[NSUserDefaults standardUserDefaults] objectForKey:DMUserDefaultKey];
        user = [NSKeyedUnarchiver unarchiveObjectWithData:notesData];
        if (!user) {
            user = [[DMUser alloc] init];
            user.firstLaunch = YES;
        }
    });
    return user;
}

- (void)save
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:DMUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isValid {
    if ((self.sex != DMSexTypeNone) && (self.weight > 0) && (self.height > 0) && (self.age > 0)) {
        NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"report = nil"];
        NSError *error = nil;
        NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
        if (error) {
            NSLog(@"%s error fetching count request: %@", __FUNCTION__, error);
        } else {
            return (count > 0);
        }
    }
    return NO;
}

- (double)BMRValue
{
    double val = 0;
    if (self.sex == DMSexTypeMan) {
        val = ( 10.0f * self.weight ) + ( 6.25f * self.height ) - ( 5.0f * self.age) + 5;
    } else if (self.sex == DMSexTypeWomen) {
        val = ( 10.0f * self.weight ) + ( 6.25f * self.height ) - ( 5.0f * self.age) - 165;
    }
    
    return val;
}

- (CGFloat)strideLength
{
    CGFloat fResult = 0.0f;
    
    if (self.sex == DMSexTypeMan) {
        fResult = 0.415f * (CGFloat)self.height;
    } else if (self.sex == DMSexTypeWomen) {
        fResult = 0.413f * (CGFloat)self.height;
    }
    
    return fResult;
}

- (CGFloat)weightInLbs
{
    return (CGFloat)self.weight / 0.45359237f;
}


@end
