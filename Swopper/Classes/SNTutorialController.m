

#import "SNTutorialController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@interface SNTutorialController ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (weak, nonatomic) IBOutlet UIButton *but01;
@property (weak, nonatomic) IBOutlet UIButton *but02;
@property (weak, nonatomic) IBOutlet UIButton *but03;


@end

@implementation SNTutorialController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [SNAppearance customizeViewController:self];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.but01 setTitle:LMLocalizedString(@"Schlaue Sache: Jetzt den 3D-\nSchüler-Sitz zum Aktionspreis sich", nil) forState:UIControlStateNormal];
    [self.but02 setTitle:LMLocalizedString(@"Schlaue Sache: Jetzt den 3D-\nSchüler-Sitz zum Aktionspreis sich", nil) forState:UIControlStateNormal];
    [self.but03 setTitle:LMLocalizedString(@"Schlaue Sache: Jetzt den 3D-\nSchüler-Sitz zum Aktionspreis sich", nil) forState:UIControlStateNormal];
    
    [self configureOutelts];
    
    
}

- (void)configureOutelts
{
    for (UIButton *button in self.buttons) {
        button.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:button.titleLabel.font.pointSize];
        button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        button.titleLabel.numberOfLines = 2;
        [button layoutSubviews];
    }
    
    
}


@end
