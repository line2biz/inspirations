
#import "SNDashboardGraphControl.h"
#import "UIFont+Customization.h"
#import "SNAdditionalMath.h"

@interface SNDashboardGraphControl() <UIGestureRecognizerDelegate>
{
    CGFloat _availableSpace;
    UILabel *_labelMsg;
}
@end

@implementation SNDashboardGraphControl

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    [self setUserInteractionEnabled:YES];
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    panGestureRecognizer.minimumNumberOfTouches = 1;
    panGestureRecognizer.delegate = self;
    [self addGestureRecognizer:panGestureRecognizer];
    _availableSpace = CGRectGetWidth(self.bounds);
    _progress = 0.5f;
    [self createArrayOfPoints];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self showLabel:YES animated:NO withMsg:[self textForLabel]];
}

#pragma mark - Calculations
- (void)createArrayOfPoints
{
    NSInteger maxPoints = 10;
    CGFloat dX = CGRectGetWidth(self.bounds)/(maxPoints - 1);
    CGFloat minY, maxY;
    minY = 20.0;
    maxY = CGRectGetHeight(self.bounds);
    NSMutableArray *mArrayOfPoints = [NSMutableArray new];
    for (NSInteger i=0; i<maxPoints; i++) {
        CGPoint point;
        point.x = i*dX;
        point.y = RandomFloatBetweenSmallAndBig(minY, maxY);
        AddPointToArray(point,mArrayOfPoints);
    }
    [self setAllPoints:(NSArray *)mArrayOfPoints];
}

- (CGPoint)positionOfFrame:(CGRect)frame atPoint:(CGPoint)point andBounds:(CGRect)bounds
{
    point.y -= 20;
    CGPoint position = point;
    // x coordinate
    if (point.x + CGRectGetWidth(frame)/2 > CGRectGetWidth(bounds)) {
        position.x -= (point.x + CGRectGetWidth(frame)/2) - CGRectGetWidth(bounds);
    } else if (point.x - CGRectGetWidth(frame)/2 < 0) {
        position.x += CGRectGetWidth(frame)/2 - point.x;
    }
    
    // y coordinate
    if (point.y + CGRectGetHeight(frame)/2 > CGRectGetHeight(bounds)) {
        position.y -= (point.y + CGRectGetHeight(frame)/2) - CGRectGetHeight(bounds);
    } else if (point.y - CGRectGetHeight(frame)/2 < 0) {
        position.y += CGRectGetHeight(frame)/2 - point.y;
    }
    
    return position;
}

- (NSString *)textForLabel
{
    NSMutableString *string = [NSMutableString new];
    NSInteger currentPoint = (self.allPoints.count - 1) * self.progress;
    [string appendString:@"Point "];
    [string appendString:[NSString stringWithFormat:@"%zd",currentPoint]];
    
    return string;
}

#pragma mark - Additional Elements
- (void)showLabel:(BOOL)show animated:(BOOL)animated withMsg:(NSString *)msg
{
    if (!_labelMsg) {
        _labelMsg = [[UILabel alloc] initWithFrame:CGRectZero];
        _labelMsg.font = [UIFont franklinGothicStdBookCondesedWithSize:12.0f];
        _labelMsg.textColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
        _labelMsg.backgroundColor = [UIColor whiteColor];
        _labelMsg.textAlignment = NSTextAlignmentCenter;
    }
    CGPoint labelPosition = [self getProgressPosition];
    _labelMsg.text = msg;
    [_labelMsg sizeToFit];
    labelPosition = [self positionOfFrame:_labelMsg.frame atPoint:labelPosition andBounds:self.bounds];
    _labelMsg.center = labelPosition;
    NSTimeInterval animationDuration = 0.25f;
    if (!show) {
        if (animated) {
            [UIView animateWithDuration:animationDuration
                             animations:^{
                                 _labelMsg.alpha = 0.0f;
                             }
                             completion:^(BOOL finished) {
                                 [_labelMsg removeFromSuperview];
                                 _labelMsg.text = @"";
                                 _labelMsg.alpha = 1.0f;
                             }];
        } else {
            [_labelMsg removeFromSuperview];
            _labelMsg.text = @"";
            _labelMsg.alpha = 1.0f;
        }
    } else {
        if (animated) {
            _labelMsg.alpha = 0.0f;
            [self addSubview:_labelMsg];
            [UIView animateWithDuration:animationDuration
                             animations:^{
                                 _labelMsg.alpha = 1.0f;
                             }];
        } else {
            [self addSubview:_labelMsg];
        }
    }
}

#pragma mark - Drawing
- (void)drawRect:(CGRect)rect
{
    UIColor *colorToDraw = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 5.0f);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    
    [bezierPath moveToPoint:GetPointFromArrayAtIndex(self.allPoints,0)];
    for (NSInteger i=0; i < self.allPoints.count - 1; i++) {
        [bezierPath addLineToPoint:GetPointFromArrayAtIndex(self.allPoints,i+1)];
    }
    
    CGContextAddPath(context, bezierPath.CGPath);
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextStrokePath(context);
    
    CGContextSetStrokeColorWithColor(context, colorToDraw.CGColor);
    
    UIBezierPath *bezierPathProgress = [UIBezierPath bezierPath];
    [bezierPathProgress moveToPoint:GetPointFromArrayAtIndex(self.allPoints,0)];
    for (NSInteger i=0; i < (NSInteger)((self.allPoints.count - 1)*self.progress); i++) {
        [bezierPathProgress addLineToPoint:GetPointFromArrayAtIndex(self.allPoints,i+1)];
    }
    NSInteger pointsInArray = [self.allPoints count];
    CGFloat buttonRadius = 10.0f;
    CGPoint point;
    if ([self progressIsOnPoint]) {
        point = GetPointFromArrayAtIndex(self.allPoints, (pointsInArray - 1)*self.progress);
    } else {
        point = [self getProgressPosition];
        [bezierPathProgress addLineToPoint:point];
    }
    
    CGContextAddPath(context, bezierPathProgress.CGPath);
    CGContextStrokePath(context);
    
    CGRect progressButtonRect = CGRectMake(point.x - buttonRadius, point.y - buttonRadius, buttonRadius*2, buttonRadius*2);
    UIBezierPath *progressButton = [UIBezierPath bezierPathWithOvalInRect:progressButtonRect];
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetFillColorWithColor(context, colorToDraw.CGColor);
    CGContextSetLineWidth(context, buttonRadius/4);
    CGContextAddPath(context, progressButton.CGPath);
    CGContextDrawPath(context, kCGPathFillStroke);
}

- (BOOL)progressIsOnPoint
{
    CGFloat pointNumber = (CGFloat)(self.allPoints.count - 1) * self.progress;
    if (pointNumber - (NSInteger)pointNumber != 0) {
        return NO;
    } else {
        return YES;
    }
}

- (CGPoint)getProgressPosition
{
    CGPoint point;
    NSInteger pointsInArray = [self.allPoints count];
    CGPoint tempStartPoint = GetPointFromArrayAtIndex(self.allPoints, (pointsInArray - 1)*self.progress);
    CGPoint tempEndPoint = GetPointFromArrayAtIndex(self.allPoints, pointsInArray*self.progress);
    CustomLine line;
    line.startPoint = tempStartPoint;
    line.endPoint = tempEndPoint;
    CGFloat lineLength = GetLineLength(line)* FloatPartOfNumber((CGFloat)(pointsInArray - 1)*self.progress);
    point = GetPointFromPointAngleLength(line.startPoint, GetAngleOfLine(line),lineLength);
    return point;
}

- (void)progressChanged
{
//    CGFloat degreesPerPoint = 360/(CGFloat)self.maxPoints;
//    CGFloat endDegree = degreesPerPoint * (CGFloat)self.currentPoint - 90;
//    CGFloat prevDegree = degreesPerPoint * (CGFloat)_previousPoint - 90;
//    CGPoint pointOnView = [self pointFromAngle:endDegree];
//    
//    BOOL clockWise = (prevDegree < endDegree) ? YES:NO;
//    
//    CGPathRef pointPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(_sideLength/2, _sideLength/2)
//                                                         radius:_sideLength / 2 - CGRectGetWidth(_pointLayer.frame)/4
//                                                     startAngle:DEGREES_TO_RADIANS(prevDegree)
//                                                       endAngle:DEGREES_TO_RADIANS(endDegree)
//                                                      clockwise:clockWise].CGPath;
//    [CATransaction begin];
//    [CATransaction setDisableActions:YES] ;
//    
//    [CATransaction setCompletionBlock:^{
//        [_pointLayer removeAllAnimations];
//        [_progressLayer removeAllAnimations];
//        [_pointLayer setPosition:pointOnView];
//        _progressLayer.strokeEnd = ((CGFloat)_currentPoint / (CGFloat)_maxPoints);
//    }];
//    
//    CAKeyframeAnimation * theAnimationOfPoint;
//    
//    CGFloat animationDuration = 0.25f;
//    
//    // Create the animation object, specifying the position property as the key path.
//    theAnimationOfPoint = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    theAnimationOfPoint.path = pointPath;
//    theAnimationOfPoint.duration = animationDuration;
//    
//    CABasicAnimation *drawAnimationOfProgres = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
//    drawAnimationOfProgres.duration = animationDuration;
//    drawAnimationOfProgres.repeatCount = 1.0;
//    
//    // Animate from no part of the stroke being drawn to the entire stroke being drawn
//    drawAnimationOfProgres.fromValue = [NSNumber numberWithFloat:((CGFloat)_previousPoint / (CGFloat)_maxPoints)];
//    drawAnimationOfProgres.toValue   = [NSNumber numberWithFloat:((CGFloat)_currentPoint / (CGFloat)_maxPoints)];
//    
//    
//    // Add the animation to the layer.
//    [_pointLayer addAnimation:theAnimationOfPoint forKey:@"position"];
//    [_progressLayer addAnimation:drawAnimationOfProgres forKey:@"drawCircleAnimation"];
//    
//    
//    [CATransaction commit];
    
}

#pragma mark - Property Accessors
- (void)setAllPoints:(NSArray *)allPoints
{
    _allPoints = allPoints;
    [self setNeedsDisplay];
}

- (void)setProgress:(CGFloat)progress
{
    if (progress > 1.0f) {
        progress = 1.0f;
    } else if (progress < 0.0f) {
        progress = 0.0f;
    }
    _progress = progress;
    [self setNeedsDisplay];
}

#pragma mark - Gesture Recognizer Delegate
- (void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
    CGPoint pointOfTouch = [sender locationInView:self];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            self.progress = pointOfTouch.x / _availableSpace;
            
            [self showLabel:YES animated:NO withMsg:[self textForLabel]];
            
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            [self showLabel:YES animated:YES withMsg:[self textForLabel]];
        }
            break;
            
        default:
            break;
    }
}
//
//#pragma mark - UIControl Override
//-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    [super beginTrackingWithTouch:touch withEvent:event];
//    return YES;
//}
//
//-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    [super continueTrackingWithTouch:touch withEvent:event];
//    CGPoint pointOfTouch = [touch locationInView:self];
//    self.progress = pointOfTouch.x / _availableSpace;
//    
//    [self showLabel:YES animated:NO withMsg:[self textForLabel]];
//    
//    [self sendActionsForControlEvents:UIControlEventValueChanged];
//    return YES;
//}
//
//-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    [super endTrackingWithTouch:touch withEvent:event];
//    [self showLabel:YES animated:YES withMsg:[self textForLabel]];
//}


@end
