

#import <UIKit/UIKit.h>

@class DMReport;

@interface SNReportController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) DMReport *report;

@end
