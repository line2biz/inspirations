

#import "UIColor+Customization.h"

@implementation UIColor (Customization)

+ (UIColor *)titleColor
{
    return [UIColor colorWithRed:115/255.f green:122/255.f blue:130/255.f alpha:1];
}

+ (UIColor *)customRedColor
{
    return [UIColor colorWithRed:255/255.f green:0/255.f blue:54/255.f alpha:1];
}

+ (UIColor *)customOrangeColor
{
    return [UIColor colorWithRed:240/255.f green:149/255.f blue:36/255.f alpha:1];
}

+ (UIColor *)customBlueColor
{
    return [UIColor colorWithRed:0/255.f green:145/255.f blue:202/255.f alpha:1];
}

+ (UIColor *)customDarkGrayGolor
{
    return [UIColor colorWithRed:115/255.f green:122/255.f blue:130/255.f alpha:1];
}

+ (UIColor *)customLightGrayColor
{
    return [UIColor colorWithRed:187/255.f green:193/255.f blue:200/255.f alpha:1];
}

@end
