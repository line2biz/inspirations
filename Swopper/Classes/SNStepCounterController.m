

#import "SNStepCounterController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHoursActivityView.h"
#import "SNProgressView3D.h"
#import "DMStep+Category.h"

#import "SNStepResultController.h"
#import "SNStepDashboardController.h"

#import "AnimatedLabel.h"
#import "SNMenusController.h"

#import <MFSideMenu.h>

@import CoreMotion;


@interface SNStepCounterController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet SNHoursActivityView *hoursActivityView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelTarget;
@property (nonatomic) NSInteger numberOfSteps;
@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@end

@implementation SNStepCounterController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    
    _loadIdx = 0;
    [SNAppearance customizeViewController:self];
    
    [self configureOutlets];
    [self configureView];
    
    self.hoursActivityView.date = [NSDate date];
    
    if ([CMMotionActivityManager isActivityAvailable]) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [self loadMotionHistory];
        });
    }
    [self controllerStart];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
    [self.hoursActivityView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Property Accessors
- (NSManagedObjectContext *)managedObjectContext {
    if (!_managedObjectContext) {
        _managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
    return _managedObjectContext;
}

#pragma mark - Private Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureOutlets {
    
    self.titleLabel.text = LMLocalizedString(@"Jeder Schritt zählt", nil);
    
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.subtitleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subtitleLabel.font.pointSize];
    self.subtitleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.detailLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
    self.detailLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.valueLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.valueLabel.font.pointSize];
    self.valueLabel.textColor = [UIColor customOrangeColor];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.titleLabel.font.pointSize];
    self.subtitleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subtitleLabel.font.pointSize];
    
    self.labelTarget.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTarget.font.pointSize];
    self.labelTarget.textColor = [UIColor customLightGrayColor];
    
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    
    [self.progressView configureProgressViewWithMaxPoints:(savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay
                                             currentPoint:0
                                                pathColor:[UIColor customLightGrayColor]
                                            progressColor:[UIColor customOrangeColor]
                                                lineWidth:10.0f];
}

- (void)configureView {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"EEEE, d. MMMM";
    self.subtitleLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    
    DMStep *maxStep = [DMStep maxStepInContext:self.managedObjectContext];
    if (maxStep) {
        NSLog(@"%s maxStep %@", __FUNCTION__, maxStep.numberOfSteps);
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    savedValue = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
    
    if (self.numberOfSteps > savedValue) {
        
        NSInteger diff = ABS(savedValue - self.numberOfSteps);
        
        NSString *str = LMLocalizedString(@"Super! Sie liegen heute <step> Schritte zurückgelegt, Ihnen fehlen noch <diff> Schritte über Ihrem Tagesziel.", @"<step> & <diff> don't translate");
        
        str = [str stringByReplacingOccurrencesOfString:@"<step>" withString:[AnimatedLabel textWithThousendsFormat:self.numberOfSteps]];
        str = [str stringByReplacingOccurrencesOfString:@"<diff>" withString:[AnimatedLabel textWithThousendsFormat:diff]];
        self.detailLabel.text = str;
        
    } else {
        
        NSInteger diff = ABS(self.numberOfSteps - savedValue);
        
        NSString *str = LMLocalizedString(@"Sie haben heute %@ Schritte zurückgelegt, Ihnen fehlen noch %@ Schritte zu Ihrem Tagesziel.", @"<step> & <diff> don't translate");
        str = [str stringByReplacingOccurrencesOfString:@"<step>" withString:[AnimatedLabel textWithThousendsFormat:self.numberOfSteps]];
        str = [str stringByReplacingOccurrencesOfString:@"<diff>" withString:[AnimatedLabel textWithThousendsFormat:diff]];
        self.detailLabel.text = str;
        
        [self.nextButton setTitle:LMLocalizedString(@"Wie werde ich besser ?", nil) forState:UIControlStateNormal];
    }
    self.labelTarget.text = [NSString stringWithFormat:@"%@: %@",LMLocalizedString(@"Ziel", nil), [AnimatedLabel textWithThousendsFormat:savedValue]];
    
    
    self.progressView.currentPoint = self.numberOfSteps;
    self.valueLabel.text = [AnimatedLabel textWithThousendsFormat:self.numberOfSteps];
    
}

- (void)reloadData {
    if ([CMStepCounter isStepCountingAvailable]) {
        CMStepCounter *stepCounter = [CMStepCounter new];
        NSOperationQueue *queue = [NSOperationQueue new];
        
        __weak typeof(self) weakSelf = self;
        [stepCounter queryStepCountStartingFrom:self.hoursActivityView.begindDate to:self.hoursActivityView.endDate toQueue:queue withHandler:^(NSInteger numberOfSteps, NSError *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf && !error) {
                strongSelf.numberOfSteps = numberOfSteps;
                dispatch_async(dispatch_get_main_queue(), ^{
                    DMStep *step = [DMStep stepByDate:[NSDate date] inContext:strongSelf.managedObjectContext];
                    if (!step) {
                        step = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMStep class]) inManagedObjectContext:strongSelf.managedObjectContext];
                        [step setCurrentDate:[NSDate date]];
                    }
                    step.numberOfSteps = @(numberOfSteps);
                    [strongSelf.managedObjectContext save:nil];
                    [strongSelf configureView];
                    
                    
                });
            }
        }];
    }
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    SNStepResultController *controller = (SNStepResultController *)[segue destinationViewController];
//    controller.numberOfSteps = self.numberOfSteps;
//    controller.date = self.hoursActivityView.date;
    
    if ([[segue destinationViewController] isKindOfClass:[SNStepDashboardController class]]) {
        SNStepDashboardController *controller = (SNStepDashboardController *)[segue destinationViewController];
        controller.managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
}



#pragma mark - Load History 
- (void)loadMotionHistory {
    DMStep *lastStepRecord = [DMStep lastStepfRecordInContet:self.managedObjectContext];
    
    NSDate *startDate = nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    
    
    
    
    if (!lastStepRecord) {
        dateComponents.day = -10;
        startDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    } else {
        startDate = lastStepRecord.date;
        
    }
    
//    NSLog(@"startDate: %@", startDate);
    
    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:startDate];
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    startDate = [calendar dateFromComponents:dateComponents];
    
//    NSLog(@"today:     %@", [NSDate date]);
//    NSLog(@"startDate: %@", startDate);
    
    dateComponents = [NSDateComponents new];
    NSDate *stepDate = startDate;
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    while ([self compareDate:stepDate withDate:[NSDate date]] == NSOrderedAscending) {
        dateComponents.day = 1;
        NSDate *nextDate = [calendar dateByAddingComponents:dateComponents toDate:stepDate options:0];
        DMStep *step = [DMStep stepByDate:stepDate inContext:self.managedObjectContext];
        if (!step) {
            step = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMStep class]) inManagedObjectContext:self.managedObjectContext];
//            NSLog(@"new record : %@", stepDate);
        }
        
        [step setCurrentDate:stepDate];
        
        CMStepCounter *stepCounter = [CMStepCounter new];
        
        [stepCounter queryStepCountStartingFrom:stepDate to:nextDate toQueue:queue withHandler:^(NSInteger numberOfSteps, NSError *error) {
            step.numberOfSteps = @(numberOfSteps);
        }];
        
        
//        NSLog(@"date: %@ nextDate: %@", stepDate, nextDate);
        
        stepDate = nextDate;
        
    }
    
    [self.managedObjectContext save:nil];
    [self configureView];
}

- (NSComparisonResult)compareDate:(NSDate *)firstDate withDate:(NSDate *)secondDate {
    
    NSDate *date1 = firstDate;
    NSDate *date2 = secondDate;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date2 interval:NULL forDate:date2];
    return [date1 compare:date2];
    
}






@end
