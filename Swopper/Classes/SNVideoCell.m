

#import "SNVideoCell.h"

@interface SNVideoCell () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewVideo;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewText;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelHeigh;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImageWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMidOne;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMidTwo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@end

@implementation SNVideoCell

#pragma mark - Life Cycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _videoImage = (self.imageViewVideo.image) ? self.imageViewVideo.image : nil;
    _titleLabel = (self.labelTitle.text.length) ? self.labelTitle.text : nil;
    _text = (self.textViewText.text.length) ? self.textViewText.text : nil;
    _titleFont = (self.labelTitle.font) ? self.labelTitle.font : nil;
    _textFont = (self.textViewText.font) ? self.textViewText.font : nil;
    
    [self.imageViewVideo setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [tapGestureRecognizer setDelegate:self];
    [self.imageViewVideo addGestureRecognizer:tapGestureRecognizer];
    
    if (_titleFont && _titleLabel.length) {
        self.constraintLabelHeigh.constant = [_titleLabel getHeightOfTextForWidth:CGRectGetWidth(self.labelTitle.frame) andFont:_titleFont];
    } else {
        self.constraintLabelHeigh.constant = 0;
    }
    
    if (_textFont && _text.length) {
        self.constraintTextHeight.constant = [_text getHeightOfTextForWidth:CGRectGetWidth(self.textViewText.frame) andFont:_textFont];
    } else {
        self.constraintTextHeight.constant = 0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public Methods
- (CGFloat)cellHeight
{
    CGFloat cellHeight = _constraintTop.constant + _constraintMidOne.constant + _constraintMidTwo.constant + _constraintBottom.constant;
    
    cellHeight += CGRectGetHeight(self.imageViewVideo.frame);
    
    if (_titleFont && _titleLabel.length) {
        cellHeight += [_titleLabel getHeightOfTextForWidth:CGRectGetWidth(self.labelTitle.frame) andFont:_titleFont];
    }
    
    if (_textFont && _text.length) {
        cellHeight += [_text getHeightOfTextForWidth:CGRectGetWidth(self.textViewText.frame) andFont:_textFont];
    }
    return cellHeight;
}

#pragma mark - Gesture Handler
- (void)handleTapGesture:(UITapGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateEnded:
        {
            if (self.videoImageBlock) {
                self.videoImageBlock();
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Property Accessors
- (void)setVideoImage:(UIImage *)videoImage
{
    _videoImage = videoImage;
    self.imageViewVideo.image = _videoImage;
    self.constraintImageWidth.constant = _videoImage.size.width / _videoImage.size.height * CGRectGetHeight(self.imageViewVideo.frame);
}

- (void)setTitleLabel:(NSString *)titleLabel
{
    _titleLabel = titleLabel;
    self.labelTitle.text = _titleLabel;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textViewText.text = _text;
}

- (void)setTitleFont:(UIFont *)titleFont
{
    if (titleFont) {
        _titleFont = titleFont;
        self.labelTitle.font = _titleFont;
    }
}

- (void)setTextFont:(UIFont *)textFont
{
    if (textFont) {
        _textFont = textFont;
        self.textViewText.font = _textFont;
    }
}

@end


#pragma mark - NSString Category

@implementation NSString (mine)

- (CGFloat)getHeightOfTextForWidth:(CGFloat)width andFont:(UIFont *)font
{
    NSString *text = self;
    CGFloat heightMultiplier = 1.0f;
    NSSet *setOfCustomFonts = [[NSSet alloc] initWithArray:[UIFont fontNamesForFamilyName:@"ITC Franklin Gothic Std"]];
    if ([setOfCustomFonts containsObject:font.fontName]) {
        heightMultiplier = 1.4f;
    }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@
                                          {
                                          NSFontAttributeName: font
                                          }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width - 10.0, 9999.0}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    return size.height*heightMultiplier + 20;
}

@end
