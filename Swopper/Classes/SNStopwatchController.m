

#import "SNStopwatchController.h"
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNOrangeButton.h"
#import "SNCountdownControl.h"
#import "SNMeasurementController.h"
#import "SNProgressView3D.h"
#import "SNMenusController.h"
#import "SNAudioVolumeController.h"

#import <AVFoundation/AVFoundation.h>
#import "SNNavController.h"
#import "SNTimerSetterController.h"

#import <MFSideMenu.h>

@import MediaPlayer;

@interface SNStopwatchController () <SNCountdownControlDelegate, AVAudioPlayerDelegate, UIGestureRecognizerDelegate>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
    NSMutableArray *_arrayOfObservers;
}

@property (weak, nonatomic) IBOutlet SNCountdownControl *countdownControl;
@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (weak, nonatomic) IBOutlet UILabel *labelTop;
@property (weak, nonatomic) IBOutlet UILabel *labelBottom;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;

@property (strong, nonatomic) UIBarButtonItem *menuBarButton;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation SNStopwatchController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadIdx = 0;
    
    [SNAppearance customizeViewController:self];
    
    BOOL firstControllerFound = NO;
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[SNTimerSetterController class]]) {
            firstControllerFound = YES;
            break;
        }
    }
    
//    if (firstControllerFound) {
//        UIImage *iconImage = [UIImage imageNamed:@"Icon-Nav-Back"];
//        NSString *sButtonTitle = LMLocalizedString(@"Zurück", nil);
//        UIFont *fontForButton = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTitle.font.pointSize-3];
//        NSDictionary *dictOfAttrs = @{ NSFontAttributeName : fontForButton };
//        CGSize size = [sButtonTitle sizeWithAttributes:dictOfAttrs];
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width + size.width, 40)];
//        UIImageView *imageView = [[UIImageView alloc] initWithImage:iconImage];
//        imageView.frame = CGRectMake(0, (40 - iconImage.size.height)/2, iconImage.size.width, iconImage.size.height);
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(iconImage.size.width, (40 - size.height)/2 + 2, size.width, size.height)];
//        label.font = fontForButton;
//        label.textColor = [UIColor customDarkGrayGolor];
//        label.text = sButtonTitle;
//        [view addSubview:imageView];
//        [view addSubview:label];
//        
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
//        tapGesture.delegate = self;
//        
//        [view addGestureRecognizer:tapGesture];
//        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:view];
//        self.navigationItem.leftBarButtonItem = leftButton;
//        
//        
//    } else {
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"]
//                                                                                 style:UIBarButtonItemStylePlain
//                                                                                target:self
//                                                                                action:@selector(didTapButtonBack:)];
//    }
    
    
    if (self.trainingTime > 0) {
        self.labelTitle.text = LMLocalizedString(@"swopper FIT Messung", nil);
    } else {
        self.labelTitle.text = LMLocalizedString(@"Mitmach-Training", nil);
    }
    
    self.labelTop.text = LMLocalizedString(@"Es geht los in...", nil);
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self controllerStart];
    
    
    

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self registerObservers];
    [self prepareStartAnimation];
    [self startCurveAnimation];
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterObservers];
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeDefault;
    
//    if ([self.countdownControl isInProgress]) {
//        [self.countdownControl stopCountdownWithoutBackJump:YES];
//    }
//    if (self.audioPlayer.isPlaying) {
//        [self.audioPlayer stop];
//    }
    [self countStop];
}

#pragma mark - Segue Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[SNMeasurementController class]]) {
        if (self.trainingTime > 0) {
            SNMeasurementController *measurementController = (SNMeasurementController *)[segue destinationViewController];
            measurementController.trainingTime = self.trainingTime;
        }
    }
}

#pragma mark - Observers
- (void)registerObservers
{
    _arrayOfObservers = [NSMutableArray new];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    __weak typeof(self) weakSelf = self;
    [_arrayOfObservers addObject:[nc addObserverForName:MFSideMenuStateNotificationEvent
                                                 object:nil
                                                  queue:[NSOperationQueue mainQueue]
                                             usingBlock:^(NSNotification *note)
                                  {
                                      typeof(weakSelf) strongSelf = weakSelf;
                                      if (strongSelf) {
                                          NSDictionary *userInfo = [note userInfo];
                                          if (userInfo) {
                                              MFSideMenuStateEvent event = (MFSideMenuStateEvent)[[userInfo valueForKey:@"eventType"] integerValue];
                                              if (event == MFSideMenuStateEventMenuDidClose) {
                                                  [strongSelf menuDidClose];
                                              } else if (event == MFSideMenuStateEventMenuWillOpen) {
                                                  [strongSelf menuWillOpen];
                                              }
                                          }
                                      }
                                  }]];
}

- (void)unregisterObservers
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (int i = 0; i < _arrayOfObservers.count; i++)
    {
        [nc removeObserver:_arrayOfObservers[i]];
    }
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    [self configureCountdown];
    [self configureFonts];
    
    [self.progressView setPathColor:[UIColor customLightGrayColor]];
    [self.progressView setProgressColor:[UIColor customOrangeColor]];
    [self.progressView setLineWidth:10.0f];
    
    self.progressView.alpha = 0.0f;
    self.countdownControl.alpha = 0.0f;
    
    // for animation purposes
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
    
    if (self.navigationController.viewControllers[0] == self) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
        self.navigationItem.leftBarButtonItem = self.menuBarButton;
    }
}

- (void)configureCountdown
{
    self.countdownControl.delegate = self;
    [self.countdownControl setPlayBeep:NO];
    [self.countdownControl setMaxTime:10];
    [self.countdownControl setColorToDraw:[UIColor clearColor]];
    [self.countdownControl setColorForPath:[UIColor clearColor]];
}

- (void)configureFonts
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelTop.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTop.font.pointSize];
    self.labelBottom.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelBottom.font.pointSize];
}

- (void)countStart
{
    NSError *error;
//    NSString *soundFilePath = [[SNLocalizationManager sharedManager] pathForResource: @"Countdown"
//                                                              ofType: @"wav"];
//    

    
    NSString *soundFilePath = [[SNLocalizationManager sharedManager] pathForResource:@"Countdown" ofType:@"wav"];
    
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
                                                                      error: &error];
    newPlayer.volume = 1.0f;
    self.audioPlayer = newPlayer;
    if (error){
        NSLog(@"%s Error in audioPlayer: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    } else {
        [self.audioPlayer prepareToPlay];
        self.audioPlayer.delegate = self;
        [self.audioPlayer play];
    }
    [self.countdownControl startCountdown];
    [self.progressView startProgressingTimerWithDuration:self.countdownControl.maxTime];
}

- (void)countStop
{
    [self.audioPlayer stop];
    [self.countdownControl stopCountdown];
    [self.progressView stopProgressingTimer];
    [self.progressView setCurrentPoint:0.0f];
}

- (void)didTapButtonBack:(id)sender
{
    BOOL firstControllerFound = NO;
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[SNTimerSetterController class]]) {
            firstControllerFound = YES;
            SNTimerSetterController *timerSetterController = (SNTimerSetterController *)viewController;
            [self.navigationController popToViewController:timerSetterController animated:YES];
            break;
        }
    }
    if (!firstControllerFound) {
        [self countStop];
        [self.stopButton setSelected:YES];
        MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
        sidemenu.panMode = MFSideMenuPanModeDefault;
        [sidemenu toggleLeftSideMenuCompletion:nil];
    }
}

#pragma mark Menu Events Handlers
- (void)menuDidClose
{
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
    [self.stopButton setSelected:NO];
    [self countStart];
}

- (void)menuWillOpen
{
    [self.stopButton setSelected:YES];
    [self countStop];
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeDefault;
}

#pragma mark Animation Part
- (void)prepareStartAnimation
{
    __weak typeof(self) weakSelf = self;
    [self.stopButton setHidden:YES];
    CATransform3D xForm = CATransform3DIdentity;
    xForm = CATransform3DScale(xForm, 0.1, 0.1, 1.0);
    
    [UIView animateWithDuration:0.001
                     animations:^{
                         typeof(weakSelf) strongSelf = weakSelf;
                         if (strongSelf) {
                             strongSelf.progressView.layer.transform = xForm;
                             strongSelf.countdownControl.layer.transform = xForm;
                         }
                     }];
    
    xForm = CATransform3DIdentity;
    xForm = CATransform3DScale(xForm, 1.0, 1.0, 1.0);
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         typeof(weakSelf) strongSelf = weakSelf;
                         if (strongSelf) {
                             strongSelf.progressView.layer.transform = xForm;
                             strongSelf.countdownControl.layer.transform = xForm;
                             strongSelf.progressView.alpha = 1.0f;
                             strongSelf.countdownControl.alpha = 1.0f;
                         }
                     }];
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf countStart];
            [strongSelf.stopButton setHidden:NO];
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Tap Gesture Recognizer Handler 
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    NSTimeInterval duration = 0.5f;
    [UIView animateWithDuration:duration/2
                     animations:^{
                         sender.view.alpha = 0.25f;
                         [UIView animateWithDuration:duration/2
                                          animations:^{
                                              sender.view.alpha = 1.0f;
                                          }
                                          completion:^(BOOL finished) {
                                              [self didTapButtonBack:nil];
                                          }];
                     }];
}

#pragma mark - Outlet Methods
- (IBAction)didTapStopButton:(UIButton *)sender
{
    if (!self.countdownControl.isInProgress) {
        [sender setSelected:NO];
        
        [self countStart];
        
        MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
        sidemenu.panMode = MFSideMenuPanModeNone;
        
    } else {
        [sender setSelected:YES];
        
        [self countStop];
        
        MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
        sidemenu.panMode = MFSideMenuPanModeDefault;
    }
}

- (IBAction)countdownFinished:(id)sender
{
    [self performSegueWithIdentifier:NSStringFromClass([SNMeasurementController class]) sender:self];
}


#pragma mark - Countdown Control Delegate
- (void)countdownControl:(SNCountdownControl *)control timerFired:(BOOL)fired
{
    __weak typeof(self) weakSelf = self;
    
    __block NSTimeInterval duration = 0.125;
    
    __block CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    __block CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DScale(xform, 1.25, 1.25, 1.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            duration = 0.125;
            
            transformAnimation.duration = duration;
            
            xform = CATransform3DIdentity;
            xform = CATransform3DScale(xform, 0.75, 0.75, 1.0);
            transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
            
            [CATransaction setAnimationDuration:duration];
            [CATransaction setCompletionBlock:^{
                if (strongSelf) {
                    duration = 0.125;
                    
                    transformAnimation.duration = duration;
                    
                    xform = CATransform3DIdentity;
                    xform = CATransform3DScale(xform, 1.0, 1.0, 1.0);
                    transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
                    
                    [CATransaction setAnimationDuration:duration];
                    [CATransaction setCompletionBlock:^{
                        
                    }];
                    
                    [CATransaction begin];
                    
                    [strongSelf.progressView.layer addAnimation:transformAnimation forKey:@"endTransform"];
                    [strongSelf.countdownControl.layer addAnimation:transformAnimation forKey:@"endTransform"];
                    
                    [CATransaction commit];
                }
            }];
            
            [CATransaction begin];
            
            [strongSelf.progressView.layer addAnimation:transformAnimation forKey:@"midTransform"];
            [strongSelf.countdownControl.layer addAnimation:transformAnimation forKey:@"midTransform"];
            
            [CATransaction commit];
        }
    }];
    
    [CATransaction begin];
    
    [self.progressView.layer addAnimation:transformAnimation forKey:@"startTransform"];
    [self.countdownControl.layer addAnimation:transformAnimation forKey:@"startTransform"];
    
    [CATransaction commit];
}




@end
