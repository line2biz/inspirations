
#import <UIKit/UIKit.h>

@interface SNAudioVolumeController : UIViewController

@property (strong, nonatomic) NSString *controllerIdentifier;
@property (assign, nonatomic) NSTimeInterval trainingDuration;
+ (BOOL)shouldBeShown;

@end
