

#import <UIKit/UIKit.h>

@interface SNMenusController : UIViewController <SNAppDelegateProtocol>

- (void)showControllerWithIdentifier:(NSString *)identifer;
- (void)hideViewController:(id)sender;

- (void)applicationWillAppearInForeground;

@end
