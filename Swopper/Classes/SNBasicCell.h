
#import <UIKit/UIKit.h>

@interface SNBasicCell : UITableViewCell

@property (assign, nonatomic) BOOL shouldShowSeparator;

@end
