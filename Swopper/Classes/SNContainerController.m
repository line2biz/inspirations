
#import "SNContainerController.h"

#import "SNAppearance.h"

@interface SNContainerController ()

@end

@implementation SNContainerController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [SNAppearance customizeViewController:self];
    
}


@end
