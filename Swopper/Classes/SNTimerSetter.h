

#import <UIKit/UIKit.h>

@interface SNTimerSetter : UIControl

@property (strong, nonatomic) UIColor *ringColor;
@property (strong, nonatomic) UIFont *textFont;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIFont *subTextFont;
@property (assign, readonly, nonatomic) NSInteger maxValue;
@property (assign, readonly, nonatomic) NSInteger index;
@property (assign, readonly, nonatomic) NSInteger elementsCount;
@property (assign, readonly, nonatomic) NSInteger value;

@end
