
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DMStep : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) NSNumber * numberOfSteps;
@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * weekOfYear;
@property (nonatomic, retain) NSNumber * year;

@end
