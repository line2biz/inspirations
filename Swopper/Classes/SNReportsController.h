

#import <UIKit/UIKit.h>

@class DMReport;

@interface SNReportsController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) DMReport *currentReport;

@end
