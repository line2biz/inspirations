
#import <UIKit/UIKit.h>

@interface SNDashboardGraphControl : UIControl

@property (strong, nonatomic) NSArray *allPoints;
@property (assign, nonatomic) CGFloat progress;

@end
