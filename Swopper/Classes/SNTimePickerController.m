

#import "SNTimePickerController.h"
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "DMUser.h"

@interface SNTimePickerController ()
{
    NSInteger _loadIdx;
    CGFloat _startCurveAlpha;
}

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) DMUser *user;
@property (assign, nonatomic) BOOL infoIsSaved;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation SNTimePickerController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _loadIdx = 0;
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Speichern", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.infoIsSaved = NO;
    NSLocale *de = [SNAppDelegate currentLocale];
    NSCalendar *cal = [SNAppDelegate currentCalendar];
    [cal setLocale:de];
    [self.datePicker setLocale:de];
    [self.datePicker setCalendar:cal];
    
    self.user = [DMUser defaultUser];
    if (self.user.notificationDate) {
        NSString *dateStr;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EE, d LLLL yyyy HH:mm:ss Z"];
        dateStr = [dateFormat stringFromDate:self.user.notificationDate];
        NSDate *date = [dateFormat dateFromString:dateStr];
        [self.datePicker setDate:date];
    }
    
    [SNAppearance customizeViewController:self];
    
    [self.nextButton addTarget:self
                        action:@selector(saveSettings)
              forControlEvents:UIControlEventTouchUpInside];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.infoIsSaved) {
        [self.delegate timePickerController:self chosenDate:self.datePicker.date];
    }
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)saveSettings
{
    self.infoIsSaved = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Animation
- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}


@end
