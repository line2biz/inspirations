

#import <UIKit/UIKit.h>

#import "SNAvatarView.h"

@interface SNReportCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sortImageView;
@property (weak, nonatomic) IBOutlet UIImageView *dateImageView;
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;

@property (assign, nonatomic) BOOL active;


@property (weak, nonatomic) IBOutlet UILabel *sortOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
