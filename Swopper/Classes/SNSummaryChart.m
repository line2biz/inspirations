

#import "SNSummaryChart.h"

#import "SNAdditionalMath.h"
#import "SNAppearance.h"
#import "SNDay.h"

#import <CoreMotion/CoreMotion.h>

@interface SNSummaryChart() <UIGestureRecognizerDelegate>
{
    CGPoint _startPoint;
    NSArray *_arrayOfCenterPoints;
    CGFloat _dayRadius;
}

@end

@implementation SNSummaryChart

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor whiteColor];
    
    _currentDate = [NSDate date];
    
    _colorSwoops = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1.0];
    _colorSteps = [UIColor colorWithRed:96/255.0 green:96/255.0 blue:96/255.0 alpha:1.0];
    _colorCalories = [UIColor colorWithRed:255/255.0 green:102/255.0 blue:36/255.0 alpha:1.0];
    _colorDayCircle = [UIColor whiteColor];
    _colorText = _colorSteps;
    _colorCurrentDayText = [UIColor orangeColor];
    
    _annotationSwoops = LMLocalizedString(@"Moves", nil);
    _annotationSteps = LMLocalizedString(@"Schritte", nil);
    _annotationCalories = @"kcal";
    
    _annotationFont = [UIFont fontWithName:@"Arial" size:9];
    _textFont = [UIFont fontWithName:@"Arial" size:12];
    
    UITapGestureRecognizer *tapGestureRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    tapGestureRecog.delegate = self;
    [self addGestureRecognizer:tapGestureRecog];
    
    UIPanGestureRecognizer *panGestureRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    panGestureRecog.delegate = self;
    [self addGestureRecognizer:panGestureRecog];
}

- (void)awakeFromNib
{
    NSArray *sourceData = [self.dataSource summaryChartView:self willChangeWeekForward:YES];
    if (sourceData) {
        self.week = sourceData;
    }
}

#pragma mark - Private Methods
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (!self.week.count) {
        NSLog(@"\n%s\nNo data to show\n",__FUNCTION__);
        return;
    }
    __block BOOL objectDoesntConfirmProtocol = NO;
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj conformsToProtocol:@protocol(SNSummaryChartProtocol)]) {
            objectDoesntConfirmProtocol = YES;
            *stop = YES;
        }
    }];
    
    if (objectDoesntConfirmProtocol) {
        NSLog(@"\n%s\nOne or more objects doesn`t confirm to protocol!\n",__FUNCTION__);
        return;
    }
    
    // **************** additional variables ******************
    CGFloat boundsWidth = CGRectGetWidth(self.bounds);
    CGFloat boundsHeight = CGRectGetHeight(self.bounds);
    
    CGFloat maxPoint = 0.00001f, maxSwoops = 0.00001f, maxSteps = 0.00001f, maxCalories = 0.00001f;
    
    NSInteger totalSwoops = 0, totalSteps = 0, totalCalories = 0;
    
    for (id obj in self.week) {
        NSInteger swoops = 0, steps = 0, calories = 0;
        
        swoops = [[obj valueForKey:@"swoops"] integerValue];
        steps = [[obj valueForKey:@"steps"] integerValue];
        calories = [[obj valueForKey:@"calories"] integerValue];
        
        totalSwoops += swoops;
        totalSteps += steps;
        totalCalories += calories;
        
        // max point value
        if (maxPoint < swoops) {
            maxPoint = swoops;
        }
        if (maxSwoops < swoops) {
            maxSwoops = swoops;
        }
        
        if (maxPoint < steps) {
            maxPoint = steps;
        }
        if (maxSteps < steps) {
            maxSteps = steps;
        }
        
        if (maxPoint < calories) {
            maxPoint = calories;
        }
        if (maxCalories < calories) {
            maxCalories = calories;
        }
    }
    
    // ====== curve ======
    CGFloat curveHeight = 0.3f * boundsHeight;
    
//    CGRect curveRect = CGRectMake(0, 0, boundsWidth, curveHeight);
    __block NSMutableSet *mSetOfExceptions = [NSMutableSet new];
    __block NSInteger nullsInOrder = 0;
    
    __block NSInteger swoppsTotal = 0, stepsTotal = 0, callorisTotal = 0;
    
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSInteger swoops = 0, steps = 0, calories = 0;
        swoops = [[obj valueForKey:@"swoops"] integerValue];
        steps = [[obj valueForKey:@"steps"] integerValue];
        calories = [[obj valueForKey:@"calories"] integerValue];
        
        swoppsTotal += swoops;
        stepsTotal += steps;
        callorisTotal += calories;
        
        if (swoops == 0 && steps == 0 && calories == 0) {
            nullsInOrder++;
        } else {
            nullsInOrder = 0;
        }
        
        if (nullsInOrder > 1) {
            if (![mSetOfExceptions containsObject:@(idx - 1)]) {
                [mSetOfExceptions addObject:@(idx - 1)];
            }
            if (![mSetOfExceptions containsObject:@(idx)]) {
                [mSetOfExceptions addObject:@(idx)];
            }
        }
        
    }];
    
    // ====== graph ======
    CGFloat graphWidth = boundsWidth;
    CGFloat graphHeight = 0.78f * boundsHeight;
    CGFloat dayDiametr = graphWidth / 7.0f - graphWidth / 7.0f * 0.1f;
    _dayRadius = dayDiametr/2;
    
    CGRect graphRect = CGRectInset(self.bounds, (boundsWidth - graphWidth) / 2, (boundsHeight - graphHeight) / 2);
    
    // ====== mid lines ======
    CGFloat midWidth = 1.05f * graphWidth;
    CGFloat widthMidDif = midWidth - graphWidth;
    
    CGRect midRect = CGRectInset(graphRect, -widthMidDif/2, 0);
    midRect.origin.y += curveHeight;
    midRect.size.height -= dayDiametr + curveHeight;
//    CGFloat midHeight = CGRectGetHeight(midRect);
    
    // ====== annotation ======
    CGFloat annotationHeight = 0.1 * boundsHeight;
    
    CGRect annotationRect = CGRectMake(0, boundsHeight - annotationHeight, boundsWidth, annotationHeight);
    
    // **************** Calculate start points for days ***************
    CustomLine daysCenterLine;
    
    daysCenterLine.startPoint = CGPointMake(graphRect.origin.x, graphRect.origin.y + graphHeight - dayDiametr/2);
    daysCenterLine.endPoint = CGPointMake(graphRect.origin.x + graphWidth, graphRect.origin.y + graphHeight - dayDiametr/2);
    NSArray *arrayOfPoints = GetMidAmountPointsForLineAndItsAngle(7, daysCenterLine, GetAngleOfLine(daysCenterLine));
    _arrayOfCenterPoints = arrayOfPoints;
    
    // **************** Calculate lines minimum positions *****************
    CGFloat linesStartHeight = graphRect.origin.y + graphHeight - dayDiametr;
    CGFloat linesEndHeight = 0.5f * dayDiametr + linesStartHeight;
    CGFloat minLineHeight = linesEndHeight - linesStartHeight;
    
    // **************** Context *****************
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // **************** draw mid lines *****************
    
//    CGFloat firstMid = 0.0f, secondMid = 0.0f, thirdMid = 0.0f;
//    NSInteger elementsAmount = [CMStepCounter isStepCountingAvailable] ? 3 : 2;
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
//    NSInteger maxStepsPoint = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
//    
//    CGFloat difference = (CGFloat)maxSteps / (CGFloat)maxStepsPoint;
//    if ( difference > 1.0f && difference < 2.0f) {
//        secondMid = FloatPartOfNumber(difference);
//    } else if (difference >= 2.0f) {
//        secondMid = 0;
//    } else {
//        secondMid = 1.0;
//    }
//    
//    if (self.week.count > 0 && maxPoint > 0) {
//        firstMid = totalSwoops / self.week.count / maxSwoops;
//        thirdMid = totalCalories / self.week.count / maxCalories;
//    }
//    
//    NSArray *arrayOfMidProgresses;
//    NSArray *arrayOfMidColors;
//    if ([CMStepCounter isStepCountingAvailable]) {
//        arrayOfMidProgresses = [[NSArray alloc] initWithObjects:@(firstMid), @(secondMid), @(thirdMid), nil];
//        arrayOfMidColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorSteps, self.colorCalories, nil];
//    } else {
//        arrayOfMidProgresses = [[NSArray alloc] initWithObjects:@(firstMid), @(thirdMid), nil];
//        arrayOfMidColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorCalories, nil];
//    }
//    
//    
//    for (NSInteger i=0; i<elementsAmount; i++) {
//        UIColor *strikeColor = ((UIColor *)[arrayOfMidColors objectAtIndex:i]);
//        CGFloat progress = [[arrayOfMidProgresses objectAtIndex:i] floatValue];
//        CGFloat midStartY = midRect.origin.y + midHeight - (midHeight - dayDiametr) * progress;
//        UIBezierPath *linePath = [UIBezierPath bezierPath];
//        [linePath moveToPoint:CGPointMake(midRect.origin.x, midStartY)];
//        [linePath addLineToPoint:CGPointMake(midRect.origin.x + midWidth, midStartY)];
//        
//        CGContextAddPath(context, linePath.CGPath);
//        CGContextSetStrokeColorWithColor(context, strikeColor.CGColor);
//        CGContextSetLineWidth(context, 1.0f);
//        
//        NSInteger amount = 150;
//        size_t num_locations = amount;
//        CGFloat dashLength = midWidth / amount;
//        CGFloat lengths[amount];
//        for (NSInteger i = 0; i < amount; i++) {
//            lengths[i] = dashLength;
//        }
//        
//        CGContextSetLineDash(context, 1.0f, lengths, num_locations);
//        CGContextStrokePath(context);
//        num_locations = 0;
//        CGContextSetLineDash(context, 0.0, nil, num_locations);
//    }
    
    // **************** Drawing Curves ******************
//    // ==== set max Values ====
//    NSArray *arrayOfMaxValues;
//    if ([CMStepCounter isStepCountingAvailable]) {
//        arrayOfMaxValues = [[NSArray alloc] initWithObjects:@(maxSwoops),@(maxSteps),@(maxCalories), nil];
//    } else {
//        arrayOfMaxValues = [[NSArray alloc] initWithObjects:@(maxSwoops),@(maxCalories), nil];
//    }
//    for (NSInteger j=0; j<elementsAmount; j++) {
//        UIColor *color = (UIColor *)[arrayOfMidColors objectAtIndex:j];
//        NSMutableArray *mArrayOfCurvePoints = [NSMutableArray new];
//        
//        CGFloat pointDistance = boundsWidth / 6;
//        CGPoint curveStart;
//        CGFloat curveLineWidth = 0.5f;
//        
//        // === find points for curves and save ===
//        for (NSInteger i=0; i<7; i++) {
//            id obj;
//            NSInteger swoops = 0, steps = 0, calories = 0;
//            
//            if (i < self.week.count) {
//                obj = [self.week objectAtIndex:i];
//                swoops = [[obj valueForKey:@"swoops"] integerValue];
//                steps = [[obj valueForKey:@"steps"] integerValue];
//                calories = [[obj valueForKey:@"calories"] integerValue];
//            }
//            
//            NSArray *arrayOfDayValues;
//            if ([CMStepCounter isStepCountingAvailable]) {
//                arrayOfDayValues = [[NSArray alloc] initWithObjects: @(swoops), @(steps), @(calories), nil];
//            } else {
//                arrayOfDayValues = [[NSArray alloc] initWithObjects: @(swoops), @(calories), nil];
//            }
//            CGFloat y = curveRect.origin.y + curveLineWidth/2 + curveHeight - curveHeight * ([[arrayOfDayValues objectAtIndex:j] integerValue] / ([[arrayOfMaxValues objectAtIndex:j] floatValue] + 0.000001));
//            curveStart.x = curveRect.origin.x + pointDistance*i;
//            curveStart.y = y;
//            AddPointToArray(curveStart, mArrayOfCurvePoints);
//        }
//        // === get points and draw curves ===
//        UIBezierPath *curveBezier = [UIBezierPath bezierPath];
//        for (NSInteger i=1; i<7; i++) {
//            CustomLine line;
//            line.startPoint = GetPointFromArrayAtIndex(mArrayOfCurvePoints,i - 1);
//            line.endPoint = GetPointFromArrayAtIndex(mArrayOfCurvePoints,i);
//            [curveBezier moveToPoint:line.startPoint];
//            NSArray *arrayForControlPoints = GetMidAmountPointsForLineAndItsAngle(2,line, GetAngleOfLine(line));
//            CGPoint cp1 = GetPointFromArrayAtIndex(arrayForControlPoints, 0);
//            cp1.y = line.startPoint.y;
//            CGPoint cp2 = GetPointFromArrayAtIndex(arrayForControlPoints,1);
//            cp2.y = line.endPoint.y;
//            [curveBezier addCurveToPoint:line.endPoint controlPoint1:cp1 controlPoint2:cp2];
//        }
//        
//        CGContextAddPath(context, curveBezier.CGPath);
//        CGContextSetStrokeColorWithColor(context, color.CGColor);
//        CGContextSetLineWidth(context, curveLineWidth);
//        CGContextStrokePath(context);
//        
//        // ============ draw additional curves ============
////        NSInteger addCurvesAmount = 20;
////        NSMutableArray *mArrayOfChangedPoints = [NSMutableArray new];
////        AddObjectsFromArrayToArray(mArrayOfCurvePoints, mArrayOfChangedPoints);
////        for (NSInteger i = 0; i < addCurvesAmount; i++) {
////            const CGFloat* components = CGColorGetComponents(color.CGColor);
////            UIColor *newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:1.0 - 1.0/addCurvesAmount*i];
////            UIBezierPath *newCurveBezier = [UIBezierPath bezierPath];
////
////            for (NSInteger a = 1; a < 7; a++) {
////                CustomLine line;
////                line.startPoint = GetPointFromArrayAtIndex(mArrayOfChangedPoints,a + 7*i - 1);
////                line.endPoint = GetPointFromArrayAtIndex(mArrayOfChangedPoints,a + 7*i);
////                
////                // == changing points ==
////                if (a+7*i == 7*i+1) {
////                    line.startPoint.x += RandomFloatBetweenSmallAndBig(0.0, 1.0);
////                    line.startPoint.y += RandomFloatBetweenSmallAndBig(0.5, 1.5);
////                    AddPointToArray(line.startPoint, mArrayOfChangedPoints);
////                }
////                
////                line.endPoint.x += RandomFloatBetweenSmallAndBig(0.0, 1.5);
////                line.endPoint.y += RandomFloatBetweenSmallAndBig(0.0, 1.5);
////                AddPointToArray(line.endPoint, mArrayOfChangedPoints);
////                
////                [newCurveBezier moveToPoint:line.startPoint];
////                NSArray *arrayForControlPoints = GetMidAmountPointsForLineAndItsAngle(2,line, GetAngleOfLine(line));
////                CGPoint cp1 = GetPointFromArrayAtIndex(arrayForControlPoints, 0);
////                cp1.y = line.startPoint.y;
////                CGPoint cp2 = GetPointFromArrayAtIndex(arrayForControlPoints,1);
////                cp2.y = line.endPoint.y;
////                [newCurveBezier addCurveToPoint:line.endPoint controlPoint1:cp1 controlPoint2:cp2];
////            }
////
////            CGContextAddPath(context, newCurveBezier.CGPath);
////            CGContextSetStrokeColorWithColor(context, newColor.CGColor);
////            CGContextSetLineWidth(context, 0.5f);
////            CGContextStrokePath(context);
////        }
//        
//        NSInteger addCurvesAmount = 20;
//        NSMutableArray *mArrayOfChangesToPoints = [NSMutableArray new];
//        
//        for (NSInteger i = 0; i < addCurvesAmount*7; i++) {
//            CGFloat x = 0.0f, y = 0.0f;
//            if (i < 7) {
//                if (![mSetOfExceptions containsObject:@(i)]) {
//                    x += RandomFloatBetweenSmallAndBig(0.1, 1.0);
//                    y += RandomFloatBetweenSmallAndBig(0.5, 1.5);
//                }
//            } else {
//                NSDictionary *previousChange = (NSDictionary *)[mArrayOfChangesToPoints objectAtIndex:i-7];
//                if ([[previousChange valueForKey:@"x"] floatValue] > 0 || [[previousChange valueForKey:@"y"] floatValue] > 0) {
//                    x += RandomFloatBetweenSmallAndBig(0.0, 1.0) + [[previousChange valueForKey:@"x"] floatValue];
//                    y += RandomFloatBetweenSmallAndBig(0.5, 1.5) + [[previousChange valueForKey:@"y"] floatValue];
//                }
//            }
//            NSDictionary *change = @{@"x":@(x), @"y":@(y)};
//            [mArrayOfChangesToPoints addObject:change];
//        }
//        
//        for (NSInteger i = 0; i<addCurvesAmount; i++) {
//            const CGFloat* components = CGColorGetComponents(color.CGColor);
//            UIColor *newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:1.0 - 1.0/addCurvesAmount*i];
//            UIBezierPath *newCurveBezier = [UIBezierPath bezierPath];
//            
//            for (NSInteger a = 1; a < 7; a++) {
//                CustomLine line;
//                line.startPoint = GetPointFromArrayAtIndex(mArrayOfCurvePoints,a - 1);
//                line.endPoint = GetPointFromArrayAtIndex(mArrayOfCurvePoints,a);
//                
//                // == changing points ==
//                line.startPoint.x += [[(NSDictionary *)[mArrayOfChangesToPoints objectAtIndex: a - 1 + i*7] valueForKey:@"x"] floatValue];
//                line.startPoint.y += [[(NSDictionary *)[mArrayOfChangesToPoints objectAtIndex: a - 1 + i*7] valueForKey:@"y"] floatValue];
//                
//                line.endPoint.x += [[(NSDictionary *)[mArrayOfChangesToPoints objectAtIndex: a + i*7] valueForKey:@"x"] floatValue];
//                line.endPoint.y += [[(NSDictionary *)[mArrayOfChangesToPoints objectAtIndex: a + i*7] valueForKey:@"y"] floatValue];
//                
//                [newCurveBezier moveToPoint:line.startPoint];
//                NSArray *arrayForControlPoints = GetMidAmountPointsForLineAndItsAngle(2,line, GetAngleOfLine(line));
//                CGPoint cp1 = GetPointFromArrayAtIndex(arrayForControlPoints, 0);
//                cp1.y = line.startPoint.y;
//                CGPoint cp2 = GetPointFromArrayAtIndex(arrayForControlPoints,1);
//                cp2.y = line.endPoint.y;
//                [newCurveBezier addCurveToPoint:line.endPoint controlPoint1:cp1 controlPoint2:cp2];
//            }
//            CGContextAddPath(context, newCurveBezier.CGPath);
//            CGContextSetStrokeColorWithColor(context, newColor.CGColor);
//            CGContextSetLineWidth(context, 0.5f);
//            CGContextStrokePath(context);
//        }
//    }
    
    // **************** Draw Progress Lines, Days and Percentage Circles *****************
    for (NSInteger i=0; i < 7; i++) {
        NSInteger swoops = 0, steps = 0, calories = 0;
        
        id obj;
        if (i < self.week.count) {
            obj = [self.week objectAtIndex:i];
            swoops = [[obj valueForKey:@"swoops"] integerValue];
            steps = [[obj valueForKey:@"steps"] integerValue];
            calories = [[obj valueForKey:@"calories"] integerValue];
        }
        
        CGPoint dayCenterPoint = GetPointFromArrayAtIndex(arrayOfPoints, i);
        CGFloat whiteLineWidth = 0.1f * dayDiametr;
        CGFloat linesWidth = dayDiametr - whiteLineWidth;
        NSInteger linesAmount;
        CGFloat eachLineWidth;
        CGFloat spacing;
        NSArray *arrayOfProgresses;
        NSArray *arrayOfColors;
        
        CGFloat linesStartX = dayCenterPoint.x - linesWidth/2;
        
        // === calculate line progresses here ===
        
        CGFloat firstLineProgress = 0.0f, secondLindeProgress = 0.0f, thirdLineProgress = 0.0f;
        
        firstLineProgress = swoops / maxSwoops;
        secondLindeProgress = steps / maxSteps;
        thirdLineProgress = calories / maxCalories;
        
        // == check if steps progress line is needed ==
        
//        if ([CMStepCounter isStepCountingAvailable]) {
//        } else {
//            linesAmount = 2;
//            arrayOfProgresses = [[NSArray alloc] initWithObjects:@(firstLineProgress),@(thirdLineProgress), nil];
//            arrayOfColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorCalories, nil];
//        }
        linesAmount = 3;
        arrayOfProgresses = [[NSArray alloc] initWithObjects:@(firstLineProgress),@(secondLindeProgress), @(thirdLineProgress), nil];
        arrayOfColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorSteps, self.colorCalories, nil];
        spacing = 0.15*linesWidth;
        eachLineWidth = (linesWidth - spacing * (3 - 1)) / 3;
        
        // === three lines drawing ====
        for (NSInteger j=0; j<linesAmount; j++) {
            NSInteger mul = (j==0) ? 0 : 1;
            CGFloat progressHeight = (graphHeight - dayDiametr - curveHeight) * [[arrayOfProgresses objectAtIndex:j] floatValue];
            CGRect rect = CGRectMake(linesStartX + eachLineWidth*j + mul*spacing*j, linesStartHeight - progressHeight, eachLineWidth, progressHeight + minLineHeight);
            UIBezierPath *lineBezier = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(eachLineWidth/2, eachLineWidth/2)];
            
            CGContextAddPath(context, lineBezier.CGPath);
            UIColor *fillColor = ((UIColor *)[arrayOfColors objectAtIndex:j]);
            
            NSDate *dayDate = [obj valueForKey:@"date"];
            if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                const CGFloat *colorComponents = CGColorGetComponents(fillColor.CGColor);
                fillColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
            }
            
            CGContextSetFillColorWithColor(context, fillColor.CGColor);
            CGContextSetLineWidth(context, 0.0f);
            CGContextFillPath(context);
        }
        
        // === day drawing ===
        UIColor *circleColor;
        if (obj) {
            NSDate *dayDate = [obj valueForKey:@"date"];
            if ([SNAdditionalMath compareDate:dayDate withDate:_currentDate] == NSOrderedSame) {
                circleColor = self.colorCurrentDayText;
            } else {
                circleColor = self.colorDayCircle;
            }
        } else {
            circleColor = self.colorDayCircle;
        }
        
        CGFloat radius = dayDiametr/2 - whiteLineWidth/2;
        UIBezierPath *dayPath = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                               radius:radius
                                                           startAngle:RadiansFromDegrees(0)
                                                             endAngle:RadiansFromDegrees(360)
                                                            clockwise:YES];
        CGContextAddPath(context, dayPath.CGPath);
        CGContextSetFillColorWithColor(context, circleColor.CGColor);
        CGContextSetStrokeColorWithColor(context, self.backgroundColor.CGColor);
        CGContextSetLineWidth(context, whiteLineWidth);
        CGContextDrawPath(context, kCGPathFillStroke);
        
        // === percentage circle drawing ===
        CGFloat startAngle = - 90;
        if (obj) {
            NSDate *dayDate = [obj valueForKey:@"date"];
            UIColor *drawColor;
            if ((swoops == 0 && steps == 0) || steps == 0) {
                UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                      radius:radius - whiteLineWidth
                                                                  startAngle:RadiansFromDegrees(startAngle)
                                                                    endAngle:RadiansFromDegrees(360)
                                                                   clockwise:YES];
                drawColor = self.colorSwoops;
                if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                    const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                    drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
                }
                CGContextAddPath(context, circle.CGPath);
                CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
                CGContextSetLineWidth(context, whiteLineWidth);
                CGContextDrawPath(context, kCGPathStroke);
                
            } else {
                CGFloat swoopEndAngle = 360 * [[obj valueForKey:@"swoppsPercentage"] floatValue] + startAngle;
                UIBezierPath *swoopPercentage = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                               radius:radius - whiteLineWidth
                                                                           startAngle:RadiansFromDegrees(startAngle)
                                                                             endAngle:RadiansFromDegrees(swoopEndAngle)
                                                                            clockwise:YES];
                
                UIBezierPath *stepsPercentage = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                               radius:radius - whiteLineWidth
                                                                           startAngle:RadiansFromDegrees(swoopEndAngle)
                                                                             endAngle:RadiansFromDegrees(startAngle - 0.00001)
                                                                            clockwise:YES];
                
                CGContextAddPath(context, swoopPercentage.CGPath);
                
                drawColor = self.colorSwoops;
                if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                    const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                    drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
                }
                
                CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
                CGContextSetLineWidth(context, whiteLineWidth);
                CGContextDrawPath(context, kCGPathStroke);
                
                if (steps > 0) {
                    drawColor = self.colorSteps;
                    if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                        const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                        drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
                    }
                    
                    CGContextAddPath(context, stepsPercentage.CGPath);
                    CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
                    CGContextSetLineWidth(context, whiteLineWidth);
                    CGContextDrawPath(context, kCGPathStroke);
                }
            }
        } else {
            NSDate *dayDate = [obj valueForKey:@"date"];
            UIColor *drawColor;
            UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                  radius:radius - whiteLineWidth
                                                              startAngle:RadiansFromDegrees(startAngle)
                                                                endAngle:RadiansFromDegrees(360)
                                                               clockwise:YES];
            drawColor = self.colorSwoops;
            if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                const CGFloat *colorComponents = CGColorGetComponents(drawColor.CGColor);
                drawColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
            }
            CGContextAddPath(context, circle.CGPath);
            CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
            CGContextSetLineWidth(context, whiteLineWidth);
            CGContextDrawPath(context, kCGPathStroke);
        }
        
        
        // === text drawing ===
        UIFont* font = _textFont;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EEEE, d, MMMM"];
        [dateFormat setLocale:[SNAppDelegate currentLocale]];
        NSString *text = @"";
        
        UIColor *textColor;
        if (obj) {
            NSDate *dayDate = [obj valueForKey:@"date"];
            NSString *dayName = [dateFormat stringFromDate:dayDate];
            text = [dayName substringToIndex:2];
            if ([SNAdditionalMath compareDate:dayDate withDate:_currentDate] == NSOrderedSame) {
                textColor = [UIColor whiteColor];
            } else if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
                const CGFloat *colorComponents = CGColorGetComponents(self.colorText.CGColor);
                textColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
            } else {
                textColor = self.colorText;
            }
        } else {
            const CGFloat *colorComponents = CGColorGetComponents(self.colorText.CGColor);
            textColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.2];
        }
        NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
        NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
        CGSize size = [text sizeWithAttributes:stringAttrs];
        CGPoint pointOfDrawing = CGPointMake(dayCenterPoint.x - size.width/2, dayCenterPoint.y - size.height/2);
        [attrStr drawAtPoint:pointOfDrawing];
    }
    
    // ****************** Draw Annotation *********************
    // === get annotation max width and height ===
    NSArray *arrayOfAnnotations;
    NSArray *textColors;
    NSInteger elements = [CMStepCounter isStepCountingAvailable] ? 3 : 2;
    NSString *swopps = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:swoppsTotal], self.annotationSwoops];
    NSString *steps = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:stepsTotal], self.annotationSteps];
    NSString *callories = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:callorisTotal], self.annotationCalories];
    if ([CMStepCounter isStepCountingAvailable]) {
        arrayOfAnnotations = [[NSArray alloc] initWithObjects:swopps, steps, callories, nil];
        textColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorSteps, self.colorCalories, nil];
    } else {
        arrayOfAnnotations = [[NSArray alloc] initWithObjects:swopps, callories, nil];
        textColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorCalories, nil];
    }
    
    // calculate positions
    NSMutableArray *mArrayOfSizesForAnn = [NSMutableArray new];
    NSMutableArray *mArrayOfAttrStrs = [NSMutableArray new];
    CGFloat sumOfSizesWidth = 0;
    CGFloat textMarginLeft = 2.0f;
    CGFloat textMarginRight = 10.0f;
    UIFont* font = self.annotationFont;
    for (NSInteger i=0; i<elements; i++) {
        NSString *text = [arrayOfAnnotations objectAtIndex:i];
        NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName : self.colorText};
        NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
        
        [mArrayOfAttrStrs addObject:attrStr];
        
        CGSize size = [text sizeWithAttributes:stringAttrs];
        sumOfSizesWidth += size.height + size.width + textMarginLeft + textMarginRight;
        [mArrayOfSizesForAnn addObject:[NSValue valueWithCGSize:size]];
    }
    CGFloat startX = (annotationRect.size.width - sumOfSizesWidth)/2;
    // === draw ===
    for (NSInteger i=0; i<elements; i++) {
        // = example =
        CGRect exampleRect = CGRectZero;
        UIColor *strikeColor = ((UIColor *)[textColors objectAtIndex:i]);
        
        CGFloat deltaX = 0;
        for (NSInteger j = 0; j < i; j++) {
            CGSize size = [((NSValue *)[mArrayOfSizesForAnn objectAtIndex:j]) CGSizeValue];
            deltaX += size.height + size.width + textMarginLeft + textMarginRight;
        }
        CGSize size = [((NSValue *)[mArrayOfSizesForAnn objectAtIndex:i]) CGSizeValue];
        
        exampleRect.origin.x = startX + deltaX;
        exampleRect.origin.y = annotationRect.origin.y + annotationRect.size.height/2 - size.height/2;
        exampleRect.size.width = size.height;
        exampleRect.size.height = size.height;
        
        UIBezierPath *examplePath = [UIBezierPath bezierPathWithOvalInRect:exampleRect];
        
        CGContextSetFillColorWithColor(context, strikeColor.CGColor);
        CGContextAddPath(context, examplePath.CGPath);
        CGContextSetLineWidth(context, 1.0f);
        CGContextFillPath(context);
        
        // = text =
        NSAttributedString* attrStr = (NSAttributedString *)[mArrayOfAttrStrs objectAtIndex:i];
        [attrStr drawInRect:CGRectMake(exampleRect.origin.x + size.height + textMarginLeft, exampleRect.origin.y, size.width, size.height)];
    }

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setNeedsDisplay];
}

- (void)animateChangeToNext:(BOOL)next
{
    UIView *viewToRender = self;
    
    UIGraphicsBeginImageContextWithOptions(viewToRender.bounds.size, YES, 0);
    //    [viewToRender drawViewHierarchyInRect:viewToRender.bounds afterScreenUpdates:YES];
    [viewToRender.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageView.image = image;
    
    [self addSubview:imageView];
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 0.5;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName: (next) ? kCAMediaTimingFunctionEaseIn : kCAMediaTimingFunctionEaseOut];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    if (next) {
        normalForm = CATransform3DTranslate(normalForm, -CGRectGetWidth(self.bounds), 0.0, 0.0);
    } else {
        normalForm = CATransform3DTranslate(normalForm, CGRectGetWidth(self.bounds), 0.0, 0.0);
    }
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    [CATransaction setCompletionBlock:^{
        [imageView removeFromSuperview];
    }];
    [CATransaction begin];
    
    [imageView.layer addAnimation:transformAnimation forKey:@"animationTransition"];
    
    [CATransaction commit];
}

- (void)animateStopOfChangeToNext:(BOOL)next
{
    __block CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 0.25;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName: (next) ? kCAMediaTimingFunctionEaseIn : kCAMediaTimingFunctionEaseOut];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    __block CATransform3D normalForm = CATransform3DIdentity;
    if (next) {
        normalForm = CATransform3DTranslate(normalForm, -CGRectGetWidth(self.bounds)*0.25, 0.0, 0.0);
    } else {
        normalForm = CATransform3DTranslate(normalForm, CGRectGetWidth(self.bounds)*0.25, 0.0, 0.0);
    }
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    [CATransaction setCompletionBlock:^{
        normalForm = CATransform3DIdentity;
        normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
        transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
        [CATransaction setAnimationDuration:0.25];
        [CATransaction setCompletionBlock:^{
            [self.layer removeAllAnimations];
        }];
        [CATransaction begin];
        
        [self.layer addAnimation:transformAnimation forKey:@"animationTransitionBack"];
        
        [CATransaction commit];
        
    }];
    [CATransaction begin];
    
    [self.layer addAnimation:transformAnimation forKey:@"animationTransitionForward"];
    
    [CATransaction commit];
}

#pragma mark - Gesture Recognizer Handler
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    __block NSInteger dayIndex = 0;
    __block BOOL found = NO;
    [_arrayOfCenterPoints enumerateObjectsUsingBlock:^(NSValue *vPoint, NSUInteger idx, BOOL *stop) {
        CGPoint center = [vPoint CGPointValue];
        CGRect rectOfDay = CGRectMake(center.x - _dayRadius, self.bounds.origin.y, _dayRadius*2, CGRectGetHeight(self.bounds));
        if (CGRectContainsPoint(rectOfDay, point)) {
            found = YES;
            dayIndex = idx;
            *stop = YES;
        }
    }];
    
    if (found) {
        self.currentDate = (NSDate *)[[self.week objectAtIndex:dayIndex] valueForKey:@"date"];
        [self.delegate summaryChartView:self changedCurrentDate:self.currentDate];
    }
}

- (void)panGestureHandler:(UIPanGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _startPoint = point;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_startPoint.x < point.x) {
                NSArray *source = [self.dataSource summaryChartView:self willChangeWeekForward:NO];
                if (source) {
                    [self animateChangeToNext:NO];
                    self.week = source;
                } else {
                    [self animateStopOfChangeToNext:NO];
                }
            } else {
                NSArray *source = [self.dataSource summaryChartView:self willChangeWeekForward:YES];
                if (source) {
                    [self animateChangeToNext:YES];
                    self.week = source;
                } else {
                    [self animateStopOfChangeToNext:YES];
                }
            }
            
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Property Accessors
- (void)setColorCalories:(UIColor *)colorCalories
{
    _colorCalories = colorCalories;
    [self setNeedsDisplay];
}

- (void)setColorSteps:(UIColor *)colorSteps
{
    _colorSteps = colorSteps;
    [self setNeedsDisplay];
}

- (void)setColorSwoops:(UIColor *)colorSwoops
{
    _colorSwoops = colorSwoops;
    [self setNeedsDisplay];
}

- (void)setColorDayCircle:(UIColor *)colorDayCircle
{
    _colorDayCircle = colorDayCircle;
    [self setNeedsDisplay];
}

- (void)setColorText:(UIColor *)colorText
{
    _colorText = colorText;
    [self setNeedsDisplay];
}

- (void)setColorCurrentDayText:(UIColor *)colorCurrentDayText
{
    _colorCurrentDayText = colorCurrentDayText;
    [self setNeedsDisplay];
}

- (void)setAnnotationCalories:(NSString *)annotationCalories
{
    _annotationCalories = annotationCalories;
    [self setNeedsDisplay];
}

- (void)setAnnotationSteps:(NSString *)annotationSteps
{
    _annotationSteps = annotationSteps;
    [self setNeedsDisplay];
}

- (void)setAnnotationSwoops:(NSString *)annotationSwoops
{
    _annotationSwoops = annotationSwoops;
    [self setNeedsDisplay];
}

- (void)setAnnotationFont:(UIFont *)annotationFont
{
    _annotationFont = annotationFont;
    [self setNeedsDisplay];
}

- (void)setTextFont:(UIFont *)textFont
{
    _textFont = textFont;
    [self setNeedsDisplay];
}

- (void)setWeek:(NSArray *)week
{
    __block BOOL objectDoesntConfirmProtocol = NO;
    [week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj conformsToProtocol:@protocol(SNSummaryChartProtocol)]) {
            objectDoesntConfirmProtocol = YES;
            *stop = YES;
        }
    }];
    
    if (objectDoesntConfirmProtocol) {
        NSLog(@"\n%s\nOne or more objects doesn`t confirm to protocol!\n",__FUNCTION__);
        return;
    }
    _week = week;
    [self.delegate summaryChartView:self didChangeWeek:self.week];
    [self setNeedsDisplay];
}

- (void)setCurrentDate:(NSDate *)currentDate
{
    if (currentDate) {
        _currentDate = currentDate;
    }
    [self setNeedsDisplay];
}


@end
