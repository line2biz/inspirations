

#import "DMSession.h"

FOUNDATION_EXTERN NSString * const DMSessionStartTimeKey;

@interface DMSession (Category)

@property (assign, readonly, nonatomic, getter=caloriesPerHour) CGFloat caloriesPerHour;

+ (DMSession *)trainingSesstionByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;

- (NSUInteger)countsForSession;
- (CGFloat)countsPerMinute;
- (CGFloat)countsPerSecond;
- (CGFloat)caloriesPerSession;
- (CGFloat)caloriesPerHour;

/// calculates amount of additional calories that were spent for time in seconds with some MET
+ (CGFloat)getAdditionalKcalByMBRforMET:(CGFloat)MET andTime:(NSTimeInterval)timeInterval;

@end
