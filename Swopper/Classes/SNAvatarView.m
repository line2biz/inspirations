

#import "SNAvatarView.h"

#import "UIColor+Customization.h"

@interface SNAvatarView ()
{
    CAShapeLayer *_backgrondLayer;
    CALayer *_imageLayer;
}
@end

@implementation SNAvatarView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.lineWidth = 1.f;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!_backgrondLayer) {
        _backgrondLayer = [CAShapeLayer new];
        
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
        _backgrondLayer.path = [path CGPath];
        _backgrondLayer.fillColor = [[UIColor clearColor] CGColor];
        _backgrondLayer.strokeColor = [[UIColor customLightGrayColor] CGColor];
        _backgrondLayer.lineWidth = self.lineWidth;
        [self.layer addSublayer:_backgrondLayer];
    }
    
    _backgrondLayer.frame = self.bounds;
    
    if (!_imageLayer) {
        
        CAShapeLayer *mask = [CAShapeLayer new];
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectInset(self.bounds, 2, 2)];
        mask.path = [path CGPath];
        mask.fillColor = [[UIColor blackColor] CGColor];
        mask.frame = self.bounds;
        
        _imageLayer = [CALayer new];
        _imageLayer.mask = mask;
        _imageLayer.backgroundColor = [[UIColor clearColor] CGColor];
        _imageLayer.contentsGravity = kCAGravityResizeAspectFill;
        [self.layer addSublayer:_imageLayer];
    
    }
    _imageLayer.frame = self.bounds;
    
    [self updateProperties];
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    [self updateProperties];
}

- (void)updateProperties
{
    if (_imageLayer) {
        _imageLayer.contents = (__bridge id)[self.image CGImage];
    }
}

@end
