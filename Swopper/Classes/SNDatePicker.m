

#import "SNDatePicker.h"

const NSUInteger NUM_COMPONENTS = 3;

typedef enum {
    kSNDatePickerInvalid = 0,
    kSNDatePickerYear,
    kSNDatePickerMonth,
    kSNDatePickerDay
} SNDatePickerComponent;

@interface SNDatePicker () <UIPickerViewDataSource, UIPickerViewDelegate> {
    SNDatePickerComponent _components[NUM_COMPONENTS];
}

@property (nonatomic, strong, readwrite) NSCalendar *calendar;
@property (nonatomic, weak, readwrite) UIPickerView *picker;

@property (nonatomic, strong, readwrite) NSDateFormatter *dateFormatter;

@property (nonatomic, strong, readwrite) NSDateComponents *currentDateComponents;

@property (nonatomic, strong, readwrite) UIFont *font;

@end

@implementation SNDatePicker

#pragma mark - Life cycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    self.tintColor = [UIColor whiteColor];
    self.font = [UIFont systemFontOfSize:23.0f];
    
    self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [self setLocale:[SNAppDelegate currentLocale]];
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:self.bounds];
    picker.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    picker.dataSource = self;
    picker.delegate = self;
    picker.tintColor = [UIColor whiteColor];
    
    self.date = [NSDate date];
    
    [self addSubview:picker];
    self.picker = picker;
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(320.0f, 216.0f);
}

#pragma mark - Setup

- (void)setMinimumDate:(NSDate *)minimumDate
{
    _minimumDate = minimumDate;
    
    [self updateComponents];
}

- (void)setMaximumDate:(NSDate *)maximumDate
{
    _maximumDate = maximumDate;
    
    [self updateComponents];
}

- (void)setDate:(NSDate *)date
{
    [self setDate:date animated:NO];
}

- (void)setDate:(NSDate *)date animated:(BOOL)animated
{
    self.currentDateComponents = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                  fromDate:date];
    
    [self.picker reloadAllComponents];
    
    [self setIndicesAnimated:YES];
}

- (NSDate *)date
{
    return [self.calendar dateFromComponents:self.currentDateComponents];
}

- (void)setLocale:(NSLocale *)locale
{
    self.calendar.locale = locale;
    
    [self updateComponents];
}

- (SNDatePickerComponent)componentFromLetter:(NSString *)letter
{
    if ([letter isEqualToString:@"y"]) {
        return kSNDatePickerYear;
    }
    else if ([letter isEqualToString:@"m"]) {
        return kSNDatePickerMonth;
    }
    else if ([letter isEqualToString:@"d"]) {
        return kSNDatePickerDay;
    }
    else {
        return kSNDatePickerInvalid;
    }
}

- (SNDatePickerComponent)thirdComponentFromFirstComponent:(SNDatePickerComponent)component1
                                       andSecondComponent:(SNDatePickerComponent)component2
{
    NSMutableIndexSet *set = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(kSNDatePickerInvalid + 1, NUM_COMPONENTS)];
    [set removeIndex:component1];
    [set removeIndex:component2];
    
    return (SNDatePickerComponent) [set firstIndex];
}

- (void)updateComponents
{
    NSString *componentsOrdering = [NSDateFormatter dateFormatFromTemplate:@"yMMMMd" options:0 locale:self.calendar.locale];
    componentsOrdering = [componentsOrdering lowercaseString];
    
    NSString *firstLetter = [componentsOrdering substringToIndex:1];
    NSString *lastLetter = [componentsOrdering substringFromIndex:(componentsOrdering.length - 1)];
    
    _components[0] = [self componentFromLetter:firstLetter];
    _components[2] = [self componentFromLetter:lastLetter];
    _components[1] = [self thirdComponentFromFirstComponent:_components[0] andSecondComponent:_components[2]];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.calendar = self.calendar;
    self.dateFormatter.locale = self.calendar.locale;
    
    [self.picker reloadAllComponents];
    
    [self setIndicesAnimated:NO];
}

- (void)setIndexForComponentIndex:(NSUInteger)componentIndex animated:(BOOL)animated
{
    SNDatePickerComponent component = [self componentForIndex:componentIndex];
    NSRange unitRange = [self rangeForComponent:component];
    
    NSInteger value;
    
    if (component == kSNDatePickerYear) {
        value = self.currentDateComponents.year;
    }
    else if (component == kSNDatePickerMonth) {
        value = self.currentDateComponents.month;
    }
    else if (component == kSNDatePickerDay) {
        value = self.currentDateComponents.day;
    }
    else {
        assert(NO);
    }
    
    NSInteger index = (value - unitRange.location);
    NSInteger middleIndex = (INT16_MAX / 2) - (INT16_MAX / 2) % unitRange.length + index;
    
    [self.picker selectRow:middleIndex inComponent:componentIndex animated:animated];
}

- (void)setIndicesAnimated:(BOOL)animated
{
    for (NSUInteger componentIndex = 0; componentIndex < NUM_COMPONENTS; componentIndex++) {
        [self setIndexForComponentIndex:componentIndex animated:animated];
    }
}

- (SNDatePickerComponent)componentForIndex:(NSInteger)componentIndex
{
    return _components[componentIndex];
}

- (NSCalendarUnit)unitForComponent:(SNDatePickerComponent)component
{
    if (component == kSNDatePickerYear) {
        return NSYearCalendarUnit;
    }
    else if (component == kSNDatePickerMonth) {
        return NSMonthCalendarUnit;
    }
    else if (component == kSNDatePickerDay) {
        return NSDayCalendarUnit;
    }
    else {
        assert(NO);
    }
}

- (NSRange)rangeForComponent:(SNDatePickerComponent)component
{
    NSCalendarUnit unit = [self unitForComponent:component];
    
    return [self.calendar maximumRangeOfUnit:unit];
}

#pragma mark - Data source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)componentIndex
{
    return INT16_MAX;
}

#pragma mark - Delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)componentIndex
{
    SNDatePickerComponent component = [self componentForIndex:componentIndex];
    
    if (component == kSNDatePickerYear) {
        CGSize size = [@"0000" sizeWithAttributes:@{NSFontAttributeName : self.font}];
        
        return size.width + 25.0f;
    }
    else if (component == kSNDatePickerMonth) {
        CGFloat maxWidth = 0.0f;
        
        for (NSString *monthName in self.dateFormatter.monthSymbols) {
            CGFloat monthWidth = [monthName sizeWithAttributes:@{NSFontAttributeName : self.font}].width;
            
            maxWidth = MAX(monthWidth, maxWidth);
        }
        
        return maxWidth + 25.0f;
    }
    else if (component == kSNDatePickerDay) {
        ///TODO: Uncomment to show days in date picker
//        CGSize size = [@"00" sizeWithAttributes:@{NSFontAttributeName : self.font}];
        return 0;//size.width + 25.0f;
    }
    else {
        return 0.01f;
    }
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(SNDatePickerComponent)component
{
    NSRange unitRange = [self rangeForComponent:component];
    NSInteger value = unitRange.location + (row % unitRange.length);
    
    if (component == kSNDatePickerYear) {
        return [NSString stringWithFormat:@"%li", (long) value];
    }
    else if (component == kSNDatePickerMonth) {
        return [self.dateFormatter.monthSymbols objectAtIndex:(value - 1)];
    }
    else if (component == kSNDatePickerDay) {
        return [NSString stringWithFormat:@"%li", (long) value];
    }
    else {
        return @"";
    }
}

- (NSInteger)valueForRow:(NSInteger)row andComponent:(SNDatePickerComponent)component
{
    NSRange unitRange = [self rangeForComponent:component];
    
    return (row % unitRange.length) + unitRange.location;
}

- (BOOL)isEnabledRow:(NSInteger)row forComponent:(NSInteger)componentIndex
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year = self.currentDateComponents.year;
    dateComponents.month = self.currentDateComponents.month;
    dateComponents.day = self.currentDateComponents.day;
    
    SNDatePickerComponent component = [self componentForIndex:componentIndex];
    NSInteger value = [self valueForRow:row andComponent:component];
    
    if (component == kSNDatePickerYear) {
        dateComponents.year = value;
    }
    else if (component == kSNDatePickerMonth) {
        dateComponents.month = value;
    }
    else if (component == kSNDatePickerDay) {
        dateComponents.day = value;
    }
    
    NSDate *rowDate = [self.calendar dateFromComponents:dateComponents];
    
    if (self.minimumDate != nil && [self.minimumDate compare:rowDate] == NSOrderedDescending) {
        return NO;
    }
    else if (self.maximumDate != nil && [rowDate compare:self.maximumDate] == NSOrderedDescending) {
        return NO;
    }
    
    return YES;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)componentIndex reusingView:(UIView *)view
{
    UILabel *label;
    
    if ([view isKindOfClass:[UILabel class]]) {
        label = (UILabel *) view;
    }
    else {
        label = [[UILabel alloc] init];
        label.font = self.font;
    }
    
    SNDatePickerComponent component = [self componentForIndex:componentIndex];
    NSString *title = [self titleForRow:row forComponent:component];
    
    UIColor *color;
    
    BOOL enabled = [self isEnabledRow:row forComponent:componentIndex];
    
    if (enabled) {
        color = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:1.0];
    }
    else {
        color = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:0.5f];
    }
    
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title
                                                                          attributes:@{NSForegroundColorAttributeName: color}];
    
    label.attributedText = attributedTitle;
    
//    if (component == kSNDatePickerMonth) {
//        label.textAlignment = NSTextAlignmentLeft;
//    }
//    else {
//        label.textAlignment = NSTextAlignmentRight;
//    }
    
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componentIndex
{
    SNDatePickerComponent component = [self componentForIndex:componentIndex];
    NSInteger value = [self valueForRow:row andComponent:component];
    
    if (component == kSNDatePickerYear) {
        self.currentDateComponents.year = value;
    }
    else if (component == kSNDatePickerMonth) {
        self.currentDateComponents.month = value;
    }
    else if (component == kSNDatePickerDay) {
        self.currentDateComponents.day = value;
    }
    else {
        assert(NO);
    }
    
    [self setIndexForComponentIndex:componentIndex animated:NO];
    
    NSDate *datePicked = self.date;
    
    if (self.minimumDate != nil && [datePicked compare:self.minimumDate] == NSOrderedAscending) {
        [self setDate:self.minimumDate animated:YES];
        NSInteger selectedDay = [pickerView selectedRowInComponent:0];
        NSInteger selectedMonth = [pickerView selectedRowInComponent:1];
        NSInteger selectedYear = [pickerView selectedRowInComponent:2];
        NSArray *array = [[NSArray alloc] initWithObjects:@(selectedDay),@(selectedMonth),@(selectedYear), nil];
        
        for (NSInteger i=0; i<3; i++) {
            UILabel *newLabel = (UILabel *)[pickerView viewForRow:[[array objectAtIndex:i] integerValue] forComponent:i];
            if (newLabel && newLabel.attributedText.length) {
                NSAttributedString *attrTitle = [[NSAttributedString alloc] initWithString:[newLabel.attributedText string]
                                                                                attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0]}];
                newLabel.attributedText = attrTitle;
            }
        }
    }
    else if (self.maximumDate != nil && [datePicked compare:self.maximumDate] == NSOrderedDescending) {
        [self setDate:self.maximumDate animated:YES];
        NSInteger selectedDay = [pickerView selectedRowInComponent:0];
        NSInteger selectedMonth = [pickerView selectedRowInComponent:1];
        NSInteger selectedYear = [pickerView selectedRowInComponent:2];
        NSArray *array = [[NSArray alloc] initWithObjects:@(selectedDay),@(selectedMonth),@(selectedYear), nil];
        
        for (NSInteger i=0; i<3; i++) {
            UILabel *newLabel = (UILabel *)[pickerView viewForRow:[[array objectAtIndex:i] integerValue] forComponent:i];
            if (newLabel && newLabel.attributedText.length) {
                NSAttributedString *attrTitle = [[NSAttributedString alloc] initWithString:[newLabel.attributedText string]
                                                                                attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0]}];
                newLabel.attributedText = attrTitle;
            }
        }
    }
    else {
        [self.picker reloadAllComponents];
        NSInteger selectedDay = [pickerView selectedRowInComponent:0];
        NSInteger selectedMonth = [pickerView selectedRowInComponent:1];
        NSInteger selectedYear = [pickerView selectedRowInComponent:2];
        NSArray *array = [[NSArray alloc] initWithObjects:@(selectedDay),@(selectedMonth),@(selectedYear), nil];
        
        for (NSInteger i=0; i<3; i++) {
            UILabel *newLabel = (UILabel *)[pickerView viewForRow:[[array objectAtIndex:i] integerValue] forComponent:i];
            if (newLabel && newLabel.attributedText.length) {
                NSAttributedString *attrTitle = [[NSAttributedString alloc] initWithString:[newLabel.attributedText string]
                                                                                attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0]}];
                newLabel.attributedText = attrTitle;                
            }
        }
    }
    
    
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end