

#import <MediaPlayer/MediaPlayer.h>

@protocol SNMoviePlayerControllerProtocol;

@interface SNMoviePlayerController : MPMoviePlayerViewController

@property (strong, nonatomic) id<SNMoviePlayerControllerProtocol> delegate;

@end

@protocol SNMoviePlayerControllerProtocol <NSObject>
@optional
- (void)moviePlayerController:(SNMoviePlayerController *)controller didExitFullScreen:(BOOL)didExit;

@end
