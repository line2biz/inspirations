
#import <UIKit/UIKit.h>

FOUNDATION_EXTERN CGFloat const TrainingTimePeriodKey;
FOUNDATION_EXTERN CGFloat const SNMETDefaultValue;
//FOUNDATION_EXTERN CGFloat const CPMFactorValue;

FOUNDATION_EXTERN NSString * const CPMFactorValueKey;
FOUNDATION_EXTERN NSString * const CPMCalloriewsKey;
FOUNDATION_EXTERN NSString * const CPMFrequencyKey;
FOUNDATION_EXTERN CGFloat const SNAppSettingsMinSoundVolumeValue;
FOUNDATION_EXTERN NSString * const SNAppSecondLaucnKey;


FOUNDATION_EXTERN NSInteger const SNAppMaxNumberOfStepsPerDay;

@import CoreMotion;

@class DMUser, DMSession;

@protocol SNAppDelegateProtocol;

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic, readonly) CMMotionManager *motionManager;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DMSession *currentSession;

+ (SNAppDelegate *)sharedDelegate;
+ (NSCalendar *)currentCalendar;
+ (NSLocale *)currentLocale;

@end

@protocol SNAppDelegateProtocol <NSObject>
@required
- (void)applicationWillAppearInForeground;

@end