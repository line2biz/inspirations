

#import "SNHoursActivityView.h"

#import "UIColor+Customization.h"

@import CoreMotion;

#define kNUMBER_OF_HOURS 24

@interface SNHoursActivityView () {
    NSDate *_firstDate;
    NSDate *_lastDate;
}

@property (strong, nonatomic) NSMutableIndexSet *activitySet;

@end

@implementation SNHoursActivityView

@dynamic begindDate, endDate;

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initializeComponents];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeComponents];
    }
    return self;
}

- (void)initializeComponents {
    self.backgroundColor = [UIColor clearColor];
    NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.frame));

    CGRect frame = CGRectMake(0, 0, CGRectGetHeight(self.bounds), CGRectGetHeight(self.bounds));
    for (NSInteger idx = 0; idx < kNUMBER_OF_HOURS; idx++) {
        UIView *view = [[UIView alloc] initWithFrame:frame];
        view.backgroundColor = [UIColor customLightGrayColor];
        view.layer.cornerRadius = CGRectGetHeight(frame) / 2;
        view.layer.masksToBounds = YES;
        view.tag = idx;
        
        
        [self addSubview:view];
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (UIView *view in self.subviews) {
        CGFloat x = CGRectGetWidth(self.frame) / (kNUMBER_OF_HOURS + 1) * (view.tag + 1);
        view.center = CGPointMake(x, CGRectGetMidY(self.bounds));
    }
}

#pragma mark - Public Methods
- (void)reloadData {
    
    if (!self.date) {
        return;
    }
    
    
    CMMotionActivityManager *manager = [[CMMotionActivityManager alloc] init];
    NSOperationQueue *queue = [NSOperationQueue new];
    
    self.activitySet = [NSMutableIndexSet new];
    
    __weak typeof(self) weakSelf = self;
    [manager queryActivityStartingFromDate:_firstDate toDate:_lastDate toQueue:queue withHandler:^(NSArray *activities, NSError *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf && !error) {
            NSLog(@"WE ARE HERE");
            
            [strongSelf.activitySet addIndex:9];
            [strongSelf.activitySet addIndex:10];

            [strongSelf.activitySet addIndex:14];
            [strongSelf.activitySet addIndex:15];
            [strongSelf.activitySet addIndex:16];
            [strongSelf.activitySet addIndex:17];

            dispatch_async(dispatch_get_main_queue(), ^{
                [strongSelf configureView];
            });
            
            
//            for (NSInteger idx = 0; idx < [activities count]; idx++) {
//                if (idx == activities.count - 1) {
//                    break;
//                }
//                
//                CMMotionActivity *activity = activities[idx];
//                if (!activity.walking && !activity.running){
//                    continue;
//                }
//                
//                
//                CMMotionActivity *nextActivity = activities[idx +1];
//                NSTimeInterval duration = [nextActivity.startDate timeIntervalSinceDate:activity.startDate];
//                
//                if (!motionActivity) {
//                    motionActivity = [SNActivity new];
//                    motionActivity.date = activity.startDate;
//                    [array addObject:motionActivity];
//                }
//                
//                
//                if ([self compareDate:motionActivity.date withDate:activity.startDate] == NSOrderedAscending) {
//                    motionActivity = [SNActivity new];
//                    motionActivity.date = activity.startDate;
//                    [array addObject:motionActivity];
//                    //                    NSLog(@"WE ARE HERE : %zd", [array count]);
//                    
//                }
//                
//                if (activity.walking)
//                    motionActivity.walkingDuration += duration;
//                else if (activity.running)
//                    motionActivity.runningDuration += duration;
//                
//            }
            
        }
    }];
}

- (void)configureView {
    for (UIView *view in self.subviews) {
        
        if ([self.activitySet containsIndex:view.tag]) {
            view.backgroundColor = [UIColor customDarkGrayGolor];
        } else {
            view.backgroundColor = [UIColor customLightGrayColor];
        }
    }
}

#pragma mark - Property Accessors
- (void)setDate:(NSDate *)currentDate {
    _date = currentDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:self.date];
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    _firstDate = [calendar dateFromComponents:dateComponents];
    
    dateComponents = [NSDateComponents new];
    dateComponents.day = 1;
    dateComponents.second = -1;
    
    _lastDate = [calendar dateByAddingComponents:dateComponents toDate:_firstDate options:0];
    
    if ([CMMotionActivityManager isActivityAvailable]) {
        [self reloadData];
    };
}

- (NSDate *)begindDate {
    return _firstDate;
}

- (NSDate *)endDate {
    return _lastDate;
}

#pragma mark - Private Methods
- (NSComparisonResult)compareDate:(NSDate *)firstDate withDate:(NSDate *)secondDate {
    
    NSDate *date1 = firstDate;
    NSDate *date2 = secondDate;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitHour startDate:&date1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitHour startDate:&date2 interval:NULL forDate:date2];
    return [date1 compare:date2];
    
}


@end
