

#import "SNReportCell.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@implementation SNReportCell

- (void)awakeFromNib
{
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.detailLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
    self.detailLabel.textColor = [UIColor customLightGrayColor];
    
    self.dateLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
    self.dateLabel.textColor = [UIColor customLightGrayColor];
    
    self.sortOrderLabel.font = [UIFont franklinGothicStdBookMedWithSize:self.sortOrderLabel.font.pointSize];
    self.sortOrderLabel.textColor = [UIColor customLightGrayColor];
    
}

- (void)setActive:(BOOL)active {
    _active = active;
    
    if (active) {
        self.sortImageView.image = [UIImage imageNamed:@"Report-Bkg-Active"];
        self.dateImageView.image = [UIImage imageNamed:@"Report-Icon-Date-Active"];
        self.detailImageView.image = [UIImage imageNamed:@"Report-Icon-Count-Active"];
        
        self.titleLabel.textColor = [UIColor customOrangeColor];
        self.detailLabel.textColor = [UIColor customOrangeColor];
        self.dateLabel.textColor = [UIColor customOrangeColor];
        
    } else {
        self.sortImageView.image = [UIImage imageNamed:@"Report-Bkg"];
        self.dateImageView.image = [UIImage imageNamed:@"Report-Icon-Date"];
        self.detailImageView.image = [UIImage imageNamed:@"Report-Icon-Count"];
        
        self.titleLabel.textColor = [UIColor customDarkGrayGolor];
        self.detailLabel.textColor = [UIColor customLightGrayColor];
        self.dateLabel.textColor = [UIColor customLightGrayColor];
    }
}

@end
