
#import "SNTrainingController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "SNStopwatchController.h"
#import "SNNavController.h"
#import "SNAudioVolumeController.h"

#import "SNMenusController.h"

#import "DMUser.h"

#import <MFSideMenu.h>

@import AVFoundation;

@interface SNTrainingController () <UIGestureRecognizerDelegate>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
    NSMutableArray *_arrayOfObservers;
    
    BOOL _bAnimationFinished;
    
    NSTimer *_timer;
    NSTimeInterval _timerInterval;
    NSTimeInterval _timeElapsed;
}

@property (weak, nonatomic) IBOutlet UIView *firstStepView;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewArrow;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhone;

@end

@implementation SNTrainingController


#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    
    [SNAppearance customizeViewController:self];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.labelTitle.text = LMLocalizedString(@"Gleich geht‘s los.", nil);
    self.labelText.text = LMLocalizedString(@"Verstauen Sie nun Ihr iPhone in der vorderen Hosentasche.", nil);
    
    if (self.navigationController.viewControllers.count == 1) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    } else {
        UIImage *iconImage = [UIImage imageNamed:@"Icon-Nav-Back"];
        NSString *sButtonTitle = LMLocalizedString(@"Zurück", nil);
        UIFont *fontForButton = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTitle.font.pointSize-3];
        NSDictionary *dictOfAttrs = @{ NSFontAttributeName : fontForButton };
        CGSize size = [sButtonTitle sizeWithAttributes:dictOfAttrs];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width + size.width, 40)];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:iconImage];
        imageView.frame = CGRectMake(0, (40 - iconImage.size.height)/2, iconImage.size.width, iconImage.size.height);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(iconImage.size.width, (40 - size.height)/2 + 2, size.width, size.height)];
        label.font = fontForButton;
        label.textColor = [UIColor customDarkGrayGolor];
        label.text = sButtonTitle;
        [view addSubview:imageView];
        [view addSubview:label];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
        tapGesture.delegate = self;
        
        [view addGestureRecognizer:tapGesture];
        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:view];
        self.navigationItem.leftBarButtonItem = leftButton;
    }
    
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
    
//    if ([SNAudioVolumeController shouldBeShown]) {
//        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNAudioVolumeController class])];
//        SNNavController *navController = [[SNNavController alloc] initWithRootViewController:viewController];
//        [self presentViewController:navController animated:NO completion:NULL];
//    }
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startTimer:YES];
    [self registerObservers];
    [self startArrowAnimation];
    [self startTextAnimation];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    for (UILabel *label in self.labels) {
        UIFont *font;
        if (label == self.labelTitle) {
            font = [UIFont franklinGothicStdBookCondesedMedWithSize:label.font.pointSize];
        } else {
            font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
        }
        label.font = font;
        label.textColor = [UIColor titleColor];
        
        NSDictionary *attributes = @{
                                     NSFontAttributeName : font,
                                     NSForegroundColorAttributeName : [UIColor titleColor]
                                     };
        
        NSAttributedString *str = [[NSAttributedString alloc] initWithString:label.text attributes:attributes];
        CGSize size = CGSizeMake(CGRectGetWidth(label.frame), CGFLOAT_MAX);
        NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
        size.height = [str boundingRectWithSize:size options:options context:NULL].size.height;
        CGRect frame = label.frame;
        frame.size.height = size.height;
        label.frame = frame;
        label.attributedText = str;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterObservers];
    
    [self startTimer:NO];
    
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeDefault;
}

#pragma mark - Observers
- (void)registerObservers
{
    _arrayOfObservers = [NSMutableArray new];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    __weak typeof(self) weakSelf = self;
    [_arrayOfObservers addObject:[nc addObserverForName:MFSideMenuStateNotificationEvent
                                                 object:nil
                                                  queue:[NSOperationQueue mainQueue]
                                             usingBlock:^(NSNotification *note)
                                  {
                                      typeof(weakSelf) strongSelf = weakSelf;
                                      if (strongSelf) {
                                          NSDictionary *userInfo = [note userInfo];
                                          if (userInfo) {
                                              MFSideMenuStateEvent event = (MFSideMenuStateEvent)[[userInfo valueForKey:@"eventType"] integerValue];
                                              if (event == MFSideMenuStateEventMenuDidClose) {
                                                  [strongSelf menuDidClose];
                                              } else if (event == MFSideMenuStateEventMenuWillOpen) {
                                                  [strongSelf menuWillOpen];
                                              }
                                          }
                                      }
                                  }]];
}

- (void)unregisterObservers
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (int i = 0; i < _arrayOfObservers.count; i++)
    {
        [nc removeObserver:_arrayOfObservers[i]];
    }
}


#pragma mark - Private Methods

- (void)controllerStart
{
    // for animation purposes
    _bAnimationFinished = NO;
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}


#pragma mark Menu Events Handlers
- (void)menuDidClose
{
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startTimer:YES];
    });
}

- (void)menuWillOpen
{
    [self startTimer:NO];
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeDefault;
}

#pragma mark Timer Methods
- (void)startTimer:(BOOL)start
{
    if (start) {
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        _timeElapsed = 0.0f;
        _timerInterval = 0.5f;
        _timer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval
                                                  target:self
                                                selector:@selector(timerHandler)
                                                userInfo:nil
                                                 repeats:YES];
    } else {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)timerHandler
{
    _timeElapsed += _timerInterval;
    if (_bAnimationFinished) {
        [self startTimer:NO];
        [self didTapNextButton:nil];
    }
}

#pragma mark Animation Part

- (void)startTextAnimation
{
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.labelText.bounds;
    
    UIColor *gradient = [UIColor colorWithWhite:1.0f alpha:0.0];
    NSArray *startLocations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.01]];
    NSArray *endLocations = @[[NSNumber numberWithFloat:0.99f],[NSNumber numberWithFloat:1.0f]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    
    gradientMask.colors = @[(id)gradient.CGColor, (id)[UIColor whiteColor].CGColor];
    gradientMask.locations = startLocations;
    gradientMask.startPoint = CGPointMake(0, 0);
    gradientMask.endPoint = CGPointMake(0, 1);
    
    [self.labelText.layer addSublayer:gradientMask];
    
    CGFloat duration = 1.5f;
    
    animation.fromValue = startLocations;
    animation.toValue = endLocations;
    animation.duration  = duration;
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf.labelText.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
            [strongSelf startCurveAnimation];
            [gradientMask removeAllAnimations];
        }
    }];
    
    [CATransaction begin];
    
    [gradientMask addAnimation:animation forKey:@"animateGradient"];
    
    [CATransaction commit];
    
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _bAnimationFinished = YES;
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];

}

- (void)startArrowAnimation
{
    [self.imageViewPhone setHidden:YES];
    NSTimeInterval duration = 1.0f;
    NSMutableArray *mArrayOfImages = [NSMutableArray new];
    for (NSInteger i=1; i<16; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"arrow%zd",i]];
        [mArrayOfImages addObject:image];
    }
    self.imageViewArrow.image = (UIImage *)[mArrayOfImages lastObject];
    self.imageViewArrow.animationImages = mArrayOfImages;
    self.imageViewArrow.animationDuration = duration;
    self.imageViewArrow.animationRepeatCount = 1;
    [self.imageViewArrow startAnimating];
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration - duration*0.05) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            
            __block CATransform3D xForm = CATransform3DIdentity;
            xForm = CATransform3DScale(xForm, 0.7, 0.7, 1.0);
            
            
            __block CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
            animation.duration = duration;
            animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
            animation.removedOnCompletion = NO;
            animation.fillMode = kCAFillModeForwards;
            
            animation.toValue = [NSValue valueWithCATransform3D:xForm];
            
            [CATransaction setAnimationDuration:0.0001];
            [CATransaction setCompletionBlock:^{
                if (strongSelf) {
                    [strongSelf.imageViewPhone setHidden:NO];
                    __block NSTimeInterval animDuration = 0.25f;
                    
                    CATransform3D xForm = CATransform3DIdentity;
                    xForm = CATransform3DScale(xForm, 1.3, 1.3, 1.0);
                    
                    animation.duration = animDuration;
                    animation.toValue = [NSValue valueWithCATransform3D:xForm];
                    
                    [CATransaction setAnimationDuration:0.0001];
                    [CATransaction setCompletionBlock:^{
                        if (strongSelf) {
                            __block NSTimeInterval animDuration = 0.25f;
                            
                            CATransform3D xForm = CATransform3DIdentity;
                            xForm = CATransform3DScale(xForm, 1.0, 1.0, 1.0);
                            
                            animation.duration = animDuration;
                            animation.toValue = [NSValue valueWithCATransform3D:xForm];
                            
                            [CATransaction begin];
                            
                            [strongSelf.imageViewPhone.layer addAnimation:animation forKey:@"endAnim"];
                            
                            [CATransaction commit];
                        }
                    }];
                    
                    [CATransaction begin];
                    
                    [strongSelf.imageViewPhone.layer addAnimation:animation forKey:@"midAnim"];
                    
                    [CATransaction commit];
                }
            }];
            
            [CATransaction begin];
            
            [strongSelf.imageViewPhone.layer addAnimation:animation forKey:@"startAnim"];
            
            [CATransaction commit];
        }
    });
    
    
    
    
//    __weak typeof(self) weakSelf = self;
//    [self animationCompletitionForTarget:self.imageViewArrow
//                               animation:animation
//                                duration:0.005f
//                              currentIdx:1
//                                  maxIdx:mArrayOfImages.count
//                                  source:mArrayOfImages
//                            completition:^{
//                                typeof(weakSelf) strongSelf = weakSelf;
//                                if (strongSelf) {
//                                    [strongSelf.imageViewPhone setHidden:NO];
//                                }
//                            }];
}

- (void)animationCompletitionForTarget:(UIImageView *)target
                             animation:(CABasicAnimation *)animation
                              duration:(NSTimeInterval)allDuration
                            currentIdx:(NSInteger)currentIdx
                                maxIdx:(NSInteger)maxIdx
                                source:(NSArray *)source
                          completition:(void(^)())block
{
    __weak typeof(self) weakSelf = self;
    void (^AnimationCompletition)(UIImageView*, CABasicAnimation*, NSTimeInterval, NSInteger, NSInteger, NSArray*) = ^(UIImageView* target, CABasicAnimation *animation, NSTimeInterval allDuration, NSInteger currentIdx, NSInteger maxIdx, NSArray *source) {
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [CATransaction setAnimationDuration:allDuration/maxIdx];
            [CATransaction setCompletionBlock:^{
                if (currentIdx < maxIdx-1) {
                    [strongSelf animationCompletitionForTarget:target
                                                     animation:animation
                                                      duration:allDuration
                                                    currentIdx:currentIdx+1
                                                        maxIdx:maxIdx
                                                        source:source
                                                  completition:block];
                } else {
                    block();
                }
            }];
            UIImage *image = [source objectAtIndex:currentIdx];
            animation.toValue = (__bridge id)image.CGImage;
            
            [CATransaction begin];
            
            [target.layer addAnimation:animation forKey:[NSString stringWithFormat:@"animation%zd",currentIdx]];
            
            [CATransaction commit];
        }
    };
    AnimationCompletition(target, animation, allDuration, currentIdx, maxIdx, source);
}

#pragma mark - Tap Gesture Handler
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    NSTimeInterval duration = 0.5f;
    [UIView animateWithDuration:duration/2
                     animations:^{
                         sender.view.alpha = 0.25f;
                         [UIView animateWithDuration:duration/2
                                          animations:^{
                                              sender.view.alpha = 1.0f;
                                          }
                                          completion:^(BOOL finished) {
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                          }];
                     }];
}

#pragma mark - Outlet Methods

- (void)didTapNextButton:(id)sender
{
    SNStopwatchController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNStopwatchController"];
    if (self.trainingTime) {
        viewController.trainingTime = self.trainingTime;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}






@end
