
#import "DMStep.h"

@interface DMStep (Category)

- (void)setCurrentDate:(NSDate *)date;

+ (DMStep *)stepByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
+ (DMStep *)maxStepInContext:(NSManagedObjectContext *)context;
+ (DMStep *)lastStepfRecordInContet:(NSManagedObjectContext *)context;

+ (NSUInteger)calculateCaloriesForSteps:(NSInteger)steps;

@end
