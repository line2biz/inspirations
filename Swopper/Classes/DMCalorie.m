
#import "DMCalorie.h"

@implementation DMCalorie

+ (NSArray *)calories
{
    static NSArray *__array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        
        NSString *path = [[SNLocalizationManager sharedManager] pathForResource:@"Callories" ofType:@"plist"];
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:path];
        
        NSMutableArray *results = [NSMutableArray arrayWithCapacity:[array count]];
        for (NSDictionary *dict in array) {
            DMCalorie *calorie = [DMCalorie new];
            calorie.title = dict[@"title"];
            calorie.value = [dict[@"value"] integerValue];
            NSString *imageName = dict[@"image"];
            if ([imageName length]) {
                calorie.image = [UIImage imageNamed:imageName];
            }
            [results addObject:calorie];
        }
        
        DMCalorie *calorie = [DMCalorie new];
        calorie.title = @"It's necessary to add text";
        calorie.value = 0;
        [results addObject:calorie];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"value" ascending:YES];
        __array = [results sortedArrayUsingDescriptors:@[sort]];
        
    });
    return __array;
}

+ (DMCalorie *)calorieForValue:(NSUInteger)value
{
    DMCalorie *ruternValue = nil;
    NSArray *array = [DMCalorie calories];
    for (DMCalorie *calorie in array) {
        if (calorie.value <= value) {
            ruternValue = calorie;
        } else {
            break;
        }
    }
    return ruternValue;
}

@end
