

#import <UIKit/UIKit.h>

@class DMSession;

@interface SNSummaryController : UIViewController


@property (strong, nonatomic) DMSession *session;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
