

#import <UIKit/UIKit.h>

@interface SNSessionCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *sortOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (weak, nonatomic) IBOutlet UILabel *swoppsLable;
@property (weak, nonatomic) IBOutlet UILabel *calloriesLabel;


@end
