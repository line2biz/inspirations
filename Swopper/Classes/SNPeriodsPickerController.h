

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SNPeriodsType) {
    SNPeriodsTypeHours,
    SNPeriodsTypeDays,
    SNPeriodsTypeWeek
};

typedef NS_ENUM(NSInteger, SNPeriodLength) {
    SNPeriodLengthHoursNever,
    SNPeriodLengthHoursOne,
    SNPeriodLengthHoursTwo,
    SNPeriodLengthHoursThree,
    SNPeriodLengthHoursFour,
    SNPeriodLengthHoursFive,
    SNPeriodLengthHoursSix,
    SNPeriodLengthHoursSeven,
    SNPeriodLengthHoursEight,
    SNPeriodLengthDaysNever,
    SNPeriodLengthDaysEveryOne,
    SNPeriodLengthDaysEveryTwo,
    SNPeriodLengthDaysEveryThree,
    SNPeriodLengthDaysEveryFour,
    SNPeriodLengthWeekNever,
    SNPeriodLengthWeekWorkdays,
    SNPeriodLengthWeekHolidays,
    SNPeriodLengthWeekAll
};

@protocol SNPeriodPickerProtocol;

@interface SNPeriodsPickerController : UIViewController

@property (assign, nonatomic) SNPeriodsType periodsType;
@property (strong, nonatomic) id <SNPeriodPickerProtocol> delegate;

+ (NSString *)titleForLength:(SNPeriodLength)length;
+ (NSInteger)periodForLength:(SNPeriodLength)length;

@end

@protocol SNPeriodPickerProtocol <NSObject>
@required
- (void)periodPickerController:(SNPeriodsPickerController *)controller withPeriodType:(SNPeriodsType)periodType chosenPeriod:(SNPeriodLength)periodLength;

@end
