
#import "SNLocalizationManager.h"

NSString *const SNLocalizationManagerDidChangeLanguage = @"SNLocalizationManagerDidChangeLanguage";

NSString * const SNAppCurrentLanguageKey    = @"SNAppCurrentLanguageKey";

@implementation SNLocalizationManager

static NSBundle *__bundle = nil;

#pragma mark - Initilization
- (id)init
{
    self = [super init];
    if (self) {
        __bundle = [NSBundle mainBundle];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:SNAppCurrentLanguageKey] == nil) {
            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
            language = [language substringToIndex:2];
            
            NSArray *availableLanguages = [self languagesAvailableForApp];
            if ([availableLanguages containsObject:language]) {
                [self setLanguage:language];
            }
            else {
                [self setLanguage:@"en"];
//                self.currentLanguageID = @"en";
            }
        }

    };
    return self;
}

+ (SNLocalizationManager *)sharedManager
{
    static dispatch_once_t once;
    static SNLocalizationManager * sharedInstance;
    dispatch_once(&once, ^{ sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

- (NSArray *)languagesAvailableForApp
{
    // get all localizations from project
    NSArray *array = [[NSBundle mainBundle] localizations];
    
    // remove base localization
    NSMutableArray *filterArray = [NSMutableArray new];
    [filterArray addObject:@"Base"];
    
    // create predicate for filtering
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)", filterArray];
    
    // return filtered array
    return [array filteredArrayUsingPredicate:predicate];
}

#pragma mark - Localization methods
- (void)setLanguage:(NSString*)langIdentifier
{
//	NSString *path = [[SNLocalizationManager sharedManager] pathForResource:langIdentifier ofType:@"lproj" ];
//    
//    
//    
//	
//	if (path == nil) //in case the language does not exists
//		[self resetLocalization];
//	else
//		__bundle = [NSBundle bundleWithPath:path];
    
    
    NSString *path = [self pathForResource:langIdentifier ofType:@"lproj" ];
    path = [[NSBundle mainBundle] resourcePath];
    path = [[path stringByAppendingPathComponent:langIdentifier] stringByAppendingPathExtension:@"lproj"];
    __bundle = [NSBundle bundleWithPath:path];
    
    NSArray *array = nil;
    if ([langIdentifier isEqualToString:@"en"]) {
        array = @[@"en", @"de"];
    } else {
        array = @[@"de", @"en"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:langIdentifier forKey:SNAppCurrentLanguageKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SNLocalizationManagerDidChangeLanguage object:self];
    
}

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
	return [__bundle localizedStringForKey:key value:comment table:nil];
}

- (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext {
    return [__bundle pathForResource:name ofType:ext];
    
}

- (NSURL *)URLForResource:(NSString *)name withExtension:(NSString *)ext {
    return [__bundle URLForResource:name withExtension:ext];
}

- (NSString*)getLanguage
{
	NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
	NSString *preferredLang = [languages objectAtIndex:0];
    
    if ([preferredLang isEqualToString:@"en"]) {
        return preferredLang;
    }
    else if ([preferredLang isEqualToString:@"de"])
    {
        return preferredLang;
    }
    
	return @"en";
}

// Resets the localization system, so it uses the OS default language.
- (void)resetLocalization
{
	__bundle = [NSBundle mainBundle];
}


- (NSBundle *)bundle
{
    return __bundle;
}

@end
