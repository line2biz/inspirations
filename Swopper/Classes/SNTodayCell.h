

#import <UIKit/UIKit.h>

#import "AnimatedLabel.h"

@protocol SNTodayCellCompletitionDelegate;

@interface SNTodayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AnimatedLabel *labelNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCircle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint;

@property (assign, nonatomic) NSInteger iValue;
@property (strong, nonatomic) id<SNTodayCellCompletitionDelegate> completitionDelegate;

@end

@protocol SNTodayCellCompletitionDelegate <NSObject>

@required
- (void)TodayCell:(SNTodayCell *)cell finishedTextAnimation:(BOOL)finished;

@end