

#import "SNDayStatisticController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

#import "DMSession+Category.h"
#import "DMReport+Category.h"
#import "DMStep+Category.h"
#import "SNAdditionalMath.h"
#import "SNDayChart.h"

@interface SNDayStatisticController ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet SNDayChart *dayChart;

@end

@implementation SNDayStatisticController

#pragma mark - Controller Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [SNAppearance customizeViewController:self];
    self.dayChart.week = self.week;
    self.dayChart.currentDate = self.chosenDate;
    self.dayChart.colorCurrentDayText = [UIColor customOrangeColor];
    self.dayChart.colorCalories = [UIColor customOrangeColor];
    self.dayChart.colorSwoops = [UIColor customDarkGrayGolor];
    self.dayChart.colorSteps = [UIColor customLightGrayColor];
    
    [self controllerStart];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    [self configureFonts];
    [self configureText];
}

- (void)configureFonts
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor customDarkGrayGolor];
    
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    self.labelSubtitle.textColor = [UIColor customDarkGrayGolor];
    
    self.labelText.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelText.font.pointSize];
    self.labelText.textColor = [UIColor customDarkGrayGolor];
}

- (void)configureText
{
    self.labelTitle.text = LMLocalizedString(@"Tagesübersicht", nil);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
    [dateFormat setDateFormat:format];
    [dateFormat setLocale:[SNAppDelegate currentLocale]];
    self.labelSubtitle.text = [dateFormat stringFromDate:self.chosenDate];
    
    NSDate *previousDay = [NSDate dateWithTimeInterval:-24*60*60 sinceDate:self.chosenDate];
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] defaultContext];
    DMStep *step = [DMStep stepByDate:previousDay inContext:managedObjectContext];
    DMReport *report = [DMReport reportByDate:previousDay inContext:managedObjectContext];
    
    __block NSInteger i = 0;
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDate *dayDate = [obj valueForKey:@"date"];
        if ([SNAdditionalMath compareDate:dayDate withDate:self.chosenDate] == NSOrderedSame) {
            i = idx;
            *stop = YES;
        }
    }];
    id object = [self.week objectAtIndex:i];
    NSInteger caloriesToday = [[object valueForKey:@"calories"] integerValue];
    NSInteger yesturdayCalories = 0;
    if (report) {
        CGFloat calories = 0;
        for (DMSession *session in report.sessions) {
            calories += [session caloriesPerSession] / session.sessionDuration.floatValue * report.interval.floatValue;
        }
        calories = calories / report.sessions.count;
        yesturdayCalories += calories;
    }
    if (step) {
        yesturdayCalories += [DMStep calculateCaloriesForSteps:[step.numberOfSteps integerValue]];
    }
    if (caloriesToday && yesturdayCalories) {
        CGFloat percentage = (CGFloat)caloriesToday/(CGFloat)yesturdayCalories;
        NSString *moreOrLess;
        if (percentage == 1.0f) {
            self.labelText.text = LMLocalizedString(@"kein Unterschied zur letzten Tag", nil);
        } else if (percentage < 0.001) {
            self.labelText.text = LMLocalizedString(@"das sind 100 % weniger als in der letzten Tag", nil);
        } else {
            moreOrLess = (percentage > 1.0f) ? LMLocalizedString(@"mehr", nil) : LMLocalizedString(@"weniger", nil);
            
            if (percentage > 1.0f) {
                if (percentage < 2.0f) {
                    percentage = (percentage - 1.0f)*100;
                } else {
                    percentage = 2.0f * 100;
                }
            } else {
                percentage = (1.0f - percentage)*100;
            }
            
            NSString *str = LMLocalizedString(@"das sind <percentage>% <moreOrLess> als in der letzten Tag", @"<percentage> & <moreOrLess> don't translate");
            str = [str stringByReplacingOccurrencesOfString:@"<percentage>" withString:[NSString stringWithFormat:@"%.0f", percentage]];
            str = [str stringByReplacingOccurrencesOfString:@"<moreOrLess>" withString:moreOrLess];
            
            self.labelText.text = str;
        }
    } else if (caloriesToday) {
        self.labelText.text = LMLocalizedString(@"das sind 100% mehr als in der letzten Tag", nil);
    } else if (yesturdayCalories) {
        self.labelText.text = LMLocalizedString(@"das sind 100 % weniger als in der letzten Tag", nil);
    } else {
        self.labelText.text =LMLocalizedString(@"kein Unterschied zur letzten Tag", nil);
    }
    
}


@end
