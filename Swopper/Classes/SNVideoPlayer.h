

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SNVideoPlayerState) {
    SNVideoPlayerStateStopped,
    SNVideoPlayerStateIsPlaying,
    SNVideoPlayerStatePaused
};

@interface SNVideoPlayer : UIControl

@property (strong, nonatomic) NSString *fileFullName;
@property (assign, readonly, nonatomic) SNVideoPlayerState playerState;

- (void)startVideo;
- (void)stopVideo;
- (void)pauseVideo;

@end
