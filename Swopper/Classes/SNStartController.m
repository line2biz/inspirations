

#import "SNStartController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNProfileController.h"
#import "SNCwopperCreatedController.h"
#import "SNProfileController.h"

#import "SNOrangeButton.h"
#import "DMUser.h"

#import <MFSideMenu.h>


#define kCELL_IDENTIFIER @"Cell"

@interface SNStartController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageViewEllipse;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SNStartController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCELL_IDENTIFIER];
    
    
    [self configureView];
    
    
    if ([[DMUser defaultUser] firstLaunch]) {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    _loadIdx = 0;
    
    [self configureFonts];
    [self controllerStart];
    
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    
    [self prepareAnimations];
}

#pragma mark - Private Methods
- (void)configureView {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:NULL];
    
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    [SNAppearance findAndMarkWord:LMLocalizedString(@"Track", @"") textBenderView:self.labelLogo withNewFont:nil newColor:[UIColor customOrangeColor]];
    
    self.labelText.text = LMLocalizedString(@"Hallo!\n\n\nDiese App bringt mehr\nBewegung in Ihr Leben!\nDas ist toll - falls Sie aber gesundheitliche Probleme haben - sprechen Sie bitte erst mit Ihrem Doc.\n\n\nDatenschutz: Ihre Daten\nbleiben Ihre - Versprochen!", @"");
    [SNAppearance findAndMarkWord:LMLocalizedString(@"Hallo!", @"") inLabel:self.labelText withNewFont:[UIFont franklinGothicStdBookCondesedWithSize:26]];
}

- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
    self.navigationItem.titleView.alpha = 0.0f;
    self.tableView.hidden = YES;
}

- (void)configureFonts
{
    self.labelLogo.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelLogo.font.pointSize];
    self.labelLogo.textColor = [UIColor customDarkGrayGolor];
    [SNAppearance findAndMarkWord:LMLocalizedString(@"Track", @"") textBenderView:self.labelLogo withNewFont:nil newColor:[UIColor customOrangeColor]];
    
    self.labelText.text = LMLocalizedString(@"Hallo!\n\n\nDiese App bringt mehr\nBewegung in Ihr Leben!\nDas ist toll - falls Sie aber gesundheitliche Probleme haben - sprechen Sie bitte erst mit Ihrem Doc.\n\n\nDatenschutz: Ihre Daten\nbleiben Ihre - Versprochen!", @"");
    self.labelText.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelText.font.pointSize];
    self.labelText.textColor = [UIColor customDarkGrayGolor];
    [SNAppearance findAndMarkWord:LMLocalizedString(@"Hallo!", @"") inLabel:self.labelText withNewFont:[UIFont franklinGothicStdBookCondesedWithSize:26]];
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
}


#pragma mark Animations
- (void)prepareAnimations
{
    self.labelText.alpha = 0.0f;
    [self startLogoAnimation];
}

- (void)startLogoAnimation
{
    __weak typeof(self) weakSelf = self;
    __block NSTimeInterval duration = 0.001;
    
    __block CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    __block CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DScale(xform, 0.25, 0.25, 1.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            duration = 0.5f;
            
            transformAnimation.duration = duration;
            xform = CATransform3DIdentity;
            xform = CATransform3DScale(xform, 1.0, 1.0, 1.0);
            transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
            
            [CATransaction setAnimationDuration:duration];
            [CATransaction setCompletionBlock:^{
                duration = 0.25f;
                
                transformAnimation.duration = duration;
                xform = CATransform3DIdentity;
                xform = CATransform3DScale(xform, 0.8, 0.8, 1.0);
                transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
                
                [CATransaction setAnimationDuration:duration];
                [CATransaction setCompletionBlock:^{
                    duration = 0.25f;
                    
                    transformAnimation.duration = duration;
                    xform = CATransform3DIdentity;
                    xform = CATransform3DScale(xform, 1.0, 1.0, 1.0);
                    transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
                    
                    [CATransaction setAnimationDuration:duration];
                    [CATransaction setCompletionBlock:^{
                        [strongSelf startLogoTextAnimation];
                    }];
                    
                    [CATransaction begin];
                    // finish
                    [strongSelf.imageViewLogo.layer addAnimation:transformAnimation forKey:@"finishTransformAnimation"];
                    [strongSelf.imageViewEllipse.layer addAnimation:transformAnimation forKey:@"finishTransformAnimation"];
                    [strongSelf.labelLogo.layer addAnimation:transformAnimation forKey:@"finishTransformAnimation"];
                    
                    [CATransaction commit];
                    
                }];
                
                [CATransaction begin];
                // mid second
                [strongSelf.imageViewLogo.layer addAnimation:transformAnimation forKey:@"midSecondTransformAnimation"];
                [strongSelf.imageViewEllipse.layer addAnimation:transformAnimation forKey:@"midSecondTransformAnimation"];
                [strongSelf.labelLogo.layer addAnimation:transformAnimation forKey:@"midSecondTransformAnimation"];
                
                [CATransaction commit];
                
            }];
            
            [UIView animateWithDuration:duration
                             animations:^{
                                 strongSelf.imageViewLogo.alpha = 1.0f;
                                 strongSelf.imageViewEllipse.alpha = 1.0f;
                             }];
            
            [CATransaction begin];
            // mid first
            [strongSelf.imageViewLogo.layer addAnimation:transformAnimation forKey:@"midFirstTransformAnimation"];
            [strongSelf.imageViewEllipse.layer addAnimation:transformAnimation forKey:@"midFirstTransformAnimation"];
            [strongSelf.labelLogo.layer addAnimation:transformAnimation forKey:@"midFirstTransformAnimation"];
            
            [CATransaction commit];
        }
        
    }];
    
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewLogo.alpha = 0.0f;
                         self.imageViewEllipse.alpha = 0.0f;
                         self.labelLogo.alpha = 0.0f;
                     }];
    
    [CATransaction begin];
    // preparation
    [self.imageViewLogo.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [self.imageViewEllipse.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [self.labelLogo.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
}

- (void)startLogoTextAnimation
{
    self.tableView.hidden = YES;
    
    self.labelLogo.alpha = 1.0f;
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.labelLogo.bounds;
    
    UIColor *gradient = [UIColor colorWithWhite:1.0f alpha:0.0];
    NSArray *startLocations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.01]];
    NSArray *endLocations = @[[NSNumber numberWithFloat:0.99f],[NSNumber numberWithFloat:1.0f]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    
    gradientMask.colors = @[(id)gradient.CGColor, (id)[UIColor whiteColor].CGColor];
    gradientMask.locations = startLocations;
    gradientMask.startPoint = CGPointMake(0, 0);
    gradientMask.endPoint = CGPointMake(1, 0);
    
    [self.labelLogo.layer addSublayer:gradientMask];
    
    CGFloat duration = 0.5f;
    
    animation.fromValue = startLocations;
    animation.toValue = endLocations;
    animation.duration  = duration;
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        [weakSelf.labelLogo.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
        [gradientMask removeAllAnimations];
        self.tableView.hidden = NO;
    }];
    
    [CATransaction begin];
    
    [gradientMask addAnimation:animation forKey:@"animateGradient"];
    
    [CATransaction commit];
}

- (void)startNavigationItemAnimation
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo-Small"]];
    UIView *view = [[UIView alloc] initWithFrame:imageView.frame];
    imageView.alpha = 0.0f;
    
    self.navigationItem.titleView = view;
    imageView.center = view.center;
    [view addSubview:imageView];
    
    NSTimeInterval duration = 1.0;
    
    CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DScale(xform, 0.25, 0.25, 1.0);
    
    imageView.layer.transform = xform;
    
    xform = CATransform3DIdentity;
    xform = CATransform3DScale(xform, 1.0, 1.0, 1.0);
    
    [UIView animateWithDuration:duration
                     animations:^{
                         imageView.alpha = 1.0f;
                         imageView.layer.transform = xform;
                     }];
}

- (void)startTextAnimation
{
    __weak typeof(self) weakSelf = self;
    self.labelText.alpha = 1.0f;
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.labelText.bounds;
    
    UIColor *gradient = [UIColor colorWithWhite:1.0f alpha:0.0];
    NSArray *startLocations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.01]];
    NSArray *endLocations = @[[NSNumber numberWithFloat:0.99f],[NSNumber numberWithFloat:1.0f]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    
    gradientMask.colors = @[(id)gradient.CGColor, (id)[UIColor whiteColor].CGColor];
    gradientMask.locations = startLocations;
    gradientMask.startPoint = CGPointMake(0, 0);
    gradientMask.endPoint = CGPointMake(0, 1);
    
    [self.labelText.layer addSublayer:gradientMask];
    
    CGFloat duration = 1.5f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration - duration*0.1f) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf.labelText.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
        }
    });
    
    animation.fromValue = startLocations;
    animation.toValue = endLocations;
    animation.duration  = duration;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf startCurveAnimation];
            [gradientMask removeAllAnimations];
        }
    }];
    
    [CATransaction begin];
    
    [gradientMask addAnimation:animation forKey:@"animateGradient"];
    
    [CATransaction commit];
    
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}





#pragma mark - Outlet Methods
- (IBAction)didTapNextButton:(id)sender
{
//    if ([[DMUser defaultUser] firstLaunch]) {
//        [self performSegueWithIdentifier:NSStringFromClass([SNCwopperCreatedController class]) sender:self];
//    } else {
//        if (![[DMUser defaultUser] isValid]) {
//            SNProfileController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNProfileController class])];
//            [self.navigationController pushViewController:profileController animated:YES];
//        } else {
//            [self performSegueWithIdentifier:NSStringFromClass([SNProfileController class]) sender:self];            
//        }
//    }
}


#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCELL_IDENTIFIER forIndexPath:indexPath];
    
    NSString *str = nil;
    switch (indexPath.row) {
        case 0: {
            str = @"English";
            if ([[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:@"en"]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
            break;
        case 1: {
            str = @"Deutsch";
            if ([[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:@"de"]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
            break;
    }
    
    cell.textLabel.text = str;
    cell.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:cell.textLabel.font.pointSize];
    cell.textLabel.textColor = [UIColor customDarkGrayGolor];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *sLang = nil;
    switch (indexPath.row) {
        case 0:
            sLang = @"en";
            break;
            
        case 1:
            sLang = @"de";
            break;
    }
    
    if (![[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:sLang]) {
        [[SNLocalizationManager sharedManager] setLanguage:sLang];
    }
    
    [self configureView];
    
    [self.tableView reloadData];
   
    
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.tableView.hidden = YES;
            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.imageViewLogo.alpha = 0.0f;
                                 self.imageViewEllipse.alpha = 0.0f;
                                 self.labelLogo.alpha = 0.0f;
                             }
                             completion:^(BOOL finished) {
                                 [self startNavigationItemAnimation];
                                 [self startTextAnimation];
                             }];
        });
   
}


@end
