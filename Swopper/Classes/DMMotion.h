

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMSession;

@interface DMMotion : NSManagedObject

@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) DMSession *session;

@end
