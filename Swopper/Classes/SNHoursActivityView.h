
#import <UIKit/UIKit.h>

@interface SNHoursActivityView : UIView

@property (strong, nonatomic) NSDate *date;

@property (strong, nonatomic, readonly) NSDate *begindDate;
@property (strong, nonatomic, readonly) NSDate *endDate;

- (void)reloadData;

@end
