
#import <UIKit/UIKit.h>

@class DMSession;

@interface SNTopTrainingTableCell : UITableViewCell

@property (strong, nonatomic) DMSession *session;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelCalloriesText;
@property (weak, nonatomic) IBOutlet UILabel *labelMovesText;
@property (weak, nonatomic) IBOutlet UILabel *labelCallories;
@property (weak, nonatomic) IBOutlet UILabel *labelMoves;

@end
