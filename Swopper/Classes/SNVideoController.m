

#import "SNVideoController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

#import "SNTrainingStep02Controller.h"
#import "SNProfileController.h"

#import "SNVideoCell.h"
#import "SNMoviePlayerController.h"
#import "SNMenusController.h"

#import <MFSideMenu.h>

@interface SNVideoController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) SNMoviePlayerController *moviePlayerController;

@end

@implementation SNVideoController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.navigationController.viewControllers[0] == self) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    }
    
    [SNAppearance customizeViewController:self];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidExitFullscreen:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayerController.moviePlayer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayerController.moviePlayer];
}

#pragma mark - Private Methods
- (void)moviePlayerDidExitFullscreen:(NSNotification *)theNotification
{
    NSLog(@"\n%s\nMovie has finished.",__FUNCTION__);
}

#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (void)configureCell:(SNVideoCell *)cell atIndexPath:(NSIndexPath *)indexPath toShow:(BOOL)show
{
    [cell setVideoImage:[UIImage imageNamed:@"Tutorial-01"]];
//    [cell setTitleLabel:@"Something"];
//    [cell setText:@"Something"];
    [cell setTitleFont:[UIFont franklinGothicStdBookCondesedWithSize:cell.titleFont.pointSize]];
    [cell setTextFont:[UIFont franklinGothicStdBookCondesedWithSize:cell.textFont.pointSize]];
    
    if (show) {
        // video player action
        __weak typeof(self) weakSelf = self;
        NSString *moviePath;
        if (indexPath.row == 0) {
            moviePath = [[SNLocalizationManager sharedManager] pathForResource:@"De-Video" ofType:@"mp4"];
        } else {
            moviePath = [[SNLocalizationManager sharedManager] pathForResource:@"Eng-Video" ofType:@"m4v"];
        }
        [cell setVideoImageBlock:^{
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
                strongSelf.moviePlayerController = [[SNMoviePlayerController alloc] initWithContentURL:movieURL];
                [strongSelf presentMoviePlayerViewControllerAnimated:strongSelf.moviePlayerController];
            }
        }];
    }
}

- (SNVideoCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SNVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    [self configureCell:cell atIndexPath:indexPath toShow:YES];
    
    return cell;
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNVideoCell *cell = (SNVideoCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    [self configureCell:cell atIndexPath:indexPath toShow:NO];
    return [cell cellHeight];
}


@end
