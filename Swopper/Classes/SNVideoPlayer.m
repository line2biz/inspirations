

#import "SNVideoPlayer.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>

#import "SNSliderControl.h"

@interface SNVideoPlayer () <SNSliderControlProtocol, UIGestureRecognizerDelegate>
{
    NSString *_fileName, *_fileExtension;
    NSTimer *_timer;
    CGFloat _videoDuration;
    CGFloat _secondsElapsed;
    
    MPMoviePlayerController *_moviePlayer;
    AVPlayerItem *_playerItem;
    AVURLAsset *_asset;
    
    // subviews
    UIView *_subviewVideo;
    SNSliderControl *_sliderControl;
    UIButton *_playPauseButton;
    
    // tapping
    
    BOOL _controlsAreShown;
}
@end

@implementation SNVideoPlayer

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor blackColor];
    _playerState = SNVideoPlayerStateStopped;
    _secondsElapsed = 0.0f;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
}

#pragma mark - Drawing
- (void)layoutSubviews
{
    [self.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        [subview removeFromSuperview];
    }];
    
    _subviewVideo = [self prepareVideoController];
    if (_subviewVideo) {
        [self addSubview:_subviewVideo];
    }
    _sliderControl = [self prepareControlsView];
    if (_sliderControl) {
        _playPauseButton = [self preparePlayPauseButton];
        [self addSubview:_sliderControl];
        [self addSubview:_playPauseButton];
        [self showControls:YES animated:NO];
    }
    
}

- (SNSliderControl *)prepareControlsView
{
    if (_videoDuration) {
        CGFloat width = CGRectGetWidth(self.bounds);
        CGFloat allHeight = CGRectGetHeight(self.bounds);
        CGFloat height = 44.0f;
        CGRect frame = CGRectMake(0, allHeight - height, width, height);
        SNSliderControl *sliderControl = [[SNSliderControl alloc] initWithFrame:frame];
        [sliderControl setMaximumValue:_videoDuration];
        [sliderControl setMinimumValue:0.0f];
        [sliderControl setCurrentValue:0.0f];
        sliderControl.delegate = self;
        
        return sliderControl;
    } else {
        return nil;
    }
}

- (UIView *)prepareVideoController
{
    if (self.fileFullName) {
        CGRect frame = self.bounds;
        frame.size.height -= 44.0f;
        UIView *presentationView = [[UIView alloc] initWithFrame:frame];
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *moviePath = [bundle pathForResource:_fileName ofType:_fileExtension];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        _moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
        _moviePlayer.controlStyle = MPMovieControlStyleNone;
        [_moviePlayer.view setFrame:presentationView.bounds];
        [presentationView addSubview:_moviePlayer.view];
        
        _secondsElapsed = 0.0f;
        
        _asset = [[AVURLAsset alloc] initWithURL:movieURL options:nil];
        NSTimeInterval durationInSeconds = 0.0;
        if (_asset)
            durationInSeconds = CMTimeGetSeconds(_asset.duration);
        
        _playerItem = [AVPlayerItem playerItemWithAsset:_asset];
        
        CMTime duration = _playerItem.duration;
        _videoDuration = CMTimeGetSeconds(duration);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieFinished:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayer];
        return presentationView;
    } else {
        return nil;
    }
}

- (UIButton *)preparePlayPauseButton
{
    UIButton *button = [[UIButton alloc] init];
    CGFloat sideLength = 44.0f;
    CGPoint centerPoint = CGPointMake(CGRectGetWidth(self.bounds)/2, _sliderControl.frame.origin.y / 2);
    CGRect frame = CGRectMake(centerPoint.x - sideLength/2, centerPoint.y - sideLength/2, sideLength, sideLength);
    button.frame = frame;
    button.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
    button.tintColor = [UIColor clearColor];
    [button addTarget:self action:@selector(didTapPlayPauseButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"Btn-Video-Play"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"Btn-Video-Play"] forState:UIControlStateSelected];
    
    return button;
}

#pragma mark - Control Methods
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint touchLocation = point;
    if (!CGRectContainsPoint(_sliderControl.frame, touchLocation) && !CGRectContainsPoint(_playPauseButton.frame, touchLocation)) {
        return self;
    } else {
        if (CGRectContainsPoint(_sliderControl.frame, touchLocation)) {
            return _sliderControl;
        } else if (CGRectContainsPoint(_playPauseButton.frame, touchLocation)){
            if (!_controlsAreShown) {
                return self;
            } else {
                return _playPauseButton;
            }
        } else {
            return self;
        }
    }
}

- (void)refreshVideo
{
    [_timer invalidate];
    _timer = nil;
    _secondsElapsed = 0.0f;
    [_moviePlayer stop];
    [_sliderControl setCurrentValue:0.0f];
    _playerState = SNVideoPlayerStateStopped;
    [self showControls:YES animated:YES];
}

- (void)playVideo:(BOOL)play
{
    if (play) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:@selector(timerFired)
                                                userInfo:nil
                                                 repeats:YES];
        [_moviePlayer play];
        [self showControls:NO animated:YES];
    } else {
        [_timer invalidate];
        [_moviePlayer pause];
        [self showControls:YES animated:YES];
    }
}

#pragma mark Showing Controls
- (void)showControls:(BOOL)show animated:(BOOL)animated
{
    void (^animationBlock)(void) = nil;
    void (^completionBlock)(BOOL finished) = nil;
    
    if (show) {
        _controlsAreShown = YES;
//        _sliderControl.alpha = 0.0f;
        _playPauseButton.alpha = 0.0f;
        
        animationBlock = ^{
//            _sliderControl.alpha = 1.0f;
            _playPauseButton.alpha = 1.0f;
        };
        
        completionBlock = ^(BOOL finished) {
            if (finished) {
                
            }
        };
    } else {
        _controlsAreShown = NO;
        animationBlock = ^{
//            _sliderControl.alpha = 0.0f;
            _playPauseButton.alpha = 0.0f;
        };
        completionBlock = ^(BOOL finished) {
            if (finished) {
                
            }
        };
    }
    
    if (animated) {
        [UIView animateWithDuration:0.5f animations:animationBlock completion:completionBlock];
    } else {
        animationBlock();
        completionBlock(YES);
    }
}

#pragma mark - Tap Gesture Handler
- (void)handleTapGesture:(UITapGestureRecognizer *)sender
{
    CGPoint touchLocation = [sender locationInView:self];
    if (!CGRectContainsPoint(_sliderControl.frame, touchLocation)) {
        if (_playerState == SNVideoPlayerStateIsPlaying) {
            [self showControls:!_controlsAreShown animated:YES];
        }
    }
}

#pragma mark - Video Player Notifications
- (void)movieFinished:(NSNotification*)aNotification
{
    [self refreshVideo];
}

#pragma mark - Timer Methods
- (void)timerFired
{
    _secondsElapsed += 0.1f;
    _sliderControl.currentValue = _secondsElapsed;
}

#pragma mark - Public Methods
- (void)stopVideo
{
    [self refreshVideo];
}

- (void)startVideo
{
    if (_playerState != SNVideoPlayerStateIsPlaying) {
        [self playVideo:YES];
        _playerState = SNVideoPlayerStateIsPlaying;
    }
}

- (void)pauseVideo
{
    if (_playerState == SNVideoPlayerStateIsPlaying) {
        [self playVideo:NO];
        _playerState = SNVideoPlayerStatePaused;
    }
}

#pragma mark - Outlet Methods
- (void)didTapPlayPauseButton:(UIButton *)sender
{
    if (![sender isSelected]) {
        [self startVideo];
        [sender setSelected:YES];
    } else {
        [self pauseVideo];
        [sender setSelected:NO];
    }
}

#pragma mark - Slider Control Delegate
- (void)slider:(SNSliderControl *)sender moveStarted:(BOOL)started
{
    if (started) {
        if (_playerState == SNVideoPlayerStateIsPlaying) {
            [self pauseVideo];
        }
    } else {
        if (_playerState == SNVideoPlayerStateStopped) {
            [self startVideo];
        } else {
            [_moviePlayer setCurrentPlaybackTime:sender.currentValue];
            _secondsElapsed = sender.currentValue;
            [self startVideo];
        }
    }
}

#pragma mark - Property Accessors
- (void)setFileFullName:(NSString *)fileFullName
{
    if (_fileFullName.length > 0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:_moviePlayer];

    }
    
    _fileFullName = fileFullName;
    
    NSArray *array = [fileFullName componentsSeparatedByString:@"."];
    if (array.count > 2) {
        __block NSMutableString *mString = [NSMutableString new];
        [array enumerateObjectsUsingBlock:^(NSString *substring, NSUInteger idx, BOOL *stop) {
            if (idx == array.count-2) {
                [mString appendString:substring];
                *stop = YES;
                return;
            } else {
                [mString appendString:substring];
                [mString appendString:@"."];
            }
        }];
        _fileName = mString;
    } else {
        _fileName = [array firstObject];
    }
    _fileExtension = [array lastObject];
    
    [self layoutSubviews];
}


@end
