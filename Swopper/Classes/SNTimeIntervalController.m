

#import "SNTimeIntervalController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNMenusController.h"
#import "SNSummaryController.h"
#import "SNTrainingSummaryController.h"
#import "DMReport+Category.h"
#import "DMSession+Category.h"

#import "SNTimeRuler.h"

@interface SNTimeIntervalController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet SNTimeRuler *timeRuler;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation SNTimeIntervalController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    // Do any additional setup after loading the view.
    [SNAppearance customizeViewController:self withTitleImage:YES];
    [self configureFonts];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    self.labelTitle.text = LMLocalizedString(@"swopper FIT Messung", nil);
    self.labelMessage.text = LMLocalizedString(@"Wie lange sitzen Sie ca. täglich?", nil);
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.labelTitle.textColor = [UIColor customDarkGrayGolor];
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    
    self.labelMessage.textColor = [UIColor customDarkGrayGolor];
    self.labelMessage.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelMessage.font.pointSize];
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonNext:(id)sender {        
    if (self.session) {
        self.session.interval = @(self.timeRuler.interval);
    } else {
        self.report.interval = @(self.timeRuler.interval);
        self.session = [[SNAppDelegate sharedDelegate] currentSession];
        self.session.interval = self.report.interval;
        [self.report addSessionsObject:self.session];

    }
    
    [[DMManager sharedManager] saveContext];
    
    UIViewController *sideMenuController = [SNAppDelegate sharedDelegate].window.rootViewController;
    for (UIViewController *viewController in sideMenuController.childViewControllers) {
        if ([viewController isKindOfClass:[SNMenusController class]]) {
            [((SNMenusController *)viewController) showControllerWithIdentifier:NSStringFromClass([SNSummaryController class])];
            break;
        }
    }

//    [((SNMenusController *)self.navigationController.parentViewController) showControllerWithIdentifier:NSStringFromClass([SNSummaryController class])];
    
}

@end
