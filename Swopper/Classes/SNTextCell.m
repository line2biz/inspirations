
#import "SNTextCell.h"

#import "UIFont+Customization.h"

@implementation SNTextCell


- (void)awakeFromNib
{
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
}


@end
