

#import "SNCountdownControl.h"
#import "UIFont+Customization.h"
#import <AudioToolbox/AudioToolbox.h>

#define DEGREES_TO_RADIANS(angle) (((angle)* M_PI) / 180.0 )

@interface SNCountdownControl ()
{
    SystemSoundID _playSoundID;
    
    NSTimer *_timer;
    CGFloat _minSide;
    CGPoint _center;
    CGRect _workingRect;
    
    CGFloat _progress;
    CGFloat _lineWidth;
    CGFloat _fireInterval;
    CGFloat _timeElapsed;
    
    NSInteger _previousSec;
    
    UILabel *_labelDigit, *_labelText;
}
@end

@implementation SNCountdownControl


#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    // sound preparation
    _playBeep = YES;
    NSURL *soundURL = [NSURL fileURLWithPath:[[SNLocalizationManager sharedManager] pathForResource:@"Bleep" ofType:@"wav"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_playSoundID);
    
    // rest
    _isInProgress = NO;
    
    _fireInterval = 0.1;
    _minSide = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    _colorToDraw = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:0/255.0 alpha:1.0];
    _colorForPath = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:1.0];
    _center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    _lineWidth = 3.5;
    _workingRect = CGRectMake(_center.x - _minSide/2 + _lineWidth/2, _center.y - _minSide/2 + _lineWidth/2, _minSide - _lineWidth, _minSide - _lineWidth);
    
    [self setNeedsDisplay];
    self.backgroundColor = [UIColor clearColor];
    [self configureLabels];
}

#pragma mark - Property Accessors
- (void)setMaxTime:(NSTimeInterval)maxTime
{
    _maxTime = maxTime;
    _currentTime = maxTime;
    [self setNeedsDisplay];
    [self configureLabels];
}

- (void)setColorForPath:(UIColor *)colorForPath
{
    _colorForPath = colorForPath;
    [self setNeedsDisplay];
    [self configureLabels];
}

- (void)setColorToDraw:(UIColor *)colorToDraw
{
    _colorToDraw = colorToDraw;
    [self setNeedsDisplay];
    [self configureLabels];
}

#pragma mark - Calculations
- (void)configureLabels
{
    if (_labelDigit) {
        [_labelDigit removeFromSuperview];
        _labelDigit = nil;
    }
    if (_labelText) {
        [_labelText removeFromSuperview];
        _labelDigit = nil;
    }
    
    UIColor *textColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    
    _labelDigit = [[UILabel alloc] initWithFrame:CGRectZero];
    _labelDigit.font = [UIFont franklinGothicStdBookCondesedWithSize:75.0f];
    _labelDigit.text = [NSString stringWithFormat:@"%zd",_currentTime];
    _labelDigit.textColor = textColor;
    _labelDigit.textAlignment = NSTextAlignmentCenter;
    [_labelDigit sizeToFit];
    
    CGRect tempRect = _labelDigit.frame;
    tempRect.size.width = _minSide - _lineWidth*2;
    _labelDigit.frame = tempRect;
    
    _labelText = [[UILabel alloc] initWithFrame:CGRectZero];
    _labelText.font = [UIFont franklinGothicStdBookCondesedWithSize:15.0f];
    _labelText.text = LMLocalizedString(@"Sekunden", nil);
    _labelText.textColor = textColor;
    _labelText.textAlignment = NSTextAlignmentCenter;
    [_labelText sizeToFit];
    
    CGPoint position = CGPointMake(_center.x, _center.y - CGRectGetHeight(_labelDigit.frame)/10);
    _labelDigit.center = position;
    position = CGPointMake(_center.x, position.y + CGRectGetHeight(_labelDigit.frame)/2);
    _labelText.center = position;
    position.y -= 1;
    
    [self addSubview:_labelDigit];
    [self addSubview:_labelText];
}

#pragma mark - Drawing Methods
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextSetLineWidth(context, _lineWidth);
    
    // path
    UIBezierPath *bkgPath = [UIBezierPath bezierPathWithOvalInRect:_workingRect];
    CGContextAddPath(context, bkgPath.CGPath);
    CGContextSetStrokeColorWithColor(context, _colorForPath.CGColor);
    CGContextStrokePath(context);
    
    // progress
    CGFloat startAngle = - 90.0f;
    CGFloat endAngle = 360.0f * _progress + startAngle;
    UIBezierPath *progressPath = [UIBezierPath bezierPathWithArcCenter:_center
                                                                radius:_minSide/2 - _lineWidth/2
                                                            startAngle:DEGREES_TO_RADIANS(startAngle)
                                                              endAngle:DEGREES_TO_RADIANS(endAngle)
                                                             clockwise:YES];
    
    CGContextSetStrokeColorWithColor(context, _colorToDraw.CGColor);
    CGContextAddPath(context, progressPath.CGPath);
    CGContextStrokePath(context);

}

#pragma mark - View Methods
- (void)timerFired
{
    if (_currentTime) {
        _timeElapsed += _fireInterval;
        _progress = _timeElapsed / _maxTime;
        _currentTime = _maxTime - (_timeElapsed - (_timeElapsed - (NSInteger)_timeElapsed));
        _labelDigit.text = [NSString stringWithFormat:@"%zd",_currentTime];
        
        if (_currentTime != _previousSec) {
            _previousSec = _currentTime;
            [self soundTimerFired];
            [self.delegate countdownControl:self timerFired:YES];
        }
        
        if (_currentTime == 1) {
            _labelText.text = LMLocalizedString(@"Sekunde", nil);
        } else {
            _labelText.text = LMLocalizedString(@"Sekunden", nil);
        }
        if (_progress >= 1.0f) {
            _progress = 0.0f;
        }
        [self setNeedsDisplay];
    } else {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
        [self stopCountdownWithoutBackJump:YES];
    }
}

- (void)soundTimerFired
{
    if (self.playBeep) {
        AudioServicesPlaySystemSound(_playSoundID);
    }
}

#pragma mark - Public Methods
- (void)startCountdown
{
    _isInProgress = YES;
    _currentTime = _maxTime;
    _timeElapsed = 0.0f;
    _progress = 0.0f;
    _previousSec = _maxTime + 1;
    _timer = [NSTimer scheduledTimerWithTimeInterval:_fireInterval
                                              target:self
                                            selector:@selector(timerFired)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)stopCountdown
{
    [self stopCountdownWithoutBackJump:NO];
}

- (void)stopCountdownWithoutBackJump:(BOOL)withoutBack
{
    _isInProgress = NO;
    [_timer invalidate];
    _progress = 0.0f;
    _timeElapsed = 0.0f;
    if (!withoutBack) {
        _currentTime = _maxTime;
        _previousSec = _maxTime + 1;
        _labelDigit.text = [NSString stringWithFormat:@"%zd",(NSInteger)_maxTime];
        _labelText.text = LMLocalizedString(@"Sekunden", nil);
    }
    [self setNeedsDisplay];
}



@end
