

#import <Foundation/Foundation.h>

#undef SNLocalizedString
#define SNLocalizedString(key, comment) \
[[SNLocalizationManager sharedManager] localizedStringForKey:(key) value:(comment)]

#define LMLocalizedString(key, comment) \
[[SNLocalizationManager sharedManager] localizedStringForKey:(key) value:(comment)]


 
FOUNDATION_EXPORT NSString *const SNLocalizationManagerDidChangeLanguage;

 
@interface SNLocalizationManager : NSObject

+ (SNLocalizationManager *)sharedManager;


- (NSBundle *)bundle;
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;
- (void)setLanguage:(NSString*)langIdentifier;
- (NSString*)getLanguage;
- (void)resetLocalization;
- (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext;
- (NSURL *)URLForResource:(NSString *)name withExtension:(NSString *)ext;

@end
