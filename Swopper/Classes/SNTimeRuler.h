

#import <UIKit/UIKit.h>

@interface SNTimeRuler : UIControl

@property (assign, readonly, nonatomic) NSTimeInterval interval;
@property (strong, nonatomic) UIColor *gridColor;
@property (strong, nonatomic) UIColor *chosenColor;
@property (strong, nonatomic) UIFont *textMainFont;
@property (strong, nonatomic) UIFont *textCurrentFont;
@property (strong, nonatomic) UIFont *textSmallFont;

@end
