

#import "SNTimerSetterController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNStopwatchController.h"
#import "SNProgressView3D.h"
#import "SNMenusController.h"

#import "SNCarouselView.h"
#import "SNAudioVolumeController.h"
#import "SNNavController.h"
#import "SNTrainingController.h"

#import <MFSideMenu.h>

@interface SNTimerSetterController () <SNCarouselViewDelegate>
{
    NSArray *_arrayOfMinutes;
    NSTimeInterval _trainingTime;
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet SNCarouselView *carouselView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation SNTimerSetterController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _loadIdx = 0;
    if (self.navigationController.viewControllers[0] == self) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    }
    
    [SNAppearance customizeViewController:self];
    
    _arrayOfMinutes = [[NSArray alloc] initWithObjects:@(5),@(15),@(30), nil];
    
    [self prepareCarousel];
    
    [self configureFonts];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated {
//    if ([SNAudioVolumeController shouldBeShown]) {
//        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNAudioVolumeController class])];
//        SNNavController *navController = [[SNNavController alloc] initWithRootViewController:viewController];
//        [self presentViewController:navController animated:NO completion:NULL];
//    }
    
    
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Controller Methods

- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    
    self.labelTitle.text = LMLocalizedString(@"swopper FIT Messung", nil);
    self.labelMessage.text = LMLocalizedString(@"Wählen Sie Ihre Minutenzahl.", nil);
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.labelTitle.textColor = [UIColor customDarkGrayGolor];
    self.labelMessage.textColor = [UIColor customDarkGrayGolor];
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelMessage.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelMessage.font.pointSize];
}

- (void)prepareCarousel
{
    self.carouselView.delegate = self;
    
    NSMutableArray *mArrayOfElements = [NSMutableArray new];
    for (NSInteger i=0; i < _arrayOfMinutes.count; i++) {
        
        UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 140, 90)];
        [view setUserInteractionEnabled:YES];
        
        view.image = (i == 0) ? [UIImage imageNamed:@"Icon-Ellipse"] : [UIImage imageNamed:@"Icon-Ellipse-Gray"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:58];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor customDarkGrayGolor];
        label.text = [NSString stringWithFormat:@"%zd",[[_arrayOfMinutes objectAtIndex:i] integerValue]];
        [view addSubview:label];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(view.bounds)*1/2 + 15, CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds)/5)];
        textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:20];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.textColor = [UIColor customDarkGrayGolor];
        textLabel.text = LMLocalizedString(@"Minuten", nil);
        [view addSubview:textLabel];
        
        [mArrayOfElements addObject:view];
    }
    
    [self.carouselView setViews:mArrayOfElements];
    
    [self.carouselView setCurrentIndexTo:0 animated:NO];
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapNextButton:(id)sender
{
    if ([SNAudioVolumeController shouldBeShown]) {
        SNAudioVolumeController *audioVolumeController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNAudioVolumeController class])];
        audioVolumeController.trainingDuration = _trainingTime;
        [self.navigationController pushViewController:audioVolumeController animated:YES];
    } else {
        SNTrainingController *trainingController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNTrainingController class])];
        trainingController.trainingTime = _trainingTime;
        [self.navigationController pushViewController:trainingController animated:YES];        
    }
}

#pragma mark - Carousel Delegate Methods
- (void)carouselView:(SNCarouselView *)view changedIndexTo:(NSInteger)index
{
    _trainingTime = [[_arrayOfMinutes objectAtIndex:index] integerValue] * 60;
}

- (void)carouselView:(SNCarouselView *)view finishedAnimation:(BOOL)finished forIndex:(NSInteger)index
{
    [view.views enumerateObjectsUsingBlock:^(UIImageView *imageView, NSUInteger idx, BOOL *stop) {
        imageView.image = (idx == index) ? [UIImage imageNamed:@"Icon-Ellipse"] : [UIImage imageNamed:@"Icon-Ellipse-Gray"];
    }];
}



@end
