

#import "SNSummaryController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNMenusController.h"
#import "SNStopwatchController.h"
#import "SNProgressView3D.h"
#import "SNReviewController.h"
#import "SNTimerSetterController.h"

#import "DMSession+Category.h"
#import "DMMotion+Category.h"
#import "DMReport+Category.h"
#import "DMUser.h"

#import "AnimatedLabel.h"
#import "SNCurvedWavesView.h"
#import "SNReportsController.h"

#import <MFSideMenu.h>

@import QuartzCore;

@interface SNSummaryController () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, SNProgressView3DProtocol>

@property (weak, nonatomic) IBOutlet AnimatedLabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueMeasurLabel;
@property (weak, nonatomic) IBOutlet UIImageView *valueMeasurImage;
@property (weak, nonatomic) IBOutlet AnimatedLabel *swopsLabel;
@property (weak, nonatomic) IBOutlet AnimatedLabel *tagLabel;
@property (weak, nonatomic) IBOutlet UILabel *LabelCalDesc;
@property (weak, nonatomic) IBOutlet UILabel *LabelSwoppsDesc;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) NSArray *items;
@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;

// swiping
@property (assign, nonatomic) CGPoint previousPoint;
@property (assign, nonatomic) BOOL firstMeasurement;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (assign, nonatomic) NSInteger swoppsValue;
@property (assign, nonatomic) NSInteger caloriesValue;

// review
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) NSTimeInterval timeInterval;
@property (weak, nonatomic) IBOutlet SNCurvedWavesView *curvedWavesView;

//@property (assign, nonatomic) BOOL dailyResults;

@property (strong, nonatomic) UIImage *sharedImage;

@end

@implementation SNSummaryController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [SNAppearance customizeViewController:self];
    
//    _dailyResults = YES;
    
    self.LabelCalDesc.text = LMLocalizedString(@"kcal pro Tag", nil);
    self.LabelSwoppsDesc.text = LMLocalizedString(@"swopps pro Tag", nil);
    
    [self.navigationItem setHidesBackButton:YES];
    self.labelTitle.text = LMLocalizedString(@"Zusammenfassung", nil);
    
    SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    NSURL *url = [[SNLocalizationManager sharedManager] URLForResource:@"MessungMenuItems" withExtension:@"plist"];
    
    self.items = [NSArray arrayWithContentsOfURL:url];
    
    [self prepareTimer];
    
    self.swoppsValue = [self.session countsForSession];
    self.caloriesValue = [self.session caloriesPerSession]*1000;
    
    [self.progressView configureProgressViewWithMaxPoints:1
                                             currentPoint:0
                                                pathColor:[UIColor customLightGrayColor]
                                            progressColor:[UIColor customOrangeColor]
                                                lineWidth:10.0f];
    [self.progressView setDelegate:self];
    self.progressView.dontTransform = YES;
    
    UIPanGestureRecognizer *panGestureRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    panGestureRecog.delegate = self;
    [self.progressView addGestureRecognizer:panGestureRecog];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    
    
//    NSData * data = UIImagePNGRepresentation(image);
//    [data writeToFile:@"foo.png" atomically:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -200.0;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, - M_PI / 8, 0.0f, 1.0f, 0.0f);
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI * 0.3f , 1.0f, 0.0f, 0.0f);
    
    CAKeyframeAnimation *keyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    keyFrameAnimation.duration = 0.001;
    keyFrameAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    keyFrameAnimation.removedOnCompletion = NO;
    keyFrameAnimation.fillMode = kCAFillModeForwards;
    keyFrameAnimation.values = @[[NSValue valueWithCATransform3D:rotationAndPerspectiveTransform]];
    
    [CATransaction begin];
    
    [self.curvedWavesView.layer addAnimation:keyFrameAnimation forKey:@"keyFrame"];
    
    [CATransaction commit];
    
    [self customizeOutlets];
    _firstMeasurement = YES;
    [self swipeToNextMeasurementType];
    
    [self.progressView startProgressingTimerWithDuration:5];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self startTimer:NO];
    [self.progressView stopProgressingTimer];
    [self.valueLabel stopTimer];
    [self.swopsLabel stopTimer];
    [self.tagLabel stopTimer];
}


#pragma mark - Property Accessors
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
    return _managedObjectContext;
}

- (DMSession *)session
{
    if (!_session) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:DMSessionStartTimeKey ascending:NO];
        fetchRequest.sortDescriptors = @[sortDesc];
        fetchRequest.fetchLimit = 1;
        NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];
        _session = [results firstObject];
    }
    
    return _session;
}


#pragma mark - Private Methods
- (void)customizeOutlets
{
    for (UILabel *label in self.labels) {
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
    }
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    
    // getting data for subtitle text
    
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setLocale:[SNAppDelegate currentLocale]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM YYYY" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormat.dateFormat = format;
    NSString *weekStart = [dateFormat stringFromDate:[NSDate date]];

    
    DMReport *report = [DMReport reportByDate:[NSDate date] inContext:self.managedObjectContext];
    NSInteger sessionsCount = [report.sessions count];
    
    if (sessionsCount == 1) {
        self.labelSubtitle.text = [NSString stringWithFormat:@"%@ - %zd%@", weekStart, sessionsCount, LMLocalizedString(@". Messung", nil)];
    } else {
        self.labelSubtitle.text = [NSString stringWithFormat:@"%@ - %zd%@", weekStart, sessionsCount, LMLocalizedString(@". Messung", nil)];
    }
    
    
    NSInteger caloriesToday = 0, swoppsToday = 0;
    swoppsToday = [self.session countsPerMinute];
    swoppsToday = swoppsToday * (self.session.interval.floatValue / 60);
    caloriesToday = [self.session caloriesPerSession] / [self.session.sessionDuration floatValue] * self.session.interval.floatValue;
    
    self.tagLabel.text = [NSString stringWithFormat:@"%zd", caloriesToday];
    self.swopsLabel.text = [NSString stringWithFormat:@"%zd", swoppsToday];
    self.swopsLabel.textColor = [UIColor customOrangeColor];
    self.tagLabel.textColor = [UIColor customOrangeColor];
    
    [self.tagLabel animateTextFromMinValue:0
                                toMaxValue:caloriesToday
                                withPrefix:nil
                                    suffix:nil
                                 maxPrefix:nil
                                 maxSuffix:nil
                                  duration:5.0
                                  andSteps:40
                           andCompletition:nil];
    [self.swopsLabel animateTextFromMinValue:0
                                  toMaxValue:swoppsToday
                                  withPrefix:nil
                                      suffix:nil
                                   maxPrefix:nil
                                   maxSuffix:nil
                                    duration:5.0
                                    andSteps:40
                             andCompletition:nil];
    [self.valueLabel animateTextFromMinValue:0
                                  toMaxValue:(NSInteger)self.caloriesValue
                                  withPrefix:nil
                                      suffix:nil
                                   maxPrefix:nil
                                   maxSuffix:nil
                                    duration:5.0
                                    andSteps:40
                             andCompletition:^{
                                 self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Repeat-Gray"] style:UIBarButtonItemStylePlain target:self action:@selector(didTapPlayButton:)];
                                 [self startTimer:YES];
                             }];
}

- (void)swipeToNextMeasurementType
{
    _firstMeasurement = !_firstMeasurement;
    self.valueLabel.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:(self.firstMeasurement) ? self.swoppsValue : self.caloriesValue]];
    self.valueMeasurLabel.text = (self.firstMeasurement) ? @"Moves" : @"cal";
    self.valueMeasurImage.image = (self.firstMeasurement) ? [UIImage imageNamed:@"Icon-Chair"] : [UIImage imageNamed:@"Icon-Flame"];
    [self.pageControl setCurrentPage:!self.firstMeasurement];
    
    
}

//- (void)dailyToCurrent
//{
//    NSInteger caloriesToday = 0, swoppsToday = 0;
//    swoppsToday = [self.session countsPerMinute];
//    swoppsToday = swoppsToday * (self.session.interval.floatValue / 60);
//    caloriesToday = [self.session caloriesPerSession] / [self.session.sessionDuration floatValue] * self.session.interval.floatValue;
//    if (_dailyResults) {
//        self.tagLabel.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:caloriesToday]];
//        self.LabelCalDesc.text = LMLocalizedString(@"kcal pro Tag", nil);
//
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            self.swopsLabel.text = [NSString stringWithFormat:@"%@",[AnimatedLabel textWithThousendsFormat:swoppsToday]];


//            self.LabelCalDesc.text = LMLocalizedString(@"kcal pro Tag", nil);
//            self.LabelSwoppsDesc.text = LMLocalizedString(@"swopps pro Tag", nil);
//        });
//    } else {
//        self.tagLabel.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:[self.session caloriesPerSession]*1000]];
//        self.LabelCalDesc.text = LMLocalizedString(@"cal", nil);
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            self.swopsLabel.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:[self.session countsForSession]]];
//            self.LabelSwoppsDesc.text = LMLocalizedString(@"swopps", nil);
//        });
//    }
//    self.labelTitle.text = (_dailyResults) ? @"Tageshochrechnung" : @"Messergebnis";
//    _dailyResults = !_dailyResults;
//}


#pragma mark Timer Methods
- (void)prepareTimer
{
    _timeInterval = 4.0f;
}

- (void)startTimer:(BOOL)start
{
    if (start) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:_timeInterval
                                                  target:self
                                                selector:@selector(timerHandler:)
                                                userInfo:nil
                                                 repeats:YES];
    } else {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)pauseTimer:(BOOL)pause
{
    if (pause) {
        [self.timer invalidate];
        self.timer = nil;
    } else {
        _timer = [NSTimer scheduledTimerWithTimeInterval:_timeInterval
                                                  target:self
                                                selector:@selector(timerHandler:)
                                                userInfo:nil
                                                 repeats:YES];
    }
}

- (void)timerHandler:(NSTimer *)sender
{
    [self swipeToNextMeasurementType];
}

#pragma mark - Outlet Methods
- (IBAction)didTapPlayButton:(UIBarButtonItem *)sender
{
    self.navigationItem.rightBarButtonItem = nil;
    [self startTimer:NO];
    NSInteger swoppsToday = 0, caloriesToday = 0;
    swoppsToday = [self.session countsPerMinute];
    swoppsToday = swoppsToday * (self.session.interval.floatValue / 60) / 10;
    caloriesToday = [self.session caloriesPerSession] / [self.session.sessionDuration floatValue] * self.session.interval.floatValue;
    self.LabelCalDesc.text = LMLocalizedString(@"kcal pro Tag", nil);
    self.LabelSwoppsDesc.text = LMLocalizedString(@"Moves pro Tag", nil);
    [self.tagLabel animateTextFromMinValue:0
                                toMaxValue:caloriesToday
                                withPrefix:nil
                                    suffix:nil
                                 maxPrefix:nil
                                 maxSuffix:nil
                                  duration:5.0
                                  andSteps:40
                           andCompletition:nil];
    [self.swopsLabel animateTextFromMinValue:0
                                  toMaxValue:swoppsToday
                                  withPrefix:nil
                                      suffix:nil
                                   maxPrefix:nil
                                   maxSuffix:nil
                                    duration:5.0
                                    andSteps:40
                             andCompletition:nil];
    [self.valueLabel animateTextFromMinValue:0
                                  toMaxValue:(NSInteger)self.caloriesValue
                                  withPrefix:nil
                                      suffix:nil
                                   maxPrefix:nil
                                   maxSuffix:nil
                                    duration:5.0
                                    andSteps:40
                             andCompletition:^{
                                 self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Repeat-Gray"] style:UIBarButtonItemStylePlain target:self action:@selector(didTapPlayButton:)];
                                 [self startTimer:YES];
                             }];
    [self.progressView startProgressingTimerWithDuration:5.0];
}

#pragma mark - Pan Gesture Methods
- (void)panGestureHandler:(UIPanGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self.progressView];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _previousPoint = point;
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_timer) {
                [self startTimer:NO];
                [self swipeToNextMeasurementType];
                [self startTimer:YES];
            } else {
                [self swipeToNextMeasurementType];
            }
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - Segue Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SNReviewController class]]) {
        [segue.destinationViewController setManagedObjectContext:self.managedObjectContext];
        [segue.destinationViewController setSession:self.session];
    }
    else if ([segue.destinationViewController isKindOfClass:[SNReportsController class]]) {
        [segue.destinationViewController setManagedObjectContext:self.managedObjectContext];
        [segue.destinationViewController setCurrentReport:self.session.report];
        
        NSLog(@"%s %@", __FUNCTION__, self.session.report);
    }
}

#pragma mark - Progress View 3D Delegate
- (void)progressView:(SNProgressView3D *)progressView timerActive:(BOOL)active
{
    if (active) {
        [self.curvedWavesView turnTimerOff:YES];
        
        CGFloat maxMul = 0;
        if (self.caloriesValue > 0 && self.caloriesValue <= 300) {
            maxMul = 0.33;
        } else if (self.caloriesValue > 300 && self.caloriesValue <= 1000) {
            maxMul = 0.66;
        } else if (self.caloriesValue > 1000) {
            maxMul = 1.0;
        }
        
        CGFloat multiplier = progressView.currentPoint / progressView.maxPoints / 3 * maxMul;
        self.curvedWavesView.multiplier = multiplier;
    } else {
        [self.curvedWavesView turnTimerOff:NO];
    }
}

#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dictionary = self.items[indexPath.row];
    cell.textLabel.text = [dictionary valueForKey:@"title"];
    NSString *imageName = [dictionary[@"icon"] stringByAppendingString:@"-Gray"];
    cell.imageView.image = [UIImage imageNamed:imageName];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *className = self.items[indexPath.row][@"class"];
    className = [className stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([className length] > 0) {
        NSString *className = self.items[indexPath.row][@"class"];
        [self performSegueWithIdentifier:className sender:self];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSString *action = self.items[indexPath.row][@"action"];
        if ([action length] > 0) {
            if ([action isEqualToString:@"Share"]) {
                [self shareResults];
            }
            else if ([action isEqualToString:@"Refresh"]) {
                UIViewController *sideMenuController = [SNAppDelegate sharedDelegate].window.rootViewController;
                for (UIViewController *viewController in sideMenuController.childViewControllers) {
                    if ([viewController isKindOfClass:[SNMenusController class]]) {
                        [((SNMenusController *)viewController) showControllerWithIdentifier:@"SNTimerSetterController"];
                        break;
                    }
                }
                
//                [((SNMenusController *)self.navigationController.parentViewController) showControllerWithIdentifier:@"SNTimerSetterController"];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
}

#pragma mark - Share Results
- (void)shareResults {
    
    UIView *viewToRender = self.tableView.tableHeaderView;
    UIGraphicsBeginImageContextWithOptions(viewToRender.bounds.size, YES, 0);
    [viewToRender drawViewHierarchyInRect:viewToRender.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.sharedImage = image;
    
    
    NSArray *objectsToShare = @[@"", self.sharedImage];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //    NSArray *excludedActivities = @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
    //                                    UIActivityTypePostToWeibo,
    //                                    UIActivityTypeMessage, UIActivityTypeMail,
    //                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
    //                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
    //                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
    //                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
    //    controller.excludedActivityTypes = excludedActivities;
    
    [self presentViewController:controller animated:YES completion:nil];
}



@end
