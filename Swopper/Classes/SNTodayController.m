
#import "SNTodayController.h"
#import <MFSideMenu.h>
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNTodayCell.h"

#import "DMSession+Category.h"
#import "DMStep+Category.h"
#import "DMReport+Category.h"
#import "SNMenusController.h"
#import "SNStepManager.h"
#import "SNLocalizationManager.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface SNTodayController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, SNTodayCellCompletitionDelegate>
{
    NSInteger _loadIdx;
    BOOL _bMovesCaloriesAnimated;
}

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *moreIcon;

@property (weak, nonatomic) IBOutlet UIImageView *bottomLogo;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SNTodayController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loadIdx = 0;
    // Do any additional setup after loading the view.
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [SNAppearance customizeViewController:self];
    
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:[[SNLocalizationManager sharedManager] pathForResource:@"Today-Logo@2x" ofType:@"png"]];
    _bottomLogo.image = image;
    
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    
    // formatters
    self.titleLabel.text = LMLocalizedString(@"Heute", nil);
    self.moreLabel.text = LMLocalizedString(@"Mehr", nil);
    
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormat.locale = [SNAppDelegate currentLocale];
    self.subtitleLabel.text = [dateFormat stringFromDate:[NSDate date]];
    
    
    CGFloat startX = _bottomLogo.frame.origin.x;
    CGFloat tableStartX = self.tableView.frame.origin.x;
    CGFloat dateStartX = self.subtitleLabel.frame.origin.x;
    
    CGFloat endX = 25;
    
    CGFloat constraintStart = 8;
    NSMutableArray *constrains = [NSMutableArray array];
    for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
        SNTodayCell *cell = (SNTodayCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [constrains addObject:cell.constraint];
    }
    
    sidemenu.leftMenuPanCallback = ^(CGFloat percent) {
        
        CGRect rect = _bottomLogo.frame;
        rect.origin.x = startX - (startX - endX)*percent;
        
        CGRect tableRect = self.tableView.frame;
        tableRect.origin.x = tableStartX - (tableStartX - endX + 10)*percent;
        
        CGRect subtitleRect = self.subtitleLabel.frame;
        subtitleRect.origin.x = dateStartX + 40*percent;
        
        self.bottomLogo.frame = rect;
        self.tableView.frame = tableRect;
        self.subtitleLabel.frame = subtitleRect;
        
        for (NSLayoutConstraint *constraint in constrains) {
            constraint.constant = constraintStart + 20*percent;
        }
        
        [self.view layoutIfNeeded]; // update constrains
    };
    void(^AddTapGesture)(UIView *, id <UIGestureRecognizerDelegate>, SEL) = ^(UIView *view, id <UIGestureRecognizerDelegate> target, SEL method) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:method];
        tapGesture.delegate = target;
        [view setUserInteractionEnabled:YES];
        [view addGestureRecognizer:tapGesture];
    };
    AddTapGesture(self.view, self, @selector(tapGestureHandler:));
    
    
    __weak typeof(self) weakSelf  = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:SNStepManagerValueChanged object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [weakSelf.tableView reloadData];
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    for (UILabel *label in self.labels) {
        UIFont *font;
        if (label == self.titleLabel) {
            font = [UIFont franklinGothicStdBookCondesedMedWithSize:label.font.pointSize];
        } else {
            font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
        }
        label.font = font;
        
        if (label == self.moreLabel) {
            label.textColor = [UIColor customOrangeColor];
        }
        else {
            label.textColor = [UIColor titleColor];
        }
        
        NSDictionary *attributes = @{
                                     NSFontAttributeName : font,
                                     NSForegroundColorAttributeName : label.textColor
                                     };
        
        NSAttributedString *str = [[NSAttributedString alloc] initWithString:label.text attributes:attributes];
        CGSize size = CGSizeMake(CGRectGetWidth(label.frame), CGFLOAT_MAX);
        NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
        size.height = [str boundingRectWithSize:size options:options context:NULL].size.height;
        CGRect frame = label.frame;
        frame.size.height = size.height;
        label.frame = frame;
        label.attributedText = str;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_loadIdx) {
        [self configureView];
        _loadIdx++;
    }
}

#pragma mark - Controller Methods
- (void)configureView
{
    
}


#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([CMPedometer isStepCountingAvailable]) {
            return 3;
        } else {
            return 2;
        }
    } else {
        if ([CMStepCounter isStepCountingAvailable]) {
            return 3;
        } else {
            return 2;
        }
    }
}

- (void)configureCell:(SNTodayCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font;
    NSArray *labels = @[cell.labelNumber, cell.labelTitle];
    
    for (UILabel *label in labels) {
        font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
        label.font = font;
        label.textColor = [UIColor titleColor];
    }
    
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"de_DE"];
    
    // get today data
    
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] defaultContext];
    DMStep *step = [DMStep stepByDate:[NSDate date] inContext:managedObjectContext];
//    DMReport *report = [DMReport reportByDate:[NSDate date] inContext:managedObjectContext];
    
    NSInteger iCalories = 0, iSwopps = 0, iSteps = 0, iStepCalories = 0;
    
    // old sessions data
//    if (report) {
//        CGFloat calories = 0;
//        NSInteger swopps = 0;
//        for (DMSession *session in report.sessions) {
//            calories += [session caloriesPerSession] / session.sessionDuration.floatValue * report.interval.floatValue;
//            swopps += [session countsForSession];
//        }
//        calories = calories / report.sessions.count;
//        iSwopps = swopps / report.sessions.count;
//        iCalories += calories;
//    }
    if (step) {
        iStepCalories += [DMStep calculateCaloriesForSteps:[step.numberOfSteps integerValue]];
        iSteps = [step.numberOfSteps integerValue];
    }
    
    // new session data
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMSessionStartTimeKey
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *todayDateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:[NSDate date]];
        
        for (DMSession *session in fetchedObjects) {
            
            NSDateComponents *sessionDateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:session.endTime];
            
            if (sessionDateComponents.day == todayDateComponents.day && sessionDateComponents.month == todayDateComponents.month && sessionDateComponents.year == todayDateComponents.year) {
                iCalories += [session caloriesPerSession];
                iSwopps += [session countsForSession];                
            }
            
        }
    } else {
        if (error) {
            NSLog(@"\n%s\nError while fetching sessions:\n%@\n",__FUNCTION__, error.localizedDescription);
        }
    }
    
    // paste data
    
    NSInteger iTableIndex = indexPath.row;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (![CMPedometer isStepCountingAvailable]) {
            if (iTableIndex > 0) {
                iTableIndex++;
            }
        }
    } else {
        if (![CMStepCounter isStepCountingAvailable]) {
            if (iTableIndex > 0) {
                iTableIndex++;
            }
        }
    }
    
    switch (iTableIndex) {
        case 0:
        {
            cell.labelTitle.text = LMLocalizedString(@"Moves", nil);
            cell.iValue = iSwopps;
            cell.imageViewCircle.image = [UIImage imageNamed:@"Today-Circle-2"];
        }
            break;
        
        case 1:
        {
            cell.labelTitle.text = LMLocalizedString(@"Schritte", nil);
            if (_bMovesCaloriesAnimated) {
                cell.iValue = [step.numberOfSteps integerValue];
            } else {
                cell.iValue = 0;
            }
            cell.imageViewCircle.image = [UIImage imageNamed:@"Today-Circle-1"];
        }
            break;
        
        case 2:
        {
            cell.labelTitle.text = LMLocalizedString(@"kcal der Trainings", nil);
            if (!_bMovesCaloriesAnimated) {
                cell.iValue = iCalories;
                cell.completitionDelegate = self;
            } else {
                cell.iValue = iCalories + iStepCalories;
            }
            cell.imageViewCircle.image = [UIImage imageNamed:@"Today-Circle-3"];
        }
            break;
            
        default:
            break;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ReusableCellIdentifier = @"Cell";
    SNTodayCell *cell = (SNTodayCell *)[tableView dequeueReusableCellWithIdentifier:ReusableCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Tap Gesture Handler
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    SNMenusController *menuController = (SNMenusController *)[sidemenu leftMenuViewController];
    
    CGPoint point = [sender locationInView:sender.view];
    
    if (CGRectContainsPoint(self.bottomLogo.frame, point)) {
        [menuController showControllerWithIdentifier:@"SNImpressumController"];
    } else if (CGRectContainsPoint(self.moreIcon.frame, point) || CGRectContainsPoint(self.moreLabel.frame, point)) {
        [sidemenu toggleLeftSideMenuCompletion:nil];
    }
}


#pragma mark - SNTodayCell Completition Delegate
- (void)TodayCell:(SNTodayCell *)cell finishedTextAnimation:(BOOL)finished
{
    cell.completitionDelegate = nil;
    
    _bMovesCaloriesAnimated = YES;
    
    [self.tableView reloadData];
}


@end
