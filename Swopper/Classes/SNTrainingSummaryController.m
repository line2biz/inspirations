

#import "SNTrainingSummaryController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "DMSession+Category.h"
#import "DMMotion+Category.h"
#import "DMReport+Category.h"

#import "SNMenusController.h"

#import "SNStopwatchController.h"
#import "AnimatedLabel.h"
#import "SNAudioVolumeController.h"

#import <MFSideMenu.h>

#import "SNBasicCell.h"

@interface SNTrainingSummaryController () <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger _loadIdx;
    BOOL _negative;
//    NSTimer *_timer;
//    BOOL _dailyResults;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProduct;
@property (weak, nonatomic) IBOutlet AnimatedLabel *labelCalories;
@property (weak, nonatomic) IBOutlet UILabel *labelCaloriesText;
@property (weak, nonatomic) IBOutlet AnimatedLabel *labelSwopps;
@property (weak, nonatomic) IBOutlet UILabel *labelSwoppsText;
@property (weak, nonatomic) IBOutlet UILabel *shareLabel;
@property (weak, nonatomic) IBOutlet UILabel *repeatLabel;

@property (strong, nonatomic) NSArray *arrayOfCells;

@end

@implementation SNTrainingSummaryController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    [SNAppearance customizeViewController:self];
    SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:menusController
                                                                            action:@selector(hideViewController:)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    self.labelCalories.text = @"0";
    self.labelSwopps.text = @"0";

    // create table view datasource
    NSDictionary *(^MenuItem)(NSString *, NSString *) = ^(NSString *title, NSString *icon) {
        NSDictionary *dict = @{@"title":title, @"icon":icon};
        return dict;
    };
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    // add new menu items here
    
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Ergebnis teilen", nil), @"Icon-Share-Gray")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Training wiederholen", nil), @"Icon-Repeat-Gray")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Erinnerung", nil), @"Icon-Memory-Gray")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Statistik", nil), @"Icon-Statistic-Gray")];
    
    self.arrayOfCells = mArrayOfDicts;
    
    [self configureFonts];
    [self configureText];
    [self startProductAnimation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self configureText];
        [self startProductAnimation];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self startTimer:NO];
}

#pragma mark - Controller Methods
- (void)configureFonts
{
    self.labelTitle.text = LMLocalizedString(@"Zusammenfassung", nil);
    
    self.labelSwoppsText.text = LMLocalizedString(@"Moves pro h", nil);
    self.shareLabel.text = LMLocalizedString(@"Ergebnis teilen", nil);
    self.repeatLabel.text = LMLocalizedString(@"Training wiederholen", nil);
    
    
    self.labelSwopps.textColor = [UIColor customOrangeColor];
    self.labelCalories.textColor = [UIColor customOrangeColor];
    
    [self.labelTitle setFont:[UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize]];
    [self.labelSubtitle setFont:[UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize]];
    [self.labelDescription setFont:[UIFont franklinGothicStdBookCondesedWithSize:self.labelDescription.font.pointSize]];
}

- (void)configureText
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocale:[SNAppDelegate currentLocale]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM YYYY" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormatter.dateFormat = format;

    NSString *sDate = [dateFormatter stringFromDate:self.session.startTime];
    
    dateFormatter.dateFormat = @"HH:mm";
    sDate = [sDate stringByAppendingFormat:@" - %@", [dateFormatter stringFromDate:self.session.startTime]];
    sDate = [sDate stringByAppendingFormat:@" %@", LMLocalizedString(@"Uhr", nil)];
    self.labelSubtitle.text = sDate;
    
    // parse file
    NSString* fileRoot = [[SNLocalizationManager sharedManager] pathForResource:@"Callories" ofType:@"tsv"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    for (NSInteger i=0; i<allLinedStrings.count; i++) {
        NSString *line = [allLinedStrings objectAtIndex:i];
        NSArray *arrayOfSubstrings = [line componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\t"]];
        
        if ([arrayOfSubstrings count] >= 4) {
            NSDictionary *dict = @{@"title":arrayOfSubstrings[0], @"callories":arrayOfSubstrings[1], @"result":arrayOfSubstrings[2], @"desc":arrayOfSubstrings[3]};
            [mArrayOfDicts addObject:dict];
        }
        
        
        
    }
    
    __block NSInteger indexOfProduct = 0;
    NSInteger caloriesToday = 0, swoppsToday = 0;
    swoppsToday = [self.session countsPerMinute] * 60;
    NSInteger iCaloriesForCheck = [self.session caloriesPerHour];
    
    
    double dCalories = [self.session caloriesPerHour];
    
    if (dCalories < 1.0f) {
        
        self.labelCaloriesText.text = LMLocalizedString(@"cal pro h", nil);
        caloriesToday = (NSInteger)(dCalories * 1000);
        
    } else {
        
        self.labelCaloriesText.text = LMLocalizedString(@"kcal pro h", nil);
        caloriesToday = (NSInteger)dCalories;        
    }
    
    [mArrayOfDicts enumerateObjectsUsingBlock:^(NSDictionary *product, NSUInteger idx, BOOL *stop) {
        NSInteger productCallories = [product[@"callories"] integerValue];
        if (iCaloriesForCheck >= productCallories) {
            indexOfProduct = idx;
        }
    }];
    
    if (indexOfProduct == 0) {
        indexOfProduct = arc4random()%5;
    }
    
    NSDictionary *productDict = [mArrayOfDicts objectAtIndex:indexOfProduct];
    NSString *productDescription = productDict[@"desc"];
    BOOL result = ([productDict[@"result"] isEqualToString:@"Positiv"]) ? YES : NO;
    
    _negative = !result;
    // text for title
    
    NSArray *arrayOfTitles = (result) ? [[NSArray alloc] initWithObjects:LMLocalizedString(@"Gratulation!", nil), LMLocalizedString(@"Grandios!", nil), LMLocalizedString(@"Super!", nil), LMLocalizedString(@"Weltklasse!", nil), LMLocalizedString(@"Toll gemacht!", nil), LMLocalizedString(@"Sehr schön!", nil), nil] : @[productDict[@"title"]];
    NSInteger randomIdx = (result) ? arc4random_uniform((uint32_t)arrayOfTitles.count-1) : 0 ;
    self.labelTitle.text = [arrayOfTitles objectAtIndex:randomIdx];
    
    // description text
    
    UIImage *productImage = [UIImage imageNamed:[NSString stringWithFormat:@"%zd",[productDict[@"callories"] integerValue]]];
    if (productImage) {
        self.imageViewProduct.image = productImage;
    } else {
        self.imageViewProduct.image = [UIImage imageNamed:@"Negativ"];
    }
    self.labelDescription.text = productDescription;
    
    
    // text for daily statistic
    
    if (caloriesToday > 0) {
        [self.labelCalories animateTextFromMinValue:0
                                         toMaxValue:caloriesToday
                                         withPrefix:nil
                                             suffix:nil
                                          maxPrefix:nil
                                          maxSuffix:nil
                                           duration:3.0
                                           andSteps:40];
    }
    if (swoppsToday) {
        [self.labelSwopps animateTextFromMinValue:0
                                       toMaxValue:swoppsToday
                                       withPrefix:nil
                                           suffix:nil
                                        maxPrefix:nil
                                        maxSuffix:nil
                                         duration:3.0
                                         andSteps:40
                                  andCompletition:^{
//                                      [self startTimer:YES];
                                  }];
    }
}

- (void)shareResults {
    
    UIView *viewToRender = self.tableView.tableHeaderView;
    UIGraphicsBeginImageContextWithOptions(viewToRender.bounds.size, YES, 0);
    [viewToRender drawViewHierarchyInRect:viewToRender.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *objectsToShare = @[@"", image];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //    NSArray *excludedActivities = @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
    //                                    UIActivityTypePostToWeibo,
    //                                    UIActivityTypeMessage, UIActivityTypeMail,
    //                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
    //                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
    //                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
    //                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
    //    controller.excludedActivityTypes = excludedActivities;
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)startProductAnimation
{
    self.imageViewProduct.alpha = 0.0f;
    __block CATransform3D xForm = CATransform3DIdentity;
    xForm = CATransform3DScale(xForm, 0.1, 0.1, 1.0);
    
    __block CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 0.001;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:0.001];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            __block NSTimeInterval duration = 0.5f;
            [UIView animateWithDuration:duration
                             animations:^{
                                 strongSelf.imageViewProduct.alpha = 1.0f;
                             }];
            xForm = CATransform3DIdentity;
            xForm = CATransform3DScale(xForm, 1.2, 1.2, 1.0);
            
            transformAnimation.duration = duration;
            transformAnimation.toValue = [NSValue valueWithCATransform3D:xForm];
            
            [CATransaction setAnimationDuration:duration];
            [CATransaction setCompletionBlock:^{
                duration = duration/2;
                xForm = CATransform3DIdentity;
                if (!_negative) {
                    xForm = CATransform3DScale(xForm, 1.0, 1.0, 1.0);
                } else {
                    xForm = CATransform3DScale(xForm, 0.6, 0.6, 1.0);
                }
                
                transformAnimation.duration = duration;
                transformAnimation.toValue = [NSValue valueWithCATransform3D:xForm];
                
                [strongSelf.imageViewProduct.layer addAnimation:transformAnimation forKey:@"endAnimation"];
            }];
            
            [CATransaction begin];
            
            [strongSelf.imageViewProduct.layer addAnimation:transformAnimation forKey:@"midAnimation"];
            
            [CATransaction commit];
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewProduct.layer addAnimation:transformAnimation forKey:@"preparationAnimation"];
    
    [CATransaction commit];
}

//#pragma mark - Timer Methods
//- (void)startTimer:(BOOL)start
//{
//    if (start) {
//        _timer = [NSTimer scheduledTimerWithTimeInterval:3.0
//                                                  target:self
//                                                selector:@selector(timerHandler:)
//                                                userInfo:nil
//                                                 repeats:YES];
//    } else {
//        [_timer invalidate];
//        _timer = nil;
//    }
//}
//
//- (void)timerHandler:(NSTimer *)sender
//{
//    NSInteger caloriesToday = 0, swoppsToday = 0;
//    swoppsToday = [self.session countsPerMinute] * 8 * 60;
//    caloriesToday = [self.session caloriesPerSession] / [self.session.sessionDuration floatValue] * 8 * 60 * 60;
//    
//    if (_dailyResults) {
//        self.labelCalories.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:caloriesToday]];
//        self.labelCaloriesText.text = LMLocalizedString(@"kcal pro Tag", nil);
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            self.labelSwopps.text = [NSString stringWithFormat:@"%@",[AnimatedLabel textWithThousendsFormat:swoppsToday]];
//            self.labelSwoppsText.text = LMLocalizedString(@"swopps pro Tag", nil);
//        });
//    } else {
//        self.labelCalories.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:[self.session caloriesPerSession]*1000]];
//        self.labelCaloriesText.text = LMLocalizedString(@"cal", nil);
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            self.labelSwopps.text = [NSString stringWithFormat:@"%@", [AnimatedLabel textWithThousendsFormat:[self.session countsForSession]]];
//            self.labelSwoppsText.text = LMLocalizedString(@"swopps", nil);
//        });
//    }
//    _dailyResults = !_dailyResults;
//}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            [self shareResults];
        }
            break;
        case 1:
        {
//            [((SNMenusController *)self.navigationController.parentViewController) showControllerWithIdentifier:@"SNTrainingController"];
            UIViewController *sideMenuController = [SNAppDelegate sharedDelegate].window.rootViewController;
            for (UIViewController *viewController in sideMenuController.childViewControllers) {
                if ([viewController isKindOfClass:[SNMenusController class]]) {
                    if ([SNAudioVolumeController shouldBeShown]) {
                        [((SNMenusController *)viewController) showControllerWithIdentifier:@"SNAudioVolumeController"];
                    } else {
                        [((SNMenusController *)viewController) showControllerWithIdentifier:@"SNTrainingController"];
                    }
                    break;
                }
            }
        }
            break;
        case 2:
        {
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNMemoryController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 3:
        {
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNStatisticViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayOfCells count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNBasicCell *cell = (SNBasicCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dictionary = self.arrayOfCells[indexPath.row];
    cell.textLabel.text = [dictionary valueForKey:@"title"];
    cell.imageView.image = [UIImage imageNamed:dictionary[@"icon"]];
    if ([cell respondsToSelector:@selector(shouldShowSeparator)]) {
        cell.shouldShowSeparator = YES;
    }
    return cell;
}

#pragma mark - Property Accessors
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
    return _managedObjectContext;
}

- (DMSession *)session
{
    if (!_session) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:DMSessionStartTimeKey ascending:NO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"report = %@", nil];
        fetchRequest.sortDescriptors = @[sortDesc];
        fetchRequest.predicate = predicate;
        fetchRequest.fetchLimit = 1;
        NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];
        _session = [results firstObject];
    }
    
    return _session;
}


@end
