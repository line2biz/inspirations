

#import "SNStepReportController.h"

#import "SNReportCell.h"
#import "DMStep+Category.h"
#import "UIColor+Customization.h"
#import "DMCalorie.h"

@interface SNStepReportController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SNStepReportController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    self.navigationItem.title = LMLocalizedString(@"Hall of Fame", nil);
    
    [self reloadData];
    
}

- (void)reloadData {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMStep class])];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"numberOfSteps" ascending:NO];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"numberOfSteps > 0"];
    fetchRequest.sortDescriptors = @[sortDesc];
    fetchRequest.fetchLimit = 10;
    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

    
    DMStep *currentEntity = [DMStep stepByDate:[NSDate date] inContext:self.managedObjectContext];
    
    if ([results containsObject:currentEntity]) {
        self.items = results;
    }
    else if (currentEntity) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:results];
        [array addObject:currentEntity];
        self.items = [array sortedArrayUsingDescriptors:@[sortDesc]];
    }
    [self.tableView reloadData];
    
}



#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SNReportCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    DMStep *step = self.items[indexPath.row];
    
//    NSString *string = [NSString stringWithFormat:@"%02u:%02u", seconds / 3600, (seconds / 60) % 60];
    
    NSUInteger value = [DMStep calculateCaloriesForSteps:[step.numberOfSteps integerValue]];
    
    NSLog(@"%s cal: %zd - %@", __FUNCTION__, value, step.numberOfSteps);
    
    if ([self compareDate:step.date withDate:[NSDate date]] == NSOrderedSame) {
        cell.titleLabel.textColor = [UIColor customOrangeColor];
    } else {
        cell.titleLabel.textColor = [UIColor customDarkGrayGolor];
    }
    
    DMCalorie *calorie = [DMCalorie calorieForValue:value];
    cell.titleLabel.text = calorie.title;
//    cell.avatarView.image = calorie.image ? calorie.image : [UIImage imageNamed:@"Icon-Calories-Gamburger"];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"dd MM yyyy" options:0 locale:[SNAppDelegate currentLocale]];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    cell.dateLabel.text = [formatter stringFromDate:step.date];
    cell.detailLabel.text = [LMLocalizedString(@"Tageswert", nil) stringByAppendingFormat:@": %zd kcal / %@", value, @""];
    
    
    UIButton *button = (UIButton *)cell.accessoryView;
    NSSet *allTargets = [button allTargets];
    if (![allTargets containsObject:self]) {
        [button addTarget:self action:@selector(didTapButtonInfo:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        NSLog(@"WE ARE HERE");
    }
    
    return cell;
}


- (void)didTapButtonInfo:(id)sender {
    
}


- (NSComparisonResult)compareDate:(NSDate *)firstDate withDate:(NSDate *)secondDate {
    
    NSDate *date1 = firstDate;
    NSDate *date2 = secondDate;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date2 interval:NULL forDate:date2];
    return [date1 compare:date2];
    
}


@end
