

#import "DMStep+Category.h"

#import "DMUser.h"

NSString * const DMStepNumberOfStepsKey = @"numberOfSteps";
NSString * const DMStrepDateKey         = @"date";

@implementation DMStep (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.date = now;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:now];
    self.year = @(dateComponents.year);
    self.month = @(dateComponents.month);
    self.weekOfYear = @(dateComponents.weekOfYear);
    self.day = @(dateComponents.day);
    
    
}

- (void)setCurrentDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:date];
    self.year = @(dateComponents.year);
    self.month = @(dateComponents.month);
    self.weekOfYear = @(dateComponents.weekOfYear);
    self.day = @(dateComponents.day);
    self.date = date;
}


#pragma mark - Public Class Methods
+ (DMStep *)stepByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMStep class])];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:date];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day = %d && month == %d && year == %d", dateComponents.day, dateComponents.month, dateComponents.year];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *resulsts = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if ([resulsts count]) {
        return [resulsts firstObject];
    }
    return nil;
    
}

//+ (DMStep *)maxStepInContext:(NSManagedObjectContext *)context;
+ (DMStep *)maxStepInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMStep class])];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:DMStepNumberOfStepsKey ascending:NO];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:[NSDate date]];
//    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day != %d && month != %d && year != %d", dateComponents.day, dateComponents.month, dateComponents.year];
    
    fetchRequest.fetchLimit = 1;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        return [results firstObject];
    }
    
    return nil;
}

+ (DMStep *)lastStepfRecordInContet:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMStep class])];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:DMStrepDateKey ascending:NO];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    fetchRequest.fetchLimit = 1;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if (results) {
        return [results firstObject];
    }
    
    return nil;
}

+ (NSUInteger)calculateCaloriesForSteps:(NSInteger)steps {
    
//    double BMR = [[DMUser defaultUser] BMRValue];
//    return [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey] * BMR * steps / 1000;
    
    // get special multiplier to calculate calorie expenditure for every mile
    // we use static data from out source
    CGFloat fMulForWalk1 = 0.57f; // walking at speed 2 miles/hour
    CGFloat fMulForWalk2 = 0.5f; // walking at speed 3.5 miles/hour
    
    CGFloat fOurEstimatedSpeed = 3.0f;
    
    CGFloat fMultiplier = fMulForWalk1 + (fMulForWalk2 - fMulForWalk1)/1.5f * (fOurEstimatedSpeed - 2);
    
    // calorie expenditure
    CGFloat fCalExp = fMultiplier * [[DMUser defaultUser] weightInLbs]; // kcal / mile
    
    // get walked distance from step length (stride) and step length
    CGFloat fDistance = steps * [[DMUser defaultUser] strideLength] / 100000.0f / 1.609344f; // distance in miles
    
    NSUInteger result = (NSUInteger)(fDistance * fCalExp);
    
    return result;
}



@end
