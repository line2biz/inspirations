

#import "SNStepReportController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHoursActivityView.h"
#import "SNProgressView3D.h"
#import "DMStep+Category.h"
#import "SNStepDashboardController.h"
#import "SNStepResultController.h"

@interface SNStepResultController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet SNHoursActivityView *hoursActivityView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@property (nonatomic) BOOL resultAchived;

@end

@implementation SNStepResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loadIdx = 0;
    self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    
    [SNAppearance customizeViewController:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    
    self.resultAchived = (self.numberOfSteps >= (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay);
    
    self.hoursActivityView.date = self.date;
    [self configureOutlets];
    [self configureView];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Segue Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[SNStepDashboardController class]]) {
        SNStepDashboardController *controller = (SNStepDashboardController *)[segue destinationViewController];
        controller.managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
}

#pragma mark - Private Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureOutlets {
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.subtitleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subtitleLabel.font.pointSize];
    self.subtitleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.detailLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
    self.detailLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.valueLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.valueLabel.font.pointSize];
    self.valueLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.textLabel.font.pointSize];
    self.textLabel.textColor = [UIColor customLightGrayColor];
    
    [self.progressView setControlType:SNProgressView3DTypeSteps];
    
}

- (void)configureView {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"EEEE, dd. MMM";
    self.subtitleLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    
    self.detailLabel.text = @"Sie haben heute 3.245 Schritte\nzurückgelegt.";
    
    DMStep *currentStep = [DMStep stepByDate:[NSDate date] inContext:self.managedObjectContext];
    DMStep *maxStep = [DMStep maxStepInContext:self.managedObjectContext];
    
    
    self.valueLabel.text = [NSString stringWithFormat:@"%zd", self.numberOfSteps];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    
    NSInteger maxPoint = MAX([maxStep.numberOfSteps integerValue], (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay);
    
    [self.progressView configureProgressViewWithMaxPoints:maxPoint
                                             currentPoint:[currentStep.numberOfSteps integerValue]
                                                pathColor:[UIColor customLightGrayColor]
                                            progressColor:[UIColor customOrangeColor]
                                                lineWidth:10.0f];
    
    [self.progressView setGoldenStep:maxPoint];
    [self.progressView setSilverStep: (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay];

    

    
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"de_DE"];
    
    if (self.resultAchived) {
        self.valueLabel.text = [numberFormatter stringFromNumber:currentStep.numberOfSteps];
        self.detailLabel.text = [NSString stringWithFormat:@"Super!\nSie habe es geschafft mehr als %@ Schritte zu gehen.", [numberFormatter stringFromNumber:@( (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay)]];
        [self.nextButton setTitle:LMLocalizedString(@"Wie werde ich besser ?", nil) forState:UIControlStateNormal];
        
        NSUInteger bigger = [currentStep.numberOfSteps integerValue] - (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
        self.textLabel.text = [NSString stringWithFormat:@"+%@", [numberFormatter stringFromNumber:@(bigger)]];
        
    } else {
      
        NSDate *endDate = self.hoursActivityView.endDate;
        NSInteger timeInterval = (NSInteger)([endDate timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970]);
        NSInteger minutes = (timeInterval / 60) % 60;
        NSInteger hours = (timeInterval / 3600);
        self.valueLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
        
        NSUInteger diff = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay - [currentStep.numberOfSteps integerValue];
        
        self.detailLabel.text = [NSString stringWithFormat:@"Sie haben noch Zeit sich zu bewegen, Ihnen fehlen noch %@ Schritte für Ihr Tagesziel​", [numberFormatter stringFromNumber:@(diff)]];
        [self.nextButton setTitle:LMLocalizedString(@"Dann los geht's zur Übersicht", nil) forState:UIControlStateNormal];
        
        
        
//        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:self.valueLabel.text attributes:@{ NSFontAttributeName : self.valueLabel.font }];
//        CGSize maxSize = CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX);
//        CGRect frame = [attrStr boundingRectWithSize:maxSize options:NSStringDrawingTruncatesLastVisibleLine context:nil];
//        NSLog(@"%s %f", __FUNCTION__, frame.size.height);
        
        self.textLabel.text = LMLocalizedString(@"Stunden", nil);
        
    }
    
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}


@end
