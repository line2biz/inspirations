

#import "SNSessionResultTableCell.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

#import "DMSession+Category.h"

@interface SNSessionResultTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelOrder;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelCalories;
@property (weak, nonatomic) IBOutlet UILabel *labelMoves;

@end

@implementation SNSessionResultTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    [self configureAppearence];
}

#pragma mark - Private Methods
- (void)configureAppearence
{
    self.labelOrder.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelOrder.font.pointSize];
    self.labelOrder.textColor = [UIColor customDarkGrayGolor];
    
    void(^CustomizeLabel)(UILabel *) = ^(UILabel *label) {
        label.textColor = [UIColor customDarkGrayGolor];
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
    };
    
    CustomizeLabel(self.labelDate);
    CustomizeLabel(self.labelCalories);
    CustomizeLabel(self.labelMoves);
}

- (void)configureView
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[SNAppDelegate currentLocale]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"d.\tMMM.\tHH:mm" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormatter.dateFormat = format;
    
    self.labelDate.text = [dateFormatter stringFromDate:self.session.endTime];
    
    self.labelCalories.text = [SNAppearance textWithThousendsFormat:(NSInteger)([self.session caloriesPerHour])];
    self.labelMoves.text = [SNAppearance textWithThousendsFormat:(NSInteger)([self.session countsPerMinute]*60)];
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSession:(DMSession *)session
{
    if (_session != session) {
        _session = session;
        [self configureView];
    }
}

- (void)setIOrder:(NSInteger)iOrder
{
    _iOrder = iOrder + 1;
    
    self.labelOrder.text = [NSString stringWithFormat:@"%zd.", _iOrder];
}

@end
