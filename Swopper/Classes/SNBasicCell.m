

#import "SNBasicCell.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

@implementation SNBasicCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.textLabel.font.pointSize];
}

#pragma mark - Property Accessors
- (void)setShouldShowSeparator:(BOOL)shouldShowSeparator
{
    _shouldShowSeparator = shouldShowSeparator;
    if (_shouldShowSeparator) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(self.separatorInset.left, CGRectGetHeight(self.bounds) - 0.5, CGRectGetWidth(self.bounds), 0.5)];
        view.backgroundColor = [UIColor customLightGrayColor];
        [self addSubview:view];
    }
}

@end
