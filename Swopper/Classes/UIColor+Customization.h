

#import <UIKit/UIKit.h>

@interface UIColor (Customization)

+ (UIColor *)titleColor;

+ (UIColor *)customRedColor;
+ (UIColor *)customOrangeColor;
+ (UIColor *)customBlueColor;
+ (UIColor *)customDarkGrayGolor;
+ (UIColor *)customLightGrayColor;

@end
