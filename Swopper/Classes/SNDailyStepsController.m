

#import "SNDailyStepsController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "SNRoundButton.h"

@interface SNDailyStepsController ()
{
    NSInteger _startValue,_valueDiff, _maxValue;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (weak, nonatomic) IBOutlet SNRoundButton *buttonLesser;
@property (weak, nonatomic) IBOutlet SNRoundButton *buttonMore;

@property (weak, nonatomic) IBOutlet UILabel *labelValue;

@property (assign, nonatomic) NSInteger currentStepsValue;

@end

@implementation SNDailyStepsController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    
    _startValue = (savedValue) ? savedValue : 10000;
    _valueDiff = 500;
    _maxValue = 99500;
    
    self.currentStepsValue = _startValue;
    
    [self controllerStart];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setInteger:self.currentStepsValue forKey:@"MaxNumberOfStepsPerDay"];
    
    [defaults synchronize];
}


#pragma mark - Controller Methods
- (void)configureOutlets
{
    self.labelTitle.text = LMLocalizedString(@"Tägliches Schrittziel", nil);
    self.labelSubtitle.text = LMLocalizedString(@"Legen Sie hier Ihr Tagesziel fest:", nil);
    
    self.labelValue.text = [SNAppearance textWithThousendsFormat:self.currentStepsValue];
}

- (void)configureFonts
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    self.labelValue.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelValue.font.pointSize];
}

- (void)controllerStart
{
    [self configureOutlets];
    
    [self configureFonts];
}

- (NSString *)formattedStringFromValue:(NSInteger)value
{
    NSInteger thousends = (self.currentStepsValue - self.currentStepsValue%1000) / 1000;
    NSInteger rest = self.currentStepsValue%1000;
    
    NSString *formattedString = [NSString stringWithFormat:@"%@%@",(thousends) ? [NSString stringWithFormat:@"%zd.",thousends] : @"  ", (rest) ? @"500" : @"000"];
    
    return formattedString;
}


#pragma mark - Outlet Methods
- (IBAction)didTapButtonLesser:(SNRoundButton *)sender
{
    if (self.currentStepsValue > _valueDiff) {
        self.currentStepsValue -= _valueDiff;
        self.labelValue.text = [self formattedStringFromValue:self.currentStepsValue];
    }
    if (self.currentStepsValue == _valueDiff) {
        sender.bEnabled = NO;
    }
    if (self.currentStepsValue < _maxValue) {
        self.buttonMore.bEnabled = YES;
    }
}


- (IBAction)didTapButtonMore:(SNRoundButton *)sender
{
    if (self.currentStepsValue < _maxValue) {
        self.currentStepsValue += _valueDiff;
        self.labelValue.text = [self formattedStringFromValue:self.currentStepsValue];
    }
    if (self.currentStepsValue == _maxValue) {
        sender.bEnabled = NO;
    }
    if (self.currentStepsValue > _valueDiff) {
        self.buttonLesser.bEnabled = YES;
    }
}







@end
