

#import "SNStatisticController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

#import "SNSummaryChart.h"
#import "SNDay.h"
#import "SNAdditionalMath.h"
#import "DMReport+Category.h"
#import "DMSession+Category.h"
#import "DMStep+Category.h"
#import "SNMenusController.h"
#import "SNDayStatisticController.h"

#import <MFSideMenu.h>

@interface SNStatisticController () <SNSummaryChartDatasource, SNSummaryChartDelegate>
{
    NSInteger _week;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *intervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet SNSummaryChart *summaryChart;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SNStatisticController

#pragma mark - View Life Cycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _week = -1;
        if (!self.managedObjectContext) {
            self.managedObjectContext = [[DMManager sharedManager] defaultContext];
        }
        self.summaryChart.dataSource = self;
        self.summaryChart.delegate = self;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [SNAppearance customizeViewController:self];
    
    if (self.navigationController.viewControllers[0] == self) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
    }
    
    self.titleLabel.text = LMLocalizedString(@"Wochenübersicht", nil);
    
    [self cofigureOutlets];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.summaryChart) {
        self.summaryChart.currentDate = [NSDate date];
    }
}


#pragma mark - Controller Methods
- (void)cofigureOutlets
{
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.intervalLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.intervalLabel.font.pointSize];
    self.intervalLabel.textColor = [UIColor customDarkGrayGolor];
    
    // configure interval text
    NSArray *arrayOfData = [self.summaryChart.week mutableCopy];
    arrayOfData = [arrayOfData sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [obj1 valueForKey:@"date"];
        NSDate *date2 = [obj2 valueForKey:@"date"];
        return [SNAdditionalMath compareDate:date1 withDate:date2];
    }];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[SNAppDelegate currentLocale]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormatter.dateFormat = format;
    
    NSDate *date1 = [[arrayOfData firstObject] valueForKey:@"date"];
    NSDate *date2 = [[arrayOfData lastObject] valueForKey:@"date"];
    NSString *firstPart = [dateFormatter stringFromDate:date1];
    NSString *secondPart = [dateFormatter stringFromDate:date2];
    
    self.intervalLabel.text = [NSString stringWithFormat:@"%@ - %@",firstPart,secondPart];
    
    self.descLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.descLabel.font.pointSize];
    self.descLabel.textColor = [UIColor customDarkGrayGolor];
    
    [self.summaryChart setTextFont:[UIFont fontWithName:@"Arial" size:13]];
    [self.summaryChart setColorSwoops:[UIColor customDarkGrayGolor]];
    [self.summaryChart setColorSteps:[UIColor customLightGrayColor]];
    [self.summaryChart setColorCalories:[UIColor customOrangeColor]];
    [self.summaryChart setColorCurrentDayText:[UIColor customOrangeColor]];
    [self.summaryChart setAnnotationSwoops:LMLocalizedString(@"Moves", nil)];
}


- (NSArray *)tempDataForWeek:(NSInteger)week
{
    CGFloat thisWeekCalories = 0.00001f, prevWeekCalories = 0.00001f;
    NSDate *weekDate = [NSDate dateWithTimeInterval:24*60*60*7*week sinceDate:[NSDate date]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:weekDate];
    dateComponents.weekday = 2; // Moday
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    NSDate *firstDate = [calendar dateFromComponents:dateComponents];
    
    dateComponents = [NSDateComponents new];
    dateComponents.day = 7;
    dateComponents.second = -1;
    
    NSMutableArray *mTestArray = [NSMutableArray new];
    for (NSInteger i = 0; i < 7; i++) {
        NSInteger timeInterval = 24*60*60*i;
        SNDay *day = [[SNDay alloc] init];
        day.date = [NSDate dateWithTimeInterval:timeInterval sinceDate:firstDate];
        
        
        DMStep *step = [DMStep stepByDate:day.date inContext:self.managedObjectContext];
        DMReport *report = [DMReport reportByDate:day.date inContext:self.managedObjectContext];
        
        NSInteger swops = 0;
        if (report.sessions.count) {
            for (DMSession *session in report.sessions) {
                swops += [session countsForSession] / session.sessionDuration.floatValue * report.interval.floatValue;
            }
            swops = swops / report.sessions.count;
        }
        
        day.calories = 0;
        day.swoops = swops;
        if (step) {
            day.steps = [step.numberOfSteps integerValue];
            day.calories += [DMStep calculateCaloriesForSteps:[step.numberOfSteps integerValue]];
        }
        
        if (report.sessions.count) {
            CGFloat calories = 0;
            for (DMSession *session in report.sessions) {
                calories += [session caloriesPerSession] / session.sessionDuration.floatValue * report.interval.floatValue;
            }
            calories = calories / report.sessions.count;
            day.calories += calories;
            
            if (step) {
                day.swoppsPercentage = calories / day.calories;
            } else {
                day.swoppsPercentage = 1.0f;
            }
            
        }
        thisWeekCalories += day.calories;
        
        [mTestArray addObject:day];
        
        
        NSDate *prevWeekDate = [NSDate dateWithTimeInterval:- 7*24*60*60 sinceDate:day.date];
        step = [DMStep stepByDate:prevWeekDate inContext:self.managedObjectContext];
        report = [DMReport reportByDate:prevWeekDate inContext:self.managedObjectContext];
        if (step) {
            NSInteger steps = [step.numberOfSteps integerValue];
            prevWeekCalories += (CGFloat)[DMStep calculateCaloriesForSteps:steps];
        }
        if (report) {
            CGFloat calories = 0;
            for (DMSession *session in report.sessions) {
                calories += [session caloriesPerSession] / session.sessionDuration.floatValue * report.interval.floatValue;
            }
            prevWeekCalories += calories / report.sessions.count;
        }
    }
    
    if (thisWeekCalories && prevWeekCalories) {
        CGFloat percentage = (CGFloat)thisWeekCalories/(CGFloat)prevWeekCalories;
        NSString *moreOrLess;
        if (percentage == 1.0f) {
            self.descLabel.text = LMLocalizedString(@"kein Unterschied zur letzten Woche", nil);
        } else if (percentage < 0.001) {
            self.descLabel.text = LMLocalizedString(@"das sind 100% weniger als in der letzten Woche", nil);
        } else {
            moreOrLess = (percentage > 1.0f) ? LMLocalizedString(@"mehr", nil) : LMLocalizedString(@"weniger", nil);
            
            if (percentage > 1.0f) {
                if (percentage < 2.0f) {
                    percentage = (percentage - 1.0f)*100;
                } else {
                    percentage = 2.0f * 100;
                }
            } else {
                percentage = (1.0f - percentage)*100;
            }
            
            NSString *str = LMLocalizedString(@"das sind <percentage>% <moreOrLess> als in der letzten Woche", @"<percentage> & <moreOrLess> don't translate");
            str = [str stringByReplacingOccurrencesOfString:@"<percentage>" withString:[NSString stringWithFormat:@"%.0f", percentage]];
            str = [str stringByReplacingOccurrencesOfString:@"<moreOrLess>" withString:moreOrLess];
            
            self.descLabel.text = str;
        }
    } else if (thisWeekCalories) {
        self.descLabel.text = LMLocalizedString(@"das sind 100% mehr als in der letzten Woche", nil);
    } else if (prevWeekCalories) {
        self.descLabel.text = LMLocalizedString(@"das sind 100% weniger als in der letzten Woche", nil);
    } else {
        self.descLabel.text = LMLocalizedString(@"kein Unterschied zur letzten Woche", nil);
    }
    
    
    return mTestArray;
}


#pragma mark - Summary Chart Delegate
- (void)summaryChartView:(SNSummaryChart *)summaryChartView didChangeWeek:(NSArray *)week
{
    [self cofigureOutlets];
}

- (void)summaryChartView:(SNSummaryChart *)summaryChartView changedCurrentDate:(NSDate *)date
{
    SNDayStatisticController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNDayStatisticController class])];
    controller.chosenDate = date;
    controller.week = summaryChartView.week;
    [summaryChartView setUserInteractionEnabled:NO];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [summaryChartView setUserInteractionEnabled:YES];
            if ([SNAdditionalMath compareDate:date withDate:[NSDate date]] == NSOrderedDescending) {
                summaryChartView.currentDate = [NSDate date];
            } else {
                [strongSelf.navigationController pushViewController:controller animated:YES];
            }
        }
    });
}

#pragma mark - Summary Chart Datasource
- (NSArray *)summaryChartView:(SNSummaryChart *)summaryChartView willChangeWeekForward:(BOOL)forward
{
    if (forward && _week == 0) {
        return nil;
    }
    if (forward) {
        _week++;
    } else {
        _week--;
    }
    return [self tempDataForWeek:_week];
}

@end
