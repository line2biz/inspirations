

#import "SNCwopperCreatedController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNMoviePlayerController.h"
#import "SNTrainingStep02Controller.h"

@interface SNCwopperCreatedController () <SNMoviePlayerControllerProtocol>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) SNMoviePlayerController *moviePlayerController;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;

@end

@implementation SNCwopperCreatedController


#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    [SNAppearance customizeViewController:self];
    self.view.backgroundColor = [UIColor whiteColor];
    [self configureOutelts];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
    
//    [self customizeTitle:nil subtitle:self.titleLabel underTopBar:NO viewController:self];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    for (UIButton *button in self.buttons) {
        [button setHidden:YES];
    }
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureOutelts
{
    
    [self.yesButton setTitle:LMLocalizedString(@"Ja, ich bin startklar.", nil) forState:UIControlStateNormal];
    [self.noButton setTitle:LMLocalizedString(@"Nein, wie geht das?", nil) forState:UIControlStateNormal];
    
    for (UIButton *button in self.buttons) {
        button.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:button.titleLabel.font.pointSize];
    }
    
    self.titleLabel.text = LMLocalizedString(@"Ist Ihr swopper schon\naufgebaut?", nil);
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    [SNAppearance findAndMarkWord:@"swopper" inLabel:self.titleLabel withNewFont:[UIFont franklinGothicStdBookMedWithSize:self.titleLabel.font.pointSize]];
    
}


- (void)moviePlayerDidExitFullscreenWithNotif:(NSNotification *)theNotification
{
    [self didTapNextButton:nil];
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                for (UIButton *button in strongSelf.buttons) {
                    [button setHidden:NO];
                }
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapShowVideoButton:(id)sender
{
    NSURL *(^UrlForVideo)(NSString *, NSString *) = ^(NSString *sResourceName, NSString *sType) {
        
        NSString *sLocalizationSuffix = [[SNLocalizationManager sharedManager] getLanguage];
        sResourceName = [NSString stringWithFormat:@"%@_%@", sResourceName, sLocalizationSuffix];
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:sResourceName ofType:sType];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        
        return movieURL;
    };
    NSURL *movieURL = UrlForVideo(@"swopperAufbau", @"mp4");
    
    self.moviePlayerController = [[SNMoviePlayerController alloc] initWithContentURL:movieURL];
    self.moviePlayerController.delegate = self;
    [self presentMoviePlayerViewControllerAnimated:self.moviePlayerController];
}

- (IBAction)didTapNextButton:(id)sender
{
    [self performSegueWithIdentifier:NSStringFromClass([SNTrainingStep02Controller class]) sender:self];
}


#pragma mark - Movie Player Controller Delegate
- (void)moviePlayerController:(SNMoviePlayerController *)controller didExitFullScreen:(BOOL)didExit
{
    [self didTapNextButton:nil];
}




@end
