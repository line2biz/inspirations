
#import "SNConfigurationController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "SNMenusController.h"

#import "SNProfileController.h"
#import "SNMemoryController.h"
#import "SNDailyStepsController.h"
#import "SNLanguagesController.h"

#import <MFSideMenu.h>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface SNConfigurationController ()
@property (weak, nonatomic) IBOutlet UILabel *settingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepTargetLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;

@property (weak, nonatomic) IBOutlet UILabel *personalDataLabel;
@end

@implementation SNConfigurationController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    
    
    SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:menusController
                                                                            action:@selector(hideViewController:)];

    __weak typeof(self) weakSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [weakSelf configureView];
        [weakSelf.tableView reloadData];
    }];
    
    [self configureView];
    
}

- (void)configureView {
    
    self.personalDataLabel.text = LMLocalizedString(@"Persönliche Daten", nil);
    self.settingsLabel.text = LMLocalizedString(@"Erinnerung", nil);
    self.stepTargetLabel.text = LMLocalizedString(@"Tägliches Schrittziel", nil);
    self.languageLabel.text = LMLocalizedString(@"Sprache", nil);
    

    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}


#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        return ([CMPedometer isStepCountingAvailable]) ? 4 : 3;
    } else {
        return ([CMStepCounter isStepCountingAvailable]) ? 4 : 3;
    }
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController;
    switch (indexPath.row) {
        case 0:
            viewController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNProfileController class])];
            break;
        case 1:
            viewController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNMemoryController class])];
            break;
        case 2:
            viewController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNLanguagesController class])];
            break;
        case 3:
            viewController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNDailyStepsController class])];
            break;
        default:
            break;
    }
    
    if (viewController) {
        if ([viewController isKindOfClass:[SNProfileController class]]) {
            ((SNProfileController *)viewController).showNextButton = NO;
        }
        [self.navigationController pushViewController:viewController animated:YES];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];        
    }
    
}


@end
