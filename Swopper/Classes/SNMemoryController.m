

#import "SNMemoryController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNSwitchCell.h"
#import "SNRightDetailCell.h"
#import "SNTextCell.h"

#import "SNTimePickerController.h"
#import "SNPeriodsPickerController.h"
#import "DMUser.h"

#import <EventKit/EventKit.h>

NSString *kReminderID = @"swopper FIT: Time for Training";

NSInteger kMaxRows = 6;
NSInteger kMidRows = 4;
NSInteger kMinRows = 1;

@interface SNMemoryController () <SNTimePickerProtocol, SNPeriodPickerProtocol, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) NSInteger rowsCount;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

// The database with calendar events and reminders
@property (strong, nonatomic) EKEventStore *eventStore;

// Indicates whether app has access to event store.
@property (nonatomic) BOOL isAccessToEventStoreGranted;

@property (strong, nonatomic) EKCalendar *calendar;

@end

@implementation SNMemoryController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
//    _loadIdx = 0;
    [SNAppearance customizeViewController:self];
    
    // update reminder access status
    [self updateAuthorizationStatusToAccessEventStore];
    
    // test
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    
//    UILocalNotification *locNot = [[UILocalNotification alloc] init];
//    locNot.repeatInterval = kCFCalendarUnitMinute;
//    locNot.repeatCalendar = [NSCalendar autoupdatingCurrentCalendar];
//    locNot.alertBody = @"A minute has passed";
//    locNot.fireDate = [NSDate dateWithTimeIntervalSinceNow:60];
//    locNot.soundName = UILocalNotificationDefaultSoundName;
//    [[UIApplication sharedApplication] scheduleLocalNotification: locNot];
    
    if ([DMUser defaultUser].memoryFirstSwitchIsOn) {
        self.rowsCount = kMidRows;
        if ([DMUser defaultUser].memorySecondSwitchIsOn) {
            self.rowsCount = kMaxRows;
        }
    } else {
        self.rowsCount = kMinRows;
    }
    
    [self configureOutlets];
    
    self.labelTitle.text = LMLocalizedString(@"Erinnerungsfunktion", nil);
    self.labelSubtitle.text = LMLocalizedString(@"Nutzen Sie die Erinnerungsfunktion, um nicht auf das regelmäßige Training zu vergessen!", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

#pragma mark - Property Accessors
- (EKEventStore *)eventStore {
    if (!_eventStore) {
        _eventStore = [[EKEventStore alloc] init];
    }
    return _eventStore;
}

- (EKCalendar *)calendar {
    if (!_calendar) {
        
        NSArray *calendars = [self.eventStore calendarsForEntityType:EKEntityTypeReminder];
        
        NSString *calendarTitle = @"swopperCalendar";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title matches %@", calendarTitle];
        NSArray *filtered = [calendars filteredArrayUsingPredicate:predicate];
        
        if ([filtered count]) {
            
            _calendar = [filtered firstObject];
            
        } else {
            
            _calendar = [EKCalendar calendarForEntityType:EKEntityTypeReminder eventStore:self.eventStore];
            _calendar.title = calendarTitle;
            _calendar.source = self.eventStore.defaultCalendarForNewReminders.source;
            
            NSError *calendarErr = nil;
            BOOL calendarSuccess = [self.eventStore saveCalendar:_calendar commit:YES error:&calendarErr];
            if (!calendarSuccess) {
                // Handle error
                NSLog(@"\n%s\n%@",__FUNCTION__, calendarErr.localizedDescription);
            }
        }
    }
    return _calendar;
}

#pragma mark - Segue Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[SNTimePickerController class]]) {
        SNTimePickerController *datePicker = (SNTimePickerController *)[segue destinationViewController];
        datePicker.delegate = self;
    }
    if ([[segue destinationViewController] isKindOfClass:[SNPeriodsPickerController class]]) {
        SNPeriodsPickerController *periodsPickerController = (SNPeriodsPickerController *)[segue destinationViewController];
        periodsPickerController.delegate = self;
        periodsPickerController.periodsType = self.selectedIndexPath.row - kMidRows + 1; // +1 because no hour frequence
    }
}

#pragma mark - Controller Methods
- (void)configureOutlets
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor customDarkGrayGolor];
    
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    self.labelSubtitle.textColor = [UIColor customDarkGrayGolor];
}

- (void)firstValueChanged:(UISwitch *)sender {
    
    NSArray *arrayOfIndexPaths = [self createIndexPathsArrayFromRow:kMinRows toRow:kMidRows];
    if (self.rowsCount == kMaxRows) {
        UISwitch *tempSwitch = ((SNSwitchCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]).switchControl;
        [tempSwitch setOn:NO];
        [self secondValueChanged:tempSwitch];
        [DMUser defaultUser].memorySecondSwitchIsOn = NO;
        
        
    }
    [DMUser defaultUser].memoryFirstSwitchIsOn = sender.isOn;
    if (sender.isOn) {
        self.rowsCount = kMidRows;
        [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationTop];
    } else {
        self.rowsCount = kMinRows;
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationBottom];
        
        [[DMUser defaultUser] setNotificationDate:nil];
    }
    [[DMUser defaultUser] save];
    
    // delete reminder if user switch remind option to off
    if (!sender.isOn) {
        [self getReminderAndDelete];
    }
    
    ////[self updateLocalNotifications];
}

- (void)secondValueChanged:(UISwitch *)sender
{
    [DMUser defaultUser].memorySecondSwitchIsOn = sender.isOn;
    NSArray *arrayOfIndexPaths = [self createIndexPathsArrayFromRow:kMidRows toRow:kMaxRows];
    if (sender.isOn) {
        self.rowsCount = kMaxRows;
        [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationTop];
    } else {
        self.rowsCount = kMidRows;
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationBottom];
        
        [[DMUser defaultUser] setHourPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthHoursNever]];
        [[DMUser defaultUser] setDayPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthDaysNever]];
        [[DMUser defaultUser] setWeekPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthWeekNever]];
    }
    [[DMUser defaultUser] save];
    
    ////[self updateLocalNotifications];
    
}

- (NSArray *)createIndexPathsArrayFromRow:(NSInteger)row toRow:(NSInteger)maxRow
{
    NSMutableArray *mArrayOfIndexPaths = [NSMutableArray new];
    NSIndexPath *indexPath;
    for (NSInteger i = row; i < maxRow; i++) {
        indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [mArrayOfIndexPaths addObject:indexPath];
    }
    return mArrayOfIndexPaths;
}

#pragma mark Reminder Methods
- (void)updateAuthorizationStatusToAccessEventStore
{
    EKAuthorizationStatus authorizationStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder];
    
    switch (authorizationStatus) {
            
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted: {
            self.isAccessToEventStoreGranted = NO;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Access Denied"
                                                                message:@"This app doesn't have access to your Reminders." delegate:nil
                                                      cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
            [alertView show];
            break;
        }
            
        case EKAuthorizationStatusAuthorized:
            self.isAccessToEventStoreGranted = YES;
            break;
            
        case EKAuthorizationStatusNotDetermined: {
            __weak typeof(self) weakSelf = self;
            [self.eventStore requestAccessToEntityType:EKEntityTypeReminder
                                            completion:^(BOOL granted, NSError *error) {
                                                typeof(weakSelf) strongSelf = weakSelf;
                                                if (strongSelf) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        strongSelf.isAccessToEventStoreGranted = granted;
                                                    });
                                                }
                                            }];
            break;
        }
    }
}

- (void)addReminder
{
    if (!self.isAccessToEventStoreGranted)
        return;
    
    if (![[DMUser defaultUser] notificationDate]) {
        return;
    }
    
    [self getReminderAndDelete];
    
    EKReminder *reminder = [EKReminder reminderWithEventStore:self.eventStore];
    reminder.title = kReminderID;
    reminder.calendar = self.calendar;
    
    NSDateComponents *dateComponents = [self dateComponentsForDefaultDueDate];
    reminder.startDateComponents = dateComponents;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *dueDateComponents = [gregorianCalendar components:unitFlags
                                                               fromDate:[NSDate dateWithTimeInterval:1 sinceDate:[dateComponents date]]];
    dueDateComponents.calendar = gregorianCalendar;
    dueDateComponents.timeZone = [NSTimeZone defaultTimeZone];
    
    reminder.dueDateComponents = dueDateComponents;
    
    EKRecurrenceRule *er;
    
    if ([[DMUser defaultUser] dayPeriod] != 0) {
        NSInteger iRepeatIntervalInDays = [[DMUser defaultUser] dayPeriod];
        
        er = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily
                                                          interval:iRepeatIntervalInDays
                                                     daysOfTheWeek:nil
                                                    daysOfTheMonth:nil
                                                   monthsOfTheYear:nil weeksOfTheYear:nil daysOfTheYear:nil setPositions:nil end:nil];
    } else if ([[DMUser defaultUser] weekPeriod] != 0) {
        NSArray *aDays;
        switch ([[DMUser defaultUser] weekPeriod]) {
            case 1:
            {
                aDays = @[[EKRecurrenceDayOfWeek dayOfWeek:2], // Monday
                          [EKRecurrenceDayOfWeek dayOfWeek:3],
                          [EKRecurrenceDayOfWeek dayOfWeek:4],
                          [EKRecurrenceDayOfWeek dayOfWeek:5],
                          [EKRecurrenceDayOfWeek dayOfWeek:6]];
            }
                break;
            case 2:
            {
                aDays = @[[EKRecurrenceDayOfWeek dayOfWeek:1],
                          [EKRecurrenceDayOfWeek dayOfWeek:7]];
            }
                break;
            case 3:
            {
                aDays = @[[EKRecurrenceDayOfWeek dayOfWeek:1],
                          [EKRecurrenceDayOfWeek dayOfWeek:2], // Monday
                          [EKRecurrenceDayOfWeek dayOfWeek:3],
                          [EKRecurrenceDayOfWeek dayOfWeek:4],
                          [EKRecurrenceDayOfWeek dayOfWeek:5],
                          [EKRecurrenceDayOfWeek dayOfWeek:6],
                          [EKRecurrenceDayOfWeek dayOfWeek:7]];
            }
                break;
            default:
                break;
        }
        
        er = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly interval:1
                                                     daysOfTheWeek:aDays
                                                    daysOfTheMonth:nil
                                                   monthsOfTheYear:nil weeksOfTheYear:nil daysOfTheYear:nil setPositions:nil end:nil];
    } else {
        er = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily interval:1
                                                     daysOfTheWeek:nil
                                                    daysOfTheMonth:nil
                                                   monthsOfTheYear:nil weeksOfTheYear:nil daysOfTheYear:nil setPositions:nil end:nil];
    }
    
    
    [reminder addRecurrenceRule:er];
    [reminder addAlarm:[EKAlarm alarmWithAbsoluteDate:[dateComponents date]]];
    
    NSError *error = nil;
    BOOL success = [self.eventStore saveReminder:reminder commit:YES error:&error];
    if (!success) {
        // Handle error.
        NSLog(@"\n%s\n%@", __FUNCTION__, error.localizedDescription);
    }
    NSString *message = (success) ? LMLocalizedString(@"Reminder was successfully added!", nil) : LMLocalizedString(@"Failed to add reminder!", nil);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LMLocalizedString(@"Dismiss", nil), nil];
    [alertView show];
}

- (void)getReminderAndDelete
{
    if (self.isAccessToEventStoreGranted) {
        
        NSPredicate *predicate = [self.eventStore predicateForRemindersInCalendars:@[self.calendar]];
        
        __weak typeof(self) weakSelf = self;
        [self.eventStore fetchRemindersMatchingPredicate:predicate
                                              completion:^(NSArray *reminders) {
                                                  
                                                  typeof(weakSelf) strongSelf = weakSelf;
                                                  if (strongSelf) {
                                                      if (reminders.count) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              
                                                              NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"title matches %@", kReminderID];
                                                              
                                                              NSArray *filteredArray = [reminders filteredArrayUsingPredicate:filterPredicate];
                                                              
                                                              if (filteredArray.count) {
                                                                  [filteredArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                                      NSError *error = nil;
                                                                      
                                                                      BOOL success = [self.eventStore removeReminder:obj commit:NO error:&error];
                                                                      if (!success) {
                                                                          // Handle delete error
                                                                          NSLog(@"\n%s\nError while deleting reminder:\n%@", __FUNCTION__, error.localizedDescription);
                                                                      }
                                                                  }];
                                                                  
                                                                  NSError *commitErr = nil;
                                                                  BOOL success = [strongSelf.eventStore commit:&commitErr];
                                                                  if (!success) {
                                                                      // Handle commit error.
                                                                      NSLog(@"\n%s\nError while commiting to event store:\n%@", __FUNCTION__, commitErr.localizedDescription);
                                                                  }
                                                              }
                                                          });
                                                      }
                                                  }
                                              }];
    }
}

- (NSDateComponents *)dateComponentsForDefaultDueDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSUInteger unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSDate *date = [[DMUser defaultUser] notificationDate];
    
    NSDateComponents *dateComponent = [gregorianCalendar components:unitFlags fromDate:date];
    dateComponent.timeZone = [NSTimeZone defaultTimeZone];
    dateComponent.calendar = gregorianCalendar;
    dateComponent.second = 0;
    
    return dateComponent;
}

#pragma mark - Time Picker Controller Delegate
- (void)timePickerController:(SNTimePickerController *)controller chosenDate:(NSDate *)date
{
    [[DMUser defaultUser] setNotificationDate:date];
    [[DMUser defaultUser] save];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"HH:mm"];
    ((SNRightDetailCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]]).detailLabel.text = [dateFormatter stringFromDate:date];
    
    if ([[DMUser defaultUser] notificationDate]) {
        [self addReminder];
    }
    ////[self updateLocalNotifications];
    
}

#pragma mark - Periods Picker Controller Delegate
- (void)periodPickerController:(SNPeriodsPickerController *)controller withPeriodType:(SNPeriodsType)periodType chosenPeriod:(SNPeriodLength)periodLength
{
    if (![[DMUser defaultUser] notificationDate]) {
        NSString *sMsg = LMLocalizedString(@"First choose the time, please", nil);
        NSString *sCancelButton = LMLocalizedString(@"Ok", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:sMsg
                                                           delegate:nil
                                                  cancelButtonTitle:sCancelButton
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:periodType+kMidRows-1 inSection:0];
    ((SNRightDetailCell *)[self.tableView cellForRowAtIndexPath:indexPath]).detailLabel.text = [SNPeriodsPickerController titleForLength:periodLength];
    switch (periodType) {
        case SNPeriodsTypeHours:
        {
            ///TODO: Unfinished - Temporary
            [[DMUser defaultUser] setHourPeriod:[SNPeriodsPickerController periodForLength:periodLength]];
            [[DMUser defaultUser] setDayPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthDaysNever]];
            [[DMUser defaultUser] setWeekPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthWeekNever]];
            [[DMUser defaultUser] save];
        }
            break;
        case SNPeriodsTypeDays:
        {
            [[DMUser defaultUser] setDayPeriod:[SNPeriodsPickerController periodForLength:periodLength]];
            
            [[DMUser defaultUser] setWeekPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthWeekNever]];
            ((SNRightDetailCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:SNPeriodsTypeWeek+kMidRows-1 inSection:0]]).detailLabel.text = [SNPeriodsPickerController titleForLength:SNPeriodLengthWeekNever];
            
            [[DMUser defaultUser] setHourPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthHoursNever]];
            
            [[DMUser defaultUser] save];
        }
            break;
        case SNPeriodsTypeWeek:
        {
            [[DMUser defaultUser] setWeekPeriod:[SNPeriodsPickerController periodForLength:periodLength]];
            
            [[DMUser defaultUser] setDayPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthDaysNever]];
            ((SNRightDetailCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:SNPeriodsTypeDays+kMidRows-1 inSection:0]]).detailLabel.text = [SNPeriodsPickerController titleForLength:SNPeriodLengthDaysNever];
            
            [[DMUser defaultUser] setHourPeriod:[SNPeriodsPickerController periodForLength:SNPeriodLengthHoursNever]];
            [[DMUser defaultUser] save];
        }
            break;
            
        default:
            break;
    }
    
    [self addReminder];
    
    ////[self updateLocalNotifications];
}

#pragma mark - Table View DataSoure
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rowsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SNSwitchCell" forIndexPath:indexPath];
            [[(SNSwitchCell *)cell titleLabel] setText:LMLocalizedString(@"Erinnerung", nil)];
            [((SNSwitchCell *)cell).switchControl setOn:[DMUser defaultUser].memoryFirstSwitchIsOn];
            [((SNSwitchCell *)cell).switchControl addTarget:self action:@selector(firstValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
            break;
            
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SNSwitchCell" forIndexPath:indexPath];
            [[(SNSwitchCell *)cell titleLabel] setText:LMLocalizedString(@"Häufigkeit angeben", nil)];
            [[(SNSwitchCell *)cell borderView] setHidden:YES];
            [((SNSwitchCell *)cell).switchControl setOn:[DMUser defaultUser].memorySecondSwitchIsOn];
            [((SNSwitchCell *)cell).switchControl addTarget:self action:@selector(secondValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
            break;
            
        case 2:
        {
            SNTextCell *textCell = [tableView dequeueReusableCellWithIdentifier:@"SNTextCell" forIndexPath:indexPath];
            textCell.titleLabel.text = LMLocalizedString(@"Geben Sie die Uhrzeit oder die Häufigkeit ein, wann Sie die App an eine Messung erinnern soll", nil);
            cell = textCell;
        }
            break;
            
        case 3:
        {
            SNRightDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"SNRightDetailCell" forIndexPath:indexPath];
            detailCell.titleLabel.text = LMLocalizedString(@"Uhrzeit", nil);
            if ([[DMUser defaultUser] notificationDate]) {
                NSDateFormatter *dateFormatter = [NSDateFormatter new];
                [dateFormatter setLocale:[SNAppDelegate currentLocale]];
                [dateFormatter setDateFormat:@"HH:mm"];
                NSString *sDate = [dateFormatter stringFromDate:[[DMUser defaultUser] notificationDate]];
                detailCell.detailLabel.text = sDate;
            } else {
                detailCell.detailLabel.text = LMLocalizedString(@"00:00", nil);
            }
            detailCell.separatorType = SNCellSeparatorTypeShort;
            cell = detailCell;
        }
            break;
//        case 4:
//        {
//            SNRightDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"SNRightDetailCell" forIndexPath:indexPath];
//            detailCell.titleLabel.text = LMLocalizedString(@"Stunde", nil);
//            if ([[DMUser defaultUser] hourPeriod]) {
//                detailCell.detailLabel.text = [SNPeriodsPickerController titleForLength:[DMUser defaultUser].hourPeriod];
//            } else {
//                detailCell.detailLabel.text = LMLocalizedString(@"nie", nil);
//            }
//            detailCell.separatorType = SNCellSeparatorTypeShort;
//
//            cell = detailCell;
//        }
//            break;
        case 4:
        {
            SNRightDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"SNRightDetailCell" forIndexPath:indexPath];
            detailCell.titleLabel.text = LMLocalizedString(@"Tag", nil);
            if ([[DMUser defaultUser] dayPeriod]) {
                detailCell.detailLabel.text = [SNPeriodsPickerController titleForLength:[DMUser defaultUser].dayPeriod+9];
            } else {
                detailCell.detailLabel.text = LMLocalizedString(@"nie", nil);
            }
            detailCell.separatorType = SNCellSeparatorTypeShort;
            cell = detailCell;
        }
            break;
        case 5:
        {
            SNRightDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"SNRightDetailCell" forIndexPath:indexPath];
            detailCell.titleLabel.text = LMLocalizedString(@"Woche", nil);
            if ([[DMUser defaultUser] weekPeriod]) {
                detailCell.detailLabel.text = [SNPeriodsPickerController titleForLength:[DMUser defaultUser].weekPeriod+14];
            } else {
                detailCell.detailLabel.text = LMLocalizedString(@"nie", nil);
            }
            detailCell.separatorType = SNCellSeparatorTypeLong;
            cell = detailCell;
        }
            break;
        default:
            cell  = [tableView dequeueReusableCellWithIdentifier:@"SNRightDetailCell" forIndexPath:indexPath];
            break;
    }
    
    return cell;
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        return 110;
    }
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 3:
            [self performSegueWithIdentifier:NSStringFromClass([SNTimePickerController class]) sender:self];
            break;
        case 4:
            [self performSegueWithIdentifier:NSStringFromClass([SNPeriodsPickerController class]) sender:self];
            break;
        case 5:
            [self performSegueWithIdentifier:NSStringFromClass([SNPeriodsPickerController class]) sender:self];
            break;
        case 6:
            [self performSegueWithIdentifier:NSStringFromClass([SNPeriodsPickerController class]) sender:self];
            break;
            
        default:
            break;
    }
}

//#pragma mark - Local Notofication
//- (void)updateLocalNotifications {
//    NSArray *array = [[UIApplication sharedApplication] scheduledLocalNotifications];
//    for (UILocalNotification *notification in array) {
//        [[UIApplication sharedApplication] cancelLocalNotification:notification];
//        NSLog(@"%s delete local notification", __FUNCTION__);
//        
//        
//        //            NSNumber *eventID = notification.userInfo[DMEventIDKey];
//        //            if ([eventID isEqualToNumber:self.eventID]) {
//        //                [[UIApplication sharedApplication] cancelLocalNotification:notification];
//        //            }
//        
//    }
//    
//    
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    
//    
//    NSLog(@"%s", __FUNCTION__);
//    NSLog(@"notification date: %@", [[DMUser defaultUser] notificationDate]);
//    
//    if ([[DMUser defaultUser] memoryFirstSwitchIsOn] == YES) {
//        
//        
//        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
////        NSDateComponents *components = [NSDateComponents new];
////        
////        //    components.second = 4;
////        //    NSDate *date = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
////        
////        components.minute = -15;
////        NSDate *date = [calendar dateByAddingComponents:components toDate:self.date options:0];
////        // Schedule the notification
//        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//        localNotification.fireDate = [[DMUser defaultUser] notificationDate];
//        localNotification.alertBody = LMLocalizedString(@"swopper FIT Messung", nil);
//        
//        localNotification.timeZone = [NSTimeZone defaultTimeZone];
////        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//////        localNotification.userInfo = @{ DMEventIDKey : self.eventID };
////        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//        
//    }
//}

@end














