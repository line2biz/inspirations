

#import <UIKit/UIKit.h>

@interface SNAvatarView : UIView

@property (strong, nonatomic) UIImage *image;
@property (assign, nonatomic) CGFloat lineWidth;
@property (assign, nonatomic) UIColor *lineColor;

@end
