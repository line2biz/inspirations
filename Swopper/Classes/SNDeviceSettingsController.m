

#import "SNDeviceSettingsController.h"

@interface SNDeviceSettingsController ()


@property (weak, nonatomic) IBOutlet UITextField *cpmLabel;
@property (weak, nonatomic) IBOutlet UITextField *frequencyLabel;
@property (weak, nonatomic) IBOutlet UITextField *calloriesLabel;


@end

@implementation SNDeviceSettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.cpmLabel.text = [NSString stringWithFormat:@"%f", [[NSUserDefaults standardUserDefaults] floatForKey:CPMFactorValueKey]];
    self.frequencyLabel.text = [NSString stringWithFormat:@"%f", [[NSUserDefaults standardUserDefaults] floatForKey:CPMFrequencyKey]];
    self.calloriesLabel.text = [NSString stringWithFormat:@"%f", [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    CGFloat f1 = [self.cpmLabel.text floatValue];
    [[NSUserDefaults standardUserDefaults] setFloat:f1 forKey:CPMFactorValueKey];
    
    CGFloat f2 = [self.calloriesLabel.text floatValue];
    [[NSUserDefaults standardUserDefaults] setFloat:f2 forKey:CPMCalloriewsKey];
    
    CGFloat f3 = [self.frequencyLabel.text floatValue];
    [[NSUserDefaults standardUserDefaults] setFloat:f3 forKey:CPMFrequencyKey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Outlet Methods
- (IBAction)valueChanged:(UISlider *)sender {
    
//    if (sender == self.cpmSlider) {
//        [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:CPMFactorValueKey];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        self.cpmLabel.text = [NSString stringWithFormat:@"%f", sender.value];
//        
//    } else {
//        
//    }
    
}




@end
