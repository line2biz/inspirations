

#import <UIKit/UIKit.h>

@interface SNStepResultController : UIViewController

@property (nonatomic) NSInteger numberOfSteps;
@property (strong, nonatomic) NSDate *date;

@end
