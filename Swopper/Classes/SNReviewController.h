

#import <UIKit/UIKit.h>

@class DMSession;

@interface SNReviewController : UIViewController

@property (strong, nonatomic) DMSession *session;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
