

#import "SNStatisticViewController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#import "SNTopTrainingTableCell.h"
#import "SNLastTrainingsTableCell.h"
#import "SNSessionResultTableCell.h"

#import "DMManager.h"
#import "DMSession+Category.h"

#import "SNMenusController.h"

#import "SNStepManager.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface SNStatisticViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSInteger _iRowsCount;
    NSInteger _iLoadIdx;
    NSArray *_aSessionsForTOP;
    NSArray *_aSessionsForResult;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SNStatisticViewController


#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!self.managedObjectContext) {
        self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
    
    [SNAppearance customizeViewController:self];
    
    [self controllerStart];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_iLoadIdx) {
        [self animateRowsDrop];
        _iLoadIdx++;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor titleColor];
    self.labelTitle.text = LMLocalizedString(@"Statistik", nil);
    
    [self loadData];
}

- (void)loadData
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMSessionStartTimeKey
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count) {
        _aSessionsForResult = fetchedObjects;
        fetchedObjects = [fetchedObjects sortedArrayWithOptions:NSSortConcurrent
                                                usingComparator:^NSComparisonResult(id obj1, id obj2) {
                                                    DMSession *session1 = (DMSession *)obj1;
                                                    DMSession *session2 = (DMSession *)obj2;
                                                    if (session1.caloriesPerHour > session2.caloriesPerHour) {
                                                        return NSOrderedAscending;
                                                    } else if (session1.caloriesPerHour < session2.caloriesPerHour) {
                                                        return NSOrderedDescending;
                                                    } else {
                                                        return NSOrderedSame;
                                                    }
                                                }];
        _aSessionsForTOP = fetchedObjects;
    }
}

- (void)animateRowsDrop
{
    _iRowsCount = 0;
    NSInteger iResultsCount = (_aSessionsForResult.count <= 4) ? _aSessionsForResult.count : 4;
    
    NSInteger iRowsMax;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        iRowsMax = (([CMPedometer isStepCountingAvailable]) ? 3 : 2) + iResultsCount;
    } else {
        iRowsMax = (([CMStepCounter isStepCountingAvailable]) ? 3 : 2) + iResultsCount;
    }
    
    __weak typeof(self) weakSelf = self;
    NSTimeInterval timeInterval = 0.33f;
    for (NSInteger i=0; i < iRowsMax; i++) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timeInterval * (i+1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.tableView beginUpdates];
                
                [strongSelf.tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:_iRowsCount inSection:0] ] withRowAnimation:UITableViewRowAnimationTop];
                _iRowsCount++;
                
                [strongSelf.tableView endUpdates];
            }
        });
    }
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _iRowsCount;
}

- (void)configureTopCell:(SNTopTrainingTableCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    if (_aSessionsForTOP.count) {
        DMSession *topSession = [_aSessionsForTOP firstObject];
        cell.session = topSession;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSInteger iSessionsCount = (_aSessionsForResult.count <= 4) ? _aSessionsForResult.count : 4;
    NSInteger iRowIdx = (indexPath.row > 1 + iSessionsCount) ? 7 : indexPath.row;
    switch (iRowIdx) {
        case 0:
        {
            // top result
            SNTopTrainingTableCell *topCell = [tableView dequeueReusableCellWithIdentifier:@"TopCell"];
            [self configureTopCell:topCell forIndexPath:indexPath];
            
            cell = topCell;
        }
            break;
        case 1:
        {
            // last results
            SNLastTrainingsTableCell *trainingsCell = [tableView dequeueReusableCellWithIdentifier:@"TrainingCell"];
            
            cell = trainingsCell;
        }
            break;
        case 7: // last cell
        {
            // navigation
            cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell"];
            
            NSString *title = LMLocalizedString(@"Zum Schrittzähler", nil);
            UIImage *icon = [UIImage imageNamed:@"Icon-Pedometr-Gray"];
            
            cell.imageView.image = icon;
            cell.textLabel.text = title;
        }
            break;
            
        default:
        {
            SNSessionResultTableCell *resultCell = [tableView dequeueReusableCellWithIdentifier:@"ResultCell"];
            
            resultCell.session = [_aSessionsForResult objectAtIndex:indexPath.row - 2];
            resultCell.iOrder = indexPath.row - 2;
            
            cell = resultCell;
        }
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat fHeight;
    switch (indexPath.row) {
        case 0:
        {
            fHeight = 140.0f;
        }
            break;
        default:
            fHeight = 44.0f;
            break;
    }
    return fHeight;
}

#pragma mark - Table View Delegate
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 2 + _aSessionsForResult.count) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2 + _aSessionsForResult.count) {
        // go to the next controller
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:^{
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNStepDashboardController"];
            [self.navigationController pushViewController:viewController animated:YES];            
        }];
    }
}


@end
