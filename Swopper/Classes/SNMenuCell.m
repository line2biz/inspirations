

#import "SNMenuCell.h"

#import "UIFont+Customization.h"

@implementation SNMenuCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textLabel.font = [UIFont robotoRegularWithSize:self.textLabel.font.pointSize];    
}



@end
