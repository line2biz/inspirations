

#import <UIKit/UIKit.h>

@interface SNSwitchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;
@property (nonatomic) BOOL borderHidden;

@property (weak, nonatomic) IBOutlet UIView *borderView;


@end
