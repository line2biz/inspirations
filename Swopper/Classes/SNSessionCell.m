

#import "SNSessionCell.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

@implementation SNSessionCell

/*
 
 @property (weak, nonatomic) IBOutlet UILabel *sortOrderLabel;
 @property (weak, nonatomic) IBOutlet UILabel *titleLabel;
 @property (weak, nonatomic) IBOutlet UILabel *timeLable;
 @property (weak, nonatomic) IBOutlet UILabel *swoppsLable;
 @property (weak, nonatomic) IBOutlet UILabel *calloriesLabel;
 
 */

- (void)awakeFromNib {
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    NSArray *array = @[ self.timeLable, self.swoppsLable, self.calloriesLabel, self.sortOrderLabel];
    for (UILabel *label in array) {
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
        label.textColor = [UIColor customLightGrayColor];
    }
}


@end
