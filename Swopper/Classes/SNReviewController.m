

#import "SNReviewController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNProgressView3D.h"
#import "DMSession+Category.h"
#import "DMMotion+Category.h"

@interface SNReviewController () <SNProgressView3DProtocol>

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (assign, nonatomic) NSInteger shownValue;

@end

@implementation SNReviewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SNAppearance customizeViewController:self];
    self.view.backgroundColor = [UIColor whiteColor];
    self.shownValue = 0;
    
    [self.valueLabel setText:[NSString stringWithFormat:@"%zd",self.shownValue]];
    for (UILabel *label in self.labels) {
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
    }
    
    NSInteger maxValue = [self.session.maxValue integerValue];
    if (maxValue == 0) {
        maxValue = 1;
    }
    
    
    NSString *title = [NSString stringWithFormat:@"%zd ", [self.session.maxValue integerValue]];
    title = [title stringByAppendingString:LMLocalizedString(@"kcal in dieser Session", nil)];
    self.titleLabel.text = title;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K < %@", DMSessionStartTimeKey, self.session.startTime];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:DMSessionStartTimeKey ascending:NO];
    fetchRequest.sortDescriptors = @[sortDesc];
    fetchRequest.fetchLimit = 1;
    
    
    NSString *subtitle = @"";
    
    NSArray *restults = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];
    if ([restults count]) {
        DMSession *previosSession = [restults firstObject];
        NSUInteger prevVal = [previosSession.maxValue integerValue];
        NSUInteger curVal = [self.session.maxValue integerValue];
        if (prevVal == curVal) {
            subtitle = LMLocalizedString(@"der gleich dem vorherigen Ergebnis", nil);
        }
        else if (curVal > prevVal) {
            NSUInteger percent = ABS((curVal - prevVal) / (curVal + prevVal) * 100);
            subtitle = [NSString stringWithFormat:@"%@ %zd", LMLocalizedString(@"das sind 40% mehr als beim letzten Mal", nil), percent];
        } else {
            
            NSUInteger percent = ABS((curVal - prevVal) / (curVal + prevVal) * 100.f);
            subtitle = [NSString stringWithFormat:@"%@ %zd", LMLocalizedString(@"das ist 40% weniger als beim letzten Mal", nil), percent];
        }
    }
    
    self.subtitleLabel.text = subtitle;
    
    [self configureOutlets];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark - Controller Methods
- (void)configureOutlets
{
    self.progressView.delegate = self;
    [self.progressView setProgressColor:[UIColor customOrangeColor]];
    [self.progressView setPathColor:[UIColor customDarkGrayGolor]];
    [self.progressView setLineWidth:10.0f];
    
    self.slider.value = 0.0f;
    
}


#pragma mark - Progress View 3D Protocol
- (void)progressView:(SNProgressView3D *)progressView timerActive:(BOOL)active
{
    if (active) {
        self.slider.value = self.progressView.currentPoint / self.progressView.maxPoints;
        if (self.session.motions.count) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
            NSArray *arrayOfMotionsSorted = [self.session.motions sortedArrayUsingDescriptors:@[descriptor]];
            
            CGFloat(^CaloriesAmountAtTime)(NSArray *, NSTimeInterval) = ^(NSArray *source, NSTimeInterval time) {
                CGFloat amount = 0.0f;
                NSInteger timePoint = (NSInteger)time;
                for (NSInteger i=0; i<timePoint+1; i++) {
                    if (i < source.count) {
                        DMMotion *motion = (DMMotion*)[source objectAtIndex:i];
                        amount += [motion.value floatValue];
                    }
                }
                return amount;
            };
            
            NSInteger calories = (NSInteger)CaloriesAmountAtTime(arrayOfMotionsSorted,self.progressView.currentPoint);
            self.valueLabel.text = [NSString stringWithFormat:@"%zd",calories];
        }
    } else {
        [self.progressView setCurrentPoint:0];
        self.slider.value = 0.0f;
    }
}

#pragma mark - Outlet Methods
- (IBAction)valueChanged:(UISlider *)sender
{
    
    [self.progressView setProgressAtPoint:(NSTimeInterval)(sender.value * self.progressView.maxPoints)];
}

- (IBAction)didTapButtonPlay:(UIButton *)sender
{
    if ([sender isSelected]) {
        [self.progressView stopProgressingTimer];
        [self.progressView setCurrentPoint:0];
        [self.slider setValue:0.0f];
    } else {
        [self.progressView startProgressingTimerWithDuration:(NSTimeInterval)self.session.motions.count];
    }
    [sender setSelected:!sender.isSelected];
}

@end
