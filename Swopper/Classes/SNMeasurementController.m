

#import "SNMeasurementController.h"
#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNOrangeButton.h"
#import "SNSummaryController.h"
#import "SNTimeIntervalController.h"
#import "SNTrainingSummaryController.h"

#import "SNAppDelegate.h"

#import "SNMenusController.h"
#import "DMUser.h"
#import "DMMotion+Category.h"
#import "DMSession+Category.h"
#import "DMReport+Category.h"
#import "SNProgressView3D.h"
#import <AudioToolbox/AudioServices.h>

#import "SNCurvedWavesView.h"
#import "SNAdditionalMath.h"

#import <MFSideMenu.h>

@import CoreMotion;
@import AVFoundation;

#define kCMDeviceMotionUpdateFrequency (1.f/20.f)
#define kMAX_VALUE 5.f
#define kFILTER_VALUE  0.106232577488922

typedef enum {
    kDeviceMotionGraphTypeAttitude = 0,
    kDeviceMotionGraphTypeRotationRate,
    kDeviceMotionGraphTypeGravity,
    kDeviceMotionGraphTypeUserAcceleration
} DeviceMotionGraphType;

@interface SNMeasurementController () <AVAudioPlayerDelegate, SNProgressView3DProtocol, UIAlertViewDelegate>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
    CGFloat _audioDuration;
    BOOL _updatesAreStopped;
    NSInteger _iAudioStep;
}

@property (strong, nonatomic) CMMotionManager *motionManager;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (weak, nonatomic) IBOutlet UILabel *labelDigits;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet SNOrangeButton *buttonStop;
@property (assign, nonatomic) CGFloat Kkal;

@property (strong, nonatomic) NSMutableArray *storeArray;
@property (nonatomic) NSUInteger bowCounter;
@property (nonatomic) double thresholdValue;

@property (strong, nonatomic) DMSession *trainingSession;

@property (assign, nonatomic) BOOL processIsPaused;
@property (strong, nonatomic) NSMutableArray *mArrayOfPauseIntervals;
@property (strong, nonatomic) NSDate *pauseStartDate;
@property (strong, nonatomic) NSDate *pauseEndDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet SNCurvedWavesView *curvedWavesView;

@property (assign, nonatomic) BOOL trainingFinished;
@property (nonatomic) BOOL screeenHidden;

@end

@implementation SNMeasurementController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    _iAudioStep = 0;
    
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    
    self.mArrayOfPauseIntervals = [NSMutableArray new];
    
    [self.navigationItem setHidesBackButton:YES];
    
    if (self.trainingTime > 0) {
        self.labelTitle.text = LMLocalizedString(@"swopper FIT Messung", nil);
    } else {
        self.labelTitle.text = LMLocalizedString(@"Mitmach-Training", nil);
    }
    
    
    
    [self.nextButton setTitle:LMLocalizedString(@"Fortsetzen", nil) forState:UIControlStateSelected];
    
    [self configureFonts];
    
    [self configureView];
    
    [self controllerStart];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeNone;
    
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = YES;
    
    if (self.trainingTime > 0) {
        self.labelSubtitle.text = [self textForSubtitleWithTime:self.trainingTime];
    } else {
        self.labelSubtitle.text = [self textForSubtitleWithTime:[self trainingMusicDuration]];
    }
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
    
    
    
    CATransform3D rotationAndPerspectiveTransform = [self.progressView transform3D];
    
    CAKeyframeAnimation *keyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    keyFrameAnimation.duration = 0.001;
    keyFrameAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    keyFrameAnimation.removedOnCompletion = NO;
    keyFrameAnimation.fillMode = kCAFillModeForwards;
    keyFrameAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DIdentity], [NSValue valueWithCATransform3D:rotationAndPerspectiveTransform]];
    
    [CATransaction begin];
    
    [self.curvedWavesView.layer addAnimation:keyFrameAnimation forKey:@"keyFrame"];
    
    [CATransaction commit];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self startUpdates];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    sidemenu.panMode = MFSideMenuPanModeDefault;
    
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    if (!self.trainingFinished) {
        for (DMMotion *motion in self.session.motions) {
            [self.managedObjectContext deleteObject:motion];
        }
        [self.managedObjectContext deleteObject:self.session];
    }
    
    [self stopUpdates];
    [self.progressView stopProgressingTimer];
}

#pragma mark - Property Accessors
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = [[DMManager sharedManager] defaultContext];
    }
    return _managedObjectContext;
}

- (DMSession *)session
{
    if (!_session) {
        _session = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMSession class]) inManagedObjectContext:self.managedObjectContext];
    }
    return _session;
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    
    self.labelDigits.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelDigits.font.pointSize];
    self.labelText.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelText.font.pointSize];
    self.buttonStop.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.buttonStop.titleLabel.font.pointSize];
}

- (void)configureView
{
//    if (self.screeenHidden) {
//        return;
//    }
    
    self.labelDigits.text = [NSString stringWithFormat:@"%zd", [self.session countsForSession]];
    
    [self.progressView setPathColor:[UIColor customLightGrayColor]];
    [self.progressView setProgressColor:[UIColor customOrangeColor]];
    [self.progressView setLineWidth:10.0f];
    [self.progressView setDelegate:self];
    
    [self.progressView setDontTransform:YES];
}

- (NSString *)textForSubtitleWithTime:(NSTimeInterval)time
{
    NSInteger minutes = 0, seconds = time;
    minutes = (seconds - seconds%60)/60;
    seconds = seconds%60;
    
    NSString *sMinutes = [NSString stringWithFormat:@"%zd",minutes];
    if (sMinutes.length == 1) {
        sMinutes = [NSString stringWithFormat:@"0%@",sMinutes];
    }
    NSString *sSeconds = [NSString stringWithFormat:@"%zd",seconds];
    if (sSeconds.length == 1) {
        sSeconds = [NSString stringWithFormat:@"0%@",sSeconds];
    }
    NSString *resultText = [NSString stringWithFormat:@"%@ %@:%@", LMLocalizedString(@"Messung läuft noch", nil), sMinutes, sSeconds];
    return resultText;
}

- (void)endOfTraining
{
    [self stopUpdates];
    
    self.session.endTime = [NSDate date];
    self.session.maxValue = @([self.session.motions count]);
    
    self.trainingFinished = YES;
    if ([self.session.training boolValue]) {        
        self.session.interval = [NSNumber numberWithInteger:8 * 60 * 60]; // default time interval for training
//        self.session.sessionDuration = [NSNumber numberWithFloat:_audioDuration];
        NSTimeInterval sessionDuration = [self trainingMusicDuration] - [self startMusicDuration] - [self endMusicDuration] - 2*[self signalDuration];
        self.session.sessionDuration = [NSNumber numberWithFloat: sessionDuration];
        [[SNAppDelegate sharedDelegate] setCurrentSession:self.session];
        [[DMManager sharedManager] saveContext];
//        [((SNMenusController *)self.navigationController.parentViewController) showControllerWithIdentifier:NSStringFromClass([SNTrainingSummaryController class])];
        
        UIViewController *sideMenuController = [SNAppDelegate sharedDelegate].window.rootViewController;
        for (UIViewController *viewController in sideMenuController.childViewControllers) {
            if ([viewController isKindOfClass:[SNMenusController class]]) {
                [((SNMenusController *)viewController) showControllerWithIdentifier:NSStringFromClass([SNTrainingSummaryController class])];
                break;
            }
        }
        
    } else {
        self.session.sessionDuration = @((CGFloat)self.trainingTime);
        DMReport *report = [DMReport reportByDate:[NSDate date] inContext:self.managedObjectContext];
        if (report) {
            self.session.interval = report.interval;
            [[SNAppDelegate sharedDelegate] setCurrentSession:self.session];
            [report addSessionsObject:self.session];
//            [((SNMenusController *)self.navigationController.parentViewController) showControllerWithIdentifier:NSStringFromClass([SNSummaryController class])];
            
            MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
            [((SNMenusController *)sidemenu.leftMenuViewController) showControllerWithIdentifier:NSStringFromClass([SNSummaryController class])];
        } else {
            SNTimeIntervalController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNTimeIntervalController class])];
            report = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMReport class]) inManagedObjectContext:self.managedObjectContext];
            // session will be saved in Time Interval Controller after setting the interval
            [[SNAppDelegate sharedDelegate] setCurrentSession:self.session];
            viewController.managedObjectContext = self.managedObjectContext;
            viewController.report = report;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        [[DMManager sharedManager] saveContext];
    }
}

#pragma mark Curve Animation

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark Audio Part
- (NSTimeInterval)trainingMusicDuration
{
    NSTimeInterval result = [self audioDurationFromSourceWithName:@"training_1" type:@"mp3"] + [self audioDurationFromSourceWithName:@"training_2" type:@"mp3"] + [self audioDurationFromSourceWithName:@"training_3" type:@"mp3"] + [self audioDurationFromSourceWithName:@"Signal" type:@"mp3"]*2;
    return result;
}

- (void)playStartMusic
{
    _iAudioStep = 1;
    if (self.trainingTime < 1) {
        [self playAudioWithName:@"training_1" type:@"mp3"];
    } else {
        [self playAudioWithName:@"messung_1" type:@"mp3"];
    }
}

- (NSTimeInterval)startMusicDuration
{
    NSTimeInterval result = 0;
    if (self.trainingTime < 1) {
        result = [self audioDurationFromSourceWithName:@"training_1" type:@"mp3"];
    } else {
        result = [self audioDurationFromSourceWithName:@"messung_1" type:@"mp3"];
    }
    return result;
}

- (void)playEndMusic
{
    if (self.trainingTime < 1) {
        _iAudioStep = 3;
        [self playAudioWithName:@"training_3" type:@"mp3"];
    } else {
        _iAudioStep = 2;
        [self playAudioWithName:@"messung_2" type:@"mp3"];
    }
}

- (NSTimeInterval)endMusicDuration
{
    NSTimeInterval result = 0;
    if (self.trainingTime < 1) {
        result = [self audioDurationFromSourceWithName:@"training_3" type:@"mp3"];
    } else {
        result = [self audioDurationFromSourceWithName:@"messung_2" type:@"mp3"];
    }
    return result;
}

- (void)playSignal
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self playAudioWithName:@"Signal" type:@"mp3"];
}

- (NSTimeInterval)signalDuration
{
    return [self audioDurationFromSourceWithName:@"Signal" type:@"mp3"];
}

- (void)playRandomSupportAudio
{
    NSString *sound1, *sound2, *sound3;
    sound1 = @"random_1.mp3";
    sound2 = @"random_2.mp3";
    sound3 = @"random_3.mp3";
    NSDictionary *(^AudiodictFromString)(NSString *) = ^(NSString *audioFullName) {
        NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:2];
        if (audioFullName.length > 0) {
            NSString *fileName, *fileExtension;
            NSArray *array = [audioFullName componentsSeparatedByString:@"."];
            if (array.count > 2) {
                __block NSMutableString *mString = [NSMutableString new];
                [array enumerateObjectsUsingBlock:^(NSString *substring, NSUInteger idx, BOOL *stop) {
                    if (idx == array.count-2) {
                        [mString appendString:substring];
                        *stop = YES;
                        return;
                    } else {
                        [mString appendString:substring];
                        [mString appendString:@"."];
                    }
                }];
                fileName = mString;
            } else {
                fileName = [array firstObject];
            }
            fileExtension = [array lastObject];
            
            [result setObject:fileName forKey:@"fileName"];
            [result setObject:fileExtension forKey:@"fileExtension"];
        }
        return result;
    };
    
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    [mArrayOfDicts addObject:AudiodictFromString(sound1)];
    [mArrayOfDicts addObject:AudiodictFromString(sound2)];
    [mArrayOfDicts addObject:AudiodictFromString(sound3)];
    
    NSInteger iArraySize = mArrayOfDicts.count - 1;
    NSInteger iRandomIdx = arc4random_uniform((u_int32_t)iArraySize);
    NSDictionary *dictRandomSound = [mArrayOfDicts objectAtIndex:iRandomIdx];
    [self playAudioWithName:dictRandomSound[@"fileName"] type:dictRandomSound[@"fileExtension"]];
}

- (void)playAudioWithName:(NSString *)name type:(NSString *)type
{
    NSError *error;
    NSString *soundFilePath = [[SNLocalizationManager sharedManager] pathForResource:name ofType:type];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
                                                                      error: &error];
    
    if (error){
        NSLog(@"%s Error in audioPlayer: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    } else {
        newPlayer.volume = 1.0f;
        self.audioPlayer = newPlayer;
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer setDelegate: self];
        [self.audioPlayer play];
    }
}

- (NSTimeInterval)audioDurationFromSourceWithName:(NSString *)name type:(NSString *)type
{
    AVAudioPlayer *sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[SNLocalizationManager sharedManager] pathForResource:name ofType:type]]
                                                                  error:nil];
    return sound.duration;
}

#pragma mark - Outlet Methods
- (IBAction)didTapStopButton:(UIButton *)sender
{
    MFSideMenuContainerViewController *sidemenu = (MFSideMenuContainerViewController *)([SNAppDelegate sharedDelegate].window.rootViewController);
    if (self.processIsPaused) {
        [self.progressView continueProgressingTimer];
        self.pauseEndDate = [NSDate date];
        [self.mArrayOfPauseIntervals addObject:@((CGFloat)[self.pauseEndDate timeIntervalSinceDate:self.pauseStartDate])];
        if (self.audioPlayer.currentTime != 0) {
            [self.audioPlayer play];
        }
        [sender setSelected:NO];
        
        sidemenu.panMode = MFSideMenuPanModeNone;

    } else {
        [self.progressView pauseProgressingTimer];
        self.pauseStartDate = [NSDate date];
        if ([self.audioPlayer isPlaying]) {
            [self.audioPlayer pause];
        }
        [sender setSelected:YES];
        
        sidemenu.panMode = MFSideMenuPanModeDefault;

    }
}

- (IBAction)didTapButtonStart:(id)sender {
    
    [self startUpdates];
}

- (void)didTapRestart:(UIBarButtonItem *)button
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:LMLocalizedString(@"Training neu starten?", nil)
                                                       delegate:self
                                              cancelButtonTitle:LMLocalizedString(@"Nein", nil)
                                              otherButtonTitles:LMLocalizedString(@"Ja", nil), nil];
    [alertView show];
}


#pragma mark - Motion Methods
- (void)startUpdates
{
    [self.nextButton setEnabled:NO];
    self.processIsPaused = NO;
    self.navigationItem.leftBarButtonItem = nil;
    if (self.trainingTime > 0) {
        
        self.labelSubtitle.text = [self textForSubtitleWithTime:self.trainingTime];
        [self.progressView startProgressingTimerWithDuration:self.trainingTime];
    } else {
        _audioDuration = [self trainingMusicDuration];
        if (_audioDuration) {
            self.labelSubtitle.text = [self textForSubtitleWithTime:_audioDuration];
            [self.progressView startProgressingTimerWithDuration:_audioDuration];
            self.processIsPaused = NO;
        }
    }
    [self playStartMusic];
}

- (void)getMotions
{
    typeof(self) __weak weakSelf = self;
    __block NSTimeInterval timeInterval = 0;
    __block double summary = 0;
    if ([self.motionManager isDeviceMotionActive] == YES) {
        [self.motionManager stopDeviceMotionUpdates];
    }
    if ([self.motionManager isDeviceMotionAvailable] == YES) {
        [self.motionManager setDeviceMotionUpdateInterval:kCMDeviceMotionUpdateFrequency];
        [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *deviceMotion, NSError *error) {
            
            if (timeInterval == 0) {
                timeInterval = [[NSDate date] timeIntervalSince1970];
            }
            double value = sqrt(pow(ABS(deviceMotion.userAcceleration.x), 2) + pow(ABS(deviceMotion.userAcceleration.y), 2) + pow(ABS(deviceMotion.userAcceleration.z), 2));
            
            if(value > kFILTER_VALUE) {
                summary = summary + value;
            }
            
            
            NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
            
            if (floor(now - timeInterval) >= 1) {
                
                self.curvedWavesView.multiplier = MAX(MAX(ABS(deviceMotion.userAcceleration.x), ABS(deviceMotion.userAcceleration.y)), ABS(deviceMotion.userAcceleration.z)) / 3.0f / 2;
                
                DMMotion *motion = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMMotion class]) inManagedObjectContext:weakSelf.managedObjectContext];
                motion.value = @(summary);
                
                // set motion date
                CGFloat timePassedInPauses = 0;
                if ([self.mArrayOfPauseIntervals count]) {
                    for (NSNumber *time in self.mArrayOfPauseIntervals) {
                        timePassedInPauses += [time floatValue];
                    }
                }
                motion.time = [NSDate dateWithTimeInterval:-timePassedInPauses sinceDate:[NSDate date]];
                
                [weakSelf.session addMotionsObject:motion];
                summary = 0;
                timeInterval = now;
                [weakSelf configureView];
            }
            
        }];
    }
}

- (void)stopUpdates
{
    [self.motionManager stopDeviceMotionUpdates];
    self.motionManager = nil;
    [self.audioPlayer stop];
    _updatesAreStopped = YES;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        self.navigationItem.rightBarButtonItem = nil;
        [self.progressView stopProgressingTimer];
        self.labelDigits.text = @"0";
        [self.nextButton setSelected:![self.nextButton isSelected]];
        [self.mArrayOfPauseIntervals removeAllObjects];
        for (DMMotion *motion in self.session.motions) {
            [self.managedObjectContext deleteObject:motion];
        }
        self.session.motions = nil;
        [self startUpdates];
    }
}

#pragma mark - Progress View 3D Protocol
- (void)progressView:(SNProgressView3D *)progressView timerActive:(BOOL)active
{
    if (!active) {
        if (self.trainingTime > 0) {
            [self endOfTraining];
        }
    } else {
        if (self.trainingTime > 0) { // only for Messung
            // end audio time
            float duration = [self endMusicDuration] + [self signalDuration];
            if (self.progressView.maxPoints - self.progressView.currentPoint <= duration + 0.05 && self.progressView.maxPoints - self.progressView.currentPoint >= duration - 0.05) {
                [self.nextButton setEnabled:NO];
                [self playSignal];
                [self.motionManager stopDeviceMotionUpdates];
                self.motionManager = nil;
                _updatesAreStopped = YES;
            }
        
            // support audio and vibro section
            CGFloat fTimeDif = FloatPartOfNumber(self.progressView.currentPoint / 60.0f);
            if (fTimeDif < 0.00001f) {
                NSDictionary *(^AudioVibroSupport)(BOOL, CGFloat) = ^(BOOL audio, CGFloat time) {
                    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithCapacity:2];
                    [mDict setObject:[NSNumber numberWithBool:audio] forKey:@"audio"];
                    [mDict setObject:[NSNumber numberWithFloat:time] forKey:@"time"];
                    return mDict;
                };
                NSArray *arrayOfTimes; // defines the array of times support audio/vibro must be played
                if (self.trainingTime == 5 * 60) { // it depends from training duration
                    arrayOfTimes = [[NSArray alloc] initWithObjects:AudioVibroSupport(NO,4*60), AudioVibroSupport(YES,3*60),
                                                                    AudioVibroSupport(NO,2*60), AudioVibroSupport(YES,1*60), nil];
                }
                if (self.trainingTime == 15 * 60) {
                    arrayOfTimes = [[NSArray alloc] initWithObjects:AudioVibroSupport(NO,14*60), AudioVibroSupport(YES,12*60),
                                                                    AudioVibroSupport(NO,11*60), AudioVibroSupport(YES,9*60),
                                                                    AudioVibroSupport(NO,8*60), AudioVibroSupport(YES,6*60),
                                                                    AudioVibroSupport(NO,4*60), AudioVibroSupport(YES,2*60), nil];
                }
                if (self.trainingTime == 30 * 60) {
                    arrayOfTimes = [[NSArray alloc] initWithObjects:AudioVibroSupport(NO,29*60), AudioVibroSupport(YES,26*60),
                                                                    AudioVibroSupport(NO,25*60), AudioVibroSupport(YES,23*60),
                                                                    AudioVibroSupport(NO,20*60), AudioVibroSupport(YES,18*60),
                                                                    AudioVibroSupport(NO,15*60), AudioVibroSupport(YES,13*60),
                                                                    AudioVibroSupport(NO,10*60), AudioVibroSupport(YES,8*60),
                                                                    AudioVibroSupport(NO,5*60), AudioVibroSupport(YES,3*60), nil];
                }
                __weak typeof(self) weakSelf = self;
                
                CGFloat fProgressTime = self.progressView.currentPoint;
                [arrayOfTimes enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
                    typeof(weakSelf) strongSelf = weakSelf;
                    if (strongSelf) {
                        CGFloat fDictTime = [dict[@"time"] floatValue];
                        CGFloat fDif = fDictTime - fProgressTime;
                        if (fDif < 0.00001f && fDif > - 0.0001) {
                            if ([dict[@"audio"] boolValue]) {
                                NSLog(@"\nAudio:\nDict time: %f Progress time: %f\n",fDictTime, fProgressTime);
                                [strongSelf playRandomSupportAudio];
                            } else {
                                NSLog(@"\nVibro:\nDict time: %f Progress time: %f\n",fDictTime, fProgressTime);
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                            }
                            *stop = YES;
                        }
                    }
                }];
            }
        }
        
        // timer text
        if (self.trainingTime > 0) {
            self.labelSubtitle.text = [self textForSubtitleWithTime:self.trainingTime - self.progressView.currentPoint];
        } else {
            self.labelSubtitle.text = [self textForSubtitleWithTime:[self trainingMusicDuration] - self.progressView.currentPoint];
        }
    }
}

- (void)progressView:(SNProgressView3D *)progressView paused:(BOOL)paused
{
    self.processIsPaused = paused;
    if (!paused) {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = nil;
        if (!_updatesAreStopped) {
            [self getMotions];
        }
    } else {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"report = nil"];
        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDesc];
        NSError *error = nil;
        NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        DMSession *firstSession = nil;
        if (error) {
            NSLog(@"%s Error during fetching counting request: %@", __FUNCTION__, error);
        } else {
            firstSession = [results firstObject];
        }
        
        if (self.session == firstSession) {
            self.navigationItem.leftBarButtonItem = nil;
            
        } else {
            SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
            
        }
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Repeat-Gray"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(didTapRestart:)];
        self.navigationItem.rightBarButtonItem = barButton;
        
        if ([self.motionManager isDeviceMotionActive] == YES) {
            [self.motionManager stopDeviceMotionUpdates];
        }
    }
}

#pragma mark - Audio Player Delegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    void(^CommonBlock)() = ^() {
        [self.nextButton setEnabled:YES];
        
        if (!self.managedObjectContext) {
            self.managedObjectContext = [[DMManager sharedManager] defaultContext];
        }
        
        if (!self.session) {
            self.session = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMSession class])
                                                         inManagedObjectContext:self.managedObjectContext];
        }
        
        self.session.cpmFactor = @([[NSUserDefaults standardUserDefaults] floatForKey:CPMFactorValueKey]);
        if (self.trainingTime == 0) {
            self.session.training = [NSNumber numberWithBool:YES];
        }
        
        CMMotionManager *mManager = [[CMMotionManager alloc] init];
        self.motionManager = mManager;
        
        
        [self getMotions];
    };
    
    if (self.trainingTime < 1) {
        if (_iAudioStep != 3) {
            
            if (_iAudioStep == 1) {
                CommonBlock();
            } else if (_iAudioStep == 2) {
                [self.nextButton setEnabled:NO];
                [self.motionManager stopDeviceMotionUpdates];
                self.motionManager = nil;
                _updatesAreStopped = YES;
            }
            
            if (player.duration == [self signalDuration]) {
                _iAudioStep++;
                NSString *sNextAudio = [NSString stringWithFormat:@"training_%zd",_iAudioStep];
                [self playAudioWithName:sNextAudio type:@"mp3"];
            } else {
                [self playSignal];
            }
            
        } else {
            [[DMUser defaultUser] setFirstLaunch:NO];
            [[DMUser defaultUser] save];
            [self endOfTraining];
        }
    } else {
        if (_iAudioStep == 1) {
            CommonBlock();
            _iAudioStep = 0;
            [self playSignal];
        } else {
            if (player.duration == [self signalDuration]) {
                if (self.progressView.currentPoint > self.progressView.maxPoints / 2) {
                    [self playEndMusic];
                }
            }
        }
    }
}




@end
