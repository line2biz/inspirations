

#import <UIKit/UIKit.h>

@class DMSession;

@interface SNMeasurementController : UIViewController

@property (strong, nonatomic) DMSession *session;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (assign, nonatomic) NSTimeInterval trainingTime;

@end
