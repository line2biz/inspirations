

#import "SNTodayCell.h"
#import "SNAppearance.h"

@implementation SNTodayCell


#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}


#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIValue:(NSInteger)iValue
{
    if (_iValue != iValue) {
        NSInteger prevValue = _iValue;
        _iValue = iValue;
        if (iValue > 0) {
            [self.labelNumber animateTextFromMinValue:prevValue
                                           toMaxValue:_iValue
                                           withPrefix:nil
                                               suffix:nil
                                            maxPrefix:nil
                                            maxSuffix:nil
                                             duration:1.5f
                                             andSteps:25
                                      andCompletition:^{
                                          if (self.completitionDelegate) {
                                              [self.completitionDelegate TodayCell:self finishedTextAnimation:YES];
                                          }
                                      }];
        } else {
            self.labelNumber.text = [SNAppearance textWithThousendsFormat:0];
            if (self.completitionDelegate) {
                [self.completitionDelegate TodayCell:self finishedTextAnimation:YES];
            }
        }
    } else {
        self.labelNumber.text = [SNAppearance textWithThousendsFormat:_iValue];
        if (self.completitionDelegate) {
            [self.completitionDelegate TodayCell:self finishedTextAnimation:YES];
        }
    }
}

@end
