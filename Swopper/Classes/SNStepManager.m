

#import "SNStepManager.h"

#import "DMStep+Category.h"

NSString * const SNStepManagerValueChanged = @"SNStepManagerValueChanged";


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface SNStepManager ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) CMPedometer *pedometr;
@end


@implementation SNStepManager

#pragma mark - Initailization
- (instancetype)init {
    self = [super init];
    if (self) {
        _managedObjectContext = [[DMManager sharedManager] defaultContext];
        
//        [CMMotionActivityManager isActivityAvailable]
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([CMPedometer isStepCountingAvailable]) {
                if ([CMPedometer isDistanceAvailable]) {
                    self.pedometr = [CMPedometer new];
                    [self loadMotionHistory];
                }
            }
        } else {
            if ([CMStepCounter isStepCountingAvailable]) {
                [self loadMotionHistory];
            }
        }
    }
    return self;
}

+ (SNStepManager *)sharedManager {
    static dispatch_once_t once;
    static SNStepManager *__inctance;
    dispatch_once(&once, ^ { __inctance = [[SNStepManager alloc] init]; });
    return __inctance;
    
}

#pragma mark - Load History
- (void)loadCurrentDateHistoryWithCompletionHandler:(void (^)(void))completionHandler {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SNAppSecondLaucnKey] == NO) {
        return;
    }
    
    // Check authorized access to the CoreMotion
    BOOL bStepCounterIsAvailable = NO;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([CMPedometer isStepCountingAvailable]) {
            if ([CMPedometer isDistanceAvailable]) {
                bStepCounterIsAvailable = YES;
            }
        }
    } else {
        if ([CMStepCounter isStepCountingAvailable]) {
            bStepCounterIsAvailable = YES;
        }
    }
    
    // if steps counter is available we should load data to database
    if (bStepCounterIsAvailable) {
        NSOperationQueue *queue = [NSOperationQueue mainQueue];
        
        __weak typeof(self) weakSelf = self;
        NSDate *date = [NSDate date];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:date];
        dateComponents.hour = 0;
        dateComponents.minute = 0;
        dateComponents.second = 0;
        
        NSDate *firstDate = [calendar dateFromComponents:dateComponents];
        
        dateComponents = [NSDateComponents new];
        dateComponents.day = 1;
        dateComponents.second = -1;
        
        NSDate *lastDate = [calendar dateByAddingComponents:dateComponents toDate:firstDate options:0];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([CMPedometer isStepCountingAvailable]) {
                
                [self.pedometr startPedometerUpdatesFromDate:firstDate withHandler:^(CMPedometerData *pedometerData, NSError *error) {
                    
                    if (error) {
                        NSLog(@"%s error: %@", __FUNCTION__, error);
                    } else {
                        typeof(weakSelf) strongSelf = weakSelf;
                        if (strongSelf && !error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] defaultContext];
                                DMStep *step = [DMStep stepByDate:[NSDate date] inContext:managedObjectContext];
                                if (!step) {
                                    step = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMStep class])
                                                                         inManagedObjectContext:managedObjectContext];
                                    [step setCurrentDate:[NSDate date]];
                                }
                                step.numberOfSteps = pedometerData.numberOfSteps;
                                [managedObjectContext save:nil];
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:SNStepManagerValueChanged object:nil];
                                
                                
                                if (completionHandler) {
                                    completionHandler();
                                }
                            });
                        }
                    }
                    
                    
                }];
                
            }
        } else {
            if ([CMStepCounter isStepCountingAvailable]) {
                CMStepCounter *stepCounter = [CMStepCounter new];
                [stepCounter queryStepCountStartingFrom:firstDate
                                                     to:lastDate
                                                toQueue:queue
                                            withHandler:^(NSInteger numberOfSteps, NSError *error) {
                                                typeof(weakSelf) strongSelf = weakSelf;
                                                if (strongSelf && !error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] defaultContext];
                                                        DMStep *step = [DMStep stepByDate:[NSDate date] inContext:managedObjectContext];
                                                        if (!step) {
                                                            step = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMStep class])
                                                                                                 inManagedObjectContext:managedObjectContext];
                                                            [step setCurrentDate:[NSDate date]];
                                                        }
                                                        step.numberOfSteps = @(numberOfSteps);
                                                        [managedObjectContext save:nil];
                                                        
                                                        [[NSNotificationCenter defaultCenter] postNotificationName:SNStepManagerValueChanged object:nil];
                                                        
                                                        
                                                        if (completionHandler) {
                                                            completionHandler();
                                                        }
                                                    });
                                                }
                                            }];
            }
        }
        
        
    }
    
}


- (void)loadMotionHistory {

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = -7;
    NSDate *startDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    
    dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:startDate];
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    startDate = [calendar dateFromComponents:dateComponents];
    
    dateComponents = [NSDateComponents new];
    NSDate *stepDate = startDate;
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    while ([self compareDate:stepDate withDate:[NSDate date]] == NSOrderedAscending) {
        dateComponents.day = 1;
        NSDate *nextDate = [calendar dateByAddingComponents:dateComponents toDate:stepDate options:0];
        
//        NSLog(@"%s NEXT DATE: %@", __FUNCTION__, nextDate);
        
        DMStep *step = [DMStep stepByDate:stepDate inContext:self.managedObjectContext];
        if (!step) {
            step = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMStep class]) inManagedObjectContext:self.managedObjectContext];
            //            NSLog(@"new record : %@", stepDate);
        }
        
        [step setCurrentDate:stepDate];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([CMPedometer isStepCountingAvailable]) {
                [self.pedometr queryPedometerDataFromDate:stepDate toDate:nextDate withHandler:^(CMPedometerData *pedometerData, NSError *error) {
                    step.numberOfSteps = pedometerData.numberOfSteps;
                }];
            }
        } else {
            if ([CMStepCounter isStepCountingAvailable]) {
                CMStepCounter *stepCounter = [CMStepCounter new];
                [stepCounter queryStepCountStartingFrom:stepDate to:nextDate toQueue:queue withHandler:^(NSInteger numberOfSteps, NSError *error) {
                    step.numberOfSteps = @(numberOfSteps);
                }];
            }
        }
        
        stepDate = nextDate;
        
    }
    
    [self.managedObjectContext save:nil];
}

- (NSComparisonResult)compareDate:(NSDate *)firstDate withDate:(NSDate *)secondDate {
    
    NSDate *date1 = firstDate;
    NSDate *date2 = secondDate;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date2 interval:NULL forDate:date2];
    return [date1 compare:date2];
    
}

@end
