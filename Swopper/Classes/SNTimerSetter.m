

#import "SNTimerSetter.h"

@interface SNTimerSetter () <UIGestureRecognizerDelegate>
{
    CGFloat _lineWidth, _minSide;
    CGPoint _center;
    CGRect _workingRect;
    NSString *_currentText;
    
    NSArray *_arrayOfValues;
    NSInteger _currentValueIndex;
    NSInteger _maxValueIndex;
    
    UIColor *_arrowColor;
    
    CGPoint _previousPoint;
}
@end

@implementation SNTimerSetter

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor clearColor];
    _ringColor = [UIColor orangeColor];
    _textFont = [UIFont fontWithName:@"Arial" size:75.0f];
    _textColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    _subTextFont = [UIFont fontWithName:@"Arial" size:26.0];
    
    _lineWidth = 3.0f;
    _minSide = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    _center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    _workingRect = CGRectMake(_center.x - _minSide/2, _center.y - _minSide/2, _minSide, _minSide);
    
    _arrayOfValues = [NSArray arrayWithObjects:@(5),@(15),@(30), nil];
    _currentValueIndex = 0;
    _maxValueIndex = _arrayOfValues.count - 1;
    [self changeText];
    
    _arrowColor = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:1.0];
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandler:)];
//    tapGesture.delegate = self;
//    [self addGestureRecognizer:tapGesture];
    
    [self setUserInteractionEnabled:YES];
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandler:)];
    panGesture.delegate = self;
    [self addGestureRecognizer:panGesture];
}

#pragma mark - Private Methods
- (void)drawRect:(CGRect)rect
{
    // drawing main ring
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextSetLineWidth(context, _lineWidth);
    
    CGRect drawingRect = CGRectInset(_workingRect, _lineWidth, _lineWidth);
    UIBezierPath *bkgPath = [UIBezierPath bezierPathWithOvalInRect:drawingRect];
    CGContextAddPath(context, bkgPath.CGPath);
    CGContextSetStrokeColorWithColor(context, _ringColor.CGColor);
    CGContextStrokePath(context);
    
    // drawing text
    NSString *text = _currentText;
    NSDictionary *attributesDictionary = @{NSFontAttributeName:_textFont, NSForegroundColorAttributeName:_textColor};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGSize size = [text sizeWithAttributes:attributesDictionary];
    
    // size modifier is added because of using custom font
    // the last one is calculated wrongly
    [attributedString drawInRect:CGRectMake(_center.x - size.width/2, _center.y - size.height*2/3 + 12, size.width, size.height)];
    
    text = LMLocalizedString(@"Minuten", nil);
    attributesDictionary = @{NSFontAttributeName:_subTextFont, NSForegroundColorAttributeName:_textColor};
    attributedString = [[NSAttributedString alloc] initWithString:text attributes:attributesDictionary];
    size = [text sizeWithAttributes:attributesDictionary];
    
    [attributedString drawInRect:CGRectMake(_center.x - size.width/2, _center.y + size.height/3 + 8, size.width, size.height)];
    
    // drawing arrows
//    
//    // top arrow
//    UIBezierPath *rectanglePath = [UIBezierPath bezierPath];
//    CGSize rectangleSize = CGSizeMake(22.5f, 8.0f);
//    [rectanglePath moveToPoint:CGPointMake(_center.x, _center.y - _minSide*3/8)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x + rectangleSize.width/2, _center.y - _minSide*3/8 + rectangleSize.height)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x - rectangleSize.width/2, _center.y - _minSide*3/8 + rectangleSize.height)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x, _center.y - _minSide*3/8)];
//    
//    CGContextAddPath(context, rectanglePath.CGPath);
//    CGContextSetFillColorWithColor(context, _arrowColor.CGColor);
//    CGContextDrawPath(context, kCGPathFill);
//    
//    // bottom arrow
//    rectanglePath = [UIBezierPath bezierPath];
//    [rectanglePath moveToPoint:CGPointMake(_center.x, _center.y + _minSide*3/8)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x + rectangleSize.width/2, _center.y + _minSide*3/8 - rectangleSize.height)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x - rectangleSize.width/2, _center.y + _minSide*3/8 - rectangleSize.height)];
//    [rectanglePath addLineToPoint:CGPointMake(_center.x, _center.y + _minSide*3/8)];
//    
//    CGContextAddPath(context, rectanglePath.CGPath);
//    CGContextSetFillColorWithColor(context, _arrowColor.CGColor);
//    CGContextDrawPath(context, kCGPathFill);
}

- (void)increaseCurrentValue:(BOOL)increase
{
    if (increase) {
        if (_currentValueIndex != _maxValueIndex) {
            _currentValueIndex++;
        } else {
            _currentValueIndex = 0;
        }
        [self changeText];
    } else {
        if (_currentValueIndex != 0) {
            _currentValueIndex--;
        } else {
            _currentValueIndex = _maxValueIndex;
        }
        [self changeText];
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)changeText
{
    NSNumber *number = [_arrayOfValues objectAtIndex:_currentValueIndex];
    _currentText = [NSString stringWithFormat:@"%@",number];
    [self setNeedsDisplay];
}

- (void)gestureHandler:(UITapGestureRecognizer *)sender
{
    CGPoint touchPoint = [sender locationInView:self];
    if (touchPoint.y <= _center.y) {
        [self increaseCurrentValue:YES];
    } else {
        [self increaseCurrentValue:NO];
    }
}

- (void)panHandler:(UIPanGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _previousPoint = point;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_previousPoint.x < point.x) {
                [self increaseCurrentValue:NO];
            } else {
                [self increaseCurrentValue:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Property Accessors
- (void)setRingColor:(UIColor *)ringColor
{
    _ringColor = ringColor;
    [self setNeedsDisplay];
}

- (void)setTextFont:(UIFont *)textFont
{
    _textFont = textFont;
    [self setNeedsDisplay];
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    [self setNeedsDisplay];
}

- (NSInteger)value
{
    NSNumber *number = [_arrayOfValues objectAtIndex:_currentValueIndex];
    return [number integerValue]*60;
}

- (NSInteger)maxValue
{
    NSInteger max = 0.00001;
    for (NSNumber *value in _arrayOfValues) {
        if ([value integerValue] > max) {
            max = [value integerValue];
        }
    }
    return max*60;
}

- (NSInteger)index
{
    return _currentValueIndex;
}

- (NSInteger)elementsCount
{
    return _arrayOfValues.count;
}

@end
