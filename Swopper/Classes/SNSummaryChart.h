

#import <UIKit/UIKit.h>

@protocol SNSummaryChartProtocol;
@protocol SNSummaryChartDelegate;
@protocol SNSummaryChartDatasource;

@interface SNSummaryChart : UIView

@property (strong, nonatomic) UIColor *colorSwoops;
@property (strong, nonatomic) UIColor *colorSteps;
@property (strong, nonatomic) UIColor *colorCalories;
@property (strong, nonatomic) UIColor *colorDayCircle;
@property (strong, nonatomic) UIColor *colorText;
@property (strong, nonatomic) UIColor *colorCurrentDayText;

@property (strong, nonatomic) NSString *annotationSwoops;
@property (strong, nonatomic) NSString *annotationSteps;
@property (strong, nonatomic) NSString *annotationCalories;

@property (strong, nonatomic) UIFont *textFont;
@property (strong, nonatomic) UIFont *annotationFont;

@property (strong, nonatomic) NSArray *week;
@property (strong, nonatomic) NSDate *currentDate;

@property (weak, nonatomic) IBOutlet id <SNSummaryChartDelegate> delegate;
@property (weak, nonatomic) IBOutlet id <SNSummaryChartDatasource> dataSource;


@end

@protocol SNSummaryChartProtocol <NSObject>

@required
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) NSInteger steps;
@property (assign, nonatomic) NSInteger swoops;
@property (assign, nonatomic) NSInteger calories;
@property (assign, nonatomic) CGFloat swoppsPercentage;

@end

@protocol SNSummaryChartDelegate <NSObject>
@optional
- (void)summaryChartView:(SNSummaryChart *)summaryChartView didChangeWeek:(NSArray *)week;
- (void)summaryChartView:(SNSummaryChart *)summaryChartView changedCurrentDate:(NSDate *)date;
@end

@protocol SNSummaryChartDatasource <NSObject>
@required
- (NSArray *)summaryChartView:(SNSummaryChart *)summaryChartView willChangeWeekForward:(BOOL)forward;

@end



