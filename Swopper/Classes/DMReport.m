

#import "DMReport.h"
#import "DMSession.h"


@implementation DMReport

@dynamic year;
@dynamic month;
@dynamic day;
@dynamic weekOfYear;
@dynamic interval;
@dynamic date;
@dynamic sessions;

@end
