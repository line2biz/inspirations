

#import "SNGenderController.h"

#import "SNAppearance.h"
#import "DMUser.h"
#import "UIFont+Customization.h"
#import "SNWeightController.h"
#import "SNOrangeButton.h"

@interface SNGenderController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UIButton *btnWoman;
@property (weak, nonatomic) IBOutlet UIButton *btnMan;
@property (weak, nonatomic) IBOutlet UILabel *lblWoman;
@property (weak, nonatomic) IBOutlet UILabel *lblMan;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@property (assign, nonatomic) BOOL genderWasChosen;

@end

@implementation SNGenderController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    [SNAppearance customizeViewController:self withTitleImage:YES];
    [self configureFonts];
    self.navigationItem.rightBarButtonItem = nil;
    
    if (self.wizard) {
        [self.navigationItem hidesBackButton];
    }
    
    
//    if (![[DMUser defaultUser] isValid]) {
//        self.navigationItem.rightBarButtonItem = nil;
////        [self.navigationItem setHidesBackButton:YES];
//        [self.buttonNext setHidden:NO];
//    } else {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Save"] style:UIBarButtonItemStyleBordered target:self action:@selector(didTapSaveButton:)];
//        [self.buttonNext setHidden:YES];
//    }
    
    [self.nextButton setHidden:YES];
    
    if ([[DMUser defaultUser] sex] != DMSexTypeNone) {
        self.genderWasChosen = YES;
        switch ([[DMUser defaultUser] sex]) {
            case DMSexTypeMan:
            {
                [self chooseGender:self.btnMan];
            }
                break;
            case DMSexTypeWomen:
            {
                [self chooseGender:self.btnWoman];
            }
                break;
                
            default:
                break;
        }
        [self.nextButton setHidden:NO];
    }
    if (self.genderWasChosen) {
        [self controllerStart];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.genderWasChosen) {
        if (!_loadIdx) {
            _loadIdx++;
        } else {
            [self controllerStart];
        }
        [self startCurveAnimation];
    } else {
        _startCurveAlpha = self.imageViewCurveDown.alpha;
        self.imageViewCurveDown.alpha = 0.0;
        self.imageViewCurveUp.alpha = 0.0;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGFloat widht = CGRectGetWidth(self.labelTitle.frame);
    CGSize size = CGSizeMake(widht, 100);
    
    NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
    CGRect frame = [self.labelTitle.attributedText boundingRectWithSize:size options:options context:nil];
    NSLog(@"%s TITLE %@", __FUNCTION__, NSStringFromCGRect(frame));
    
    frame = [self.labelSubtitle.attributedText boundingRectWithSize:size options:options context:nil];
    NSLog(@"%s SUBTITLE %@", __FUNCTION__, NSStringFromCGRect(frame));
    
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.labelTitle.text = LMLocalizedString(@"Kalorien-Berechnung", nil);
    self.labelSubtitle.text = LMLocalizedString(@"Geschlecht", nil);
    
    self.lblWoman.text = LMLocalizedString(@"Weiblich", nil);
    self.lblMan.text = LMLocalizedString(@"Männlich", nil);
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
    
    self.lblWoman.font = [UIFont franklinGothicStdBookCondesedWithSize:self.lblWoman.font.pointSize];
    self.lblMan.font = [UIFont franklinGothicStdBookCondesedWithSize:self.lblMan.font.pointSize];
}

- (void)saveChanges
{
    if (self.genderWasChosen) {
        if ([self.btnWoman isSelected]) {
            [[DMUser defaultUser] setSex:DMSexTypeWomen];
        } else {
            [[DMUser defaultUser] setSex:DMSexTypeMan];
        }
        [[DMUser defaultUser] save];
    }
}

- (void)chooseGender:(UIButton *)sender
{
    if (!self.genderWasChosen) {
        self.genderWasChosen = YES;
        [self startCurveAnimation];
    }
    if (sender.tag == 0) {
        [sender setSelected:YES];
        [self.btnMan setSelected:NO];
        self.lblMan.textColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
        self.lblWoman.textColor = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0];
    } else {
        [sender setSelected:YES];
        [self.btnWoman setSelected:NO];
        self.lblMan.textColor = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:1.0];
        self.lblWoman.textColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    }
    
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapSaveButton:(id)sender
{
    if (self.genderWasChosen) {
        [self saveChanges];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)didTapNextButton:(id)sender
{
    [self saveChanges];
    if (self.wizard) {
        SNWeightController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNWeightController class])];
        controller.wizard = YES;
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (IBAction)didTapGenderButton:(UIButton *)sender
{
    if (![sender isSelected]) {
        [self chooseGender:sender];
    }
}

#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}




@end
