

#import "SNSliderControl.h"

#define DEGREES_TO_RADIANS(angle) (((angle)* M_PI) / 180.0 )

@interface SNSliderControl() <UIGestureRecognizerDelegate>
{
    CGFloat _availableWidth;
    CGRect _workingRect;
    
    CGFloat _progress;
    
    UIColor *_bkgColor, *_progressColor, *_buttonColor;
}

@end

@implementation SNSliderControl

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    _maximumValue = 3.0;
    _minimumValue = 0.0;
    _currentValue = 0.0;
    _alpha = 1.0;
    _progress = _currentValue / _maximumValue;
    
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat spacing = width / 16;
    _availableWidth = width - spacing * 2;
    _workingRect = CGRectMake(spacing, CGRectGetHeight(self.bounds) / 3, _availableWidth, CGRectGetHeight(self.bounds) / 3);
    
    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
    
    _bkgColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:_alpha];
    _progressColor = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:_alpha];
    _buttonColor = [UIColor colorWithWhite:1.0 alpha:_alpha];
    
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandler:)];
    [gestureRecognizer setMinimumNumberOfTouches:1];
    gestureRecognizer.delegate = self;
    [self addGestureRecognizer:gestureRecognizer];
    
    [self setNeedsDisplay];
}

- (void)reinitColors
{
    _bkgColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:_alpha];
    _progressColor = [UIColor colorWithRed:240/255.0 green:149/255.0 blue:36/255.0 alpha:_alpha];
    _buttonColor = [UIColor colorWithWhite:1.0 alpha:_alpha];
}


#pragma mark - Drawing
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat height = CGRectGetHeight(_workingRect);
    CGFloat width = _availableWidth;
    
    // control background
    UIBezierPath *backgroundPath = [UIBezierPath bezierPathWithRect:self.bounds];
    CGContextAddPath(context, backgroundPath.CGPath);
    CGContextSetLineWidth(context, 1.5f);
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextSetStrokeColorWithColor(context, _bkgColor.CGColor);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    // bar background and progress
    UIColor *drawColor;
    CGFloat multiplier;
    for (NSInteger i = 0; i < 2; i++) {
        if (!i) {
            multiplier = 1.0f;
            drawColor = _bkgColor;
        } else {
            multiplier = _progress;
            drawColor = _progressColor;
        }
        UIBezierPath *bkgPath = [UIBezierPath bezierPath];
        CGPoint arcPoint = CGPointMake(_workingRect.origin.x + height/2, _workingRect.origin.y + height/2);
        [bkgPath moveToPoint:arcPoint];
        [bkgPath addArcWithCenter:arcPoint
                           radius:height/2
                       startAngle:DEGREES_TO_RADIANS(90)
                         endAngle:DEGREES_TO_RADIANS(-90)
                        clockwise:YES];
        
        CGPoint point = CGPointMake(_workingRect.origin.x + height/2, _workingRect.origin.y);
        [bkgPath moveToPoint:point];
        point = CGPointMake(_workingRect.origin.x + (width - height/2)* multiplier, _workingRect.origin.y);
        [bkgPath addLineToPoint:point];
        [bkgPath addLineToPoint:CGPointMake(_workingRect.origin.x + (width - height/2)* multiplier, _workingRect.origin.y + height)];
        [bkgPath addLineToPoint:CGPointMake(_workingRect.origin.x + height/2, _workingRect.origin.y + height)];
        [bkgPath addLineToPoint:CGPointMake(_workingRect.origin.x + height/2, _workingRect.origin.y)];
        
        arcPoint = CGPointMake(_workingRect.origin.x + (width - height/2)* multiplier, _workingRect.origin.y + height/2);
        [bkgPath moveToPoint:arcPoint];
        [bkgPath addArcWithCenter:arcPoint
                           radius:height/2
                       startAngle:DEGREES_TO_RADIANS(-90)
                         endAngle:DEGREES_TO_RADIANS(90)
                        clockwise:YES];
        
        CGContextAddPath(context, bkgPath.CGPath);
        CGContextSetLineWidth(context, 0.5f);
        CGContextSetFillColorWithColor(context, drawColor.CGColor);
        CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
        CGContextDrawPath(context, kCGPathFill);
    }
    
    // button
    CGPoint btnCenter = CGPointMake(_workingRect.origin.x + _availableWidth * _progress, _workingRect.origin.y + height/2);
    UIBezierPath *btnPath = [UIBezierPath bezierPathWithArcCenter:btnCenter
                                                           radius:CGRectGetHeight(self.bounds)/2 - CGRectGetHeight(self.bounds)/10
                                                       startAngle:DEGREES_TO_RADIANS(-90)
                                                         endAngle:DEGREES_TO_RADIANS(360)
                                                        clockwise:YES];
    
    CGContextAddPath(context, btnPath.CGPath);
    CGContextSetLineWidth(context, 0.5f);
    CGContextSetFillColorWithColor(context, _buttonColor.CGColor);
    CGContextSetStrokeColorWithColor(context, _bkgColor.CGColor);
    CGContextDrawPath(context, kCGPathFillStroke);
}

#pragma mark - Gesture Recognizer Handler
- (void)gestureHandler:(UIPanGestureRecognizer *)sender
{
    CGPoint touchPoint = [sender locationInView:self];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self.delegate slider:self moveStarted:YES];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            if (!(touchPoint.x < _workingRect.origin.x || touchPoint.x > _availableWidth + _workingRect.origin.x)) {
                _progress = (touchPoint.x - _workingRect.origin.x) / _availableWidth;
                _currentValue = _progress * _maximumValue;
                [self setNeedsDisplay];
                [self sendActionsForControlEvents:UIControlEventValueChanged];
            }
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (!(touchPoint.x < _workingRect.origin.x || touchPoint.x > _availableWidth + _workingRect.origin.x)) {
                _progress = (touchPoint.x - _workingRect.origin.x) / _availableWidth;
                _currentValue = _progress * _maximumValue;
                [self setNeedsDisplay];
                [self sendActionsForControlEvents:UIControlEventValueChanged];
            }
            [self.delegate slider:self moveStarted:NO];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Property Accessors
- (void)setMinimumValue:(float)minimumValue
{
    if (_minimumValue != minimumValue) {
        _minimumValue = minimumValue;
        [self setNeedsDisplay];
    }
}

- (void)setMaximumValue:(float)maximumValue
{
    if (_maximumValue != maximumValue) {
        _maximumValue = maximumValue;
        [self setNeedsDisplay];
    }
}

- (void)setCurrentValue:(float)currentValue
{
    if (_currentValue != currentValue) {
        if (currentValue >= _maximumValue) {
            currentValue = _maximumValue;
        }
        if (currentValue <= _minimumValue) {
            currentValue = _minimumValue;
        }
        _currentValue = currentValue;
        _progress = _currentValue / _maximumValue;
        [self setNeedsDisplay];
    }
}

- (void)setAlpha:(CGFloat)alpha
{
    _alpha = alpha;
    [self reinitColors];
    [self setNeedsDisplay];
}

@end
