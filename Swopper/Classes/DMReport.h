
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMSession;

@interface DMReport : NSManagedObject

@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) NSNumber * weekOfYear;
@property (nonatomic, retain) NSNumber * interval;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSSet *sessions;
@end

@interface DMReport (CoreDataGeneratedAccessors)

- (void)addSessionsObject:(DMSession *)value;
- (void)removeSessionsObject:(DMSession *)value;
- (void)addSessions:(NSSet *)values;
- (void)removeSessions:(NSSet *)values;

@end
