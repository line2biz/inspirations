
#import "SNLastTrainingsTableCell.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@implementation SNLastTrainingsTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    
    self.labelTitle.text = LMLocalizedString(@"Letzte Mitmach-Trainings", nil);
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor customOrangeColor];
    
    [self configureView];
}


#pragma mark - Private Methods
- (void)configureView
{
    [SNAppearance findAndMarkWord:LMLocalizedString(@"Letzte", nil)
                          inLabel:self.labelTitle
                      withNewFont:[UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize]];
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
