

#import "DMSession.h"
#import "DMMotion.h"
#import "DMReport.h"


@implementation DMSession

@dynamic cpmFactor;
@dynamic day;
@dynamic endTime;
@dynamic interval;
@dynamic maxValue;
@dynamic month;
@dynamic sessionState;
@dynamic sid;
@dynamic startTime;
@dynamic targetTimePeriod;
@dynamic training;
@dynamic weekOfYear;
@dynamic year;
@dynamic sessionDuration;
@dynamic motions;
@dynamic report;

@end
