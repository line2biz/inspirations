
#import <Foundation/Foundation.h>

@interface SNAppearance : NSObject

+ (void)customizeAppearance;
+ (void)customizeViewController:(UIViewController *)viewController;
+ (void)customizeViewController:(UIViewController *)viewController withTitleImage:(BOOL)titleImage;
+ (void)findAndMarkWord:(NSString *)word inLabel:(UILabel *)label withNewFont:(UIFont *)font;
+ (void)findAndMarkWord:(NSString *)word textBenderView:(UIView *)view withNewFont:(UIFont *)font newColor:(UIColor *)newColor;
+ (void)findAndMarkWord:(NSString *)word inButton:(UIButton *)button forState:(UIControlState)state withNewFont:(UIFont *)font;

/// Find in attributed text needed word and mark it at needed indexes;
/// if aIndexes are nil then all found words would be marked
+ (NSAttributedString *)findAndMarkWord:(NSString *)sWord
                     inAttributedString:(NSAttributedString *)asTarget
                              withColor:(UIColor *)newColor
                                   font:(UIFont *)newFont
                              atIndexes:(NSArray *)aIndexes;
+ (NSString *)timeStringFromSeconds:(NSInteger)seconds;
+ (NSString *)textWithThousendsFormat:(NSInteger)amount;

@end
