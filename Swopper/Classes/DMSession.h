

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMMotion, DMReport;

@interface DMSession : NSManagedObject

@property (nonatomic, retain) NSNumber * cpmFactor;
@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, retain) NSNumber * interval;
@property (nonatomic, retain) NSNumber * maxValue;
@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * sessionState;
@property (nonatomic, retain) NSNumber * sid;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSNumber * targetTimePeriod;
@property (nonatomic, retain) NSNumber * training;
@property (nonatomic, retain) NSNumber * weekOfYear;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * sessionDuration;
@property (nonatomic, retain) NSSet *motions;
@property (nonatomic, retain) DMReport *report;
@end

@interface DMSession (CoreDataGeneratedAccessors)

- (void)addMotionsObject:(DMMotion *)value;
- (void)removeMotionsObject:(DMMotion *)value;
- (void)addMotions:(NSSet *)values;
- (void)removeMotions:(NSSet *)values;

@end
