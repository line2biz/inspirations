

#import "SNRightDetailCell.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

#define kMARGIN 15.f

@interface SNRightDetailCell ()
{
    CALayer *_bottomBorderLayer;
    CALayer *_accessoryBottomBorderLayer;
}

@end

@implementation SNRightDetailCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
        _detailLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
        _titleLabel.textColor = [UIColor customDarkGrayGolor];
        _detailLabel.textColor = [UIColor customDarkGrayGolor];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    _detailLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.detailLabel.font.pointSize];
    _titleLabel.textColor = [UIColor customDarkGrayGolor];
    _detailLabel.textColor = [UIColor customDarkGrayGolor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.separatorType != SNCellSeparatorTypeNone) {
        if (!_bottomBorderLayer) {
            _bottomBorderLayer = [CALayer new];
            [self.layer addSublayer:_bottomBorderLayer];
            
        }
        
        CGFloat red;
        CGFloat green;
        CGFloat blue;
        CGFloat alpha;
        [[UIColor lightGrayColor] getRed:&red green:&green blue:&blue alpha:&alpha];
        UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:0.4];
        _bottomBorderLayer.backgroundColor = [color CGColor];
        
        CGFloat x = 0;
        if (self.separatorType == SNCellSeparatorTypeShort) {
            x = 15;
        }
        
        CGFloat height = 1;
        _bottomBorderLayer.frame = CGRectMake(x, CGRectGetHeight(self.contentView.bounds) - height, CGRectGetWidth(self.frame), height);
        
    } else {
        [_bottomBorderLayer removeFromSuperlayer];
    }
    
    
}

- (CGRect)frameForLabel:(UILabel *)label withSize:(CGSize)size
{
    NSDictionary *attrubutes = @{
                                 NSFontAttributeName : label.font,
                                 NSForegroundColorAttributeName : label.textColor
                                 };
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:label.text attributes:attrubutes];
    
    NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
    
    CGRect frame = [str boundingRectWithSize:size options:options context:NULL];
    
    return frame;
}

#pragma mark - Property Accessors

- (void)setTitleText:(NSString *)titleText
{
    if (titleText) {
        _titleText = titleText;
        _titleLabel.text = _titleText;
    }
}

- (void)setDetailText:(NSString *)detailText
{
    if (detailText) {
        _detailText = detailText;
        _detailLabel.text = _detailText;
    }
}


@end
