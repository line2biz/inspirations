

#import "SNAgeController.h"

#import "SNAppearance.h"
#import "DMUser.h"
#import "SNDatePicker.h"
#import "UIFont+Customization.h"
#import "SNOrangeButton.h"

#import "SNProfileController.h"

@interface SNAgeController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet SNDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@end

@implementation SNAgeController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    [SNAppearance customizeViewController:self withTitleImage:YES];
    self.navigationItem.rightBarButtonItem = nil;
    
//    if (![[DMUser defaultUser] isValid]) {
//        self.navigationItem.rightBarButtonItem = nil;
//        [self.navigationItem setHidesBackButton:YES];
//        
//        [self.buttonNext setHidden:NO];
//    } else {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Save"] style:UIBarButtonItemStyleBordered target:self action:@selector(didTapSaveButton:)];
//        [self.buttonNext setHidden:YES];
//    }
    
    [self configureFonts];
    NSLocale *de = [SNAppDelegate currentLocale];
    NSCalendar *cal = [SNAppDelegate currentCalendar];
    [cal setLocale:de];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d MM yyyy"];
    NSString *minDate = @"01 01 1920";
    [self.datePicker setMinimumDate:[dateFormat dateFromString:minDate]];
    if ([[DMUser defaultUser] birthDate]) {
        [self.datePicker setDate:[[DMUser defaultUser] birthDate]];
    } else {
        [self.datePicker setDate:[NSDate dateWithTimeIntervalSince1970:0]];
    }
    [self.datePicker setMaximumDate:[NSDate date]];
    
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGFloat widht = CGRectGetWidth(self.labelTitle.frame);
    CGSize size = CGSizeMake(widht, 100);
    
    NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
    CGRect frame = [self.labelTitle.attributedText boundingRectWithSize:size options:options context:nil];
    NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(frame));
    
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.labelTitle.text = LMLocalizedString(@"Kalorien-Berechnung", nil);
    self.labelSubtitle.text = LMLocalizedString(@"Geburtsdatum", nil);
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
}

- (void)saveChanges
{
    NSInteger age = [[NSDate date] timeIntervalSinceDate:self.datePicker.date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    age = [[dateFormat stringFromDate:[NSDate date]] integerValue] - [[dateFormat stringFromDate:self.datePicker.date] integerValue];
    [dateFormat setDateFormat:@"MM"];
    
    if ([[dateFormat stringFromDate:[NSDate date]] integerValue] - [[dateFormat stringFromDate:self.datePicker.date] integerValue] < 0) {
        age --;
    }
    [[DMUser defaultUser] setBirthDate:self.datePicker.date];
    [[DMUser defaultUser] setAge:age];
    [[DMUser defaultUser] save];
}

#pragma mark Animation
- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapSaveButton:(id)sender
{
    [self saveChanges];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapNextButton:(id)sender
{
    [self saveChanges];
    if (self.wizard) {
//        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
        
        SNProfileController *profileController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNProfileController class])];
        profileController.showNextButton = YES;
        profileController.wizard = YES;
        [self.navigationController pushViewController:profileController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



@end
