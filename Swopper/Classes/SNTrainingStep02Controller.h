
#import <UIKit/UIKit.h>

@protocol SNTrainingStep02Protocol;

@interface SNTrainingStep02Controller : UIViewController

@property (strong, nonatomic) id <SNTrainingStep02Protocol> delegate;

@end

@protocol SNTrainingStep02Protocol <NSObject>
@required
- (void)trainingController:(SNTrainingStep02Controller *)controller finishedTraining:(BOOL)finished;

@end
