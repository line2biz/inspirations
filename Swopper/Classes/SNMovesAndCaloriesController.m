

#import "SNMovesAndCaloriesController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNAppearance.h"

const CGFloat kTitleFontSize = 21.0f;
const CGFloat kTextFontSize = 15.0f;

@interface SNMovesAndCaloriesController ()
@property (weak, nonatomic) IBOutlet UITextView *textViewMain;
@property (weak, nonatomic) IBOutlet UIView *viewCutLine;

@end

@implementation SNMovesAndCaloriesController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [SNAppearance customizeViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self controllerStart];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    [self createAttributedText];
    
    [self createTextCut];
}

- (void)createTextCut
{
    CAGradientLayer *gradLayer = [CAGradientLayer layer];
    gradLayer.frame = self.viewCutLine.bounds;
    gradLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f], nil];
    gradLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1.0f alpha:0.0f] CGColor], (id)[[UIColor colorWithWhite:1.0f alpha:0.75f] CGColor], nil];
    gradLayer.startPoint = CGPointMake(0, 0);
    gradLayer.endPoint = CGPointMake(0, 1);
    
//    [self.textViewMain.layer addSublayer:gradLayer];
    
    [self.viewCutLine.layer setBackgroundColor:[UIColor clearColor].CGColor];
    [self.viewCutLine.layer addSublayer:gradLayer];
}

- (NSString *)mainText
{
    NSMutableString *msResult = [NSMutableString new];
    [msResult appendString:[self firstTitle]];
    [msResult appendString:[self firstTextPart]];
    [msResult appendString:[self secondTitle]];
    [msResult appendString:[self secondTextPart]];
    
    return msResult;
}

- (void)createAttributedText
{
    BOOL bRightSize = NO;
    NSInteger iFontShrink = 0;
    NSAttributedString *asAnswer;
    NSString *stringID = @"string";
    NSString *attributesID = @"attributes";
    
    while (bRightSize == NO) {
        
        // create font size for shrinking step
        CGFloat fTitleFontSize = kTitleFontSize - iFontShrink;
        CGFloat fTextFontSize = kTextFontSize - iFontShrink;
        
        // create attributes for titles and usual text
        NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentLeft;
        
        NSDictionary *dictNormalAttributes = @{
                                               NSFontAttributeName : [UIFont franklinGothicStdBookCondesedWithSize:fTextFontSize],
                                               NSParagraphStyleAttributeName : paragraphStyle,
                                               NSForegroundColorAttributeName : [UIColor customDarkGrayGolor]
                                               };
        
        NSMutableAttributedString *masResult = [[NSMutableAttributedString alloc] initWithString:[self mainText]
                                                                                      attributes:dictNormalAttributes];
        
        NSMutableParagraphStyle *centerParagraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        centerParagraphStyle.alignment = NSTextAlignmentCenter;
        
        NSDictionary *dictTitleAttributes = @{
                                              NSFontAttributeName : [UIFont franklinGothicStdBookCondesedMedWithSize:fTitleFontSize],
                                              NSParagraphStyleAttributeName : centerParagraphStyle,
                                              NSForegroundColorAttributeName : [UIColor customDarkGrayGolor]
                                              };
        
        NSDictionary *dictTitle2Attributes = @{
                                              NSFontAttributeName : [UIFont franklinGothicStdBookCondesedMedWithSize:fTitleFontSize],
                                              NSParagraphStyleAttributeName : centerParagraphStyle,
                                              NSForegroundColorAttributeName : [UIColor customDarkGrayGolor]
                                              };
        
        
        // create array of text parts for better programming experience
        NSDictionary *(^StringDict)(NSString *, NSDictionary *) = ^(NSString *string, NSDictionary *attributes) {
            NSMutableDictionary *mdictResult = [NSMutableDictionary dictionaryWithCapacity:2];
            
            [mdictResult setObject:string forKey:stringID];
            [mdictResult setObject:attributes forKey:attributesID];
            
            return mdictResult;
        };
        
        NSMutableArray *maParts = [NSMutableArray new];
        [maParts addObject:StringDict([self firstTitle], dictTitleAttributes)];
        [maParts addObject:StringDict([self firstTextPart], dictNormalAttributes)];
        [maParts addObject:StringDict([self secondTitle], dictTitle2Attributes)];
        [maParts addObject:StringDict([self secondTextPart], dictNormalAttributes)];
        
        // set attributes of every part to needed parts of text
        for (NSDictionary *dict in maParts) {
            NSRange range = [[self mainText] rangeOfString:dict[stringID]];
            if (range.location != NSNotFound) {
                [masResult setAttributes:dict[attributesID] range:range];
            }
        }
        
        // calculate if text height is normal for text view
        CGSize size = CGSizeMake(CGRectGetWidth(self.textViewMain.frame) - 5, 9999.0f);
        
        
        CGFloat(^GetHeightOfText)(NSString *, CGSize, NSDictionary *) = ^(NSString *string, CGSize size, NSDictionary *dictAttributes) {
            NSStringDrawingContext *context = [NSStringDrawingContext new];
            context.minimumScaleFactor = 1.0f;
            CGRect rect = [string boundingRectWithSize:size
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:dictAttributes
                                               context:context];
            return CGRectGetHeight(rect);
        };
        
        CGFloat fAllHeight = 0;
        
        for (NSDictionary *dict in maParts) {
            fAllHeight += GetHeightOfText(dict[stringID], size, dict[attributesID]);
        }
        
        
        bRightSize = YES;
        asAnswer = [[NSAttributedString alloc] initWithAttributedString:masResult];
        
        asAnswer = [SNAppearance findAndMarkWord:LMLocalizedString(@"Leistungsumsatz", nil)
                              inAttributedString:asAnswer
                                       withColor:nil
                                            font:[UIFont franklinGothicStdBookCondesedMedWithSize:fTextFontSize]
                                       atIndexes:@[@(1)]];
        
        asAnswer = [SNAppearance findAndMarkWord:LMLocalizedString(@"Grundumsatz", nil)
                              inAttributedString:asAnswer
                                       withColor:nil
                                            font:[UIFont franklinGothicStdBookCondesedMedWithSize:fTextFontSize]
                                       atIndexes:@[@(1)]];
        
//        // choose what to do
//        if (fAllHeight <= CGRectGetHeight(self.textViewMain.frame) - 5) {
//            bRightSize = YES;
//            asAnswer = [[NSAttributedString alloc] initWithAttributedString:masResult];
//        } else {
//            iFontShrink ++;
//        }
    }

    self.textViewMain.attributedText = asAnswer;
    
    
}

#pragma mark - Text Parts
- (NSString *)firstTitle
{
    return LMLocalizedString(@"Moves (SIB)",nil);
}

- (NSString *)firstTextPart
{
    return LMLocalizedString(@"\n\nDie Einheit „Moves“ steht für spontane, intuitive Bewegungseinheiten. Im Alltag sind es nämlich nicht die großen Sprünge, Läufe oder Distanzen, die über die Gesundheit entscheiden. Wesentlich ist die „Alltagsmotorik“ – also eine Vielzahl kleiner, natürlicher Bewegungen, die sich positiv auf Muskeln, Sehnen, Atmung, Sauerstoffversorgung, Gehirnaktivität und Stoffwechsel auswirken.\n\n", nil);
}

- (NSString *)secondTitle
{
    return LMLocalizedString(@"Grundumsatz vs. Leistungsumsatz", nil);
}

- (NSString *)secondTextPart
{
    return LMLocalizedString(@"\n\nDer Grundumsatz ist diejenige Energiemenge, die der Körper pro Tag bei völliger Ruhe während eines Tages zur Aufrechterhaltung seiner Funktion benötigt. Der Leistungsumsatz ist die Energiemenge, die der Körper innerhalb eines Tages benötigt, um Arbeit verrichten zu können. Als Leistungsumsatz wird dabei die Energie bezeichnet, die über den Grundumsatz hinausgeht.\n\nDie Kalorienangaben (kcal) in dieser App beziehen ￼sich immer auf den Leistungsumsatz.", nil);
}



@end
