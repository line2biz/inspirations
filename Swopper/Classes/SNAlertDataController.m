
#import "SNAlertDataController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNProfileController.h"

#import "SNGenderController.h"

@interface SNAlertDataController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelAlert;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation SNAlertDataController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGFloat widht = CGRectGetWidth(self.labelAlert.frame);
    CGSize size = CGSizeMake(widht, 100);
    
    NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
    CGRect frame = [self.labelAlert.attributedText boundingRectWithSize:size options:options context:nil];
    NSLog(@"%s TITLE %@", __FUNCTION__, NSStringFromCGRect(frame));
    
    frame = [self.labelMessage.attributedText boundingRectWithSize:size options:options context:nil];
    NSLog(@"%s SUBTITLE %@", __FUNCTION__, NSStringFromCGRect(frame));
    
}


#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    // Do any additional setup after loading the view.
    [SNAppearance customizeViewController:self];
    [self configureFonts];
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    self.labelAlert.text = LMLocalizedString(@"Vorbereitung", nil);
    self.labelMessage.text = LMLocalizedString(@"Damit es losgehen kann, geben Sie bitte noch Ihre Daten ein, um Ihren Energieverbrauch zu ermitteln.", nil);
    
    self.labelMessage.textColor = [UIColor customDarkGrayGolor];
    self.labelMessage.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelMessage.font.pointSize];
    
    self.labelAlert.textColor = [UIColor customDarkGrayGolor];
    self.labelAlert.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelAlert.font.pointSize];
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapNextButton:(UIButton *)sender
{
//    SNProfileController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNProfileController class])];
//    viewController.showNextButton = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
    SNGenderController *genderController = [[self storyboard] instantiateViewControllerWithIdentifier:NSStringFromClass([SNGenderController class])];
    genderController.wizard = YES;
    [self.navigationController pushViewController:genderController animated:YES];
}



@end
