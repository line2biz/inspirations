

#import "SNReportController.h"

#import "DMCalorie.h"
#import "DMReport+Category.h"
#import "DMSession+Category.h"
#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "AnimatedLabel.h"
#import "SNSessionCell.h"


@interface SNReportController () <NSFetchedResultsControllerDelegate>
{
    NSInteger _loadIdx;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@property (weak, nonatomic) IBOutlet AnimatedLabel *labelCalories;
@property (weak, nonatomic) IBOutlet AnimatedLabel *labelSwopps;
@property (weak, nonatomic) IBOutlet UILabel *labelCalAnn;
@property (weak, nonatomic) IBOutlet UILabel *labelSwopAnn;

@end

@implementation SNReportController


#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = LMLocalizedString(@"Ergebnis", nil);
    
    _loadIdx = 0;
    [self startOfController];
    
    self.labelCalAnn.text = LMLocalizedString(@"kcal pro Tag", nil);
    self.labelSwopAnn.text = LMLocalizedString(@"Moves pro Tag", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self startOfController];
    }
}


#pragma mark - Controller Methods
- (void)startOfController
{
    [self configureFonts];
    [self configureView];
}

- (void)configureFonts
{
    NSArray *arrayOfLabels = [[NSArray alloc] initWithObjects:_labelTitle,_labelDescription,_labelCalories,_labelSwopps,_labelCalAnn,_labelSwopAnn, nil];
    for (UILabel *label in arrayOfLabels) {
        if (label == self.labelTitle) {
            label.font = [UIFont franklinGothicStdBookCondesedMedWithSize:label.font.pointSize];
        } else {
            label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
        }
        if (label == self.labelTitle || label == self.labelDescription) {
            label.textColor = [UIColor customDarkGrayGolor];
        } else {
            label.textColor = [UIColor whiteColor];
        }
    }
}

- (void)configureView
{
    // parse file
    NSString* fileRoot = [[SNLocalizationManager sharedManager] pathForResource:@"Callories" ofType:@"tsv"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    for (NSInteger i=0; i<allLinedStrings.count; i++) {
        NSString *line = [allLinedStrings objectAtIndex:i];
        NSArray *arrayOfSubstrings = [line componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\t"]];
        if ([arrayOfSubstrings count] >= 4) {
            NSDictionary *dict = @{@"title":arrayOfSubstrings[0], @"callories":arrayOfSubstrings[1], @"result":arrayOfSubstrings[2], @"desc":arrayOfSubstrings[3]};
            [mArrayOfDicts addObject:dict];
        }
    }
    
    __block NSInteger indexOfProduct = 0;
    NSInteger caloriesToday = 0, swoppsToday = 0;
    
    for (DMSession *session in self.report.sessions) {
        swoppsToday += [session countsForSession] / session.sessionDuration.floatValue * _report.interval.floatValue;
        caloriesToday += [session caloriesPerSession] / session.sessionDuration.floatValue * _report.interval.floatValue;
    }
    swoppsToday = swoppsToday / self.report.sessions.count;
    caloriesToday = caloriesToday / self.report.sessions.count;
    
    
    [mArrayOfDicts enumerateObjectsUsingBlock:^(NSDictionary *product, NSUInteger idx, BOOL *stop) {
        NSInteger productCallories = [product[@"callories"] integerValue];
        if (caloriesToday >= productCallories) {
            indexOfProduct = idx;
        }
    }];
    
    NSDictionary *productDict = [mArrayOfDicts objectAtIndex:indexOfProduct];
    NSString *productDescription = productDict[@"desc"];
    BOOL result = [productDict[@"result"] isEqualToString:@"Positiv"];
    
    // text for title
    NSArray *arrayOfTitles = (result) ? [[NSArray alloc] initWithObjects:@"Gratulation!", @"Grandios!", @"Super!", @"Weltklasse!", @"Toll gemacht!", @"Sehr schön!", nil] : @[productDict[@"title"]];
    NSInteger randomIdx = (result) ? arc4random_uniform((uint32_t)arrayOfTitles.count-1) : 0 ;
    self.labelTitle.text = [arrayOfTitles objectAtIndex:randomIdx];
    
    // description text
    
    UIImage *productImage = [UIImage imageNamed:[NSString stringWithFormat:@"%zd",[productDict[@"callories"] integerValue]]];
    if (productImage) {
        self.imageViewProduct.image = productImage;
    } else {
        self.imageViewProduct.image = [UIImage imageNamed:@"Negativ"];
    }
    self.labelDescription.text = productDescription;
    
    // value labels
    self.labelCalories.text = [AnimatedLabel textWithThousendsFormat:caloriesToday];
    self.labelCalories.textColor = [UIColor customOrangeColor];
    self.labelSwopps.text = [AnimatedLabel textWithThousendsFormat:swoppsToday];
    self.labelSwopps.textColor = [UIColor customOrangeColor];
}

#pragma mark - Table View Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (void)configureCell:(SNSessionCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMSession *session = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *title  = LMLocalizedString(@"Messzeitpunkt", nil);
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"HH:mm";
    title = [title stringByAppendingFormat:@" %@", [dateFormatter stringFromDate:session.startTime]];
    cell.titleLabel.text = title;
    
//    NSCalendar *c = [NSCalendar currentCalendar];
//    NSDate *d1 = session.startTime;
//    NSDate *d2 = session.endTime;
//    NSDateComponents *components = [c components:NSMinuteCalendarUnit fromDate:d1 toDate:d2 options:0];
//    NSInteger diff = components.minute;
    
    NSInteger diff = session.sessionDuration.integerValue / 60;
    
//    NSInteger mins = [session.interval integerValue] / 360;
    cell.timeLable.text = [NSString stringWithFormat:@"%zd %@", diff, LMLocalizedString(@"Min", nil)];
    
    cell.swoppsLable.text = [NSString stringWithFormat:@"%@ %@", [AnimatedLabel textWithThousendsFormat:[session countsForSession]], LMLocalizedString(@"Moves", nil)];
    
    cell.calloriesLabel.text = [NSString stringWithFormat:@"%@ %@", [AnimatedLabel textWithThousendsFormat:(NSInteger)([session caloriesPerSession]*1000)], LMLocalizedString(@"cal", nil)];
    
    cell.sortOrderLabel.text = [NSString stringWithFormat:@"%zd", indexPath.row + 1];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIndentifier = @"Cell";
    SNSessionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


#pragma mark - Fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startTime" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"report = %@", self.report];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(SNSessionCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
