

#import "SNProfileController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "DMUser.h"
#import "SNHeightController.h"
#import "SNWeightController.h"
#import "SNAgeController.h"
#import "SNGenderController.h"
#import "SNMenusController.h"
#import "SNTrainingController.h"

#import "SNAdditionalMath.h"

#import <MFSideMenu.h>

#define kINDEX_PATH_USER_SEX_TYPE           [NSIndexPath indexPathForRow:0 inSection:0]
#define kINDEX_PATH_WEIGHT                  [NSIndexPath indexPathForRow:1 inSection:0]
#define kINDEX_PATH_HEIGHT                  [NSIndexPath indexPathForRow:2 inSection:0]
#define kINDEX_PATH_AGE                     [NSIndexPath indexPathForRow:3 inSection:0]

const CGFloat kCmToFeet = 0.032808399f;
const CGFloat kKiloToPound = 2.20462262f;
const NSInteger kFeetToInches = 12;

@interface SNProfileController () <UITableViewDataSource, UITableViewDelegate>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
    
    NSString *_weightSymbol;
    NSString *_heightSymbol;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *subHeaderLabel;
@property (strong, nonatomic) DMUser *user;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@end

@implementation SNProfileController



- (void)viewDidLoad
{
    [super viewDidLoad];
    _loadIdx = 0;
    
    [SNAppearance customizeViewController:self];
    
    [self customizeOutlets];
    
    if (self.navigationController.viewControllers[0] == self) {
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
        
        self.nextButton.hidden = YES;
    }
    
    if (!self.showNextButton) {
        self.nextButton.hidden = YES;
    }
    
    [NSIndexPath indexPathForRow:0 inSection:0];
    
    CGFloat height = 300;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -height, CGRectGetWidth(self.view.bounds), height)];
    view.backgroundColor = [UIColor whiteColor];
    [self.tableView addSubview:view];
    
//    self.tableView.backgroundColor = [UIColor colorWithRed:247/255.f green:247/255.f blue:247/255.f alpha:1];
    [SNAppearance customizeViewController:self];
    [self registerObservers];
    [self configureView];
    
    
    if (self.wizard) {
        [self.navigationItem setHidesBackButton:YES];
    }
    
//    if (![self.user isValid]) {
//        SNGenderController *genderController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNGenderController class])];
//        genderController.wizard = YES;
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:genderController];
//        [self.navigationController presentViewController:navController animated:NO completion:NULL];
//    }
    
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self measurementSymbols];
    NSString *sRightButton;
    if ([[DMUser defaultUser] USunits]) {
        sRightButton = @"kg/cm";
    } else {
        sRightButton = @"lbs/ft";
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:sRightButton
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(changeMeasurementUnitsSystem)];
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)dealloc
{
    [self unregisterObservers];
}

#pragma mark - Private Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)customizeOutlets
{
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.headerLabel.text = LMLocalizedString(@"Kalorien-Berechnung", nil);
    self.subHeaderLabel.text = LMLocalizedString(@"Zusammenfassung", nil);
    
    
    self.headerLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.headerLabel.font.pointSize];
    self.subHeaderLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subHeaderLabel.font.pointSize];
}

- (void)configureView {
    
    DMUser *user = [DMUser defaultUser];
    if ((user.sex != DMSexTypeNone) && (user.weight > 0) && (user.height > 0) && (user.age > 0))
        self.nextButton.enabled = YES;
    else
        self.nextButton.enabled = NO;
    
}

- (void)changeMeasurementUnitsSystem
{
    BOOL bUSunits = [[DMUser defaultUser] USunits];
    
    bUSunits = !bUSunits;
    [[DMUser defaultUser] setUSunits:bUSunits];
    [[DMUser defaultUser] save];
    
    NSString *sRightButton;
    if (bUSunits) {
        sRightButton = @"kg/cm";
    } else {
        sRightButton = @"lbs/ft";
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:sRightButton
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(changeMeasurementUnitsSystem)];
    [self measurementSymbols];
}

- (void)measurementSymbols
{
    if ([[DMUser defaultUser] USunits]) {
        _weightSymbol = @"lbs";
        _heightSymbol = @"ft";
    } else {
        _weightSymbol = @"kg";
        _heightSymbol = @"cm";
    }
    [self.tableView reloadData];
}

#pragma mark Animation Methods
- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Property Accessors
- (DMUser *)user
{
    if (!_user) {
        _user = [DMUser defaultUser];
    }
    return _user;
}

#pragma mark - Observers Methods
- (void)registerObservers
{
    [self.user addObserver:self forKeyPath:DMUserSexKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserWeightKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserHeightKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserAgeKey options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)unregisterObservers
{
    [self.user removeObserver:self forKeyPath:DMUserSexKey];
    [self.user removeObserver:self forKeyPath:DMUserWeightKey];
    [self.user removeObserver:self forKeyPath:DMUserHeightKey];
    [self.user removeObserver:self forKeyPath:DMUserAgeKey];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:DMUserSexKey]) {
        [self.tableView reloadRowsAtIndexPaths:@[kINDEX_PATH_USER_SEX_TYPE] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if ([keyPath isEqualToString:DMUserWeightKey]) {
        [self.tableView reloadRowsAtIndexPaths:@[kINDEX_PATH_WEIGHT] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if ([keyPath isEqualToString:DMUserHeightKey]) {
        [self.tableView reloadRowsAtIndexPaths:@[kINDEX_PATH_HEIGHT] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if ([keyPath isEqualToString:DMUserAgeKey]) {
        [self.tableView reloadRowsAtIndexPaths:@[kINDEX_PATH_AGE] withRowAnimation:UITableViewRowAnimationFade];
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
    
    [self configureView];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (IBAction)didTapNextButton:(id)sender
{
    [[DMUser defaultUser] save];
    if (self.wizard) {
        [self performSegueWithIdentifier:NSStringFromClass([SNTrainingController class]) sender:self];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Talbe View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSString *sWeight;
    NSString *sHeight;
    
    if ([[DMUser defaultUser] USunits]) {
        sWeight = [NSString stringWithFormat:@"%zd", (NSInteger)roundf(self.user.weight * kKiloToPound)];
        
        CGFloat fFtFromCm = self.user.height * kCmToFeet;
        sHeight = [NSString stringWithFormat:@"%zd ft %.0f\"", (NSInteger)fFtFromCm, FloatPartOfNumber(fFtFromCm) *kFeetToInches];
    } else {
        sWeight = [NSString stringWithFormat:@"%zd", (NSInteger)self.user.weight];
        sHeight = [NSString stringWithFormat:@"%zd cm", (NSInteger)self.user.height];
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"CellDetail" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = LMLocalizedString(@"Geschlecht", nil);
            NSString *sexType;
            if (self.user.sex == DMSexTypeNone) {
                sexType = LMLocalizedString(@"Ausgewählt nicht",nil);
            } else if (self.user.sex == DMSexTypeMan) {
                sexType = LMLocalizedString(@"Männlich",nil);
            } else {
                sexType = LMLocalizedString(@"Weiblich",nil);
            }
            cell.detailTextLabel.text = sexType;
        }
            break;
        case 1:
            cell.textLabel.text = LMLocalizedString(@"Gewicht", nil);
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", sWeight, _weightSymbol];
            break;
        case 2:
            cell.textLabel.text = LMLocalizedString(@"Größe", nil);
            cell.detailTextLabel.text = sHeight;
            break;
        case 3:
            cell.textLabel.text = LMLocalizedString(@"Alter", nil);
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%zd %@", self.user.age, LMLocalizedString(@"Jahre", nil)];
            break;
    }
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath isEqual:kINDEX_PATH_USER_SEX_TYPE]) {
        [self performSegueWithIdentifier:NSStringFromClass([SNGenderController class]) sender:self];
    }
    else if ([indexPath isEqual:kINDEX_PATH_WEIGHT]) {
        [self performSegueWithIdentifier:NSStringFromClass([SNWeightController class]) sender:self];
    }
    else if ([indexPath isEqual:kINDEX_PATH_HEIGHT]) {
        [self performSegueWithIdentifier:NSStringFromClass([SNHeightController class]) sender:self];
    }
    else if ([indexPath isEqual:kINDEX_PATH_AGE]) {
        [self performSegueWithIdentifier:NSStringFromClass([SNAgeController class]) sender:self];
    }
    
    [self.user save];
}

#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



@end
