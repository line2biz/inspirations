

#import "SNCalorieCalculatorController.h"

#import "SNCalorieCalculatorTableCell.h"
#import "SNBasicTableCell.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNAppearance.h"

#import "DMSession+Category.h"

#import "DMUser.h"

NSInteger _iCalculatorCellsAmount = 6;

@interface SNCalorieCalculatorController () <UITableViewDataSource, UITableViewDelegate>
{
    NSInteger _iRowsCount;
    NSArray *_aTimes;
    NSArray *_aCalories;
    NSInteger _iLoadIdx;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end

@implementation SNCalorieCalculatorController


#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _iLoadIdx = 0;
    
    [SNAppearance customizeViewController:self];
    
    [self controllerStart];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_iLoadIdx) {
        [self animateRowsDrop];
        _iLoadIdx++;
    } else {
        [self.tableView beginUpdates];
        
        NSMutableArray *maIndexPaths = [NSMutableArray arrayWithCapacity:_aTimes.count];
        for (NSInteger i=0; i < _iCalculatorCellsAmount+1; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [maIndexPaths addObject:indexPath];
        }
        _aTimes = nil;
        _aCalories = nil;
        _iRowsCount = 0;
        
        [self.tableView deleteRowsAtIndexPaths:(NSArray *)maIndexPaths withRowAnimation:UITableViewRowAnimationBottom];
        
        [self.tableView endUpdates];
        
        __weak typeof(self) weakSelf = self;
        [self createData:^{
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf animateRowsDrop];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    [self configureTextAndFont];
    
    [self createData:nil];
}

- (void)createData:(void(^)())completition
{
    __weak typeof(self) weakSelf = self;
    [self createArrayOfTimesWithComletition:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf createArrayOfCaloriesWithComletition:completition];
        }
    }];
}

- (void)configureTextAndFont
{
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize: self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor titleColor];
    self.labelTitle.text = LMLocalizedString(@"swopper Kalorienrechner", nil);
    
    self.labelText.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelText.font.pointSize];
    self.labelText.textColor = [UIColor customDarkGrayGolor];
    self.labelText.text = LMLocalizedString(@"swoppen bedeutet nicht nur Gutes für Rücken und Muskulatur zu tun, sondern es kurbelt auch den Stoffwechsel und die Fettverbrennung an. In wissenschaftlichen Studien wurde gezeigt, dass Sitzen auf dem swopper einen ähnlichen Einfluss auf die Fettverbrennung hat wie „leichtes Gehen“.\n\nHier können Sie berechnen, welchen Effekt der swopper für Sie erzeugt.", nil);
    
    // find and mark words: swoppen, swopper
    NSAttributedString *asResult = [SNAppearance findAndMarkWord:LMLocalizedString(@"swoppen", nil)
                                              inAttributedString:self.labelText.attributedText
                                                       withColor:nil
                                                            font:[UIFont franklinGothicStdBookCondesedMedWithSize:self.labelText.font.pointSize]
                                                       atIndexes:nil];
    asResult = [SNAppearance findAndMarkWord:LMLocalizedString(@"swopper", nil)
                          inAttributedString:asResult
                                   withColor:nil
                                        font:[UIFont franklinGothicStdBookCondesedMedWithSize:self.labelText.font.pointSize]
                                   atIndexes:nil];
    
    self.labelText.attributedText = asResult;
}

- (void)createArrayOfTimesWithComletition:(void(^)())completition
{
    NSNumber *(^NumberWithFloat)(CGFloat) = ^(CGFloat input) {
        return [NSNumber numberWithFloat:input];
    };
    NSMutableArray *maTimes = [NSMutableArray arrayWithCapacity:_iCalculatorCellsAmount];
    [maTimes addObject:NumberWithFloat(0.5f)];
    [maTimes addObject:NumberWithFloat(1.0f)];
    [maTimes addObject:NumberWithFloat(3.0f)];
    [maTimes addObject:NumberWithFloat(5.0f)];
    [maTimes addObject:NumberWithFloat(8.0f)];
    [maTimes addObject:NumberWithFloat(10.0f)];
    
    _aTimes = [NSArray arrayWithArray:maTimes];
    
    if (completition) {
        completition();
    }
}

- (void)createArrayOfCaloriesWithComletition:(void(^)())completition
{
    NSNumber *(^CaloriesForTime)(NSNumber *) = ^(NSNumber *timeInHours) {
        
        CGFloat MET = 3.3f; // statiс value of MET
        NSTimeInterval timeInterval = [timeInHours floatValue] * 60 * 60; // Seconds
       
        CGFloat result = [DMSession getAdditionalKcalByMBRforMET:MET andTime:timeInterval];
        return [NSNumber numberWithFloat:result];
    };
    
    NSMutableArray *maCalories = [NSMutableArray arrayWithCapacity:_iCalculatorCellsAmount];
    for (NSNumber *timeInHours in _aTimes) {
        [maCalories addObject:CaloriesForTime(timeInHours)];
    }
    
    _aCalories = [NSArray arrayWithArray:maCalories];
    if (completition) {
        completition();
    }
}

- (void)animateRowsDrop
{
    _iRowsCount = 0;
    NSInteger iRowsMax = _iCalculatorCellsAmount + 1;
    __weak typeof(self) weakSelf = self;
    NSTimeInterval timeInterval = 1.0f / iRowsMax;
    for (NSInteger i=0; i < iRowsMax; i++) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timeInterval * (i+1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.tableView beginUpdates];
                
                [strongSelf.tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:_iRowsCount inSection:0] ] withRowAnimation:UITableViewRowAnimationTop];
                _iRowsCount++;
                
                [strongSelf.tableView endUpdates];
            }
        });
    }
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _iRowsCount;
}

- (void)configureCalCell:(SNCalorieCalculatorTableCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *nTime = [_aTimes objectAtIndex:indexPath.row];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.allowsFloats = YES;
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString *sLang = [[SNLocalizationManager sharedManager] getLanguage];
    if ([sLang isEqualToString:@"en"]) {
        sLang = [sLang stringByAppendingString:@"_US"];
    } else {
        sLang = [sLang stringByAppendingString:@"_DE"];
    }
    numberFormatter.locale = [NSLocale localeWithLocaleIdentifier:sLang];
    
    NSString *sText = [numberFormatter stringFromNumber:nTime];
    
    cell.labelTime.text = [NSString stringWithFormat:@"%@ h", sText];
    
    cell.dCalories = [[_aCalories objectAtIndex:indexPath.row] doubleValue];
    
    if (indexPath.row == _iCalculatorCellsAmount - 1) {
        cell.bLast = YES;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row < _iCalculatorCellsAmount) {
        SNCalorieCalculatorTableCell *calcCell = [tableView dequeueReusableCellWithIdentifier:@"CalcCell"];
        
        [self configureCalCell:calcCell atIndexPath:indexPath];
        
        cell = calcCell;
    } else {
        SNBasicTableCell *basicCell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell"];
        
        basicCell.imageView.image = [UIImage imageNamed:@"Icon-Statistic-Gray"];
        basicCell.textLabel.text = LMLocalizedString(@"Persönliche Daten ändern", nil);
        
        cell = basicCell;
    }
    
    return cell;
}

#pragma mark - Table View Delegate
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < _iCalculatorCellsAmount) {
        return nil;
    }
    return indexPath;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < _iCalculatorCellsAmount) {
        return 53.0f;
    }
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNProfileController"];
    [self.navigationController pushViewController:viewController animated:YES];
}



@end
