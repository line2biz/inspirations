

#import "SNMenusController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNSummaryController.h"
#import "SNMeasurementController.h"
#import "SNTimeIntervalController.h"
#import "SNReportsController.h"
#import "SNStepDashboardController.h"
#import "SNStepCounterController.h"
#import "SNStepReportController.h"
#import "DMSession+Category.h"
#import "DMReport+Category.h"
#import "SNTodayController.h"

#import "DMUser.h"

#import "SNMoviePlayerController.h"
#import "SNTrainingSummaryController.h"
#import "SNAudioVolumeController.h"
#import "SNStepManager.h"

#import <MFSideMenu.h>

#import "DMStep+Category.h"

#define kANIMATION_DURATION .2f

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface SNMenusController () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, SNMoviePlayerControllerProtocol>
{
    UIViewController *_currentController;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) DMUser *user;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL containerIsHidden;
@property (assign, nonatomic) CGFloat availablePanDistance;
@property (strong, nonatomic) SNMoviePlayerController *moviePlayerController;
@property (strong, nonatomic) NSString *currentControllerIdentifier;


//@property (nonatomic) CGRect prevFrame;

@end

@implementation SNMenusController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.containerIsHidden = NO;
    
    self.user = [DMUser defaultUser];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // side menu
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController new];
    container.leftMenuWidth = 200;
    container.shadow.enabled = YES;
    container.shadow.opacity = .2;
    container.menuSlideAnimationEnabled = YES;
    container.menuSlideAnimationFactor = 1; // control the exaggeration of the menu slide animation
    //    container.panMode = MFSideMenuPanModeNone; // disable swipe
    
    [SNAppDelegate sharedDelegate].window.rootViewController = container;
    
    container.leftMenuViewController = self;
    
    NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"report = nil"];
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s error fetching count request: %@", __FUNCTION__, error);
    }
    
    // default controller
    if (![[DMUser defaultUser] isValid] || count == 0) {
        [self showControllerWithIdentifier:@"SNStartController"];
    } else {
        [self showControllerWithIdentifier:@"SNTodayController"];
    }
    
//    [self showControllerWithIdentifier:@"SNTodayController"];
    
    [self customizeOutlets];
    
    __weak typeof(self) weakSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [weakSelf createArrayOfMainData];
        [weakSelf.tableView reloadData];
    }];
    
    // create menu items
    [self createArrayOfMainData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Private Methods
- (void)createArrayOfMainData
{
    NSDictionary *(^MenuItem)(NSString *, NSString *) = ^(NSString *title, NSString *icon) {
        NSDictionary *dict = @{@"title":title, @"icon":icon};
        return dict;
    };
    NSMutableArray *mArrayOfDicts = [NSMutableArray new];
    // add new menu items here
    
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Mitmach-Training", nil), @"Icon-Measurement")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"swopper Kalorienrechner", nil), @"Icon-Measurement")];
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Schrittzähler", nil), @"Icon-Pedometer")];
    } else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([CMPedometer isStepCountingAvailable]) {
                [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Schrittzähler", nil), @"Icon-Pedometer")];
            }
        } else {
            if ([CMStepCounter isStepCountingAvailable]) {
                [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Schrittzähler", nil), @"Icon-Pedometer")];
            }
        }
    }
    
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Statistik", nil), @"Icon-Statistic")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"swopper Aufbau", nil), @"Icon-Video")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"swopper Einstellungen", nil), @"Icon-Video")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"So geht swoppen", nil), @"Icon-Video")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Einstellungen", nil), @"Icon-Settings")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Moves und kcal", nil), @"Icon-Info")];
    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Impressum", nil), @"Icon-Info")];
//    [mArrayOfDicts addObject:MenuItem(LMLocalizedString(@"Test", nil), @"Icon-Info")];
    
    self.items = mArrayOfDicts;
}

- (void)customizeOutlets
{
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
 
    self.tableView.backgroundColor = [UIColor clearColor];
}


- (void)showControllerWithIdentifier:(NSString *)identifer
{
    MFSideMenuContainerViewController *sidemenu = ((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController);
    
    UIViewController *viewController;
    if ([identifer isEqualToString:_currentControllerIdentifier]) {
        sidemenu.menuState = MFSideMenuStateClosed; // hide side menu
        return;
    }
    viewController = [self.storyboard instantiateViewControllerWithIdentifier:identifer];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"]
                                                                                       style:UIBarButtonItemStylePlain
                                                                                      target:self
                                                                                      action:@selector(hideViewController:)];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    if ([NSStringFromClass([viewController class]) isEqualToString:@"SNTodayController"]) {
        sidemenu.leftMenuWidth = 200;
    } else {
        sidemenu.leftMenuWidth = 320;
    }
    sidemenu.centerViewController = navController;
    
    sidemenu.menuState = MFSideMenuStateClosed;
    sidemenu.panMode = MFSideMenuPanModeDefault;
    
    _currentControllerIdentifier = identifer;
}

- (void)didStopAnimation:(id)sender {
    NSLog(@"%s", __FUNCTION__);
    
    _containerView.frame = self.view.bounds;
}

- (void)hideViewController:(id)sender
{
    MFSideMenuContainerViewController *sidemenu = ((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController);
    [sidemenu toggleLeftSideMenuCompletion:nil];
}

- (void)transformViewControllerAccordingProgress:(CGFloat)progress
{
    CGFloat maxScale = 0.6f;
    CGFloat maxTranslation = 170.0f;
    
    maxScale = maxScale + (1.0f - maxScale) * progress;
    maxTranslation = maxTranslation - maxTranslation * progress;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:kANIMATION_DURATION - kANIMATION_DURATION * progress];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    CGAffineTransform scaleTrans = CGAffineTransformIdentity;
    scaleTrans = CGAffineTransformScale(scaleTrans, maxScale, maxScale);
    CGAffineTransform lefttorightTrans = CGAffineTransformIdentity;
    lefttorightTrans = CGAffineTransformTranslate(lefttorightTrans, maxTranslation, 0.0f);
    _containerView.transform = CGAffineTransformConcat(scaleTrans, lefttorightTrans);
    [UIView commitAnimations];
}

#pragma mark - Application Protocol
- (void)applicationWillAppearInForeground
{
    [self showControllerWithIdentifier:@"SNTodayController"];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
}

#pragma mark - Gesture Recognizer Delegate
- (void)gestureHandler:(UIPanGestureRecognizer *)sender
{
    if (self.containerIsHidden) {
        CGPoint locationInMainView = [sender locationInView:self.view];
        switch (sender.state) {
            case UIGestureRecognizerStateBegan:
            {
                CGFloat screenWidth = CGRectGetWidth(self.view.bounds);
                _availablePanDistance = screenWidth - (screenWidth - locationInMainView.x);
            }
                break;
            case UIGestureRecognizerStateChanged:
            {
                CGFloat progress = 1 - (locationInMainView.x / _availablePanDistance);
                [self transformViewControllerAccordingProgress:progress];
            }
                break;
            case UIGestureRecognizerStateEnded:
            {
                CGFloat progress = 1 - (locationInMainView.x / _availablePanDistance);
                if (progress >= 0.5f) {
                    [self showControllerWithIdentifier:self.currentControllerIdentifier];
                } else {
                    [self hideViewController:nil];
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Movie Player Controller Delegate
- (void)moviePlayerController:(SNMoviePlayerController *)controller didExitFullScreen:(BOOL)didExit
{
    if (didExit) {
        
        MFSideMenuContainerViewController *sidemenu = ((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController);
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTodayController"];
        UIViewController *controllerToCheck;
        if ([sidemenu.centerViewController class] == [UINavigationController class]) {
            UINavigationController *navController = (UINavigationController *)sidemenu.centerViewController;
            controllerToCheck = [navController.childViewControllers lastObject];
        } else {
            controllerToCheck = viewController;
        }
        if ([controllerToCheck class] != [viewController class]) {
            sidemenu.centerViewController = nil;
            _currentControllerIdentifier = nil;
            sidemenu.panMode = MFSideMenuPanModeNone;
        }
    }
}


#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dictionary = self.items[indexPath.row];
    cell.textLabel.text = [dictionary valueForKey:@"title"];
    cell.imageView.image = [UIImage imageNamed:dictionary[@"icon"]];
    return cell;
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_WIDESCREEN) {
        return 40.0f;
    } else {
        return 32.0f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger iTableIndex = indexPath.row;
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        
    } else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if (![CMPedometer isStepCountingAvailable]) {
                if (iTableIndex > 1) {
                    iTableIndex += 1;
                }
            }
        } else {
            if (![CMStepCounter isStepCountingAvailable]) {
                if (iTableIndex > 1) {
                    iTableIndex += 1;
                }
            }
        }
    }
    
    
    NSURL *(^UrlForVideo)(NSString *, NSString *) = ^(NSString *sResourceName, NSString *sType) {
        
        NSString *sLocalizationSuffix = [[SNLocalizationManager sharedManager] getLanguage];
        sResourceName = [NSString stringWithFormat:@"%@_%@", sResourceName, sLocalizationSuffix];
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:sResourceName ofType:sType];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        
        return movieURL;
    };
    switch (iTableIndex) {
        case 0:
        {
            DMSession *session = [DMSession trainingSesstionByDate:[NSDate date] inContext:[[DMManager sharedManager] defaultContext]];
            if (session) {
                [self showControllerWithIdentifier:@"SNTrainingSummaryController"];
            } else {
                if ([SNAudioVolumeController shouldBeShown]) {
                    [self showControllerWithIdentifier:NSStringFromClass([SNAudioVolumeController class])];
                } else {
                    [self showControllerWithIdentifier:@"SNTrainingController"];
                }
            }
        }
            break;
            
        case 1:
        {
            
//            DMReport *report = [DMReport reportByDate:[NSDate date] inContext:[[DMManager sharedManager] defaultContext]];
//            if (report) {
//                [self showControllerWithIdentifier:NSStringFromClass([SNSummaryController class])];
//            } else {
//                [self showControllerWithIdentifier:@"SNTimerSetterController"];
//            }
            [self showControllerWithIdentifier:@"SNCalorieCalculatorController"];
        }
            break;
            
        case 2:
        {
            if ([model isEqualToString:@"iPhone Simulator"]) {
                //device is simulator
                [self showControllerWithIdentifier:@"SNStepDashboardController"];
                
            } else {
                [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:^{
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self showControllerWithIdentifier:@"SNStepDashboardController"];
                    });
                }];
            }
        }
            break;
            
        case 3:
//            [self showControllerWithIdentifier:@"SNStatisticController"];
            [self showControllerWithIdentifier:@"SNStatisticViewController"];
            
            break;
            
        case 4:
        {
            NSURL *movieURL = UrlForVideo(@"swopperAufbau", @"mp4");
            
            self.moviePlayerController = [[SNMoviePlayerController alloc] initWithContentURL:movieURL];
            self.moviePlayerController.delegate = self;
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayerController];
        }
            break;
        case 5:
        {
            NSURL *movieURL = UrlForVideo(@"swopperEinstellungen", @"mp4");
            
            self.moviePlayerController = [[SNMoviePlayerController alloc] initWithContentURL:movieURL];
            self.moviePlayerController.delegate = self;
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayerController];
        }
            break;
        case 6:
        {
            NSURL *movieURL = UrlForVideo(@"So_geht_swoppen", @"mp4");
            
            self.moviePlayerController = [[SNMoviePlayerController alloc] initWithContentURL:movieURL];
            self.moviePlayerController.delegate = self;
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayerController];
        }
            break;
        case 7:
        {
            [self showControllerWithIdentifier:@"SNConfigurationController"];
        }
            break;
        case 8:
        {
            [self showControllerWithIdentifier:@"SNMovesAndCaloriesController"];
        }
            break;
        case 9:
        {
            [self showControllerWithIdentifier:@"SNImpressumController"];
        }
            break;
            
        default: {
//            [self showControllerWithIdentifier:NSStringFromClass([SNTodayController class])];
        }
            break;
    }
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}




@end
