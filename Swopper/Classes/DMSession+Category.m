

#import "DMSession+Category.h"
#import "DMMotion+Category.h"
#import "DMUser.h"

NSString * const DMSessionStartTimeKey = @"startTime";

@implementation DMSession (Category)

- (void)awakeFromInsert {
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.startTime = now;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:now];
    self.year = @(dateComponents.year);
    self.month = @(dateComponents.month);
    self.weekOfYear = @(dateComponents.weekOfYear);
    self.day = @(dateComponents.day);
    
    
}

+ (DMSession *)trainingSesstionByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMSession class])];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:date];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day = %d && month == %d && year == %d && report == nil", dateComponents.day, dateComponents.month, dateComponents.year];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *resulsts = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if ([resulsts count]) {
        return [resulsts firstObject];
    }
    
    return nil;
}

- (NSUInteger)countsForSession
{
    NSUInteger count = 0;
    
    CGFloat motionSummary = 0;
    
    for (DMMotion *motion in self.motions) {
//        count += (int)round([motion.value floatValue] / [[NSUserDefaults standardUserDefaults] floatForKey:CPMFactorValueKey]);
        motionSummary += [motion.value floatValue];
    }
    count = (int)round(motionSummary);// / [[NSUserDefaults standardUserDefaults] floatForKey:CPMFactorValueKey]);
    
    if (self.endTime) {
        if (count < 20.0f) {
            
            NSInteger iZeroAmount = 0;
            for (DMMotion *motion in self.motions) {
                if ([motion.value floatValue] <= 0.0001f) {
                    iZeroAmount++;
                } else {
                    iZeroAmount = 0;
                }
                if (iZeroAmount >= 10) {
                    break;
                }
            }
            
            if (iZeroAmount >= 10) {
                count = 0;
            }
        }        
    }
    
    return count;
}

- (CGFloat)countsPerMinute
{
    return [self countsPerSecond] * 60;
}

- (CGFloat)countsPerSecond {
    return (float)self.countsForSession / [self.sessionDuration floatValue];
}

- (CGFloat)caloriesPerSession {
//    double BMR = [[DMUser defaultUser] BMRValue];
//    double MET;// = [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey] * (double)[self countsPerMinute];
//    MET = 1.439008 + (0.000795 * [self countsPerMinute]);
//    NSTimeInterval interval = [self.interval floatValue] / 60 ; // Minutes
//    double result = (BMR * MET / (24 * 60)) * interval * 1.3;
//    
//    NSLog(@"%s %f", __FUNCTION__, result);
    
    CGFloat result = [self getCaloriesByBMRForTime:[self.sessionDuration floatValue]];
    
    return result;
}

- (CGFloat)caloriesPerHour
{
//    double BMR = [[DMUser defaultUser] BMRValue];
//    double MET;// = [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey] * (double)[self countsPerMinute];
//    MET = 1.439008 + (0.000795 * [self countsPerMinute]);
//    NSTimeInterval interval = 60; // 1 hour
//    
//    double result = (BMR * MET / (24 * 60)) * interval * 1.3f;
    
    
//    NSLog(@"\n\n%s\nBMR: %f\nMET: %f\nCPM: %f\nDuration: %f\nCalories: %f\n", __FUNCTION__, BMR, MET, [self countsPerMinute], [self.sessionDuration floatValue], result);
    
    CGFloat result = [self getCaloriesByBMRForTime:60.0f*60.0f];
    return result;
}

#pragma mark - Public Methods
+ (CGFloat)getAdditionalKcalByMBRforMET:(CGFloat)MET andTime:(NSTimeInterval)timeInterval
{
    CGFloat BMR = [[DMUser defaultUser] BMRValue];
    CGFloat result = (BMR * MET) / (24 * 60 * 60) * timeInterval - (BMR * 0.9f) / (24*60*60)*timeInterval;
    
    return result;
}

#pragma mark - Private Methods
/// calculates additional kcalories by BMR for time in seconds
- (CGFloat)getCaloriesByBMRForTime:(NSTimeInterval)timeInterval
{
    CGFloat fFinalMET = [self getMETfromCPM];
    
    if (fFinalMET <= 0.0001) {
        return 0.0f;
    }
    
    CGFloat BMR = [[DMUser defaultUser] BMRValue];
    CGFloat result = (BMR * fFinalMET) / (24 * 60 * 60) * timeInterval - (BMR * 0.9f) / (24*60*60)*timeInterval;
    
    return result;
}

/// calculates kcalories by oxygen consumption for time in seconds
- (CGFloat)getCaloriesByOxygenForTime:(NSTimeInterval)timeInterval
{
    CGFloat fFinalMET = [self getMETfromCPM];
    
    CGFloat fOxygenPerMET = 3.5f; // in ml/kg/min
    CGFloat fOxygenPerMinForBodyWeight = [[DMUser defaultUser] weight] * fOxygenPerMET * fFinalMET / 1000; // in Litres
    CGFloat fKcalPerLitrOfOxygen = 5.0f;
    CGFloat fKcalPerMin = fOxygenPerMinForBodyWeight * fKcalPerLitrOfOxygen;
    
    CGFloat result = fKcalPerMin * (timeInterval / 60.0f);
    
    return result;
}

/// calculates average calories from BMR and Oxygen methods for time in seconds
- (CGFloat)getAverageCaloriesForTime:(NSTimeInterval)timeInterval
{
    CGFloat fBMRCalories = [self getCaloriesByBMRForTime:timeInterval];
    CGFloat fOxygenCalories = [self getCaloriesByOxygenForTime:timeInterval];
    
    CGFloat result;
    if ([[DMUser defaultUser] sex] == DMSexTypeMan) {
        result = 0.5f * fBMRCalories + 0.5f * fOxygenCalories;
    } else if ([[DMUser defaultUser] sex] == DMSexTypeWomen) {
        result = 0.65f * fBMRCalories + 0.65f * fOxygenCalories;
    }
    return result;
}

/// calculates MET from our abstract counts per minutes
- (CGFloat)getMETfromCPM
{
    CGFloat fMaxCounts = 900.0f;
    CGFloat fMaxMET = 8.3f;
    CGFloat fMinMET = 3.3f;
    
    CGFloat fCountsPerMinute = [self countsPerMinute];
    
    if (fCountsPerMinute == 0) {
        return 0;
    }
    
    CGFloat fPercentage = fCountsPerMinute / fMaxCounts;
    
    CGFloat fFinalMET = (fMaxMET - fMinMET) * fPercentage + fMinMET;
    
    return fFinalMET;
}


@end
