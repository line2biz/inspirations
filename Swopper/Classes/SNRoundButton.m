
#import "SNRoundButton.h"
#import "UIColor+Customization.h"

@interface SNRoundButton ()
{
    CGFloat _fMinSide;
}
@end


@implementation SNRoundButton


#pragma mark - Life Cycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}


- (void)defaultInit
{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat fWidth = CGRectGetWidth(self.bounds);
    CGFloat fHeight = CGRectGetHeight(self.bounds);
    
    _fMinSide = MIN(fWidth, fHeight);
    
    self.layer.cornerRadius = _fMinSide / 2.0f;
    
    _bEnabled = YES;
}

#pragma mark Drawing
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    // **************** Context *****************
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // *************** Background ***************
    
    CGFloat fStartX = CGRectGetMidX(self.bounds) - _fMinSide/2.0f;
    CGFloat fStartY = CGRectGetMidY(self.bounds) - _fMinSide/2.0f;
    
    // create Bezier Path
    UIBezierPath *roundBackgroundPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fStartX, fStartY, _fMinSide, _fMinSide)];
    CGContextAddPath(context, roundBackgroundPath.CGPath);
    
    // set needed color
    NSArray *aColors = @[[UIColor customDarkGrayGolor], [UIColor customOrangeColor]];
    
    CGContextSetFillColorWithColor(context, ((UIColor *)[aColors objectAtIndex:self.bEnabled]).CGColor);
    CGContextSetLineWidth(context, 0.0f);
    
    // fill our path
    CGContextFillPath(context);
}


#pragma mark - Property accessors
- (void)setBEnabled:(BOOL)bEnabled
{
    if (_bEnabled != bEnabled) {
        _bEnabled = bEnabled;
        
        [self setNeedsDisplay];
    }
}

@end
