

#import <Foundation/Foundation.h>

@interface DMCalorie : NSObject

@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) NSUInteger value;
@property (strong, nonatomic) UIImage *image;

+ (NSArray *)calories;
+ (DMCalorie *)calorieForValue:(NSUInteger)value;

@end
