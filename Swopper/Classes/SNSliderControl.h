

#import <UIKit/UIKit.h>

@protocol SNSliderControlProtocol;

@interface SNSliderControl : UIControl

@property (assign, nonatomic) float maximumValue;
@property (assign, nonatomic) float minimumValue;
@property (assign, nonatomic) float currentValue;
@property (assign, nonatomic) CGFloat alpha;

@property (strong, nonatomic) id <SNSliderControlProtocol> delegate;

@end


@protocol SNSliderControlProtocol <NSObject>
@required
- (void)slider:(SNSliderControl *)sender moveStarted:(BOOL)started;

@end