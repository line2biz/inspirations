

#import "SNAudioVolumeController.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SKMuteSwitchDetector.h"

#import "SNMenusController.h"
#import "SNTrainingController.h"

@import AVFoundation;
@import MediaPlayer;

@interface SNAudioVolumeController () <UIGestureRecognizerDelegate>
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@property (strong, nonatomic) MPVolumeView *volumeView;

@end

@implementation SNAudioVolumeController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _loadIdx = 0;
    // Do any additional setup after loading the view.
    [SNAppearance customizeViewController:self];
    [self configureFonts];
    
    self.nextButton.enabled = ![SNAudioVolumeController shouldBeShown];
    
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.volumeView = [[MPVolumeView alloc] initWithFrame:CGRectMake(16, 200, 298, 60)];
    self.volumeView.backgroundColor = [UIColor lightGrayColor];
    [self.volumeView sizeToFit];
    
    if (self.navigationController.viewControllers.count > 1) {
        UIImage *iconImage = [UIImage imageNamed:@"Icon-Nav-Back"];
        NSString *sButtonTitle = LMLocalizedString(@"Zurück", nil);
        UIFont *fontForButton = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize-3];
        NSDictionary *dictOfAttrs = @{ NSFontAttributeName : fontForButton };
        CGSize size = [sButtonTitle sizeWithAttributes:dictOfAttrs];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width + size.width, 40)];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:iconImage];
        imageView.frame = CGRectMake(0, (40 - iconImage.size.height)/2, iconImage.size.width, iconImage.size.height);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(iconImage.size.width, (40 - size.height)/2 + 2, size.width, size.height)];
        label.font = fontForButton;
        label.textColor = [UIColor customDarkGrayGolor];
        label.text = sButtonTitle;
        [view addSubview:imageView];
        [view addSubview:label];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
        tapGesture.delegate = self;
        
        [view addGestureRecognizer:tapGesture];
        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:view];
        self.navigationItem.leftBarButtonItem = leftButton;
    } else {
        UIViewController *sidemenu = [SNAppDelegate sharedDelegate].window.rootViewController;
        for (UIViewController *childController in sidemenu.childViewControllers) {
            if ([childController isKindOfClass:[SNMenusController class]]) {
                SNMenusController *menuController = (SNMenusController *)childController;
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"]
                                                                                         style:UIBarButtonItemStylePlain
                                                                                        target:menuController
                                                                                        action:@selector(hideViewController:)];
                break;
            }
        }
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(volumeChanged:)
                                                 name:@"AVSystemController_SystemVolumeDidChangeNotification"
                                               object:nil];
    
}

- (void)volumeChanged:(NSNotification *)notify {
    float volume = [[[notify userInfo] valueForKey:@"AVSystemController_AudioVolumeNotificationParameter"] floatValue];
    self.nextButton.enabled = volume > SNAppSettingsMinSoundVolumeValue;
    
    
}

+ (BOOL)shouldBeShown {
    float volume = [[[AVAudioSession alloc] init] outputVolume];
    return volume < SNAppSettingsMinSoundVolumeValue;
    
    __block BOOL isSilent = NO;
    
//    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    [SKMuteSwitchDetector checkSwitch:^(BOOL success, BOOL silent) {
        if (success) {
            isSilent = silent;
        }
//        dispatch_semaphore_signal(sem);
    }];
//    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    return ((volume > SNAppSettingsMinSoundVolumeValue) && !isSilent);
}

#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Controller Methods
- (void)configureFonts
{
    self.labelMessage.textColor = [UIColor customDarkGrayGolor];
    self.labelMessage.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelMessage.font.pointSize];
    self.labelMessage.text = LMLocalizedString(@"Drehen Sie die Lautstärke auf.", nil);
    
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.text = LMLocalizedString(@"Lautsprecher an?", nil);
}

- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [self.nextButton setHidden:NO];
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Tap Gesture Handler
- (void)tapGestureHandler:(UITapGestureRecognizer *)sender
{
    NSTimeInterval duration = 0.5f;
    [UIView animateWithDuration:duration/2
                     animations:^{
                         sender.view.alpha = 0.25f;
                         [UIView animateWithDuration:duration/2
                                          animations:^{
                                              sender.view.alpha = 1.0f;
                                          }
                                          completion:^(BOOL finished) {
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                          }];
                     }];
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonNext:(id)sender
{
    if ([self.navigationController.viewControllers count] > 1) {
        SNTrainingController *trainingController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNTrainingController class])];
        trainingController.trainingTime = self.trainingDuration;
        [self.navigationController pushViewController:trainingController animated:YES];
    } else {
        UIViewController *sidemenu = [SNAppDelegate sharedDelegate].window.rootViewController;
        for (UIViewController *childController in sidemenu.childViewControllers) {
            if ([childController isKindOfClass:[SNMenusController class]]) {
                SNMenusController *menuController = (SNMenusController *)childController;
                [menuController showControllerWithIdentifier:@"SNTrainingController"];
                break;
            }
        }

    }
}





@end
