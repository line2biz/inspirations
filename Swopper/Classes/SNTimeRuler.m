

#import "SNTimeRuler.h"
#import "SNAdditionalMath.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

@interface SNTimeRuler () <UIGestureRecognizerDelegate>
{
    CGFloat _currentValue;
    CGFloat _rulerHeight, _rulerLength;
    CGFloat _mainLine, _middleLine, _smallLine;
    CGFloat _minValue, _maxValue, _minShownV, _maxShownV;
    NSInteger _amountOfValues;
    CGFloat _spacing, _pointWidth, _availableDragSpace;
    
    CGPoint _startTouchPoint, _endTouchPoint;
    CGFloat _moveDelta, _preMinShownV;
}
@end

@implementation SNTimeRuler

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor clearColor];
    _amountOfValues = 26;
    _pointWidth = 3.5f;
    _gridColor = [UIColor customLightGrayColor];
    _chosenColor = [UIColor customOrangeColor];
    
    _textMainFont = [UIFont franklinGothicStdBookCondesedWithSize:27];
    _textSmallFont = [UIFont franklinGothicStdBookCondesedWithSize:10];
    _textCurrentFont = [UIFont franklinGothicStdBookCondesedWithSize:75];
    
    [self mainCalculations];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandler:)];
    [panGesture setMinimumNumberOfTouches:1];
    [self addGestureRecognizer:panGesture];
    panGesture.delegate = self;
}

#pragma mark - Private Methods
#pragma mark Calculations
- (void)mainCalculations
{
    [self calculateLinesSizes];
    [self calculateMinMaxValues];
}

- (void)calculateLinesSizes
{
    _rulerHeight = 0.4f * CGRectGetHeight(self.bounds);
    _rulerLength = CGRectGetWidth(self.bounds);
    _availableDragSpace = CGRectGetWidth(self.bounds);
    _mainLine = 0.6f * _rulerHeight;
    _middleLine = 0.6f * _mainLine;
    _smallLine = 0.6f * _middleLine;
    
    _spacing = _rulerLength / _amountOfValues;
}

- (void)calculateMinMaxValues
{
    _minValue = 1 - _amountOfValues/2;
    _maxValue = 24 * 6 - 1 + _amountOfValues/2;
    _currentValue = 8 * 6;
    _minShownV = _currentValue - _amountOfValues/2;
    _maxShownV = _currentValue + _amountOfValues/2;
}

#pragma mark Gesture Handler
- (void)gestureHandler:(UIPanGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _moveDelta = 0.0f;
            _preMinShownV = _minShownV;
            _startTouchPoint = [sender locationInView:self];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            _endTouchPoint = [sender locationInView:self];
            
            CGFloat deltaValue = _startTouchPoint.x - _endTouchPoint.x;
            
            _moveDelta = deltaValue / _spacing;
            _minShownV = _preMinShownV + _moveDelta;
            _maxShownV = _minShownV + _amountOfValues;
            
            if (_minShownV <= _minValue) {
                _minShownV = _minValue;
                _maxShownV = _minValue + _amountOfValues;
            } else if (_maxShownV >= _maxValue) {
                _maxShownV = _maxValue;
                _minShownV = _maxShownV - _amountOfValues;
            }
            _currentValue = _minShownV + _amountOfValues/2;
            [self setNeedsDisplay];
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            _currentValue = _currentValue - (_currentValue - roundf(_currentValue));
            _minShownV = _currentValue - _amountOfValues/2;
            _maxShownV = _minShownV + _amountOfValues;
            [self setNeedsDisplay];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark Drawing
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // all lines
    for (NSInteger i=_minShownV; i<_minShownV + _amountOfValues; i++) {
        if ( i > -1 && i < 24*6) {
            // prepare context
            CGContextSetStrokeColorWithColor(context, _gridColor.CGColor);
            CGContextSetLineWidth(context, _pointWidth);
            // prepare data
            CGPoint startPoint;
            CGFloat angle;
            startPoint = CGPointMake((i - _minShownV) * _spacing, 0.0f);
            angle = 90.0f;
            CustomLine line;
            line.startPoint = startPoint;
            CGFloat lineLength;
            if (i % 6) {
                if (i % 3) {
                    lineLength = _smallLine;
                } else {
                    lineLength = _middleLine;
                }
            } else {
                lineLength = _mainLine;
            }
            
            line.endPoint = GetPointFromPointAngleLength(line.startPoint,angle,lineLength);
            
            if (!(i%6)) {
                // main value text drawing
                UIFont* font = _textMainFont;
                UIColor* textColor = _gridColor;
                UIColor *newColor;
                const CGFloat *colorComponents = CGColorGetComponents(textColor.CGColor);
                if (i >= _currentValue - 1 && i <= _currentValue + 1) {
                    newColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.33f];
                } else if (i >= _currentValue - 2 && i <= _currentValue +2) {
                    newColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:0.66f];
                } else {
                    newColor = textColor;
                }
                NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : newColor };
                NSString *textToDraw = [NSString stringWithFormat:@"%zd",i/6];
                NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:textToDraw attributes:stringAttrs];
                CGSize size = [textToDraw sizeWithAttributes:stringAttrs];
                
                CGPoint textPoint = line.endPoint;
                textPoint.x -= size.width / 2;
                textPoint.y += 5;
                
                if (i != _currentValue) {
                    [attrStr drawAtPoint:textPoint];                    
                }
            }
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:line.startPoint];
            [path addLineToPoint:line.endPoint];
            
            CGContextAddPath(context, path.CGPath);
            CGContextStrokePath(context);
        }
    }
    
    // current value line
    CGContextSetStrokeColorWithColor(context, _chosenColor.CGColor);
    CGPoint startPoint;
    CGFloat angle;
    startPoint = CGPointMake(CGRectGetWidth(self.bounds)/2, 0.0f);
    angle = 90.0f;
    CustomLine line;
    line.startPoint = startPoint;
    line.endPoint = GetPointFromPointAngleLength(line.startPoint,angle,_rulerHeight);
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:line.startPoint];
    [path addLineToPoint:line.endPoint];
    
    CGContextAddPath(context, path.CGPath);
    CGContextStrokePath(context);
    
    // current value text drawing
    UIFont *font = _textCurrentFont;
    UIFont *smallFont = _textMainFont;
    UIColor *textColor = _chosenColor;
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName : textColor };
    NSDictionary *stringSmallAttrs = @{ NSFontAttributeName : smallFont, NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName : textColor };
    
    NSString *hoursCheck = [NSString stringWithFormat:@"%zd", (NSInteger)(_currentValue / 6)];
    if (hoursCheck.length < 2) {
        hoursCheck = [NSString stringWithFormat:@"0%@",hoursCheck];
    }
    NSString *hours = [NSString stringWithFormat:@"%@", hoursCheck];
    NSString *minutesCheck = [NSString stringWithFormat:@"%zd", ((NSInteger)_currentValue) % 6 * 10];
    if (minutesCheck.length < 2) {
        minutesCheck = [NSString stringWithFormat:@"0%@",minutesCheck];
    }
    NSString *minutes = [NSString stringWithFormat:@" %@", minutesCheck];
    
    NSAttributedString *attrHours = [[NSAttributedString alloc] initWithString:hours attributes:stringAttrs];
    NSAttributedString *attrHourMeasurment = [[NSAttributedString alloc] initWithString:@"h" attributes:stringSmallAttrs];
    NSAttributedString *attrMinutes = [[NSAttributedString alloc] initWithString:minutes attributes:stringAttrs];
    NSAttributedString *attrMinutesMeasurement = [[NSAttributedString alloc] initWithString:@"m" attributes:stringSmallAttrs];
    
    NSMutableAttributedString *mAttrStr = [NSMutableAttributedString new];
    [mAttrStr appendAttributedString:attrHours];
    [mAttrStr appendAttributedString:attrHourMeasurment];
    [mAttrStr appendAttributedString:attrMinutes];
    [mAttrStr appendAttributedString:attrMinutesMeasurement];
    
    CGRect textRect = CGRectMake(0, line.endPoint.y, CGRectGetWidth(self.bounds), 100);
    
    [mAttrStr drawInRect:textRect];
    
    // gradient color filling
    CGPoint myStartPoint, myEndPoint;
    UIBezierPath *gradientPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 0.0f, _rulerLength, _rulerHeight + 5)];
    
    myStartPoint.x = 0.0;
    myStartPoint.y = _rulerHeight / 2;
    myEndPoint.x = _rulerLength;
    myEndPoint.y = _rulerHeight / 2;
    CGContextAddPath(context, gradientPath.CGPath);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGGradientRef myGradient;
    CGColorSpaceRef myColorspace;
    size_t num_locations = 4;
    CGFloat locations[4] = { 0.0, 0.25, 0.75, 1.0 };
    CGFloat components[16] =  { 1.0, 1.0, 1.0, 1.0,   // Start color
        1.0, 1.0, 1.0, 0.0,   // Mid Color
        1.0, 1.0, 1.0, 0.0,   // Mid Color
        1.0, 1.0, 1.0, 1.0 }; // End color
    
    myColorspace = CGColorSpaceCreateDeviceRGB();
    myGradient = CGGradientCreateWithColorComponents (myColorspace, components,
                                                      locations, num_locations);
    
    CGContextDrawLinearGradient (context, myGradient, myStartPoint, myEndPoint, 0);
}

#pragma mark - Property Accessors
- (void)setGridColor:(UIColor *)gridColor
{
    _gridColor = gridColor;
    [self setNeedsDisplay];
}

- (void)setChosenColor:(UIColor *)chosenColor
{
    _chosenColor = chosenColor;
    [self setNeedsDisplay];
}

- (void)setTextCurrentFont:(UIFont *)textCurrentFont
{
    _textCurrentFont = textCurrentFont;
    [self setNeedsDisplay];
}

- (void)setTextMainFont:(UIFont *)textMainFont
{
    _textMainFont = textMainFont;
    [self setNeedsDisplay];
}

- (void)setTextSmallFont:(UIFont *)textSmallFont
{
    _textSmallFont = textSmallFont;
    [self setNeedsDisplay];
}

- (NSTimeInterval)interval
{
    NSTimeInterval interval = 0;
    
    NSInteger hours = (NSInteger)(_currentValue / 6);
    NSInteger minutes = ((NSInteger)_currentValue) % 6 * 10;
    
    interval = hours * 60 * 60 + minutes * 60;
    
    return interval;
}


@end
