

#import "UIFont+Customization.h"

@implementation UIFont (Customization)

+ (UIFont *)robotoRegularWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Roboto-Regular" size:size];
}

+ (UIFont *)franklinGothicStdBookCondesedWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ITCFranklinGothicStd-BkCd" size:size];
}

+ (UIFont *)franklinGothicStdBookCondesedMedWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ITCFranklinGothicStd-MdCd" size:size];
}

+ (UIFont *)franklinGothicStdBookMedWithSize:(CGFloat)size
{
    UIFont *font = [UIFont fontWithName:@"ITCFranklinGothicStd-Med" size:size];
    //    NSLog(@"%s %@", __FUNCTION__, font);
    return font;
}

@end
