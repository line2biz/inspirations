

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const SNStepManagerValueChanged;

@interface SNStepManager : NSObject

+ (SNStepManager *)sharedManager;
- (void)loadCurrentDateHistoryWithCompletionHandler:(void (^)(void))completionHandler;

@end
