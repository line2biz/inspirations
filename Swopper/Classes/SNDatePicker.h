

#import <UIKit/UIKit.h>

@interface SNDatePicker : UIControl

@property (nonatomic, strong, readwrite) NSDate *minimumDate;
@property (nonatomic, strong, readwrite) NSDate *maximumDate;
@property (nonatomic, strong, readwrite) NSDate *date;

- (void)setDate:(NSDate *)date animated:(BOOL)animated;
- (void)setMinimumDate:(NSDate *)minimumDate;
- (void)setMaximumDate:(NSDate *)maximumDate;

@end
