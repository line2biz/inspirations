

#import "DMReport+Category.h"

#import "DMUser.h"
#import "DMSession+Category.h"

NSString * const DMReportDayKey = @"day";
NSString * const DMReportMonthKey = @"month";
NSString * const DMReportYeaKey = @"year";

@implementation DMReport (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.date = now;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:now];
    self.year = @(dateComponents.year);
    self.month = @(dateComponents.month);
    self.weekOfYear = @(dateComponents.weekOfYear);
    self.day = @(dateComponents.day);
    
    
}

+ (DMReport *)reportByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMReport class])];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekOfYearCalendarUnit fromDate:date];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day = %d && month == %d && year == %d", dateComponents.day, dateComponents.month, dateComponents.year];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *resulsts = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
    }
    if ([resulsts count]) {
        return [resulsts firstObject];
    }
    
    return nil;
}

- (NSUInteger)calculateCalloriesForSession:(DMSession *)session
{
    double BMR = [[DMUser defaultUser] BMRValue];
    
    
    
    double MET = [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey] * (double)[session countsPerMinute];
    NSTimeInterval interval = [self.interval doubleValue] / 60; // interval is in secs => Minutes
    double result = (BMR * MET / (24 * 60)) * interval / 1000;
    return (int)round(result);
}

- (NSUInteger)calculateCalloriesForAllSession
{
    double BMR = [[DMUser defaultUser] BMRValue];
    
    double avgCountsPerMinute = 0;
    for (DMSession *session in self.sessions) {
        avgCountsPerMinute += [session countsPerMinute];
    }
    if ([self.sessions count]) {
        avgCountsPerMinute = avgCountsPerMinute / self.sessions.count;
    }
    
    double MET = [[NSUserDefaults standardUserDefaults] floatForKey:CPMCalloriewsKey] * (double)avgCountsPerMinute;
    NSTimeInterval interval = [self.interval doubleValue] / 60; // Minutes
    double result = (BMR * MET / (24 * 60)) * interval / 1000;
    return (int)round(result);
}

@end
