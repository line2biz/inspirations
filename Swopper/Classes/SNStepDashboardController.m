
#import "SNStepDashboardController.h"
#import "SNProgressView3D.h"
#import "SNWeekGraph.h"

#import "SNAppearance.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "DMStep+Category.h"
#import "SNStatisticController.h"
#import "SNMenusController.h"
#import "SNAdditionalMath.h"
#import "AnimatedLabel.h"
#import "SNHoursActivityView.h"
#import "SNStepManager.h"

#import "SNDailyStepsController.h"

#import "SNDay.h"

#import <MFSideMenu.h>

@import CoreMotion;

@interface SNStepDashboardController () <SNWeekGraphDelegate, SNWeekGraphDatasource>
{
    NSInteger _loadIdx;
    NSInteger _week;
}

@property (weak, nonatomic) IBOutlet SNProgressView3D *progressView;
@property (weak, nonatomic) IBOutlet SNWeekGraph *weekGraph;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet AnimatedLabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelValueDesc;


@property (strong, nonatomic) CMStepCounter *stepCounter;
@property (strong, nonatomic) NSMutableArray *items;


///TODO: It is necessary to remove those properties
@property (nonatomic) NSInteger numberOfSteps;

@property (weak, nonatomic) IBOutlet UILabel *shareLabel;
@property (weak, nonatomic) IBOutlet UILabel *stasticLabel;
@property (weak, nonatomic) IBOutlet UILabel *targetLabel;



@end

@implementation SNStepDashboardController

#pragma mark - Life Cycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _week = -1;
        if (!self.managedObjectContext) {
            self.managedObjectContext = [[DMManager sharedManager] defaultContext];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadIdx = 0;
    
    self.weekGraph.delegate = self;
    self.weekGraph.dataSource = self;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Repeat-Gray"]
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(didTapRestart:)];
    self.navigationItem.rightBarButtonItem = barButton;
    
    
    [SNAppearance customizeViewController:self];
    
    
    if ([self.navigationController childViewControllers].count < 2) {
        [self.navigationItem setHidesBackButton:YES];
        SNMenusController *menusController = (SNMenusController *)[((MFSideMenuContainerViewController *)[SNAppDelegate sharedDelegate].window.rootViewController) leftMenuViewController];;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStylePlain target:menusController action:@selector(hideViewController:)];
        
    }
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self configureFonts];
    
    [self.progressView configureProgressViewWithMaxPoints:1
                                             currentPoint:0
                                                pathColor:[UIColor customLightGrayColor]
                                            progressColor:[UIColor customOrangeColor]
                                                lineWidth:10.0f];
    
    
    self.items = [NSMutableArray arrayWithCapacity:7];
    
    [self configureControls];
    [self startAnimation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self configureControls];
        [self startAnimation];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.valueLabel stopTimer];
}


#pragma mark - Outlet Methods
- (void)didTapRestart:(id)sender {
    
    __weak typeof(self) weakSekf = self;
    [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:^{
        [weakSekf startAnimation];
    }];
    
}
#pragma mark - Controller Methods
- (void)configureFonts
{
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.subtitleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subtitleLabel.font.pointSize];
    self.subtitleLabel.textColor = [UIColor customDarkGrayGolor];
    
    self.valueLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.valueLabel.font.pointSize];
    self.valueLabel.textColor = [UIColor customOrangeColor];
    
    self.textLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.textLabel.font.pointSize];
    self.textLabel.textColor = [UIColor customLightGrayColor];
    
    
    self.subtitleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.subtitleLabel.font.pointSize];
    
    self.labelValueDesc.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelValueDesc.font.pointSize];
    self.labelValueDesc.textColor = [UIColor customLightGrayColor];
    
    [self.progressView setControlType:SNProgressView3DTypeNormal];
}

- (void)configureControls
{
    self.shareLabel.text = LMLocalizedString(@"Ergebnis teilen", nil);
    self.stasticLabel.text = LMLocalizedString(@"Statistik", nil);
    self.targetLabel.text = LMLocalizedString(@"Neues Schrittziel", nil);
    
    NSArray *arrayOfData = [self.weekGraph.week mutableCopy];
    arrayOfData = [arrayOfData sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [obj1 valueForKey:@"date"];
        NSDate *date2 = [obj2 valueForKey:@"date"];
        return [SNAdditionalMath compareDate:date1 withDate:date2];
    }];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [SNAppDelegate currentLocale];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
    dateFormatter.dateFormat = format;
    
    
//    NSDate *date1 = [[arrayOfData firstObject] valueForKey:@"date"];
//    NSDate *date2 = [[arrayOfData lastObject] valueForKey:@"date"];
//    NSString *firstPart = [dateFormat stringFromDate:date1];
//    NSString *secondPart = [dateFormat stringFromDate:date2];

    
    self.titleLabel.text = LMLocalizedString(@"Jeder Schritt zählt", nil);
    self.subtitleLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:[NSDate date]]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    NSInteger diff = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
    
    self.textLabel.text = [NSString stringWithFormat:@"%@: %@",LMLocalizedString(@"Schrittziel", nil), [AnimatedLabel textWithThousendsFormat:diff]];
    self.labelValueDesc.text = LMLocalizedString(@"Schritte", nil);
    
    NSInteger maxPoint = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
    
//    [self.weekGraph setWeek:self.items];
    self.weekGraph.minimumAmount = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
    self.weekGraph.maxAmount = maxPoint;
    [self.weekGraph setTextFont:[UIFont fontWithName:@"Arial" size:13]];
}

- (void)startAnimation
{
    [self.valueLabel stopTimer];
    [self.textLabel setHidden:YES];
    DMStep *currentStep = [DMStep stepByDate:self.weekGraph.currentDate inContext:self.managedObjectContext];
    NSUInteger numberOfSteps = currentStep.numberOfSteps ? [currentStep.numberOfSteps integerValue] : 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
    NSInteger diff = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
    if (numberOfSteps > 0) {
        [self.valueLabel animateTextFromMinValue:0
                                      toMaxValue:numberOfSteps
                                      withPrefix:nil
                                          suffix:nil
                                       maxPrefix:nil
                                       maxSuffix:nil
                                        duration:3.0f
                                        andSteps:40
                                 andCompletition:^{
                                     [_textLabel setHidden:NO];
                                 }];
        [self.progressView animateToValue:(CGFloat)numberOfSteps
                                  withMax:diff
                             withDuration:3.0f];
        
    } else {
        self.valueLabel.text = @"0";
        [self.progressView setCurrentPoint:0.0f];
        [_textLabel setHidden:NO];
    }
}

#pragma mark - Temporay Method
- (NSArray *)tempDataForWeek:(NSInteger)week
{    
    NSDate *weekDate = [NSDate dateWithTimeInterval:24*60*60*7*week sinceDate:[NSDate date]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    
    NSDateComponents *dateComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:weekDate];
    dateComponents.weekday = 2; // Moday
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    NSDate *firstDate = [calendar dateFromComponents:dateComponents];
    
    dateComponents = [NSDateComponents new];
    dateComponents.day = 7;
    dateComponents.second = -1;
    
    NSMutableArray *mTestArray = [NSMutableArray new];
    for (NSInteger i = 0; i < 7; i++) {
        NSInteger timeInterval = 24*60*60*i;
        SNDay *day = [[SNDay alloc] init];
        day.date = [NSDate dateWithTimeInterval:timeInterval sinceDate:firstDate];
        
        
        DMStep *step = [DMStep stepByDate:day.date inContext:self.managedObjectContext];
        NSLog(@"%@  :   %@", day.date, step.numberOfSteps);
        
        if (step) {
            day.swoops = [step.numberOfSteps integerValue];
            day.steps = [step.numberOfSteps integerValue];
            day.calories = [step.numberOfSteps integerValue];
        } else {
            day.swoops = 0;
            day.steps = 0;
            day.calories = 0;
            NSLog(@"EMPTY");
        }
        
        [mTestArray addObject:day];
    }
    self.items = mTestArray;
    return mTestArray;
    
}

#pragma mark - Share Results
- (void)shareResults {
    
    UIView *viewToRender = self.tableView.tableHeaderView;
    UIGraphicsBeginImageContextWithOptions(viewToRender.bounds.size, YES, 0);
    [viewToRender drawViewHierarchyInRect:viewToRender.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *objectsToShare = @[@"", image];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - SNWeekGraph Data Source
- (NSArray *)weekGraphView:(SNWeekGraph *)weekGraphView willChangeWeekForward:(BOOL)forward
{
    if (forward && _week == 0) {
        return nil;
    }
    
    if (forward) {
        _week++;
    } else {
        _week--;
    }
    return [self tempDataForWeek:_week];
}

#pragma mark - SNWeekGraph Delegate
- (void)weekGraphView:(SNWeekGraph *)weekGraphView changedCurrentDate:(NSDate *)date
{
    NSDate *dayDate = date;
    if ([SNAdditionalMath compareDate:dayDate withDate:[NSDate date]] == NSOrderedDescending) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weekGraphView.currentDate = [NSDate date];
        });
    } else {
        [self startAnimation];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [SNAppDelegate currentLocale];
        NSString *format = [NSDateFormatter dateFormatFromTemplate:@"EEEE d MMMM" options:0 locale:[SNAppDelegate currentLocale]];
        dateFormatter.dateFormat = format;
        self.subtitleLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:date]];
    }
}

- (void)weekGraphView:(SNWeekGraph *)weekGraphView didChangeWeek:(NSArray *)week
{
    
}

#pragma mark - Table View Delegate 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            [self shareResults];
        }
            break;
        
        case 1:
        {
            SNDailyStepsController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNDailyStepsController class])];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
            
        default:
            break;
    }
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == 2) {
//        UIEdgeInsets insets = UIEdgeInsetsMake(0, 50, 0, 0);
//        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//            [cell setSeparatorInset:insets];
//        }
//        
//        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//            [cell setLayoutMargins:insets];
//        }
//    }
//}


@end
