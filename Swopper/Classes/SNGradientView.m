

#import "SNGradientView.h"

@interface SNGradientView()
{
    CGGradientRef _gradientRef;
}
@end

@implementation SNGradientView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef aRef = UIGraphicsGetCurrentContext();
    CGContextSaveGState(aRef);
    
    const CGFloat *startComponets = CGColorGetComponents(_startColor.CGColor);
    CGFloat redStart    = startComponets[0];
    CGFloat greenStart  = startComponets[1];
    CGFloat blueStart   = startComponets[2];
    CGFloat alphaStart  = startComponets[3];
    
    const CGFloat *endComponents = CGColorGetComponents(_endColor.CGColor);
    CGFloat redEnd      = endComponents[0];
    CGFloat greenEnd    = endComponents[1];
    CGFloat blueEnd     = endComponents[2];
    CGFloat alphaEnd    = endComponents[3];
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGFloat colors[] =
    {
        redStart, greenStart, blueStart, alphaStart,
        redEnd, greenEnd, blueEnd, alphaEnd,
    };
    
    _gradientRef = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors) / (sizeof(colors[0]) * 4));
    CGColorSpaceRelease(rgb);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint start = rect.origin;
    start.y = 0;
    CGPoint end = CGPointMake(rect.origin.x, rect.size.height);
    CGContextDrawLinearGradient(context, _gradientRef, start, end, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    
    UIColor *grayColor = [UIColor colorWithRed:210/255.f green:210/255.f blue:210/255.f alpha:1];
    CGContextSetStrokeColorWithColor(context, grayColor.CGColor);
    CGFloat lineWidth = 1.f;
    CGContextSetLineWidth(context, lineWidth);
    
    CGContextMoveToPoint(context, 0.0f, CGRectGetHeight(self.frame) - lineWidth / 2); //start at this point
    
    CGContextAddLineToPoint(context, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) - lineWidth / 2); //draw to this point
    
    // and now draw the Path!
    CGContextStrokePath(context);
    
    
    CGContextRestoreGState(aRef);
}

@end
