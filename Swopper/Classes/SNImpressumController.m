

#import "SNImpressumController.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@interface SNImpressumController ()
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewMain;
@property (weak, nonatomic) IBOutlet UIView *viewCutLine;

@end

@implementation SNImpressumController


#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    
    [self controllerStart];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self createTextCut];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    self.textViewMain.backgroundColor = [UIColor whiteColor];
    self.viewCutLine.backgroundColor = [UIColor whiteColor];
    
    [self configureFonts];
    [self configureText];
    
    self.textViewMain.dataDetectorTypes = UIDataDetectorTypeLink;
}

- (void)configureFonts
{
    self.labelTitle.textColor = [UIColor titleColor];
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
}

- (void)configureText
{
    NSString *sTitle = LMLocalizedString(@"Über aeris GmbH", nil);
    NSString *sTop = LMLocalizedString(@"Hans-Stießberger-Straße 2a,\nD-85540 Haar bei München\nTel.: +49 89 900506-0\nFax: +49 89 903939-1\nMail: info@aeris.de", nil);
    NSString *sCenter =  LMLocalizedString(@"aeris GmbH: Sitz Haar bei München\nAmtsgericht München HRB 210854\nGeschäftsführer: Josef Glöckl\nWeb: www.aeris.de", nil);
    
    self.labelTitle.text = sTitle;
    
    self.textViewMain.textColor = [UIColor customDarkGrayGolor];
    self.textViewMain.font = [UIFont franklinGothicStdBookCondesedWithSize:17.0f];
    self.textViewMain.text = [NSString stringWithFormat:@"%@\n\n%@", sTop, sCenter];
    
    self.textViewMain.linkTextAttributes = @{NSForegroundColorAttributeName:[UIColor customOrangeColor]};
}

- (void)createTextCut
{
    CAGradientLayer *gradLayer = [CAGradientLayer layer];
    gradLayer.frame = self.viewCutLine.bounds;
    gradLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f], nil];
    gradLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1.0f alpha:0.0f] CGColor], (id)[[UIColor colorWithWhite:1.0f alpha:0.75f] CGColor], nil];
    gradLayer.startPoint = CGPointMake(0, 0);
    gradLayer.endPoint = CGPointMake(0, 1);
    
    [self.viewCutLine.layer setBackgroundColor:[UIColor clearColor].CGColor];
    [self.viewCutLine.layer addSublayer:gradLayer];
}

@end
