

#import <UIKit/UIKit.h>

@protocol SNCountdownControlDelegate;

@interface SNCountdownControl : UIControl

@property (assign, nonatomic) NSTimeInterval maxTime; // in seconds
@property (assign, readonly, nonatomic) NSInteger currentTime; // in seconds
@property (assign, readonly, nonatomic) BOOL isInProgress;
@property (strong, nonatomic) UIColor *colorToDraw;
@property (strong, nonatomic) UIColor *colorForPath;
@property (assign, nonatomic) BOOL playBeep;

@property (strong, nonatomic) id <SNCountdownControlDelegate> delegate;

- (void)startCountdown;
- (void)stopCountdown;
- (void)stopCountdownWithoutBackJump:(BOOL)withoutBack;

@end


@protocol SNCountdownControlDelegate <NSObject>

@optional
- (void)countdownControl:(SNCountdownControl *)control timerFired:(BOOL)fired;

@end