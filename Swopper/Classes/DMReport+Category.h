

#import "DMReport.h"

@interface DMReport (Category)

+ (DMReport *)reportByDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
- (NSUInteger)calculateCalloriesForSession:(DMSession *)session;
- (NSUInteger)calculateCalloriesForAllSession;

@end
