
#import "SNAppearance.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

@implementation SNAppearance

#pragma mark - Navigation Bar
+ (void)customizeNavigationBar
{
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    [navigationBar setShadowImage:[UIImage imageNamed:@"Nav-Shadow"]];
    [navigationBar setBackgroundImage:[UIImage imageNamed:@"Nav-Bkg"] forBarMetrics:UIBarMetricsDefault];
    
    [navigationBar setBarTintColor:[UIColor clearColor]];
    
    NSDictionary *attributes = @{
                                NSFontAttributeName : [UIFont franklinGothicStdBookCondesedWithSize:24],
                                NSForegroundColorAttributeName : [UIColor customDarkGrayGolor]
                                };
    
    [navigationBar setTitleTextAttributes:attributes];
    
    navigationBar.tintColor = [UIColor titleColor];
    
    
    attributes = @{
                   NSFontAttributeName : [UIFont franklinGothicStdBookCondesedWithSize:20],
                   NSForegroundColorAttributeName : [UIColor titleColor]
                   };
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
}

#pragma mark - Public Class Methods
+ (void)customizeAppearance
{
    [self customizeNavigationBar];
}

+ (void)customizeViewController:(UIViewController *)viewController
{
    [SNAppearance customizeViewController:viewController withTitleImage:YES];
}

+ (void)customizeViewController:(UIViewController *)viewController withTitleImage:(BOOL)titleImage
{
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil) style:UIBarButtonItemStylePlain target:nil action:NULL];
    
    if (titleImage) {
        viewController.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo-Small"]];
    }
    
//    if (![viewController isKindOfClass:[UITableViewController class]]) {
//        viewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
//        view.backgroundColor = [UIColor colorWithWhite:1 alpha:.99f];
//        [viewController.view addSubview:view];        
//    }
}

+ (void)findAndMarkWord:(NSString *)word inLabel:(UILabel *)label withNewFont:(UIFont *)font
{
    [SNAppearance findAndMarkWord:word textBenderView:label withNewFont:font newColor:nil];
}

+ (void)findAndMarkWord:(NSString *)word textBenderView:(UIView *)view withNewFont:(UIFont *)font newColor:(UIColor *)newColor
{
    NSString *string;
    NSRange range;
    UIColor *color;
    UIFont *simpleFont;
    UIFont *markedFont;
    if ([view isKindOfClass:[UILabel class]]) {
        string = ((UILabel *)view).text;
        range = [string rangeOfString: word];
        color = [((UILabel *)view) textColor];
        simpleFont = [((UILabel *)view) font];
        markedFont = (font) ? font : simpleFont;
    } else if ([view isKindOfClass:[UITextView class]]) {
        string = ((UITextView *)view).text;
        range = [string rangeOfString: word];
        color = [((UITextView *)view) textColor];
        simpleFont = [((UITextView *)view) font];
        markedFont = (font) ? font : simpleFont;
    }
    if (range.length) {
        NSMutableAttributedString *mAttrStr = [NSMutableAttributedString new];
        NSDictionary *simpleAttributes = @{NSFontAttributeName : simpleFont, NSForegroundColorAttributeName : color};
        NSDictionary *markedAttributes = @{NSFontAttributeName : markedFont, NSForegroundColorAttributeName : (newColor) ? newColor : color};
        
        NSString *firstPart = [string substringToIndex:range.location];
        if (firstPart.length) {
            NSAttributedString *firstAttrStr = [[NSAttributedString alloc] initWithString:firstPart attributes:simpleAttributes];
            [mAttrStr appendAttributedString:firstAttrStr];
        }
        
        NSString *markedPart = [string substringWithRange:range];
        if (markedPart.length) {
            NSAttributedString *markedAttrStr = [[NSAttributedString alloc] initWithString:markedPart attributes:markedAttributes];
            [mAttrStr appendAttributedString:markedAttrStr];
        }
        
        NSString *finalPart = [string substringFromIndex:range.location+range.length];
        if (finalPart.length) {
            NSAttributedString *finalAttrtr = [[NSAttributedString alloc] initWithString:finalPart attributes:simpleAttributes];
            [mAttrStr appendAttributedString:finalAttrtr];
        }
        
        if ([view isKindOfClass:[UILabel class]]) {
            [((UILabel *)view) setAttributedText:mAttrStr];
        } else if ([view isKindOfClass:[UITextView class]]) {
            [((UITextView *)view) setAttributedText:mAttrStr];
        }
    }
}

/// find and mark word in button title with responding state
+ (void)findAndMarkWord:(NSString *)word inButton:(UIButton *)button forState:(UIControlState)state withNewFont:(UIFont *)font
{
    NSString *string = [button titleForState:state];
    NSRange range = [string rangeOfString: word];
    UIColor *color = [button titleColorForState:state];
    UIFont *simpleFont = button.titleLabel.font;
    UIFont *markedFont = font;
    if (range.length) {
        NSMutableAttributedString *mAttrStr = [NSMutableAttributedString new];
        NSDictionary *simpleAttributes = @{NSFontAttributeName : simpleFont, NSForegroundColorAttributeName : color};
        NSDictionary *markedAttributes = @{NSFontAttributeName : markedFont, NSForegroundColorAttributeName : color};
        
        NSString *firstPart = [string substringToIndex:range.location];
        if (firstPart.length) {
            NSAttributedString *firstAttrStr = [[NSAttributedString alloc] initWithString:firstPart attributes:simpleAttributes];
            [mAttrStr appendAttributedString:firstAttrStr];
        }
        
        NSString *markedPart = [string substringWithRange:range];
        if (markedPart.length) {
            NSAttributedString *markedAttrStr = [[NSAttributedString alloc] initWithString:markedPart attributes:markedAttributes];
            [mAttrStr appendAttributedString:markedAttrStr];
        }
        
        NSString *finalPart = [string substringFromIndex:range.location+range.length];
        if (finalPart.length) {
            NSAttributedString *finalAttrtr = [[NSAttributedString alloc] initWithString:finalPart attributes:simpleAttributes];
            [mAttrStr appendAttributedString:finalAttrtr];
        }
        
        [button setTitle:nil forState:state];
        [button setAttributedTitle:mAttrStr forState:state];
    }
}

+ (NSAttributedString *)findAndMarkWord:(NSString *)sWord
                     inAttributedString:(NSAttributedString *)asTarget
                              withColor:(UIColor *)newColor
                                   font:(UIFont *)newFont
                              atIndexes:(NSArray *)aIndexes
{
    NSMutableAttributedString *masTarget = [asTarget mutableCopy];
    if (newColor || newFont) {
        
        NSMutableDictionary *mdNewAttributes = [NSMutableDictionary dictionaryWithCapacity:2];
        if (newColor) {
            [mdNewAttributes setObject:newColor forKey:NSForegroundColorAttributeName];
        }
        if (newFont) {
            [mdNewAttributes setObject:newFont forKey:NSFontAttributeName];
        }
        
        NSMutableArray *maFoundIndexes = [NSMutableArray new];
        
        for (NSInteger i=0; i<masTarget.length - 1 - sWord.length; i++) {
            NSRange range = NSMakeRange(i, sWord.length);
            NSString *sSubString = [[masTarget string] substringWithRange:range];
            
            if ([sSubString isEqualToString:sWord]) {
                if (aIndexes.count) {
                    [maFoundIndexes addObject:@(i)];
                } else {
                    [masTarget addAttributes:mdNewAttributes range:range];
                }
            }
        }
        
        if (maFoundIndexes.count) {
            for (NSNumber *nIndex in aIndexes) {
                if ([nIndex integerValue] < maFoundIndexes.count && [nIndex integerValue] > -1) {
                    
                    NSNumber *nFoundIndex = [maFoundIndexes objectAtIndex:[nIndex integerValue]];
                    
                    NSRange range = NSMakeRange([nFoundIndex integerValue], sWord.length);
                    [masTarget addAttributes:mdNewAttributes range:range];
                }
            }
        }
        
    } else {
        
        NSLog(@"\n%s:\nThere is no new attribute parametr",__FUNCTION__);
    }
    
    return masTarget;
}

#pragma mark - Time Formatter
+ (NSString *)timeStringFromSeconds:(NSInteger)seconds
{
    NSString *time;
    
    NSInteger min = 60;
    NSInteger hour = 60 * min;
    NSInteger day = 24 * hour;
    NSInteger week = 7 * day;
    
    typedef NSString * (^TimeFormatterBlock) (NSInteger, NSInteger, NSInteger, NSString *, NSString *);
    TimeFormatterBlock block = ^(NSInteger MAX, NSInteger MIN, NSInteger seconds, NSString *maxLetter, NSString *minLetter) {
        NSString *result;
        NSInteger a = (seconds/MAX - (seconds%MAX)/MAX);
        NSInteger b = seconds - a*MAX;
        NSInteger c = (b/MIN - (b%MIN)/MIN);
        result = [NSString stringWithFormat:@"%zd : %zd",a,c];
        
        return result;
    };
    
    if (seconds >= week) {
        time = block(week, day, seconds, @"W", @"D");
    } else if (seconds >= day && seconds < week) {
        time = block(day, hour, seconds, @"D", @"h");
    } else if (seconds >= hour && seconds < day) {
        time = block(hour, min, seconds, @"h", @"m");
    } else if (seconds >= min && seconds < hour) {
        time = block(min, 1, seconds, @"m", @"s");
    } else {
        time = [NSString stringWithFormat:@"%zds",seconds];
    }
    
    return time;
}

#pragma mark -
+ (NSString *)textWithThousendsFormat:(NSInteger)amount
{
    NSNumber *nAmount = [NSNumber numberWithInteger:amount];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString *sLang = [[SNLocalizationManager sharedManager] getLanguage];
    if ([sLang isEqualToString:@"en"]) {
        sLang = [sLang stringByAppendingString:@"_US"];
    } else {
        sLang = [sLang stringByAppendingString:@"_DE"];
    }
    numberFormatter.locale = [NSLocale localeWithLocaleIdentifier:sLang];
    
    return [numberFormatter stringFromNumber:nAmount];
}



@end
