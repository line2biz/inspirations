

#import "SNCurvedWavesView.h"

#import "SNAdditionalMath.h"

@interface SNCurvedWavesView()
{
    UIColor *_colorOfLines;
    CGPoint _center;
    CGFloat _radius;
    CGFloat _lineWidth;
    
    NSTimer *_timer;
    
    BOOL _timerWasStoppedManually;
}

@property (assign, nonatomic) CGFloat startAngle;

@end

@implementation SNCurvedWavesView

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor clearColor];
    
    _colorOfLines = [UIColor colorWithRed:188/255.0 green:188/255.0 blue:188/255.0 alpha:1.0];
    _center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    _radius = MIN(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    _lineWidth = 0.5f;
    _multiplier = 0.05f;
    
    _timerWasStoppedManually = NO;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self startTimer:YES];
}

- (void)dealloc
{
    [self startTimer:NO];
}

#pragma mark - Private Methods
- (void)layoutSubviews
{
    [super layoutSubviews];
    
}


- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // create custom circle from amount of points
    NSInteger amountOfPoints = 16;
    NSMutableArray *mArrayOfCustomPoints = [NSMutableArray new];
    
    for (NSInteger i=0; i<amountOfPoints; i++) {
        CustomLine line;
        line.startPoint = _center;
        CGFloat endDegree = _startAngle + i*360/(CGFloat)amountOfPoints;
        line.endPoint = GetPointFromPointAngleLength(line.startPoint,endDegree,_radius*0.70 - _lineWidth);
        AddPointToArray(line.endPoint, mArrayOfCustomPoints);
    }
    
    // =========  curves ===========
    __block NSMutableSet *mSetOfExceptions = [NSMutableSet new];
    for (NSInteger i=0; i<mArrayOfCustomPoints.count; i++) {
        if ((i%2)) {
            [mSetOfExceptions addObject:@(i)];
        }
    }
    
    // create new points
    NSMutableArray *mArrayOfDistanceChanges = [NSMutableArray new];
    NSMutableArray *mArrayOfNewPoints = [NSMutableArray new];
    for (NSInteger i=0; i<mArrayOfCustomPoints.count; i++) {
        CGPoint point = GetPointFromArrayAtIndex(mArrayOfCustomPoints,i);
        if (![mSetOfExceptions containsObject:@(i)]) {
            CustomLine line;
            line.startPoint = _center;
            line.endPoint = point;
            
            CGFloat prevDistance = GetLineLength(line) - _lineWidth;
            CGFloat newDistance = GetLineLength(line) + _radius*_multiplier - _lineWidth;
            CGFloat difference = newDistance - prevDistance;
            [mArrayOfDistanceChanges addObject:[NSNumber numberWithFloat:difference]];
            
            point = GetPointFromPointAngleLength(_center,GetAngleOfLine(line),newDistance);
        } else {
            CustomLine line;
            line.startPoint = _center;
            line.endPoint = point;
            
            CGFloat prevDistance = GetLineLength(line) - _lineWidth;
            CGFloat newDistance = GetLineLength(line) - (_radius*_multiplier)/4 - _lineWidth;
            CGFloat difference = prevDistance - newDistance;
            [mArrayOfDistanceChanges addObject:[NSNumber numberWithFloat:difference]];
            
            point = GetPointFromPointAngleLength(_center,GetAngleOfLine(line),newDistance);
        }
        AddPointToArray(point,mArrayOfNewPoints);
    }
    
    NSInteger addCurvesAmount = 20;
    
    // create changes for length
    NSMutableArray *mArrayOfLengthChanges = [NSMutableArray new];
    
    for (NSInteger i=0; i < addCurvesAmount * mArrayOfNewPoints.count; i++) {
        CGFloat distanceChange = 0.0f;
        if (i < mArrayOfNewPoints.count) {
            if (![mSetOfExceptions containsObject:@(i)]) {
                distanceChange += [[mArrayOfDistanceChanges objectAtIndex:i/2] floatValue]/addCurvesAmount;
            } else {
                distanceChange -= [[mArrayOfDistanceChanges objectAtIndex:i/2] floatValue]/addCurvesAmount;
            }
        } else {
            NSNumber *change = [mArrayOfLengthChanges objectAtIndex:i - mArrayOfNewPoints.count];
            if ([change floatValue] != 0) {
                NSInteger dirrection = ([mSetOfExceptions containsObject:@(i%mArrayOfNewPoints.count)]) ? -1 : 1;
                distanceChange += dirrection * [[mArrayOfDistanceChanges objectAtIndex:i%mArrayOfNewPoints.count /2] floatValue]/addCurvesAmount + [change floatValue];
            }
        }
        NSNumber *change = [NSNumber numberWithFloat:distanceChange];
        [mArrayOfLengthChanges addObject:change];
    }
    
    // draw all curves
    for (NSInteger i = 0; i<addCurvesAmount; i++) {
        const CGFloat* components = CGColorGetComponents(_colorOfLines.CGColor);
        UIColor *newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:1.0 - 1.0/addCurvesAmount*i];
        UIBezierPath *newCurveBezier = [UIBezierPath bezierPath];
        
        NSMutableArray *mArrayOfPointForInterpolation = [NSMutableArray new];
        for (NSInteger a = 0; a < mArrayOfNewPoints.count; a++) {
            CGPoint startPoint = GetPointFromArrayAtIndex(mArrayOfNewPoints, a);
            CGFloat changeInLegth = [[mArrayOfLengthChanges objectAtIndex:a + i*mArrayOfNewPoints.count] floatValue];
            CustomLine lineToStart;
            lineToStart.startPoint = _center;
            lineToStart.endPoint = startPoint;
            startPoint = GetPointFromPointAngleLength(lineToStart.startPoint,GetAngleOfLine(lineToStart),GetLineLength(lineToStart)-changeInLegth);
            AddPointToArray(startPoint,mArrayOfPointForInterpolation);
        }
        newCurveBezier = [UIBezierPath interpolateCGPointsWithCatmullRom:mArrayOfPointForInterpolation closed:YES alpha:0.95];
        CGContextAddPath(context, newCurveBezier.CGPath);
        CGContextSetStrokeColorWithColor(context, newColor.CGColor);
        CGContextSetLineWidth(context, _lineWidth);
        CGContextStrokePath(context);
    }
}

#pragma mark Timer Methods
- (void)startTimer:(BOOL)start
{
    if (start) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                  target:self
                                                selector:@selector(timerFired)
                                                userInfo:nil
                                                 repeats:YES];
    } else {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)timerFired
{
    if (!_timerWasStoppedManually) {
        self.multiplier -= 0.005f;
        self.startAngle += 0.1f;
    } else {
        [self setNeedsDisplay];
    }
}

#pragma mark - Public Methods
- (void)turnTimerOff:(BOOL)turnOff
{
    if (turnOff) {
        _timerWasStoppedManually = YES;
    } else {
        _timerWasStoppedManually = NO;
    }
}


#pragma mark - Property Accessors
- (void)setStartAngle:(CGFloat)startAngle
{
    if (startAngle > 359.9f) {
        startAngle = 0;
    }
    _startAngle = startAngle;
    [self setNeedsDisplay];
}

- (void)setMultiplier:(CGFloat)multiplier
{
    if (multiplier < 0.001f) {
        multiplier = 0.0f;
    }
    if (multiplier < _multiplier - 0.005f) {
        return;
    }
    _multiplier = multiplier;
}







@end
