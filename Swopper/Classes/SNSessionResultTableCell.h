

#import <UIKit/UIKit.h>
#import "DMManager.h"

@interface SNSessionResultTableCell : UITableViewCell

@property (strong, nonatomic) DMSession *session;
@property (assign, nonatomic) NSInteger iOrder;

@end
