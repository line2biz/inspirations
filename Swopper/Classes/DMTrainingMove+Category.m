//
//  DMTrainingMove+Category.m
//  Swopper
//
//  Created by Denis on 26.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMTrainingMove+Category.h"
#import "DMSession+Category.h"

@implementation DMTrainingMove (Category)

+ (DMTrainingMove *)moveByMID:(NSNumber *)mid
{
    DMTrainingMove *move = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"mid = %@", mid];
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count] > 0) {
        move = [results lastObject];
    }
    return move;
}

+ (NSArray *)movesWithSession:(DMSession *)session;
{
    NSArray *arrayOfResults;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"session = %@",session];
    arrayOfResults = [DMTrainingMove getMovesWithPredicate:predicate];
    
    return arrayOfResults;
}

+ (NSArray *)getMovesWithPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    if (predicate) {
        fetchRequest.predicate = predicate;
    }
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    return results;
}

- (CGFloat)calories
{
    CGFloat calories;
    
    return calories;
}


@end
