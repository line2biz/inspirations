
#import <Foundation/Foundation.h>

@class DMReport;

@interface SNReportTemp : NSObject

@property (strong, nonatomic) DMReport *report;
@property (assign, nonatomic) NSInteger avgCMPValue;

@end
