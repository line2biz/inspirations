

#import <UIKit/UIKit.h>

@interface SNCalorieCalculatorTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelCalories;
@property (weak, nonatomic) IBOutlet UILabel *labelCaloriesText;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewEllipse;

@property (assign, nonatomic) BOOL bLast;
@property (assign, nonatomic) double dCalories;

@end
