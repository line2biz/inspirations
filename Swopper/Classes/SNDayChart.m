

#import "SNDayChart.h"
#import "SNAdditionalMath.h"
#import "SNAppearance.h"

@implementation SNDayChart

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.colorSwoops = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1.0];
    self.colorSteps = [UIColor colorWithRed:96/255.0 green:96/255.0 blue:96/255.0 alpha:1.0];
    self.colorCalories = [UIColor colorWithRed:255/255.0 green:102/255.0 blue:36/255.0 alpha:1.0];
    self.colorDayCircle = [UIColor whiteColor];
    self.colorText = self.colorSteps;
    self.colorCurrentDayText = [UIColor orangeColor];
    
    self.annotationSwoops = LMLocalizedString(@"Moves", nil);
    self.annotationSteps = LMLocalizedString(@"Schritte", nil);
    self.annotationCalories = @"kcal";
    
    self.annotationFont = [UIFont fontWithName:@"Arial" size:9];
    self.textFont = [UIFont fontWithName:@"Arial" size:20];
}

#pragma mark - Private Methods
- (void)drawRect:(CGRect)rect
{
    if (!self.week.count) {
        NSLog(@"\n%s\nNo data to show\n",__FUNCTION__);
        return;
    }
    __block BOOL objectDoesntConfirmProtocol = NO;
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj conformsToProtocol:@protocol(SNSummaryChartProtocol)]) {
            objectDoesntConfirmProtocol = YES;
            *stop = YES;
        }
    }];
    
    if (objectDoesntConfirmProtocol) {
        NSLog(@"\n%s\nOne or more objects doesn`t confirm to protocol!\n",__FUNCTION__);
        return;
    }
    
    // **************** additional variables ******************
    CGFloat boundsWidth = CGRectGetWidth(self.bounds);
    CGFloat boundsHeight = CGRectGetHeight(self.bounds);
    
    CGFloat maxPoint = 0.00001f, maxSwoops = 0.00001f, maxSteps = 0.00001f, maxCalories = 0.00001f;
    
    NSInteger totalSwoops = 0, totalSteps = 0, totalCalories = 0;
    
    for (id obj in self.week) {
        NSInteger swoops = 0, steps = 0, calories = 0;
        
        swoops = [[obj valueForKey:@"swoops"] integerValue];
        steps = [[obj valueForKey:@"steps"] integerValue];
        calories = [[obj valueForKey:@"calories"] integerValue];
        
        totalSwoops += swoops;
        totalSteps += steps;
        totalCalories += calories;
        
        // max point value
        if (maxPoint < swoops) {
            maxPoint = swoops;
        }
        if (maxSwoops < swoops) {
            maxSwoops = swoops;
        }
        
        if (maxPoint < steps) {
            maxPoint = steps;
        }
        if (maxSteps < steps) {
            maxSteps = steps;
        }
        
        if (maxPoint < calories) {
            maxPoint = calories;
        }
        if (maxCalories < calories) {
            maxCalories = calories;
        }
    }
    
    __block NSInteger swoppsTotal = 0, stepsTotal = 0, callorisTotal = 0;
    
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSInteger swoops = 0, steps = 0, calories = 0;
        swoops = [[obj valueForKey:@"swoops"] integerValue];
        steps = [[obj valueForKey:@"steps"] integerValue];
        calories = [[obj valueForKey:@"calories"] integerValue];
        
        swoppsTotal += swoops;
        stepsTotal += steps;
        callorisTotal += calories;
    }];
    
    __block NSInteger i = 0;
    [self.week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDate *dayDate = [obj valueForKey:@"date"];
        if ([SNAdditionalMath compareDate:dayDate withDate:self.currentDate] == NSOrderedSame) {
            i = idx;
            *stop = YES;
        }
    }];
    
    // ====== graph ======
    CGFloat graphWidth = boundsWidth/2 * 2/3;
    CGFloat graphHeight = boundsHeight;
    CGFloat dayDiametr = graphWidth * 2/3;
    
    CGRect graphRect = CGRectMake((boundsWidth/2 - graphWidth)/2, self.bounds.origin.y, graphWidth, graphHeight);
    
    // ====== mid lines ======
//    CGFloat midWidth = graphWidth * 1.1;
//    CGFloat widthMidDif = midWidth - graphWidth;
    
//    CGRect midRect = CGRectMake(graphRect.origin.x - widthMidDif/2, graphRect.origin.y, midWidth, boundsHeight - dayDiametr);
//    CGFloat midHeight = CGRectGetHeight(midRect);
    
    // ====== annotation ======
    id object = [self.week objectAtIndex:i];
    NSInteger iSwopps = [[object valueForKey:@"swoops"] integerValue];
    NSInteger iSteps = [[object valueForKey:@"steps"] integerValue];
    NSInteger iCalories = [[object valueForKey:@"calories"] integerValue];
    NSString *strSwopps = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:iSwopps], self.annotationSwoops];
    NSString *strSteps = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:iSteps], self.annotationSteps];
    NSString *strCalories = [NSString stringWithFormat:@"%@ %@", [SNAppearance textWithThousendsFormat:iCalories], self.annotationCalories];
    NSArray *arrayAnnSwoppsObjs = SizeAndAttrStrFromTextWithFontColor(strSwopps, self.annotationFont, self.colorText);
    NSArray *arrayAnnStepsObjs = SizeAndAttrStrFromTextWithFontColor(strSteps, self.annotationFont, self.colorText);
    NSArray *arrayAnnCalObjs = SizeAndAttrStrFromTextWithFontColor(strCalories, self.annotationFont, self.colorText);
    
    // **************** Calculate lines minimum positions *****************
    CGFloat linesStartHeight = graphRect.origin.y + graphHeight - dayDiametr;
    CGFloat linesEndHeight = 0.5f * dayDiametr + linesStartHeight;
    CGFloat minLineHeight = linesEndHeight - linesStartHeight;
    
    // **************** Context *****************
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // **************** draw mid lines *****************
//    CGFloat firstMid = 0.0f, secondMid = 0.0f, thirdMid = 0.0f;
//    NSInteger elementsAmount = [CMStepCounter isStepCountingAvailable] ? 3 : 2;
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSInteger savedValue = [defaults integerForKey:@"MaxNumberOfStepsPerDay"];
//    NSInteger maxStepsPoint = (savedValue) ? savedValue : SNAppMaxNumberOfStepsPerDay;
//    
//    CGFloat difference = (CGFloat)maxSteps / (CGFloat)maxStepsPoint;
//    if ( difference > 1.0f && difference < 2.0f) {
//        secondMid = FloatPartOfNumber(difference);
//    } else if (difference >= 2.0f) {
//        secondMid = 0;
//    } else {
//        secondMid = 1.0;
//    }
//    
//    if (self.week.count > 0 && maxPoint > 0) {
//        firstMid = totalSwoops / self.week.count / maxSwoops;
//        thirdMid = totalCalories / self.week.count / maxCalories;
//    }
//    
//    NSArray *arrayOfMidProgresses;
//    NSArray *arrayOfMidColors;
//    if ([CMStepCounter isStepCountingAvailable]) {
//        arrayOfMidProgresses = [[NSArray alloc] initWithObjects:@(firstMid), @(secondMid), @(thirdMid), nil];
//        arrayOfMidColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorSteps, self.colorCalories, nil];
//    } else {
//        arrayOfMidProgresses = [[NSArray alloc] initWithObjects:@(firstMid), @(thirdMid), nil];
//        arrayOfMidColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorCalories, nil];
//    }
//    
//    
//    for (NSInteger i=0; i<elementsAmount; i++) {
//        UIColor *strikeColor = ((UIColor *)[arrayOfMidColors objectAtIndex:i]);
//        CGFloat progress = [[arrayOfMidProgresses objectAtIndex:i] floatValue];
//        CGFloat midStartY = midRect.origin.y + midHeight - (midHeight - dayDiametr) * progress;
//        UIBezierPath *linePath = [UIBezierPath bezierPath];
//        [linePath moveToPoint:CGPointMake(midRect.origin.x, midStartY)];
//        [linePath addLineToPoint:CGPointMake(midRect.origin.x + midWidth, midStartY)];
//        
//        CGContextAddPath(context, linePath.CGPath);
//        CGContextSetStrokeColorWithColor(context, strikeColor.CGColor);
//        CGContextSetLineWidth(context, 1.0f);
//        
//        NSInteger amount = 75;
//        size_t num_locations = amount;
//        CGFloat dashLength = midWidth / amount;
//        CGFloat lengths[amount];
//        for (NSInteger i = 0; i < amount; i++) {
//            lengths[i] = dashLength;
//        }
//        
//        CGContextSetLineDash(context, 1.0f, lengths, num_locations);
//        CGContextStrokePath(context);
//        num_locations = 0;
//        CGContextSetLineDash(context, 0.0, nil, num_locations);
//    }
    
    // **************** Draw Progress Lines, Days and Percentage Circles *****************
    NSInteger swoops = 0, steps = 0, calories = 0;
    
    id obj;
    if (i < self.week.count) {
        obj = [self.week objectAtIndex:i];
        swoops = [[obj valueForKey:@"swoops"] integerValue];
        steps = [[obj valueForKey:@"steps"] integerValue];
        calories = [[obj valueForKey:@"calories"] integerValue];
    }
    
    CGPoint dayCenterPoint = CGPointMake(graphRect.origin.x + graphWidth/2, graphRect.origin.y + graphHeight - dayDiametr/2);
    CGFloat whiteLineWidth = 0.1f * dayDiametr;
    CGFloat linesWidth = dayDiametr - whiteLineWidth;
    NSInteger linesAmount;
    CGFloat eachLineWidth;
    CGFloat spacing;
    NSArray *arrayOfProgresses;
    NSArray *arrayOfColors;
    
    CGFloat linesStartX = dayCenterPoint.x - linesWidth/2;
    
    // === calculate line progresses here ===
    
    CGFloat firstLineProgress = 0.0f, secondLindeProgress = 0.0f, thirdLineProgress = 0.0f;
    
    firstLineProgress = swoops / maxSwoops;
    secondLindeProgress = steps / maxSteps;
    thirdLineProgress = calories / maxCalories;
    
    linesAmount = 3;
    arrayOfProgresses = [[NSArray alloc] initWithObjects:@(firstLineProgress),@(secondLindeProgress), @(thirdLineProgress), nil];
    arrayOfColors = [[NSArray alloc] initWithObjects:self.colorSwoops, self.colorSteps, self.colorCalories, nil];
    spacing = 0.15*linesWidth;
    eachLineWidth = (linesWidth - spacing * (3 - 1)) / 3;
    
    // === three lines drawing ====
    for (NSInteger j=0; j<linesAmount; j++) {
        NSInteger mul = (j==0) ? 0 : 1;
        CGFloat progressHeight = (graphHeight - dayDiametr) * [[arrayOfProgresses objectAtIndex:j] floatValue];
        CGRect rect = CGRectMake(linesStartX + eachLineWidth*j + mul*spacing*j, linesStartHeight - progressHeight, eachLineWidth, progressHeight + minLineHeight);
        UIBezierPath *lineBezier = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(eachLineWidth/2, eachLineWidth/2)];
        
        CGContextAddPath(context, lineBezier.CGPath);
        UIColor *fillColor = ((UIColor *)[arrayOfColors objectAtIndex:j]);
        
        CGContextSetFillColorWithColor(context, fillColor.CGColor);
        CGContextSetLineWidth(context, 0.0f);
        CGContextFillPath(context);
    }
    
    // === day drawing ===
    UIColor *circleColor = self.colorCalories;
    
    CGFloat radius = dayDiametr/2 - whiteLineWidth/2;
    UIBezierPath *dayPath = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                           radius:radius
                                                       startAngle:RadiansFromDegrees(0)
                                                         endAngle:RadiansFromDegrees(360)
                                                        clockwise:YES];
    CGContextAddPath(context, dayPath.CGPath);
    CGContextSetFillColorWithColor(context, circleColor.CGColor);
    CGContextSetStrokeColorWithColor(context, self.backgroundColor.CGColor);
    CGContextSetLineWidth(context, whiteLineWidth);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    // === percentage circle drawing ===
    CGFloat startAngle = - 90;
    if (obj) {
        if ((swoops == 0 && steps == 0) || steps == 0) {
            UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                  radius:radius - whiteLineWidth
                                                              startAngle:RadiansFromDegrees(startAngle)
                                                                endAngle:RadiansFromDegrees(360)
                                                               clockwise:YES];
            CGContextAddPath(context, circle.CGPath);
            CGContextSetStrokeColorWithColor(context, self.colorSwoops.CGColor);
            CGContextSetLineWidth(context, whiteLineWidth);
            CGContextDrawPath(context, kCGPathStroke);
            
        } else {
            CGFloat swoopEndAngle = 360 * [[obj valueForKey:@"swoppsPercentage"] floatValue] + startAngle;
            UIBezierPath *swoopPercentage = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                           radius:radius - whiteLineWidth
                                                                       startAngle:RadiansFromDegrees(startAngle)
                                                                         endAngle:RadiansFromDegrees(swoopEndAngle)
                                                                        clockwise:YES];
            
            UIBezierPath *stepsPercentage = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                                           radius:radius - whiteLineWidth
                                                                       startAngle:RadiansFromDegrees(swoopEndAngle)
                                                                         endAngle:RadiansFromDegrees(startAngle - 0.00001)
                                                                        clockwise:YES];
            
            CGContextAddPath(context, swoopPercentage.CGPath);
            CGContextSetStrokeColorWithColor(context, self.colorSwoops.CGColor);
            CGContextSetLineWidth(context, whiteLineWidth);
            CGContextDrawPath(context, kCGPathStroke);
            
            if (steps > 0) {
                CGContextAddPath(context, stepsPercentage.CGPath);
                CGContextSetStrokeColorWithColor(context, self.colorSteps.CGColor);
                CGContextSetLineWidth(context, whiteLineWidth);
                CGContextDrawPath(context, kCGPathStroke);
            }
        }
    } else {
        UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:dayCenterPoint
                                                              radius:radius - whiteLineWidth
                                                          startAngle:RadiansFromDegrees(startAngle)
                                                            endAngle:RadiansFromDegrees(360)
                                                           clockwise:YES];
        CGContextAddPath(context, circle.CGPath);
        CGContextSetStrokeColorWithColor(context, self.colorSwoops.CGColor);
        CGContextSetLineWidth(context, whiteLineWidth);
        CGContextDrawPath(context, kCGPathStroke);
    }
    
    
    // === text drawing ===
    UIFont* font = self.textFont;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEEE, d, MMMM"];
    [dateFormat setLocale:[SNAppDelegate currentLocale]];
    NSString *text = @"";
    
    NSString *dayName = [dateFormat stringFromDate:self.currentDate];
    text = [dayName substringToIndex:2];
    
    UIColor *textColor = [UIColor whiteColor];
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
    NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
    CGSize size = [text sizeWithAttributes:stringAttrs];
    CGPoint pointOfDrawing = CGPointMake(dayCenterPoint.x - size.width/2, dayCenterPoint.y - size.height/2);
    [attrStr drawAtPoint:pointOfDrawing];
    
    // ****************** Draw Annotation *********************
    CGSize sizeSwoppsAnn = [[arrayAnnSwoppsObjs firstObject] CGSizeValue];
    CGSize sizeStepsAnn = [[arrayAnnStepsObjs firstObject] CGSizeValue];
    CGSize sizeCalAnn = [[arrayAnnCalObjs firstObject] CGSizeValue];
    
    NSAttributedString *attrStrSwopps = [arrayAnnSwoppsObjs lastObject];
    NSAttributedString *attrStrSteps = [arrayAnnStepsObjs lastObject];
    NSAttributedString *attrStrCal = [arrayAnnCalObjs lastObject];
    
    CGFloat fMarginTop = 5.0;
    
    CGFloat fSumHeight = 0;
    if ([CMStepCounter isStepCountingAvailable]) {
        fSumHeight = sizeSwoppsAnn.height + sizeStepsAnn.height + sizeCalAnn.height + fMarginTop * 2;
    } else {
        fSumHeight = sizeSwoppsAnn.height + sizeCalAnn.height + fMarginTop;
    }
    
    CGFloat fStartY = (boundsHeight - fSumHeight) / 2;
    
    NSInteger iCircleHeight = sizeCalAnn.height;
    CGFloat fStartX = boundsWidth/2 + iCircleHeight/2 + 2;
    
    [attrStrSwopps drawInRect:CGRectMake(fStartX, fStartY, sizeSwoppsAnn.width, sizeSwoppsAnn.height)];
    if ([CMStepCounter isStepCountingAvailable]) {
        [attrStrSteps drawInRect:CGRectMake(fStartX, fStartY + sizeSwoppsAnn.height + fMarginTop, sizeStepsAnn.width, sizeStepsAnn.height)];
        [attrStrCal drawInRect:CGRectMake(fStartX, fStartY + sizeSwoppsAnn.height + sizeStepsAnn.height + fMarginTop*2, sizeCalAnn.width, sizeCalAnn.height)];
    } else {
        [attrStrCal drawInRect:CGRectMake(fStartX, fStartY + sizeSwoppsAnn.height + fMarginTop, sizeCalAnn.width, sizeCalAnn.height)];
    }
    
    fStartX = boundsWidth/2;
    
    UIBezierPath *examplePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fStartX - iCircleHeight/2, fStartY, iCircleHeight, iCircleHeight)];
    CGContextSetFillColorWithColor(context, self.colorSwoops.CGColor);
    CGContextAddPath(context, examplePath.CGPath);
    CGContextSetLineWidth(context, 0.0f);
    CGContextFillPath(context);
    
    if ([CMStepCounter isStepCountingAvailable]) {
        examplePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fStartX - iCircleHeight/2, fStartY + sizeSwoppsAnn.height + fMarginTop, iCircleHeight, iCircleHeight)];
        CGContextSetFillColorWithColor(context, self.colorSteps.CGColor);
        CGContextAddPath(context, examplePath.CGPath);
        CGContextSetLineWidth(context, 0.0f);
        CGContextFillPath(context);
        
        examplePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fStartX - iCircleHeight/2, fStartY + sizeSwoppsAnn.height + sizeStepsAnn.height + fMarginTop*2, iCircleHeight, iCircleHeight)];
        CGContextSetFillColorWithColor(context, self.colorCalories.CGColor);
        CGContextAddPath(context, examplePath.CGPath);
        CGContextSetLineWidth(context, 0.0f);
        CGContextFillPath(context);
    } else {
        examplePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(fStartX - iCircleHeight/2, fStartY + sizeSwoppsAnn.height + fMarginTop, iCircleHeight, iCircleHeight)];
        CGContextSetFillColorWithColor(context, self.colorCalories.CGColor);
        CGContextAddPath(context, examplePath.CGPath);
        CGContextSetLineWidth(context, 0.0f);
        CGContextFillPath(context);
    }
}











@end
