

#import "SNTopTrainingTableCell.h"

#import "SNAppearance.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

#import "DMSession+Category.h"

@implementation SNTopTrainingTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    
    self.labelTitle.text = LMLocalizedString(@"TOP Mitmach-Training", nil);
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelTitle.font.pointSize];
    self.labelTitle.textColor = [UIColor customOrangeColor];
    
    void(^CustomizeLabel)(UILabel *) = ^(UILabel *label) {
        label.textColor = [UIColor customDarkGrayGolor];
        label.font = [UIFont franklinGothicStdBookCondesedWithSize:label.font.pointSize];
    };
    CustomizeLabel(self.labelCalloriesText);
    CustomizeLabel(self.labelCallories);
    CustomizeLabel(self.labelMovesText);
    CustomizeLabel(self.labelMoves);
}

#pragma mark - Private Methods
- (void)configureView
{
    [SNAppearance findAndMarkWord:LMLocalizedString(@"TOP", nil)
                          inLabel:self.labelTitle
                      withNewFont:[UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize]];
    
    self.labelCalloriesText.text = LMLocalizedString(@"kcal", nil);
    self.labelMovesText.text = LMLocalizedString(@"Moves", nil);
    if (self.session) {
        self.labelCallories.text = [SNAppearance textWithThousendsFormat:(NSInteger)[self.session caloriesPerHour]];
        self.labelMoves.text = [SNAppearance textWithThousendsFormat:(NSInteger)([self.session countsPerMinute]*60)];
    } else {
        self.labelCallories.text = @"-";
        self.labelMoves.text = @"-";
    }
    
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSession:(DMSession *)session
{
    if (_session != session) {
        _session = session;
    }
    [self configureView];
}



@end
