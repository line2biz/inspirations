

#import "SNTestMeasurementController.h"

#import "SNGraphView.h"
#import "DMManager.h"
#import "DMSession+Category.h"
#import "DMMotion+Category.h"

static const NSTimeInterval SNTestDeviceMotionMin = 0.34;
static const double SNTestThresholdValue = .8;
static const double SNTestThresholdMin = .1;
static const double SNTestThresholdMax = 2.;
static const NSUInteger SNCountMax = 20;


#define kCMDeviceMotionUpdateFrequency (1.f/15.f)

typedef enum {
    kDeviceMotionGraphTypeAttitude = 0,
    kDeviceMotionGraphTypeRotationRate,
    kDeviceMotionGraphTypeGravity,
    kDeviceMotionGraphTypeUserAcceleration
} DeviceMotionGraphType;

@interface SNTestMeasurementController ()

@property (weak, nonatomic) IBOutlet UISlider *intervalSlider;
@property (weak, nonatomic) IBOutlet UILabel *intervalLabel;


@property (weak, nonatomic) IBOutlet SNGraphView *attitudeGraphView;
@property (weak, nonatomic) IBOutlet SNGraphView *rotationGraphView;
@property (weak, nonatomic) IBOutlet SNGraphView *gravityGraphView;
@property (weak, nonatomic) IBOutlet SNGraphView *accelerationGraphView;

@property (weak, nonatomic) IBOutlet UISlider *thresholdSlider;
@property (weak, nonatomic) IBOutlet UILabel *thresholdLabel;
@property (nonatomic) double thresholdValue;



@property (weak, nonatomic) IBOutlet UILabel *bowLabel;
@property (nonatomic) NSUInteger bowCounter;


@property (strong, nonatomic) NSMutableArray *storeArray;

@property (strong, nonatomic) NSArray *graphViews;
@property (nonatomic) NSTimeInterval timeInterval;


@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) DMSession *session;

@end

@implementation SNTestMeasurementController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.graphViews = @[_attitudeGraphView, _rotationGraphView, _gravityGraphView, _accelerationGraphView];
    
    self.thresholdValue = SNTestThresholdValue;
    
    self.storeArray = [NSMutableArray new];
    self.bowCounter = 0;
    self.intervalSlider.value = SNTestDeviceMotionMin;
    self.thresholdSlider.value = SNTestThresholdValue / SNTestThresholdMax;
    [self configureView];
    
    NSLog(@"%s", __FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startUpdatesWithSliderValue:(int)(self.intervalSlider.value * 100)];
}

- (void)configureView
{
    self.intervalLabel.text = [NSString stringWithFormat:@"%.2f", self.intervalSlider.value];
    self.thresholdLabel.text = [NSString stringWithFormat:@"%.2f", self.thresholdValue];
    self.bowLabel.text = [NSString stringWithFormat:@"%zd", self.bowCounter];
}

#pragma mark - Outlet Methods
- (IBAction)intervalValueChanged:(UISlider *)sender
{
    if (sender == self.intervalSlider) {
        [self startUpdatesWithSliderValue:(int)(sender.value * 100)];
    }
    else if (sender == self.thresholdSlider) {
        self.thresholdValue = MAX(SNTestThresholdMin, sender.value * (SNTestThresholdMax / 1));
    }
    [self configureView];
}

- (IBAction)didTapClearButton:(id)sender
{
    self.bowCounter = 0;
    [self configureView];
}

#pragma mark - Motion Methods
- (void)startUpdatesWithSliderValue:(int)sliderValue
{
    CMMotionManager *mManager = [[SNAppDelegate sharedDelegate] motionManager];
    
    typeof(self) __weak weakSelf = self;
    
    __block double summary = 0;
    
    if ([mManager isDeviceMotionAvailable] == YES) {
        [mManager setDeviceMotionUpdateInterval:kCMDeviceMotionUpdateFrequency];
        [mManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *deviceMotion, NSError *error) {
            
            [weakSelf.rotationGraphView addX:deviceMotion.userAcceleration.x y:deviceMotion.userAcceleration.y z:deviceMotion.userAcceleration.z];
            
            double value = sqrt(pow(ABS(deviceMotion.userAcceleration.x), 2) + pow(ABS(deviceMotion.userAcceleration.y), 2) + pow(ABS(deviceMotion.userAcceleration.z), 2));
            
            
            
            [weakSelf.storeArray insertObject:@(value) atIndex:0];
            
            if ([self.storeArray count] > SNCountMax) {
                [self.storeArray removeLastObject];
            }
            
            if (value >= (4.26 - 2.13)) {
                summary = summary + value;
                NSLog(@"summary: %f + %f", summary, value);
                
            }
            
            
            
            
////            if ([self.storeArray count] > SNCountMax) {
//////                [self.storeArray removeObjectAtIndex:(self.storeArray.count - 1)];
////                [self.storeArray removeLastObject];
////            }
//            
////            NSLog(@"%@", self.storeArray);
////
//            double max = ABS([[self.storeArray valueForKeyPath:@"@max.doubleValue"] doubleValue]);
//            double first = ABS([[self.storeArray firstObject] doubleValue]);
////            double last = ABS([[self.storeArray lastObject] doubleValue]);
//            
//            
//            
//            double bottom = .1;
//            
//            
//            if (first <  bottom && max > self.thresholdValue) {
//                self.bowCounter++;
//                [self configureView];
//                
//                for (NSUInteger idx = 0; idx < [self.storeArray count]; idx++) {
//                    self.storeArray[idx] = @0;
//                }
//                
//                NSLog(@"Count %zd", self.bowCounter);
//            }

            
            
//            if (ABS(deviceMotion.rotationRate.y) > 1) {
//                NSLog(@"y :%f", deviceMotion.rotationRate.y);
//            }
//            
//            if (ABS(deviceMotion.rotationRate.z) > 1) {
//                NSLog(@"z :%f", deviceMotion.rotationRate.z);
//            }
            
            
//            // gravity
//            [[weakSelf.graphViews objectAtIndex:kDeviceMotionGraphTypeGravity] addX:deviceMotion.gravity.x y:deviceMotion.gravity.y z:deviceMotion.gravity.z];
//            // userAcceleration
//            [[weakSelf.graphViews objectAtIndex:kDeviceMotionGraphTypeUserAcceleration] addX:deviceMotion.userAcceleration.x y:deviceMotion.userAcceleration.y z:deviceMotion.userAcceleration.z];
            
//            switch (weakSelf.segmentedControl.selectedSegmentIndex) {
//                case kDeviceMotionGraphTypeAttitude:
//                    [weakSelf setLabelValueRoll:deviceMotion.attitude.roll pitch:deviceMotion.attitude.pitch yaw:deviceMotion.attitude.yaw];
//                    break;
//                case kDeviceMotionGraphTypeRotationRate:
//                    [weakSelf setLabelValueX:deviceMotion.rotationRate.x y:deviceMotion.rotationRate.y z:deviceMotion.rotationRate.z];
//                    break;
//                case kDeviceMotionGraphTypeGravity:
//                    [weakSelf setLabelValueX:deviceMotion.gravity.x y:deviceMotion.gravity.y z:deviceMotion.gravity.z];
//                    break;
//                case kDeviceMotionGraphTypeUserAcceleration:
//                    [weakSelf setLabelValueX:deviceMotion.userAcceleration.x y:deviceMotion.userAcceleration.y z:deviceMotion.userAcceleration.z];
//                    break;
//                default:
//                    break;
//            }
        }];
    }
    
//    self.graphLabel.text = [self.graphTitles objectAtIndex:[self.segmentedControl selectedSegmentIndex]];
//    self.updateIntervalLabel.text = [NSString stringWithFormat:@"%f", updateInterval];
}

- (void)stopUpdates
{
    CMMotionManager *mManager = [[SNAppDelegate sharedDelegate] motionManager];
    if ([mManager isDeviceMotionActive] == YES) {
        [mManager stopDeviceMotionUpdates];
    }
}


@end
