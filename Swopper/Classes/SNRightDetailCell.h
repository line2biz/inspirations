

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SNCellSeparatorType) {
    SNCellSeparatorTypeNone,
    SNCellSeparatorTypeShort,
    SNCellSeparatorTypeLong
};

@interface SNRightDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) NSString *titleText;
@property (strong, nonatomic) NSString *detailText;
@property (assign, nonatomic) SNCellSeparatorType separatorType;

@end
