

#import "SNCalorieCalculatorTableCell.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SNAppearance.h"

@interface SNCalorieCalculatorTableCell ()
{
    CAShapeLayer *_borderLayer;
}
@end

@implementation SNCalorieCalculatorTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    
    self.labelTime.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTime.font.pointSize];
    self.labelTime.textColor = [UIColor customDarkGrayGolor];
    
    self.labelCalories.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelCalories.font.pointSize];
    self.labelCalories.textColor = [UIColor customOrangeColor];
    
    self.labelCaloriesText.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelCaloriesText.font.pointSize];
    self.labelCaloriesText.textColor = [UIColor customDarkGrayGolor];
    self.labelCaloriesText.text = LMLocalizedString(@"kcal", nil);
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.bLast == NO) {
        CGFloat fLineWidth = 0.5f;
        
        if (!_borderLayer) {
            _borderLayer = [CAShapeLayer new];
            _borderLayer.lineWidth = fLineWidth;
            _borderLayer.fillColor = [[UIColor clearColor] CGColor];
            _borderLayer.strokeColor = [[UIColor customLightGrayColor] CGColor];
            [self.layer addSublayer:_borderLayer];
        }
        
        UIBezierPath *bezierPath = [[UIBezierPath alloc] init];
        [bezierPath moveToPoint:CGPointMake(CGRectGetMinX(self.labelTime.frame), CGRectGetHeight(self.bounds) - fLineWidth / 2)];
        [bezierPath addLineToPoint:CGPointMake(CGRectGetMaxX(self.imageViewEllipse.frame) - fLineWidth/2, CGRectGetHeight(self.bounds) - fLineWidth / 2)];
        _borderLayer.path = [bezierPath CGPath];
        _borderLayer.frame = self.bounds;
    }
}

#pragma mark - Private Methods
- (void)configureView
{
    NSString *sCaloriesMultiplier;
    NSInteger iCaloriesValue;
    if (self.dCalories < 1.0f) {
        sCaloriesMultiplier = LMLocalizedString(@"cal", nil);
        iCaloriesValue = (NSInteger)(self.dCalories * 1000);
    } else {
        sCaloriesMultiplier = LMLocalizedString(@"kcal", nil);
        iCaloriesValue = (NSInteger)self.dCalories;
    }
    
    self.labelCalories.text = [SNAppearance textWithThousendsFormat:iCaloriesValue];
    self.labelCaloriesText.text = sCaloriesMultiplier;
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDCalories:(double)dCalories
{
    if (_dCalories != dCalories) {
        _dCalories = dCalories;
        [self configureView];
    }
}

@end
