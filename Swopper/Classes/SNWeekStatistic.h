

#import <UIKit/UIKit.h>
#import "SNDay.h"

@interface SNWeekStatistic : UIControl

@property (strong, nonatomic) NSArray *arrayOfDays;
@property (strong, nonatomic) UIFont *font;

+ (NSArray *)createTestData;

@end
