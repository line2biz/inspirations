//
//  DMTrainingMove+Category.h
//  Swopper
//
//  Created by Denis on 26.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMTrainingMove.h"
@class DMSession;

@interface DMTrainingMove (Category)

+ (DMTrainingMove *)moveByMID:(NSNumber *)mid;
+ (NSArray *)movesWithSession:(DMSession *)session;
+ (NSArray *)getMovesWithPredicate:(NSPredicate *)predicate;

- (CGFloat)calories;

@end
