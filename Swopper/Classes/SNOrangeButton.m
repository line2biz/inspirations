

#import "SNOrangeButton.h"

#import "UIFont+Customization.h"

@implementation SNOrangeButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundImage:[UIImage imageNamed:@"Btn-Bkg-Orange-47-Highlighted"] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[UIImage imageNamed:@"Btn-Bkg-Gray-47"] forState:UIControlStateDisabled];
    
    UIImage *image = [self imageForState:UIControlStateNormal];
    if (image) {
        [self setImage:image forState:UIControlStateHighlighted];
    }
    
    self.titleLabel.font = [UIFont franklinGothicStdBookCondesedWithSize:self.titleLabel.font.pointSize];
}

@end
