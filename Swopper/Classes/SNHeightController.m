

#import "SNHeightController.h"

#import "SNAppearance.h"
#import "DMUser.h"
#import "UIFont+Customization.h"
#import "SNHeightWeightControl.h"
#import "SNOrangeButton.h"

#import "SNAgeController.h"

@interface SNHeightController ()
{
    CGFloat _startCurveAlpha;
    NSInteger _loadIdx;
}
@property (weak, nonatomic) IBOutlet SNHeightWeightControl *heightWeightControl;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet SNOrangeButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveUp;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCurveDown;

@end

@implementation SNHeightController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SNAppearance customizeViewController:self withTitleImage:YES];
    [self configureFonts];
    self.navigationItem.rightBarButtonItem = nil;
    _loadIdx = 0;
    
//    if (![[DMUser defaultUser] isValid]) {
//        [self.navigationItem setHidesBackButton:YES];
//        [self.buttonNext setHidden:NO];
//    } else {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Save"] style:UIBarButtonItemStyleBordered target:self action:@selector(didTapSaveButton:)];
//        [self.buttonNext setHidden:YES];
//    }
    
    self.heightWeightControl.controlType = SNControlTypeHeight;
    if ([[DMUser defaultUser] height]) {
        self.heightWeightControl.currentValue = [[DMUser defaultUser] height];
    }
    
    // right navigation item to change measurement
    
    NSString *sRightButton;
    if ([[DMUser defaultUser] USunits]) {
        sRightButton = @"cm";
        self.heightWeightControl.measureUnit = SNMeasureUnitFeets;
    } else {
        sRightButton = @"ft";
        self.heightWeightControl.measureUnit = SNMeasureUnitMeters;
    }
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:sRightButton
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(changeMeasurementUnit)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    
    // controller start method
    [self controllerStart];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self controllerStart];
    }
    [self startCurveAnimation];
}

#pragma mark - Controller Methods
- (void)controllerStart
{
    // for animation purposes
    [self.nextButton setHidden:YES];
    _startCurveAlpha = self.imageViewCurveDown.alpha;
    self.imageViewCurveDown.alpha = 0.0;
    self.imageViewCurveUp.alpha = 0.0;
}

- (void)configureFonts
{
    NSString *sTitle = [@" " stringByAppendingString:LMLocalizedString(@"Weiter", nil)];
    [self.nextButton setTitle:sTitle forState:UIControlStateNormal];
    
    self.labelTitle.text = LMLocalizedString(@"Kalorien-Berechnung", nil);
    self.labelSubtitle.text = LMLocalizedString(@"Größe", nil);
    
    self.labelTitle.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelTitle.font.pointSize];
    self.labelSubtitle.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubtitle.font.pointSize];
}

- (void)saveChanges
{
    [[DMUser defaultUser] setHeight:(NSInteger)self.heightWeightControl.currentValue];
    [[DMUser defaultUser] save];
}

- (void)changeMeasurementUnit
{
    BOOL USunits = [[DMUser defaultUser] USunits];
    [[DMUser defaultUser] setUSunits:!USunits];
    [[DMUser defaultUser] save];
    
    [self.heightWeightControl changeToAlternativeMeasureUnit];
    NSString *sRightBarButtonItemTitle;
    switch (self.heightWeightControl.measureUnit) {
        case SNMeasureUnitFeets:
            sRightBarButtonItemTitle = @"cm";
            break;
            
        case SNMeasureUnitMeters:
            sRightBarButtonItemTitle = @"ft";
            break;
            
        default:
            break;
    }
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:sRightBarButtonItemTitle
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(changeMeasurementUnit)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

#pragma mark - Outlet Methods
- (IBAction)didTapSaveButton:(id)sender
{
    [self saveChanges];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapNextButton:(id)sender
{
    [self saveChanges];
    if (self.wizard) {
        SNAgeController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNAgeController class])];
        controller.wizard = YES;
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (void)startCurveAnimation
{
    // ********* Preparation Animations **********
    NSTimeInterval duration = 0.001;
    
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D xformDown = CATransform3DIdentity;
    xformDown = CATransform3DTranslate(xformDown, 130.0, 60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformDown];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    
    [CATransaction commit];
    
    CATransform3D xformUp = CATransform3DIdentity;
    xformUp = CATransform3DTranslate(xformUp, -130.0, -60.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xformUp];
    
    [CATransaction begin];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"startTransformAnimation"];
    [CATransaction commit];
    
    [self finishAnimationWithAlpha:_startCurveAlpha];
}

- (void)finishAnimationWithAlpha:(CGFloat)alpha
{
    NSTimeInterval duration = 1.5;
    
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = duration;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transformAnimation.removedOnCompletion = NO;
    transformAnimation.fillMode = kCAFillModeForwards;
    
    CATransform3D normalForm = CATransform3DIdentity;
    normalForm = CATransform3DTranslate(normalForm, 0.0, 0.0, 0.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:normalForm];
    
    __weak typeof(self) weakSelf = self;
    
    [CATransaction setAnimationDuration:duration];
    [CATransaction setCompletionBlock:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.nextButton setHidden:NO];
            });
        }
    }];
    
    [CATransaction begin];
    
    [self.imageViewCurveUp.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [self.imageViewCurveDown.layer addAnimation:transformAnimation forKey:@"endTransformAnimation"];
    [UIView animateWithDuration:duration
                     animations:^{
                         self.imageViewCurveUp.alpha = alpha;
                         self.imageViewCurveDown.alpha = alpha;
                     }];
    
    [CATransaction commit];
    
}

#pragma mark - Orientation Handling
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
