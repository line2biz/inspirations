

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const DMUserSexKey;
FOUNDATION_EXTERN NSString * const DMUserWeightKey;
FOUNDATION_EXTERN NSString * const DMUserHeightKey;
FOUNDATION_EXTERN NSString * const DMUserAgeKey;
FOUNDATION_EXTERN NSString * const DMUserNotificationDate;
FOUNDATION_EXTERN NSString * const DMUserMemoryFirst;
FOUNDATION_EXTERN NSString * const DMUserMemorySecond;

FOUNDATION_EXTERN NSString * const DMUserHourPeriod;
FOUNDATION_EXTERN NSString * const DMUserDayPeriod;
FOUNDATION_EXTERN NSString * const DMUserWeekPeriod;
FOUNDATION_EXTERN NSString * const DMUserBD;
FOUNDATION_EXTERN NSString * const DMUserFirstLaunch;
FOUNDATION_EXTERN NSString * const DMUserFirstExercise;
FOUNDATION_EXTERN NSString * const DMUserUSunits;

typedef NS_ENUM(NSUInteger, DMSexType) {
    DMSexTypeNone,
    DMSexTypeMan,
    DMSexTypeWomen
};

typedef NS_ENUM(NSInteger, WeekPeriodLength) {
    WeekPeriodLengthNever,
    WeekPeriodLengthWorkdays,
    WeekPeriodLengthHolidays,
    WeekPeriodLengthAll
};

@interface DMUser : NSObject <NSCoding>

@property (assign, nonatomic) DMSexType sex;
@property (assign, nonatomic) NSUInteger weight;
@property (assign, nonatomic) NSUInteger height;
@property (assign, nonatomic) NSInteger age;
@property (strong, nonatomic) NSDate *notificationDate;
@property (assign, nonatomic) NSInteger hourPeriod;
@property (assign, nonatomic) NSInteger dayPeriod;
@property (assign, nonatomic) NSInteger weekPeriod;
@property (assign, nonatomic) BOOL memoryFirstSwitchIsOn;
@property (assign, nonatomic) BOOL memorySecondSwitchIsOn;
@property (strong, nonatomic) NSDate *birthDate;
@property (assign, nonatomic) BOOL firstLaunch;
@property (assign, nonatomic) BOOL firstExercise;
@property (assign, nonatomic) BOOL USunits;

+ (DMUser *)defaultUser;
- (void)save;

- (BOOL)isValid;
- (double)BMRValue;
- (CGFloat)strideLength;
- (CGFloat)weightInLbs;

@end
