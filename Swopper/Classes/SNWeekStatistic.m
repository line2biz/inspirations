

#import "SNWeekStatistic.h"

#define DEGREES_TO_RADIANS(angle) (((angle)* M_PI) / 180.0 )

@interface SNWeekStatistic ()

@property (assign, nonatomic) CGFloat minimumValue;
@property (assign, nonatomic) CGFloat maximumValue;
@property (strong, nonatomic) UIColor *lowColor;
@property (strong, nonatomic) UIColor *midColor;
@property (strong, nonatomic) UIColor *highColor;

@end

@implementation SNWeekStatistic

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self= [super initWithCoder:aDecoder];
    if (self) {
        [self defaultInit];
    }
    return self;
}

- (void)defaultInit
{
    _font = [UIFont fontWithName:@"Arial" size:15];
    _lowColor = [UIColor colorWithRed:187/255.0 green:193/255.0 blue:200/255.0 alpha:1.0];
    _midColor = [UIColor colorWithRed:115/255.0 green:122/255.0 blue:130/255.0 alpha:1.0];
    _highColor = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:0/255.0 alpha:1.0];
    self.backgroundColor = [UIColor whiteColor];
}

#pragma mark Custom Types
typedef struct {
	CGPoint startPoint, endPoint;
} CustomLine;

#pragma mark - Drawing
- (void)drawRect:(CGRect)rect
{
    // preparations
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat heigth = CGRectGetHeight(self.bounds);
    
    NSInteger pointsPerHeight = 5;
    
    NSInteger pointsPerWidth = [[self arrayOfDays] count];
    
    CGFloat minShownPoint, maxShownPoint;
    minShownPoint = 0.0f;
    if (self.maximumValue <= 400) {
        maxShownPoint = 400.0f;
    } else {
        maxShownPoint = ((NSInteger)self.maximumValue - ((NSInteger)self.maximumValue % 100)) + 100;
    }
    
    // custom blocks for calculations
    // math blocks
    CGFloat(^SQRF)(CGFloat) = ^(CGFloat value) {
        CGFloat result = value * value;
        return result;
    };
    
    // blocks for lines
    CGFloat (^GetLineLength)(CustomLine) = ^(CustomLine line) {
        CGFloat length = sqrt(SQRF(line.endPoint.x-line.startPoint.x) + SQRF(line.endPoint.y - line.startPoint.y));
        return length;
    };
    
    CGPoint (^GetPointFromPointAngleLength)(CGPoint, CGFloat, CGFloat) = ^(CGPoint point, CGFloat angle, CGFloat length) {
        CGPoint centerPoint = point;
        CGPoint result;
        CGFloat eps = 0.00001;
        result.x = centerPoint.x + length * cosf(DEGREES_TO_RADIANS(angle));
        result.y = centerPoint.y + length * sinf(DEGREES_TO_RADIANS(angle));
        if (result.x < eps) {
            result.x = 0;
        }
        if (result.y < eps) {
            result.y = 0;
        }
        return result;
    };
    
    // blocks for arrays and points
    void(^AddPointToArray)(CGPoint, NSMutableArray*) = ^(CGPoint point, NSMutableArray *mArray) {
        [mArray addObject:[NSValue valueWithCGPoint:point]];
    };
    
    CGPoint(^GetPointFromArrayAtIndex)(NSArray *, NSInteger) = ^(NSArray *array, NSInteger index) {
        NSValue *point = [array objectAtIndex:index];
        CGPoint pointToReturn = [point CGPointValue];
        return pointToReturn;
    };
    
    // create array with points amout from line
    NSArray *(^GetArrayOfPointsFromLineItsAngleAndPointsAmount)(CustomLine, CGFloat, NSInteger) = ^(CustomLine line, CGFloat angle, NSInteger amount) {
        NSMutableArray *mArray = [NSMutableArray new];
        CGFloat linePartLength = GetLineLength(line) / (amount - 1);
        CGPoint prevPoint = line.startPoint;
        AddPointToArray(prevPoint, mArray);
        for (NSInteger i=1; i < amount; i++) {
            CustomLine newLine;
            newLine.startPoint = prevPoint;
            newLine.endPoint = GetPointFromPointAngleLength(newLine.startPoint, angle, linePartLength);
            prevPoint = newLine.endPoint;
            AddPointToArray(prevPoint, mArray);
        }
        return mArray;
    };
    
    // draw vertical line
    UIFont* font = _font;
    NSString *text = @"0000";
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : self.lowColor};
    CGSize size = [text sizeWithAttributes:stringAttrs];
    CustomLine verticalLine;
    CGFloat horizontalMargin = 5;
    verticalLine.startPoint = CGPointMake(size.width / 2 + horizontalMargin, heigth - size.height - 5);
    verticalLine.endPoint = CGPointMake(size.width / 2 + horizontalMargin, size.height/2 - 2.5);
    NSArray *arrayOfVerticalPoints = GetArrayOfPointsFromLineItsAngleAndPointsAmount(verticalLine,-90,pointsPerHeight);
    
    CustomLine valuesLine;
    valuesLine.startPoint = CGPointMake(minShownPoint, 0);
    valuesLine.endPoint = CGPointMake(maxShownPoint, 0);
    NSArray *arrayOfVerticalValuesInPoint = GetArrayOfPointsFromLineItsAngleAndPointsAmount(valuesLine, 0, pointsPerHeight);
    
    for (NSInteger i=0; i<pointsPerHeight; i++) {
        CGPoint textCenter = GetPointFromArrayAtIndex(arrayOfVerticalPoints, i);
        NSString *ntext = [NSString stringWithFormat:@"%.0f",GetPointFromArrayAtIndex(arrayOfVerticalValuesInPoint, i).x];
        NSAttributedString *nattrStr = [[NSAttributedString alloc] initWithString:ntext attributes:stringAttrs];
        size = [text sizeWithAttributes:stringAttrs];
        textCenter.x -= size.width/2;
        textCenter.y -= size.height/2;
        [nattrStr drawAtPoint:textCenter];
    }
    
    // draw horizontal line
    CustomLine horizontalLine;
    horizontalLine.startPoint = CGPointMake(size.width, heigth - size.height);
    horizontalLine.endPoint = CGPointMake(width - size.width, heigth - size.height/2 - 5);
    NSArray *arrayOfHorizontalPoints = GetArrayOfPointsFromLineItsAngleAndPointsAmount(horizontalLine, 0, pointsPerWidth);
    
    [self.arrayOfDays enumerateObjectsUsingBlock:^(SNDay *day, NSUInteger idx, BOOL *stop) {
        CGPoint textCenter = GetPointFromArrayAtIndex(arrayOfHorizontalPoints, idx);
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EE"];
        [dateFormat setLocale:[SNAppDelegate currentLocale]];
        NSString *text = @"";
        NSString *dayName = [dateFormat stringFromDate:day.date];
        text = [dayName substringToIndex:2];
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:text attributes:stringAttrs];
        CGSize nsize = [text sizeWithAttributes:stringAttrs];
        textCenter.x += size.width/2 - nsize.width/2;
        [attrStr drawAtPoint:textCenter];
    }];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.arrayOfDays enumerateObjectsUsingBlock:^(SNDay *day, NSUInteger idx, BOOL *stop) {
        if (day.dayState != SNDayStateFuture) {
            UIColor *drawColor;
            if (day.greatAchievement) {
                drawColor = self.highColor;
            } else {
                if (day.smallAchivement) {
                    drawColor = self.midColor;
                } else {
                    drawColor = self.lowColor;
                }
            }
            CGPoint point = GetPointFromArrayAtIndex(arrayOfHorizontalPoints, idx);
            CGFloat heightOfLine = (heigth - size.height) * (day.points/maxShownPoint);
            CGFloat widhOfLine = size.width * 3 / 4;
            point.x += widhOfLine/2 + size.width/4;
            
            CGRect rect = CGRectMake(point.x - size.width/2, point.y - heightOfLine, widhOfLine, heightOfLine);
            
            UIBezierPath *progressPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:rect.size.width/2];
            CGContextAddPath(context, progressPath.CGPath);
            CGContextSetFillColorWithColor(context, drawColor.CGColor);
            CGContextFillPath(context);
        }
    }];
}


#pragma mark - Private Methods
#pragma mark Calculations
- (void)recalculateMinAndMax
{
    CGFloat min = 0.0f, max = 0.0f;
    for (SNDay *day in self.arrayOfDays) {
        if (min == 0.0f) {
            min = day.points;
        } else {
            if (min > day.points) {
                min = day.points;
            }
        }
        if (max == 0.0f) {
            max = day.points;
        } else {
            if (max < day.points) {
                max = day.points;
            }
        }
    }
    _minimumValue = min;
    _maximumValue = max;
}


#pragma mark - Public Methods
+ (NSArray *)createTestData
{
    NSMutableArray *mArray = [NSMutableArray new];
    for (NSInteger i=0; i<7; i++) {
        SNDayState dayState;
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [gregorian setLocale:[SNAppDelegate currentLocale]];
        NSDateComponents *todayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        NSInteger weekday = [todayComponents weekday];
        
        weekday = ((weekday + 5) % 7) + 1;
        weekday -= 1;
        
        if (i>weekday) {
            dayState = SNDayStateFuture;
        } else if (i == weekday) {
            dayState = SNDayStateInProgress;
        } else {
            dayState = SNDayStateCompleted;
        }
        CGFloat points = arc4random_uniform(501);
        SNDay *day = [[SNDay alloc] initWithMaxPoints:500 points:points dayType:i dayState:dayState];
        [mArray addObject:day];
    }
    
    return mArray;
}

#pragma mark - Property Accessors
- (void)setArrayOfDays:(NSArray *)arrayOfDays
{
    _arrayOfDays = arrayOfDays;
    [self recalculateMinAndMax];
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    [self setNeedsDisplay];
}



@end
