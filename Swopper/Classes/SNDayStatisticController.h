

#import <UIKit/UIKit.h>

@interface SNDayStatisticController : UIViewController

@property (strong, nonatomic) NSArray *week;
@property (strong, nonatomic) NSDate *chosenDate;

@end
