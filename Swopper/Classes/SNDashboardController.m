
#import "SNDashboardController.h"
#import "UIFont+Customization.h"
#import "SNAppearance.h"
#import "DMUser.h"
#import "SNStartController.h"
#import "SNStopwatchController.h"
#import "SNStatisticController.h"

#import "SNDashboardGraphControl.h"
#import "SNReviewController.h"
#import "SNStepDashboardController.h"

@interface SNDashboardController ()
@property (strong, nonatomic) DMUser *user;
@property (weak, nonatomic) IBOutlet UILabel *labelMain;
@property (weak, nonatomic) IBOutlet UILabel *labelSubMain;
@property (weak, nonatomic) IBOutlet SNDashboardGraphControl *graphControl;

@end

@implementation SNDashboardController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [SNAppearance customizeViewController:self];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LMLocalizedString(@"Zurück", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    [SNAppearance customizeViewController:self];
    self.user = [DMUser defaultUser];
    [self configureFonts];
}

#pragma mark - Controller Methods
- (void)configureFonts
{
    self.labelMain.font = [UIFont franklinGothicStdBookCondesedMedWithSize:self.labelMain.font.pointSize];
    self.labelSubMain.font = [UIFont franklinGothicStdBookCondesedWithSize:self.labelSubMain.font.pointSize];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if ([self.user isValid]) {
            [self performSegueWithIdentifier:NSStringFromClass([SNStopwatchController class]) sender:self];
        } else {
            [self performSegueWithIdentifier:NSStringFromClass([SNStartController class]) sender:self];
        }
    }
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:NSStringFromClass([SNStepDashboardController class]) sender:self];
    }
    if (indexPath.row == 2) {
        [self performSegueWithIdentifier:NSStringFromClass([SNStatisticController class]) sender:self];
    }
    if (indexPath.row == 3) {
        [self performSegueWithIdentifier:NSStringFromClass([SNReviewController class]) sender:self];
    }
}

#pragma mark - Trainig Step Controller Protocol
- (void)trainingController:(SNTrainingStep02Controller *)controller finishedTraining:(BOOL)finished
{
    NSString *msg = LMLocalizedString(@"Zur ersten Test-Messung?", nil);
    NSString *cancelBtn = LMLocalizedString(@"Nein", nil);
    NSString *yesBtn = @"Ja";
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:cancelBtn otherButtonTitles:yesBtn, nil];
    [alertView show];
}


@end
