
#import "SNAppDelegate.h"

#import "SNAppearance.h"
#import "DMUser.h"
#import "DMStep+Category.h"
#import "SNStepManager.h"
#import "MFSideMenu.h"

#import <Crittercism/Crittercism.h>

CGFloat const SNMETDefaultValue = 0.01664f;

CGFloat const CPMFactorValue = 1.00379f;

NSString * const CPMFactorValueKey = @"CPMFactorValueKey";
NSString * const CPMCalloriewsKey = @"CPMCalloriewsKey";

CGFloat const TrainingTimePeriodKey = 1.0f;
NSInteger const SNAppMaxNumberOfStepsPerDay = 10000;
NSString * const SNSharedUserKey = @"SNSharedUserKey";
NSString * const CPMFrequencyKey = @"CPMFrequencyKey";

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

CGFloat const SNAppSettingsMinSoundVolumeValue = .2f;

NSString * const SNAppSecondLaucnKey = @"SNAppSecondLaucnKey";

#define BACGROUND_TIME_KEY             @"BACGROUND_TIME_KEY"
#define RELOGIN_TIME_INTERVAL         600                    // sec


@interface SNAppDelegate ()
{
    CMMotionManager *_motionManager;
    NSDate *_goneToBackground, *_appearInForeground;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SNAppDelegate

@dynamic motionManager;

#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 
    // It is necessary to launche single class
    [SNLocalizationManager sharedManager];
    
    [Crittercism enableWithAppID:@"555f09a7b60a7d3e63908d3a"];
    
    [SNAppearance customizeAppearance];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    
//    NSArray *array = [[NSBundle mainBundle] localizations];
//    NSLog(@"%@", array);
//    
//    NSString *sLang = [[[SNAppDelegate currentLocale] localeIdentifier] substringToIndex:2];
//    
//    NSLog(@"%s LANG: %@ sLang :%@", __FUNCTION__, sLang, [[SNAppDelegate currentLocale] localeIdentifier]);
    
    NSDictionary *params = @{ CPMFactorValueKey : @(CPMFactorValue), CPMCalloriewsKey : @(0.017505747), CPMFrequencyKey : @(50.f) };
    [[NSUserDefaults standardUserDefaults] registerDefaults:params];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SNAppSecondLaucnKey]) {
        [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:NULL];
    }
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BACGROUND_TIME_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    [self applicationDocumentsDirectory];
    
    return YES;
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[DMUser defaultUser] save];
}


/**
#pragma mark - Saving and Restoration
- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}
 */

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
    [[NSUserDefaults standardUserDefaults] setDouble:timeInterval forKey:BACGROUND_TIME_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SNAppSecondLaucnKey]) {
        _appearInForeground = [NSDate date];
        NSTimeInterval timeInterval = [[NSUserDefaults standardUserDefaults] doubleForKey:BACGROUND_TIME_KEY];
        NSTimeInterval difference = [[NSDate date] timeIntervalSince1970] - timeInterval;
        if (difference > RELOGIN_TIME_INTERVAL) {
            [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:^{
                MFSideMenuContainerViewController *controllerSideMenu = (MFSideMenuContainerViewController *)self.window.rootViewController;
                UIViewController *controller = [controllerSideMenu leftMenuViewController];
                if ([controller conformsToProtocol:@protocol(SNAppDelegateProtocol)]) {
                    [controller performSelector:@selector(applicationWillAppearInForeground)];
                }
            }];
            
        }        
    }

}

#pragma mark - Property Accessors
- (CMMotionManager *)motionManager
{
        static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _motionManager = [[CMMotionManager alloc] init];
    });
    return _motionManager;
}

+ (NSCalendar *)currentCalendar
{
    return [NSCalendar currentCalendar];
}

+ (NSLocale *)currentLocale
{
    
    return [[NSLocale alloc] initWithLocaleIdentifier:[[SNLocalizationManager sharedManager] getLanguage]];
//    return [[NSLocale alloc] initWithLocaleIdentifier:@"de_DE"];
//    return [SNAppDelegate currentLocale];
}


#pragma mark - Public Class Methods
+ (SNAppDelegate *)sharedDelegate
{
    return (SNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Utility Methods
- (NSURL *)applicationDocumentsDirectory {
    
    NSLog(@"%s %@", __FUNCTION__, [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    return nil;
}


#pragma mark - Nootificantion
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    NSLog(@"%s", __FUNCTION__);
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SNAppSecondLaucnKey] == NO) {
        
        // Defults will be saved when app will enter background
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SNAppSecondLaucnKey];
        [[SNStepManager sharedManager] loadCurrentDateHistoryWithCompletionHandler:NULL];
        
        
    }
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
}


@end
