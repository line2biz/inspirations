
#import "DMStep.h"


@implementation DMStep

@dynamic date;
@dynamic day;
@dynamic numberOfSteps;
@dynamic month;
@dynamic weekOfYear;
@dynamic year;

@end
