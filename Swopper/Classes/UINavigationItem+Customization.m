

#import "UINavigationItem+Customization.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"

@implementation UINavigationItem (Customization)

- (void)setTitle:(NSString *)title
{
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont franklinGothicStdBookCondesedWithSize:22],
                                 NSForegroundColorAttributeName : [UIColor customDarkGrayGolor]
                                 };
    
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:title attributes:attributes];
    CGSize size = CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX);
    CGRect rect = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine context:NULL];
    CGFloat offset = 4;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectInset(rect, 0, offset * (-1))];
    label.backgroundColor = [UIColor clearColor];
    label.attributedText = str;
    UIView *containerView = [[UIView alloc] initWithFrame:label.bounds];
    containerView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:label];
    label.center = CGPointMake(CGRectGetMidX(containerView.bounds), CGRectGetMidY(containerView.bounds) + offset);
    
    self.titleView = containerView;
}

- (NSString *)title
{
    return nil;
}

@end
