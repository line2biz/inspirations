

#import <UIKit/UIKit.h>

@protocol SNTimePickerProtocol;

@interface SNTimePickerController : UIViewController

@property (strong, nonatomic) id <SNTimePickerProtocol> delegate;

@end

@protocol SNTimePickerProtocol <NSObject>
@required
- (void)timePickerController:(SNTimePickerController *)controller chosenDate:(NSDate *)date;

@end
