

#import <UIKit/UIKit.h>
@protocol SNCarouselViewDelegate;

@interface SNCarouselView : UIView

@property (strong, nonatomic) NSArray *views;
@property (strong, nonatomic) id <SNCarouselViewDelegate> delegate;

- (void)setCurrentIndexTo:(NSInteger)index animated:(BOOL)animated;

@end

@protocol SNCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(SNCarouselView *)view changedIndexTo:(NSInteger)index;
- (void)carouselView:(SNCarouselView *)view finishedAnimation:(BOOL)finished forIndex:(NSInteger)index;

@end


