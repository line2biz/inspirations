

#import <UIKit/UIKit.h>

@protocol SNProgressView3DProtocol;

typedef NS_ENUM(NSInteger, SNProgressView3DType) {
    SNProgressView3DTypeNormal,
    SNProgressView3DTypeSteps
};

@interface SNProgressView3D : UIView

@property (strong, nonatomic) UIView *superView;
@property (assign, nonatomic) CGFloat maxPoints;
@property (assign, nonatomic) CGFloat currentPoint;
@property (assign, nonatomic) CGFloat lineWidth;
@property (strong, nonatomic) UIColor *pathColor;
@property (strong, nonatomic) UIColor *progressColor;

@property (assign, nonatomic) SNProgressView3DType controlType;

@property (assign, nonatomic) NSInteger goldenStep;
@property (assign, nonatomic) NSInteger silverStep;
@property (assign, nonatomic) NSInteger bronzeStep;

@property (assign, nonatomic) BOOL showProgressPoint;
@property (assign, nonatomic) BOOL dontTransform;

@property (strong, nonatomic) id<SNProgressView3DProtocol> delegate;

- (void)configureProgressViewWithMaxPoints:(CGFloat)maxPoints
                              currentPoint:(CGFloat)currentPoint
                                 pathColor:(UIColor *)pathColor
                             progressColor:(UIColor *)progressColor
                                 lineWidth:(CGFloat)lineWidth;

- (void)startProgressingTimerWithDuration:(NSTimeInterval)duration;
- (void)stopProgressingTimer;
- (void)pauseProgressingTimer;
- (void)continueProgressingTimer;
- (void)setProgressAtPoint:(NSTimeInterval)point;
- (BOOL)timerIsInProgress;
- (void)animateToValue:(CGFloat)currentValue
               withMax:(CGFloat)maxValue
          withDuration:(CGFloat)duration;
- (CATransform3D)transform3D;

@end

@protocol SNProgressView3DProtocol <NSObject>
@optional
- (void)progressView:(SNProgressView3D *)progressView timerActive:(BOOL)active;
- (void)progressView:(SNProgressView3D *)progressView paused:(BOOL)paused;

@end